# Handle CLI Args and pass these settings to the main method
import core.main as main
import argparse

# Initialize the parser
parser = argparse.ArgumentParser(description="Eco Risk Projector Tool")

# Run File Location
parser.add_argument("run_file", type=str, help="Relative file path to the json run file")
# Output File Location
parser.add_argument("-out_dir", type=str, help="Relative path to the output directory, Default is the input directory")
# Output Directory Name
parser.add_argument("-result_dir_name", type=str, help="Override result directory name")
# Run progress
parser.add_argument(
    "-report_progress",
    default=False,
    action=argparse.BooleanOptionalAction,
    help="Write progress_ack to stdout after each node completes",
)

# Parse the args
args = parser.parse_args()

# Call the main method with the cli args
main.run(args.run_file, args.out_dir, args.result_dir_name, args.report_progress)
