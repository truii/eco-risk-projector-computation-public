import statistics
from typing import List, Optional
from core.globals import Months
from helper import validation as validation


def get_curve_value(value: float, curve: List[List[float]], inverse_curve: bool = False) -> float:
    """
    Interpolate a curve to get y value from an x value

    value is the x value to lookup, curve is a list of datapoints that form the function
    Curve points are sorted by x value before interpolation
    Inverse curve will reverse the x and y values

    I.E. [[0,0],[0.5,0.5],[1,1]] Is a linear curve from 0,0 to 1,1
    A value of 0.2 would return 0.2
    A value outside of the range will return the closest value in range (I.E. 0 or 1 in this case)
    """

    if inverse_curve:
        curve = extract_curve(curve, 1, 0)

    if not validation.valid_curve(curve):
        raise ValueError("Curve is not valid")

    bounds = [[], []]
    for index, datapoint in enumerate(curve):
        if datapoint[0] <= value:
            bounds[0] = datapoint
            try:
                bounds[1] = curve[index + 1]
            except Exception:
                return curve[-1][1]
    try:
        x_range = bounds[1][0] - bounds[0][0]
        y_range = bounds[1][1] - bounds[0][1]
        grad = y_range / x_range
        increase = value - bounds[0][0]
        increase = increase * grad
        value = bounds[0][1] + increase
    except Exception:
        return curve[0][1]
    return value


def get_dict_list_mean(data: List[dict[str, float]], key: str, dp: int = 3) -> float:
    """
    Get the mean of a given key from a list of dictionaries, rounded
    """

    data = extract_values_from_dict_list(data, key)
    if not (len(data) > 0):
        return None
    return round(statistics.mean(data), dp)


def get_dict_list_median(data: List[dict[str, float]], key: str, dp: int = 3) -> float:
    """
    Get the median of a given key from a list of dictionaries, rounded
    """

    data = extract_values_from_dict_list(data, key)
    if not (len(data) > 0):
        return None
    return round(statistics.median(data), dp)


def get_weighted_average(data: List[dict[str, float]], key: str, weight_key: str, dp: int = 3) -> float:
    """
    Get the weighted average of a given key and weight key from a list of dictionaries, rounded
    """

    if not (len(data) > 0) or (
        (len(extract_values_from_dict_list(data, key))) != (len(extract_values_from_dict_list(data, weight_key)))
    ):
        return 0

    res = round(sum((subset[key] * subset[weight_key]) for subset in data), dp)
    return res


def get_risk_category(high_risk_min: float, low_risk_max: float, value: float) -> str:
    """
    Get the risk category a value falls into (high, moderate or low)
    """

    if value >= high_risk_min:
        return "high"

    if value <= low_risk_max:
        return "low"

    return "moderate"


def get_flattened_list(data: List[List[any]]) -> List[any]:
    """
    flatten a list of lists into a single list
    """

    res = []
    for _list in data:
        if type(_list) is not list:
            res.append(_list)
        else:
            res.extend(_list)
    return res


def round_to_positive_int(val: float) -> int:
    """
    round any number value to a positive integer (>= 1)
    """

    rounded = round(val)
    if rounded < 1:
        return 1
    return rounded


def extract_curve(
    data: List[List[any]], x_index: int, y_index: int, parse_floats=False, validate_curve=False
) -> List[List[float]]:
    """
    Extract 2 cols from a list of lists for use with curve interpolation functions

    if parse floats will assume values as floats
    if validate curve will run validation on the extracted curve - this will sort the curve points by x
    """

    res = []
    for item in data:
        if parse_floats:
            try:
                res.append([float(item[x_index]), float(item[y_index])])
            except Exception:
                pass
        else:
            try:
                res.append([item[x_index], item[y_index]])
            except Exception:
                pass
    if validate_curve:
        if validation.valid_curve(res):
            return res
        raise ValueError("Invalid Curve!")
    return res


def extract_values_from_dict_list(data: List[dict[str, float]], key: str) -> List[float]:
    """
    Extract a value from each dictionary in a list, if it exists
    """

    return list(filter(lambda i: i is not None, map(lambda i: i[key], data)))


def get_all_values_for_key_from_dict_list(dict_list: List[dict[str, any]], key: str) -> dict[any, int]:
    """
    Count the number of occurrences of each values a given key takes on

    Returns a dict with each different value found and the number of occurrences
    """

    res = {}
    for dict_ in dict_list:
        if dict_[key] in res:
            res[dict_[key]] += 1
        else:
            res[dict_[key]] = 1
    return res


def get_month_number(month: Months) -> int:
    """
    Get the number of a month from it's name
    """

    months_mapping = {
        Months.JANUARY: 1,
        Months.FEBRUARY: 2,
        Months.MARCH: 3,
        Months.APRIL: 4,
        Months.MAY: 5,
        Months.JUNE: 6,
        Months.JULY: 7,
        Months.AUGUST: 8,
        Months.SEPTEMBER: 9,
        Months.OCTOBER: 10,
        Months.NOVEMBER: 11,
        Months.DECEMBER: 12,
    }
    return months_mapping[month]


def get_dict_key_with_max_value(data: dict[str, float]) -> str:
    """
    Return the key with that largest value in a dictionary
    """

    max_key = {"key": None, "value": None}

    for key, value in data.items():
        if max_key["key"] is None:
            max_key["key"] = key
            max_key["value"] = value
            continue

        if value > max_key["value"]:
            max_key["key"] = key
            max_key["value"] = value

    return max_key["key"]


def is_value_in_range(min: float, value: float, max: float) -> bool:
    """
    Safely check if min <= value <= max

    Returns false if it is not, or a value is None
    """

    if value is not None and min is not None and max is not None:
        if min <= value <= max:
            return True

    return False
