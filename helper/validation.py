import datetime
from core.exceptions import InvalidParameterException
from helper.exception_utils import assert_wrapper
from core.globals import CriteriaOptions, Months, PluginTypes, ComparisonOptions


def valid_season(season):
    """Check if a season obj is valid

    A season obj: {'start':{'day':1,'month':1},'end':{'day':31,'month':12}}
    Checks that both dates are valid

    Returns True if valid
    """

    try:
        assert valid_date(season["start"]["day"], season["start"]["month"])
        assert valid_date(season["end"]["day"], season["end"]["month"])
    except Exception:
        return False
    return True


def valid_date(day, month, year=None):
    """Check if a date is valid

    Year not requires, but if provided will check for leap year

    Returns True if valid
    """

    try:
        max_days_per_month = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        if year is not None:
            if year < 0:
                return False
            if not (year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)):
                max_days_per_month[2] = 28
        return 1 <= month <= 12 and 1 <= day <= max_days_per_month[month]
    except Exception:
        return False


def valid_rule(rule, keys):
    """Check if flow rule is valid (lowflow, freshes, oversupply, multiyear freshes)

    Checks if each key in keys in present and has a valid value in rule dictionary

    Returns True if valid
    """

    try:
        for key in keys:
            if not (rule[key] >= 0):
                return False
        return True
    except Exception:
        return False


def valid_range(range, type=None):
    """Check if a range is valid

    both min and max are numbers, and min <= max

    returns True if valid
    """
    if "min" in range and "max" in range:
        if is_num(range["min"]) and is_num(range["max"]):
            if range["min"] <= range["max"]:
                if type is not None:
                    if is_num(range["min"], type) and is_num(range["max"], type):
                        return True
                else:
                    return True

    return False


def valid_curve(curve_points, sort=True):
    """Check if a curve function is valid, optionally sorts

    Checks if each list (data point) in list is valid
    Checks for enough data points - >=2
    If sort, sorts by x value in ascending order
    I.E. [[0,0],[1,1],[0.5,0.5]] -> [[0,0],[0.5,0.5],[1,1]]

    Returns True if valid
    """
    try:
        if sort:
            curve_points.sort(key=lambda x: x[0])
        if len(curve_points) > 0:
            for point in curve_points:
                if not (len(point) == 2):
                    return False
                if point[0] is None or point[1] is None:
                    return False
                if not (is_num(point[0])) or not (is_num(point[1])):
                    return False
        else:
            return False
        return True
    except Exception:
        return False


def dict_contains_keys(data_dict, parents, children=[]):
    """Check if a dictionary contains parent and child keys

    Checks dict for parents, then checks parents for children
    parents is a list of keys ['key1','key2']
    children is a list of lists, [parent_key, [child_keys]]
    I.E. [['key1',['childkey1','childkey2']]]

    Returns True if dict contains all parent and child keys
    """
    try:
        dict_parents = []
        dict_children = {}

        # Get dict parents and children
        for key, value in data_dict.items():
            dict_parents.append(key)
            if type(value) is dict:
                dict_children[key] = []
                for child_key, child_value in value.items():
                    dict_children[key].append(child_key)

        # Check all parent and child keys are present
        for parent in parents:
            if parent not in dict_parents:
                return False
            for child in children:
                if child[0] == parent:
                    if parent not in dict_children:
                        return False
                    for child_value in child[1]:
                        if child_value not in dict_children[parent]:
                            return False
        return True
    except Exception:
        return False


def value_in_range(value, min, max):
    """Checks that a value is between a min and max

    Returns True if in range
    """

    try:
        if min <= value <= max:
            return True
        return False
    except Exception:
        return False


def date_preceeds_date(date_1, date_2):
    """Check if a date precedes another date

    Returns True if date_1 precedes date_2
    """
    try:
        start_date = datetime.datetime(date_1["year"], date_1["month"], date_1["day"])
        end_date = datetime.datetime(date_2["year"], date_2["month"], date_2["day"])

        if start_date < end_date:
            return True

        return False

    except Exception:
        return False


def is_num(value, type=None):
    """Check if a variable is a number (float or int)
    Optionally provide type to check (float or int)

    Args:
        value (Any Type): The value to check

    Returns:
        bool: true if the value is a num, otherwise false
    """

    try:
        value += 2

        if type is not None:
            if type == int:
                if isinstance(value, int):
                    return True
            elif type == float:
                if isinstance(value, float):
                    return True
            return False
        else:
            return True
    except Exception:
        return False


def valid_risk(risk_categories):
    """A method to validate the risk_category spatial paramater"""

    assert_wrapper(risk_categories["high"][0] < risk_categories["high"][1], "high", "lower bound must be < upper bound")
    assert_wrapper(
        risk_categories["moderate"][0] < risk_categories["moderate"][1], "moderate", "lower bound must be < upper bound"
    )
    assert_wrapper(risk_categories["low"][0] < risk_categories["low"][1], "low", "lower bound must be < upper bound")

    # ensure risk contains range 0-100
    risk_sum = 0
    risk_sum += risk_categories["high"][1] - risk_categories["high"][0]
    risk_sum += risk_categories["moderate"][1] - risk_categories["moderate"][0]
    risk_sum += risk_categories["low"][1] - risk_categories["low"][0]
    assert_wrapper(risk_sum == 98, "risk_categories", "must include full range 0-100")

    # Ensure the high > moderate > low
    assert_wrapper(
        risk_categories["high"][0] > risk_categories["moderate"][1]
        and risk_categories["moderate"][0] > risk_categories["low"][1],
        "risk_categories",
        "high must be > moderate must be > low",
    )
    return True


def valid_categories(categories):
    """A method to validate 2 part (high/low) categories"""
    assert_wrapper(is_num(categories["high"]), "high", "must be a number")
    assert_wrapper(is_num(categories["low"]), "high", "must be a number")

    assert_wrapper(categories["high"] > categories["low"], "categories", "high must be > low")

    return True


def valid_assessment(plugin_type, assessment):
    # There are 4 assessment configs to check

    if plugin_type == PluginTypes.NONE:
        return True

    assert_wrapper(assessment is not None, "assessment", "section must exist")

    assert_wrapper("temporal_agg" in assessment, "assessment", "must contain temporal_agg section")

    if plugin_type == PluginTypes.TEMPORAL:
        valid_temporal(assessment["temporal_agg"])

    elif plugin_type == PluginTypes.TEMPORAL_CONVERSION:
        valid_temporal(assessment["temporal_agg"])
        valid_yearly_conversion(assessment["yearly_conversion"])

    elif plugin_type == PluginTypes.TEMPORAL_YEARLY:
        valid_temporal(assessment["temporal_agg"])
        assert_wrapper("yearly_agg" in assessment, "assessment", "must contain yearly_agg section")
        valid_yearly_agg(assessment["yearly_agg"])

    elif plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY:
        valid_temporal(assessment["temporal_agg"])
        assert_wrapper("yearly_agg" in assessment, "assessment", "must contain yearly_agg section")
        assert_wrapper("daily_agg" in assessment, "assessment", "must contain daily_agg section")
        valid_yearly_agg(assessment["yearly_agg"])
        valid_daily_agg(assessment["daily_agg"])
    else:
        raise Exception

    return True


def valid_temporal(temporal_agg):
    # Check comparison type is valid
    try:
        temporal_agg["comparison"] = ComparisonOptions(temporal_agg["comparison"])
    except Exception:
        raise InvalidParameterException("comparison", "comparison type is not valid")
    # Check num year is valid
    assert_wrapper(temporal_agg["num_years"] > 0, "num_years", "must be > 0")
    # Check threshold is valid
    assert_wrapper(is_num(temporal_agg["threshold"]), "threshold", "must be a number")
    return True


def valid_yearly_agg(yearly_agg):
    # Check comparison type is valid
    try:
        yearly_agg["comparison"] = ComparisonOptions(yearly_agg["comparison"])
    except Exception:
        raise InvalidParameterException("comparison", "comparison type is not valid")
    # Check num year is valid
    assert_wrapper(yearly_agg["num_successes"] >= 0, "num_successes", "must be >= 0")
    # Check start month is valid
    try:
        yearly_agg["start_month"] = Months(yearly_agg["start_month"])
    except Exception:
        raise InvalidParameterException("start_month", "start_month is not valid")
    return True


def valid_daily_agg(daily_agg):
    # Check comparison type is valid
    try:
        daily_agg["comparison"] = ComparisonOptions(daily_agg["comparison"])
    except Exception:
        raise InvalidParameterException("comparison", "comparison type is not valid")

    try:
        daily_agg["criteria"] = CriteriaOptions(daily_agg["criteria"])
    except Exception:
        raise InvalidParameterException("criteria", "criteria type is not valid")

    if daily_agg["criteria"] == CriteriaOptions.THRESHOLD:
        assert_wrapper(is_num(daily_agg["threshold"]), "threshold", "must be a number")
    return True


def valid_yearly_conversion(yearly_conversion):
    # Check comparison type is valid
    try:
        yearly_conversion["comparison"] = ComparisonOptions(yearly_conversion["comparison"])
    except Exception:
        raise InvalidParameterException("comparison", "comparison type is not valid")

    try:
        yearly_conversion["criteria"] = CriteriaOptions(yearly_conversion["criteria"])
    except Exception:
        raise InvalidParameterException("criteria", "criteria type is not valid")

    if yearly_conversion["criteria"] == CriteriaOptions.THRESHOLD:
        assert_wrapper(is_num(yearly_conversion["threshold"]), "threshold", "must be a number")
    return True


def remove_null_keys(input_dict):
    for key, value in list(input_dict.items()):
        if value is None:
            del input_dict[key]
            continue
        if type(value) is dict:
            remove_null_keys(value)
        if type(value) is list:
            for item in value:
                if type(item) is dict:
                    remove_null_keys(item)
