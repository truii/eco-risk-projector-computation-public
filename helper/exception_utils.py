from core.exceptions import *
from core import globals
from helper import validation as validation


def assert_wrapper(assert_statement, name, reason):
    """A function to evaluate an assertive expression and raise an
    invalid paramater exception with relvent additional details"""

    if not assert_statement:
        raise InvalidParameterException(name, reason)


def handle_exception(error: Exception, node=None):
    """A function that handles the logic of parsing a validation exception"""

    # Safely get the node name - it might not exist or might be None (Args based exception)
    node_name = None
    if node is not None:
        try:
            node_name = node["meta_data"]["name"]
        except KeyError:
            node_name = "unknown"

    if type(error) is KeyError:
        raise MissingParameterException(error.args[0], node_name)
    elif type(error) is MissingDataFileException:
        error.set_node(node_name)
        raise error
    elif type(error) is InvalidParameterException:
        error.set_node(node_name)
        raise error
    elif type(error) is InvalidRunPeriodException:
        raise error
    elif type(error) is InvalidSpatialAggException:
        raise error
    elif type(error) is InvalidDataInfillSettingsException:
        raise error
    elif type(error) is InvalidDataException:
        error.set_node(node_name)
        raise error
    else:
        raise UnhandledValidationException(node_name)


def validate_metadata(meta_data):
    """A method used to validate the meta_data of a plugin"""
    parent_fields = globals.PluginSettings.PARENTS.value
    for field in parent_fields:
        if field not in meta_data:
            raise ValueError("Plugin meta_data does not contain {0} field!".format(field))
    return True


def validate_run_period(run_period):
    """A method to validate the run period of a plugin"""

    if run_period is not None:
        try:
            # There are 3 allowed options. Start & end date, just start or just end. Anything else is invalid.
            if "start" not in run_period and "end" not in run_period:
                raise InvalidRunPeriodException()
            if "start" in run_period:
                try:
                    assert_wrapper(
                        validation.valid_date(
                            run_period["start"]["day"], run_period["start"]["month"], run_period["start"]["year"]
                        ),
                        "run_period - start",
                        "Invalid Date",
                    )
                except Exception:
                    raise InvalidRunPeriodException("the start date is invalid")
            if "end" in run_period:
                try:
                    assert_wrapper(
                        validation.valid_date(
                            run_period["end"]["day"], run_period["end"]["month"], run_period["end"]["year"]
                        ),
                        "run_period - end",
                        "Invalid Date",
                    )
                except Exception:
                    raise InvalidRunPeriodException("the end date is invalid")
            if "start" in run_period and "end" in run_period:
                try:
                    assert_wrapper(
                        validation.date_preceeds_date(run_period["start"], run_period["end"]),
                        "run_period",
                        "the start date must preceed the end date",
                    )
                except Exception:
                    raise InvalidRunPeriodException("the start date must preceed the end date")
        except InvalidRunPeriodException as e:
            raise e
        except Exception:
            raise InvalidRunPeriodException()

    return True


def validate_data_infill_settings(data_infill_settings):
    """A method to validate the infill settings of a plugin"""

    if data_infill_settings is not None:
        try:
            assert_wrapper(
                type(data_infill_settings["should_infill_data"]) is bool, "should_infill_data", "must be a boolean"
            )
        except Exception:
            raise InvalidDataInfillSettingsException("should_infill_data must be a boolean")

        if data_infill_settings["should_infill_data"]:
            try:
                data_infill_settings["infill_method"] = globals.DataInfillTypes(data_infill_settings["infill_method"])
            except Exception:
                raise InvalidDataInfillSettingsException("infill_method must be a valid DataInfillType")

    return True


def validate_spatial_agg(spatial_agg, plugin_type):
    """A method to validate the spatial_agg params"""
    if plugin_type == globals.PluginTypes.NONE:
        return
    try:
        assert_wrapper(validation.valid_risk(spatial_agg), "spatial_agg", "invalid spatial_agg")
    except InvalidParameterException as e:
        raise InvalidSpatialAggException(e.parameter, e.reason)
    except Exception:
        raise InvalidSpatialAggException()
