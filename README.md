# Eco Risk Projector Computation README

## Purpose

This tool is used in water resource assessment studies. The tool transforms daily time series of flow (and other assets) into habitat scores for water dependent species or communities (environmental assets). Daily scores are temporally aggregated and scores across locations are spatially aggregated to give overall risk scores. The tool has an overall run architecture that calls on plugins of model computation to allow for easy batch running and for the creation of new environmental asset plugins.

This tool aims to simplify the process of creating a plugin, so anyone with basic python knowledge can create and run their own. This tool includes a range of pre-built helper functions covering a wide range of common IO, calculation, validation and timeseries analysis methods. The goal was to provide users with all the functionality required to write their own plugin without needing to create complex algorithms requiring more detailed python/programming knowledge.

The initial development of this computation was funded by the Department of Environment and Science, for integration with a web application developed by Truii providing a user friendly interface for interaction with this computation.

## Plugins

The following plugins have been included in the initial development:

1. Ambassis Agassizii
2. Banana Prawn Growth
3. Barramundi Growth
4. Barramundi Juvenile Recruitment
5. Barramundi Year Class Strength
6. Bass
7. Carp Recruitment
8. Fish Barriers and Connectivity
9. Fish Resilience
10. Freshes Partial Season
11. Kind Threadfin Salmon
12. Low Flow Spawning Fish
13. Lowflow Partial Season
14. Machrophyte Growth
15. Mary River Cod
16. Mary River Turtles
17. Multiyear Freshes Partial Season
18. Northern Snake Necked Turtle
19. Offshore Reef Fishery
20. Oversupply Partial Season
21. Rating Curve
22. Summary
23. Tandanus
24. Turtle
25. Waterhole (Including plugins for calibration and run to empty)
26. Spell Analysis
27. Simultaneous Spell Analysis
28. Baseflow Separation
29. Colwells Index
30. Australian Bass

## Support

For support and to report issues please contact zach.marsh@tuii.com

## Notes

This tool is available under a CC BY 4.0 license. See the license file for more info.

## Quick start

### Requirements

This tool is developed in python 3.8+ Some plugins won't work on ealier versions of python3.

This tool uses a handful of external python packages, which must be installed to run the computation.  
These are noted in the `requirements.txt` file, install them using pip3 with the following command:  
`eco-risk-projector-computation> pip3 install -r requirements.txt`

### Running the Plugin Examples

Assuming you are located in the project (eco-risk-projector-computation) directory, you can use the following command to run any of the example plugin run files:  
`eco-risk-projector-computation> python .\run.py .\data\runs\file_name.json -out_dir .\data\results\`  
For example, to run the Ambassis Agassizii example:  
`eco-risk-projector-computation> python .\run.py .\data\runs\ambassis_agassizii.json -out_dir .\data\results\`

This will output the results to the `data\results` directory.

### Results

The computation will output a results directory with the following naming convention:
`Eco Risk Projector Results - <Plugin Name> - dd.mm.yyyy-hh.mm.ss`  
This directory will contain a number of csv files, depending on the plugin. All plugins will produce either daily/yearly intermediate results (These are the raw results) along with some combination of daily/yearly/temporal/spatial (depending on the plugin) results (there are aggregated assessment results).

### Calling the computation

The computation is run by calling the `run.py` script, providing the relative or absolute path to a valid run file and optionally a results directory.

To view the help:  
 `python run.py -h`  
Shows:

    usage: run.py [-h] [-out_dir OUT_DIR] [-result_dir_name RESULT_DIR_NAME] run_file

    Eco Risk Projector Tool

    positional arguments:
    run_file          Relative file path to the json run file

    optional arguments:
    -h, --help        show this help message and exit
    -out_dir          Relative path to the output directory, Default is the input directory
    -result_dir_name  Override result directory name
    -report_progress      Write progress_ack to stdout after each node completes (default: False)

The computation is written in python 3+. Thus, ensure that you call `run.py` using python3, if this is not your system default you may need to use `python3 run.py`

Additionally, the relative file path is relative to where you call the computation from.

For Example, take the following directory structure:

    User\Work\Modelling\Example Report
                                        \Runs
                                            example_run.json
                                        \Data
                                            flow.csv
                                        \Results

If you wanted to call the computation on example_run.json from the Example Report Dir:  
`User\Work\Modelling\Example Report> python .\path\to\computation\run.py Runs\example_run.json`

This would output the results into the Runs directory, as no out_dir was specified.  
To output the results to the Results directory:  
`User\Work\Modelling\Example Report> python .\path\to\computation\run.py Runs\example_run.json -out_dir Results`

The data paths within example_run.json also need to be relative to where you call the computation from.  
Thus, in this example the data paths would look like `data\flow.csv`.
You can also use full absolute paths if prefered, for both the data paths and the computation paths.

Remember, if your filepath contains spaces you must wrap the path in quotation marks `"data\mary data\flow.csv"`

To summarize, you do not need to move your data into the computation directory to run the tool (although you can if you want to), you can simply call the computation from the location of your data. You can also just use full paths for everything if desired.

### The run file

Computation run files are JSON files that follow a specific format.  
The general format is as follows:

   `{
        "plugin": "",
        "spatial_agg":{},
        "nodes":[
            {},
            {},
            ...
        ],
        "run_period":{},
        "data_infill_settings":{}
        }
    }`

Where the spatial_agg (spatial risk categories) is formatted like:

`"spatial_agg": {
    "high": [75, 100],
    "moderate": [50, 74],
    "low": [0, 49]
  }`

The spatial agg categories represent % of simultaneous failures assosiated with a given risk.
In the above example >=75% simultaneous failure = high risk

And the run period is formatted like:

`"run_period": {
            "start": {"day": 1, "month": 13, "year": 2018},
            "end": {"day": 1, "month": 9, "year": 2018}
  }`

This determines how long the model runs for. It is optional, and if not included the model runs for the full data period provided.
Further, you can specify just the start date, to run from this date to the end of the data. Vice versa for the end date.

And the data_infill_settings is formatted like:

`"data_infill_settings": {
            "should_infill_data": true,
            "infill_method": "previous_value"
  }`

This determines how the computation will deal with invalid data. If should_infill_data is false, the computation will raise an error if it finds invalid data.
If should_infill_data is true, the computation will use the infill_method provided to set a value for the issue days. 
Valid infill_methods are "previous_value" (use the previous valid value) and "empty" (use no value).
This section (data_infill_settings) is optional. If it is not provided, the default behaviour of raising an error for bad data will occur.

A node has the following structure

`{
      "meta_data": {},
      "default_data": {},
      "data": {},
      "parameters": {},
      "assessment": {}
}`

The meta data is formatted like:

`"meta_data": {
        "name": "node 1"
  }`

The node name is used to identify the node in the output file.

The default_data, data and paramaters are plugin specifc (what data/parameters the plugin needs). The default_data section is currently only used by plugins which require the baseline (default) flow file to calulate an ARI threshold.

Below is an example for ambassis_agassizii:
`{
      "default_data": {}
      "data": {
        "depth": ".\\data\\sample\\Site 1 Depth 10 Years.csv",
        "temperature": ".\\data\\sample\\Site 1 Temp 10 Years.csv",
        "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"
      },
      "parameters": {
        "lowflow": {
          "use_median_flow": true,
          "flow_threshold": 5
        },
        "breeding_season": {
          "season": {
            "start": { "day": 1, "month": 7 },
            "end": { "day": 30, "month": 6 }
          },
          "temperature_threshold": 22
        },
        "life_stages_criteria": {
          "egg": { "max_change": 0.05, "consecutive_days": 7 },
          "larvae": { "daily_change": 0.05, "consecutive_days": 20 }
        }
      }
}`

Below is an example for mary_river_cod (using default data):
`{
      "default_data": {
        "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"
      },
      "data": {
        "depth": ".\\data\\sample\\Site 1 Depth 10 Years.csv",
        "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
        "temperature": ".\\data\\sample\\Site 1 Temp 10 Years.csv"
      },
      "parameters": {
        "connectivity_event_1": {
          "season": {
            "start": { "day": 1, "month": 7 },
            "end": { "day": 31, "month": 8 }
          },
          "CTF": 0,
          "amount_above_CTF": 0.25
        },
        "connectivity_event_2": {
          "season": {
            "start": { "day": 1, "month": 7 },
            "end": { "day": 31, "month": 12 }
          },
          "temperature_range": [18, 25],
          "CTF": 0,
          "amount_above_CTF": 0.25
        },
        "period_without_disturbance": {
          "use_ARI": true,
          "ARI": 5,
          "flow_threshold": 5,
          "use_default_data": true,
          "duration": 28
        }
      }
}`

Finally, the assessment section is formatted as follows:

`"assessment": {
        "yearly_conversion": {
          "comparison": "gte",
          "criteria": "median",
          "threshold": 1
        },
        "yearly_agg": {
          "comparison": "gte",
          "num_successes": 1,
          "start_month": "january"
        },
        "daily_agg": { "comparison": "lte", "criteria": "median" },
        "temporal_agg": {
          "num_years": 4,
          "comparison": "gte",
          "threshold": 1
        }
      }`

These parameters define the aggregation from daily unbounded to daily binary, daily to yearly, and yearly to temporal (Across years).
All plugins require the temporal_agg section, however yearly_agg, yearly_conversion and daily_agg varies across plugins based on their intermitant result type.

### Plugin Examples

Every plugin that is included in this release has an example run file, showcasing all plugin parameters.  
These are available in the `data\runs` directory.

### Example Data

Example flow, depth, temperature, rainfall and salinity data has been provided from an example site.  
Each of these data types have been split into 4 different lengths.

- Full Length (01/07/1889 - 30/06/2019)
- 10 Years (01/07/1889 - 30/06/1899)
- Single Year (01/07/2018 - 30/06/2019)
- Annual (01/01/2018 - 31/12/2018)

This allows you to run the plugins for differing lengths of time to reduce computation time when testing.

This is located in the `data\sample` directory.

Additionally, all data required to run the waterhole model has been included in the `data\sample\waterhole` directory.  
This includes:

- Bathymetry
- Depth
- Evaporation
- Flow
- Rainfall

## Creating a Plugin

A plugin is effectively just an implemented version of the base plugin class.  
This class includes the following compulsory implementable functions:

- load_data
- validate_parameters
- run
- output

These functions are how the computation interacts with the plugins, and thus failure to implement them will prevent the plugin from executing.

Additionally, it is recommended to implement the following functions within your plugin:

- ts_generation
- ts_aggregation

This is how all 30 of the built-in plugins have been developed and it is recommended for consistency.  
For more detailed info, look at the `plugin.py` script, containing the plugin base class with documentation around each method and the suggested/required use. Also look at some of the example plugins to see how they have been implemented, this will give a good idea of the available helper functions which can greatly simplify the process of developing a plugin. Also look through the helper directory to find all available helper functions and documentation around each function.

## Unit Tests

This tool contains a comprehensive suite of unit tests, developed with the python unittest package.  
These tests cover all 30 plugins and a range of helper functions, to ensure the tool is functioning as intended.  
The tests are located in the `tests` directory, and can be run by calling the `run_tests.py` file.  
This file takes no parameters and must be called from the project directory.
