from datetime import date
import statistics
from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, CriteriaOptions, PluginTypes, ResultTypes
from core.node import Node
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from core.exceptions import *
from core.node_data import *
from core.node_default_data import *
from helper.calculations import get_month_number
from helper.exception_utils import *


class Plugin:
    """The base plugin class, all plugins extend this class and implement its functions

    A plugin must implement the validate_args, run and output functions,
    as these are used to interact with the plugin. It's recommended that
    a plugin uses the ts_generation, ts_aggregation and spatial_aggregation methods
    along with the predefined self.results object, but this isn't strictly required.
    """

    def __init__(self, meta_data, plugin_type):
        if validate_metadata(meta_data):
            self.meta_data = meta_data
        self.plugin_type = plugin_type

    # Define abstract methods
    def validate_node(self, node):
        """Used to validate each node in the run file

        A method that takes a provided node (from the model) and validates that all parameters required for the run are present and valid
        Simply raise (intentionally or not) an exception and it will be handled by the model
        It is recomended to use the custom exceptions provided in the exceptions file as these provide meaningful error messages
        This method is effectivly a wrapper for the validate_parameters method, including extra validation
        """

        self.validate_meta_data(node["meta_data"])
        self.validate_parameters(node["parameters"], node["data"] if "data" in node else None)
        self.validate_assessment(node["assessment"] if "assessment" in node else None)

    def ts_generation(self, node, ts):
        """Take input TS and generate TS of results for further analysis, use varies between plugins

        A method, that whilst not strictly required it there to assist good plugin structure
        This method is called from within the plugin logic
        See any built in plugin for an example of how this should be used
        """

        raise NotImplementedError("Plugin does not implement the ts_generation method!")

    def ts_aggregation(self, node, ts):
        """Take result TS and aggregate it to summarise results, use varies between plugins

        A method, that whilst not strictly required it there to assist good plugin structure
        This method is called from within the plugin logic
        See any built in plugin for an example of how this should be used
        """

        raise NotImplementedError("Plugin does not implement the ts_aggregation method!")

    def run(self, node: Node) -> PluginResult:
        """A method that runs a plugins computation for the given node - compulsory

        This method is called for each node present in the model
        The implementation of this is up to the plugin author - see the built in plugins for inspiration
        This method is called by the core and is absolutely compulsory - A plugin will not execute without it
        """

        raise NotImplementedError("Plugin does not implement the run method!")

    def write_intermediate_results(self, write_result):
        """A method that outputs a plugins results

        The implementation of this is up to the plugin author - see the built in plugins for inspiration
        This method will vary based on what sort of results are being generated and how you want them to be stored
        This method is called by the core and is absolutely compulsory - A plugin will not execute without it
        """

        raise NotImplementedError("Plugin does not implement the output method!")

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        """A method that modifies a plugins node results and stores the results into the data_store

        The implementation of this is up to the plugin author - see the built in plugins for inspiration
        This method will vary based on what sort of results are being generated and how you want them to be stored
        This method is called by the core and is absolutely compulsory - A plugin will not execute without it
        """

        raise NotImplementedError("Plugin does not implement the save_result_to_data_store method!")

    def load_data(self, node) -> tuple[NodeData, NodeDefaultData]:
        """A method to load the data required to compute a node

        This method is called by the core and is absolutely compulsory - A plugin will not execute without it
        It is reccomended to use the NodeData class to handle the import and storage data
        """

        raise NotImplementedError("Plugin does not implement the load_data method!")

    def validate_parameters(self, node_parameters, node_data):
        """Used to validate the parameters provided by a node

        A method that takes a provided a nodes parameters (from the model) and validates that all parameters required for the run are present and valid
        Simply raise (intentionally or not) an exception and it will be handled by the model
        It is recomended to use the custom exceptions provided in the exceptions file as these provide meaningful error messages
        This method is called by the core and is absolutely compulsory - A plugin will not execute without it
        """

        raise NotImplementedError("Plugin does not implement the validate_parameters method!")

    # Define helper methods
    def validate_assessment(self, assessment):
        """A method to handle to logic of validating the plugin assessment parameters

        This method is a helper to the plugin to prevent repeated code, however the logic of setting
        the plugin type remains in the individual plugins script
        """

        try:
            assert_wrapper(
                validation.valid_assessment(self.plugin_type, assessment), "assessment", "invalid assessment"
            )
        except Exception as e:
            raise e

    def validate_meta_data(self, meta_data):
        """A method to handle to logic of validating the plugin meta_data

        This method is a helper to the plugin to prevent repeated code, however the logic of setting
        the plugin type remains in the individual plugins script
        """

        assert_wrapper((len(meta_data["name"]) > 0), "name", "node cannot have an empty name")

    def compute_assessment(self, assessment, result: PluginResult) -> PluginResult:
        """A method to compute the assessment results for a plugin

        There are 3 levels of assessment to compute. These are:
        1. Temporal (All Plugins)
        2. Yearly (Plugins with daily success results)
        3. Daily (Plugins with unbounded daily results)
        These must be computed in order of Daily->Yearly->Temporal
        """

        if self.plugin_type != PluginTypes.NONE:
            # Compute Daily if required
            if self.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY:
                conversion_params = assessment["daily_agg"]
                result.assessment_daily = self.assessment_daily_success(
                    result.ts, result.assessment_key, conversion_params
                )

            # Compute Yearly if required
            if self.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY or self.plugin_type == PluginTypes.TEMPORAL_YEARLY:
                conversion_params = assessment["yearly_agg"]
                result.assessment_yearly = self.assessment_yearly_success(result.assessment_daily, conversion_params)

            # Compute Yearly Conversion if required
            if self.plugin_type == PluginTypes.TEMPORAL_CONVERSION:
                conversion_params = assessment["yearly_conversion"]
                result.assessment_yearly = self.assessment_yearly_conversion(
                    result.assessment_yearly, conversion_params
                )

            # Compute temporal
            conversion_params = assessment["temporal_agg"]
            result.assessment_temporal = self.assessment_temporal_success(result.assessment_yearly, conversion_params)

        return result

    def get_assessment_results(self, plugin_result: PluginResult) -> List[NodeResult]:
        if self.plugin_type == PluginTypes.NONE:
            return []

        results = []

        if self.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY:
            parsed_result = list(
                map(
                    lambda r: {"date": r[0].strftime("%d/%m/%Y"), "success": r[1]},
                    plugin_result.assessment_daily,
                )
            )
            result = NodeResult(plugin_result.node_name, ResultTypes.DAILY_ASSESSMENT, parsed_result)
            results.append(result)

        # Compute Yearly if required
        if self.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY or self.plugin_type == PluginTypes.TEMPORAL_YEARLY:
            parsed_result = list(
                map(
                    lambda r: {"year": r[0], "success": r[1]},
                    plugin_result.assessment_yearly,
                )
            )
            result = NodeResult(plugin_result.node_name, ResultTypes.YEARLY_ASSESSMENT, parsed_result)
            results.append(result)

        # Compute temporal
        parsed_result = list(
            map(
                lambda r: {"year": r[0], "success": r[1]},
                plugin_result.assessment_temporal,
            )
        )
        result = NodeResult(plugin_result.node_name, ResultTypes.TEMPORAL_ASSESSMENT, parsed_result)
        results.append(result)

        return results

    def compute_spatial(self, params, data_store: DataStore) -> None:
        if self.plugin_type == PluginTypes.NONE:
            return

        node_temporal_results = data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)
        result = self.spatial_agg(node_temporal_results, params)
        data_store.create_result(result)

    def sort_result_tuples_by_year(self, ts, start_month, date_index=0):
        """
        split a list of tuples by a date key
        """

        yearly_split = {}
        current_year = ts[0][date_index].year
        start_month_number = get_month_number(start_month)

        for day in ts:
            if day[date_index].month == start_month_number:
                if day[date_index].year not in yearly_split:
                    current_year = day[date_index].year
            if current_year in yearly_split:
                yearly_split[current_year].append(day)
            else:
                yearly_split[day[date_index].year] = []
                yearly_split[day[date_index].year].append(day)

        return yearly_split

    def assessment_daily_success(self, ts: Timeseries, assessment_key: str, params) -> List[tuple[date, int]]:
        is_failure_based = False
        if "is_failure_based" in params and params["is_failure_based"] is True:
            is_failure_based = True

        if params["criteria"] == CriteriaOptions.MEDIAN:
            params["threshold"] = ts.get_data_type_value_median(assessment_key, dp=20)
        return_ts = []
        for day in ts.data:
            if day.get_value_from_key(assessment_key) is None:
                return_ts.append((day.date, 0))
                continue

            try:
                if not is_failure_based:
                    if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                        if day.get_value_from_key(assessment_key) >= params["threshold"]:
                            return_ts.append((day.date, 1))
                        else:
                            return_ts.append((day.date, 0))

                    elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                        if day.get_value_from_key(assessment_key) <= params["threshold"]:
                            return_ts.append((day.date, 1))
                        else:
                            return_ts.append((day.date, 0))

                else:
                    if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                        if day.get_value_from_key(assessment_key) >= params["threshold"]:
                            return_ts.append((day.date, 0))
                        else:
                            return_ts.append((day.date, 1))

                    elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                        if day.get_value_from_key(assessment_key) <= params["threshold"]:
                            return_ts.append((day.date, 0))
                        else:
                            return_ts.append((day.date, 1))

            except Exception:
                return_ts.append((day.date, 0))
        return return_ts

    def assessment_yearly_conversion(self, ts, params) -> List[tuple[date, int]]:
        if params["criteria"] == CriteriaOptions.MEDIAN:
            daily_values = list(map(lambda d: d[1], filter(lambda d: d[1] is not None, ts)))
            if len(daily_values) > 0:
                params["threshold"] = statistics.median(daily_values)
            else:
                params["threshold"] = None

        return_ts = []
        for year in ts:
            try:
                if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                    if year[1] >= params["threshold"]:
                        return_ts.append((year[0], 1))
                    else:
                        return_ts.append((year[0], 0))
                elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                    if year[1] <= params["threshold"]:
                        return_ts.append((year[0], 1))
                    else:
                        return_ts.append((year[0], 0))
            except Exception:
                return_ts.append((year[0], 0))
        return return_ts

    def assessment_yearly_success(self, daily_success: tuple[date, int], params) -> List[tuple[date, int]]:
        is_failure_based = False
        if "is_failure_based" in params and params["is_failure_based"] is True:
            is_failure_based = True

        yearly_split = self.sort_result_tuples_by_year(daily_success, params["start_month"])
        yearly_results = []
        for year, data in yearly_split.items():
            if not is_failure_based:
                num_success = sum(map(lambda d: d[1], data))
                if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                    if num_success >= params["num_successes"]:
                        yearly_results.append((year, 1))
                    else:
                        yearly_results.append((year, 0))
                elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                    if num_success <= params["num_successes"]:
                        yearly_results.append((year, 1))
                    else:
                        yearly_results.append((year, 0))

            else:
                num_failures = len(data) - sum(map(lambda d: d[1], data))
                if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                    if num_failures >= params["num_successes"]:
                        yearly_results.append((year, 0))
                    else:
                        yearly_results.append((year, 1))
                elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                    if num_failures <= params["num_successes"]:
                        yearly_results.append((year, 0))
                    else:
                        yearly_results.append((year, 1))

        return yearly_results

    def assessment_temporal_success(self, ts, params) -> List[tuple[date, int]]:
        is_failure_based = False
        if "is_failure_based" in params and params["is_failure_based"] is True:
            is_failure_based = True

        yearly_results = []
        for index, year in enumerate(ts):
            # Get the previous results
            previous_scores = []
            check_index = index
            while check_index >= 0 and len(previous_scores) < params["num_years"]:
                previous_scores.append(ts[check_index][1] if ts[check_index][1] is not None else 0)
                check_index -= 1
            # Ensure we have enough years to score
            if len(previous_scores) < params["num_years"]:
                yearly_results.append((year[0], None))
                continue

            if not is_failure_based:
                # Get the agg value
                agg_value = sum(previous_scores)
                # Compare the agg_value with the threshold based on the comparison
                if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                    if agg_value >= params["threshold"]:
                        yearly_results.append((year[0], 1))
                    else:
                        yearly_results.append((year[0], 0))
                elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                    if agg_value <= params["threshold"]:
                        yearly_results.append((year[0], 1))
                    else:
                        yearly_results.append((year[0], 0))

            else:
                # Get the agg value
                agg_value = len(previous_scores) - sum(previous_scores)
                # Compare the agg_value with the threshold based on the comparison
                if params["comparison"] is ComparisonOptions.GREATER_EQUAL:
                    if agg_value >= params["threshold"]:
                        yearly_results.append((year[0], 0))
                    else:
                        yearly_results.append((year[0], 1))
                elif params["comparison"] is ComparisonOptions.LESS_EQUAL:
                    if agg_value <= params["threshold"]:
                        yearly_results.append((year[0], 0))
                    else:
                        yearly_results.append((year[0], 1))
        return yearly_results

    def spatial_agg(self, temporal_results: List[NodeResult], params) -> NodeResult:
        """Calculate the yearly risk based on the % of simultaneous failures"""

        yearly_split = {}
        for node in list(map(lambda r: r.data, temporal_results)):
            for year in node:
                if year["year"] not in yearly_split:
                    yearly_split[year["year"]] = []
                yearly_split[year["year"]].append(year["success"])

        yearly_results = []
        for year, results in yearly_split.items():
            # Check if we have any null results
            if len(list(filter(lambda r: r is None, results))) > 0:
                yearly_results.append({"year": year, "risk": None})
                continue

            num_results = len(results)
            num_fails = len(list(filter(lambda r: r == 0, results)))

            perc_fail = (num_fails / num_results) * 100

            # Get the category this % falls into
            for category, range in params.items():
                if perc_fail >= range[0] and perc_fail <= range[1]:
                    yearly_results.append({"year": year, "risk": category})
                    break

        return NodeResult("all", ResultTypes.SPATIAL_ASSESSMENT, yearly_results)
