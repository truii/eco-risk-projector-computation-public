import statistics
from typing import List
from datetime import date, datetime, timedelta, timezone
from core.timeseries_day import TimeseriesDay

from helper import io as io
from core.exceptions import *
from core.globals import OtherDataTypes, TimeseriesDataTypes, InvalidDataTypes


class Data:
    """Class to handle run data csvs to convert into Timeseries objects

    This class should only be used for the conversion
    """

    def __init__(self):
        self.timeseries_fields: List[TimeseriesDataTypes] = list(TimeseriesDataTypes)
        self.other_fields: List[OtherDataTypes] = list(OtherDataTypes)
        self.flow = None
        self.depth = None
        self.temperature = None
        self.rainfall = None
        self.compliance = None
        self.climate = None
        self.data = None
        self.bathymetry = None
        self.evaporation = None
        self.salinity = None
        self.other_timeseries = None
        self.run_period = None
        self.data_infill_settings = None

    def handle_null_paths(self, paths):
        """Remove keys with a null value, used for plugin data validation"""

        keys_to_remove = []
        for key, path in paths.items():
            if path is None:
                keys_to_remove.append(key)
        return {key: paths[key] for key in paths if key not in keys_to_remove}

    def try_import_data(self, data_dict, key: TimeseriesDataTypes or OtherDataTypes or str) -> List[List[any]]:
        """A method to try and import a data file, and raise an exception if required"""

        try:
            return io.import_csv_from_relative_path(data_dict[key])
        except Exception as e:
            raise MissingDataFileException(key)

    def parse_run_period(self, period):
        if period is not None:
            self.run_period = {"start": None, "end": None}
            if "start" in period:
                try:
                    self.run_period["start"] = (
                        datetime(period["start"]["year"], period["start"]["month"], period["start"]["day"])
                        .replace(tzinfo=timezone.utc)
                        .replace(hour=0, minute=0, second=0, microsecond=0)
                    )
                except Exception:
                    pass
            if "end" in period:
                try:
                    self.run_period["end"] = (
                        datetime(period["end"]["year"], period["end"]["month"], period["end"]["day"])
                        .replace(tzinfo=timezone.utc)
                        .replace(hour=0, minute=0, second=0, microsecond=0)
                    )
                except Exception:
                    pass

    def parse_csv_to_timeseries_days(
        self,
        field,
        field_name: str = None,
        consider_run_period: bool = True,
        is_number_values: bool = True,
        infill_with_previous_value: bool = False,
        should_error_on_invalid_data: bool = False,
    ) -> List[TimeseriesDay]:
        """
        Parse csv data to TimeseriesDay[]
        Aggregate inter-day values into single day (mean of values)
        Missing days are infilled with None or the previous value

        Expects a list of [date,value] lists (csv rows), converts date to date objects and value to floats (if is_number_values)
        Returns a list of TimeseriesDay, sorted by date (oldest first)
        """

        if field_name is None:
            field_name = field

        data = getattr(self, field)

        if data is None:
            return []

        parsed_data = []
        previous_day = None

        for index, day in enumerate(data):
            try:
                # Parse the data
                day = self.parse_day(day[0], field_name, day[1], is_number_values, should_error_on_invalid_data)

                # Infill any missing days between this date and the previous_day.date
                if previous_day is not None:
                    if day.date == previous_day.date:
                        parsed_data[-1].append(day)
                        continue

                    next_date = (
                        previous_day.date + timedelta(days=1)
                        if day.date > previous_day.date
                        else previous_day.date - timedelta(days=1)
                    )

                    if day.date != next_date:
                        if should_error_on_invalid_data:
                            raise InvalidDataException(InvalidDataTypes.MISSING_DATE, file=field, date=next_date)

                        value = previous_day.get_value(field_name) if infill_with_previous_value else None
                        infil_day = TimeseriesDay(next_date, field_name, value)

                        while infil_day.date != day.date:
                            parsed_data.append([infil_day])

                            next_date = (
                                infil_day.date + timedelta(days=1)
                                if day.date > previous_day.date
                                else infil_day.date - timedelta(days=1)
                            )

                            value = previous_day.get_value(field_name) if infill_with_previous_value else None
                            infil_day = TimeseriesDay(next_date, field_name, value)

                    if day.get_value(field_name) is None and infill_with_previous_value:
                        day.set_value(field_name, previous_day.get_value(field_name))

                previous_day = day

                if len(parsed_data) > 0 and day.date == parsed_data[-1][-1].date:
                    parsed_data[-1].append(day)
                else:
                    parsed_data.append([day])

            except InvalidDataException as e:
                raise e
            except Exception:
                if should_error_on_invalid_data:
                    raise InvalidDataException(
                        InvalidDataTypes.UNKNOWN_EXCEPTION, file=field, value=data[index], index=index
                    )

        # Summarise parsed data to ensure one value for each day
        summarised_data = []
        for date_values in parsed_data:
            if len(date_values) == 0 or not is_number_values:
                summarised_data.append(date_values[0])
                continue

            values = list(filter(lambda v: v is not None, map(lambda v: v.get_value(field_name), date_values)))
            mean_value = statistics.mean(values) if len(values) > 0 else None
            summarised_data.append(TimeseriesDay(date_values[0].date, field_name, mean_value))

        # Reduce to only data within the run period
        if self.run_period is not None and consider_run_period:
            summarised_data = list(
                filter(
                    lambda d: d.is_between_dates(self.run_period["start"], self.run_period["end"]),
                    summarised_data,
                )
            )

        if len(summarised_data) == 0:
            raise InvalidRunPeriodException(
                "node {0} has no data in the period {1}".format(
                    self.node["meta_data"]["name"], self.format_run_period()
                )
            )

        return sorted(summarised_data, key=lambda day: day.date)

    def parse_day(
        self,
        date_str: str,
        field: TimeseriesDataTypes,
        value: float or str,
        should_parse_to_float: bool,
        should_error_on_invalid_data: bool,
    ) -> TimeseriesDay:
        """Parse a date, field and value into a new TimeseriesDay"""

        try:
            date = (
                datetime.strptime(date_str, self.find_date_format(date_str))
                .replace(tzinfo=timezone.utc)
                .replace(hour=0, minute=0, second=0, microsecond=0)
            )
        except ValueError as e:
            if should_error_on_invalid_data:
                raise InvalidDataException(InvalidDataTypes.UNKNOWN_DATE_FORMAT, file=field, date=date_str)
            else:
                raise e

        try:
            parsed_value = float(value) if should_parse_to_float else value
        except ValueError:
            if should_error_on_invalid_data:
                raise InvalidDataException(InvalidDataTypes.INVALID_VALUE, file=field, date=date, value=value)
            else:
                parsed_value = None

        return TimeseriesDay(date, field, parsed_value)

    def find_date_format(self, date_string: str):
        """returns the datetime datestring required to describe the given date"""

        date_formats = [
            "%d/%m/%Y",
            "%d/%m/%Y %H:%M",
            "%d/%m/%Y %H:%M:%S",
            "%Y/%m/%d",
            "%Y/%m/%d %H:%M",
            "%Y/%m/%d %H:%M:%S",
            "%d-%m-%Y",
            "%d-%m-%Y %H:%M",
            "%d-%m-%Y %H:%M:%S",
            "%Y-%m-%d",
            "%Y-%m-%d %H:%M",
            "%Y-%m-%d %H:%M:%S",
        ]

        for date_format in date_formats:
            try:
                datetime.strptime(date_string, date_format).replace(tzinfo=timezone.utc)
                return date_format
            except Exception:
                pass

        raise ValueError("Cannot determine the date format!")

    def format_run_period(self):
        """
        Format a run period into a printable string
        """

        try:
            start_date = "{0}/{1}/{2}".format(
                self.run_period["start"].day, self.run_period["start"].month, self.run_period["start"].year
            )
        except Exception:
            start_date = "N/A"

        try:
            end_date = "{0}/{1}/{2}".format(
                self.run_period["end"].day, self.run_period["end"].month, self.run_period["end"].year
            )
        except Exception:
            end_date = "N/A"

        date = "{0} - {1}".format(start_date, end_date)

        return date
