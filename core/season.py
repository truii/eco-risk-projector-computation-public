class Season:
    def __init__(self, season_dict: dict[str, dict[str, int]] = None, start_month: int = None):
        self.start_day = None
        self.start_month = None
        self.end_day = None
        self.end_month = None

        if season_dict is not None:
            self.set_from_season_dict(season_dict)
            return

        if start_month is not None:
            self.set_from_start_month(start_month)
            return

    def set_from_season_dict(self, season_dict: dict[str, dict[str, int]]) -> None:
        self.start_day = season_dict["start"]["day"]
        self.start_month = season_dict["start"]["month"]
        self.end_day = season_dict["end"]["day"]
        self.end_month = season_dict["end"]["month"]

    def set_from_start_month(self, start_month: int) -> None:
        start = {"month": start_month, "day": 1}
        end_month = start_month - 1
        if end_month == 0:
            end_month = 12
        max_days_per_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        end = {"month": end_month, "day": max_days_per_month[end_month]}

        self.start_day = start["day"]
        self.start_month = start["month"]
        self.end_day = end["day"]
        self.end_month = end["month"]
