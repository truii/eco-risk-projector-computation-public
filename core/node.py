from core.node_data import NodeData
from core.node_default_data import NodeDefaultData


class Node:
    def __init__(self, node_obj, data: NodeData, default_data: NodeDefaultData) -> None:
        self.name = node_obj["meta_data"]["name"]
        self.parameters = node_obj["parameters"]
        self.assessment = node_obj["assessment"] if "assessment" in node_obj else None
        self.data = data
        self.default_data = default_data
