from typing import List


class PluginResult:
    def __init__(self, name, ts) -> None:
        self.node_name = name
        self.ts_gen: List = []
        self.ts_agg: List = []
        self.event_log: List = []
        self.summary: List = []
        self.assessment_key: str | None = None
        self.assessment_daily: List = []
        self.assessment_yearly: List = []
        self.assessment_temporal: List = []
        self.ts = ts
