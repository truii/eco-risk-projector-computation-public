from datetime import date
import math
from typing import List

from core.globals import TimeseriesDataTypes
from core.season import Season


class TimeseriesDay:
    """A class to handle the concept of a day, used by the timeseries class which contains a list of days"""

    def __init__(self, date: date, data_type: TimeseriesDataTypes = None, value: float = None):
        self.date = date
        self.values = {data_type: value} if data_type is not None else {}
        self.calculated_values = {}
        self.result_values = {}

    ### Getters and setters

    def set_values(self, _values: dict[TimeseriesDataTypes, float]) -> None:
        self.values = _values

    def set_value(self, data_type: TimeseriesDataTypes, value: float) -> None:
        self.values[data_type] = value

    def set_calculated_values(self, _values: dict[str, float]) -> None:
        self.calculated_values = _values

    def set_calculated_value(self, key: str, value: float) -> None:
        self.calculated_values[key] = value

    def set_result_values(self, _values: dict[str, float]) -> None:
        self.result_values = _values

    def set_result_value(self, key: str, value: float or str) -> None:
        self.result_values[key] = value

    def get_value(self, data_type: TimeseriesDataTypes) -> float or str:
        return self.values[data_type] if data_type in self.values else None

    def get_calculated_value(self, value_key: str) -> float or str:
        return self.calculated_values[value_key] if value_key in self.calculated_values else None

    def get_result_value(self, value_key: str) -> float or str:
        return self.result_values[value_key] if value_key in self.result_values else None

    def has_value(self, data_type: TimeseriesDataTypes) -> bool:
        return data_type in self.values and self.values[data_type] is not None

    def has_calculated_value(self, key: str) -> bool:
        return key in self.calculated_values and self.calculated_values[key] is not None

    def has_result_value(self, key: str) -> bool:
        return key in self.result_values and self.result_values[key] is not None

    def init_result_value_keys(self, keys: List[str], value: float = None) -> None:
        for key in keys:
            self.result_values[key] = value

    def add_day(self, day, data_type: TimeseriesDataTypes or List[TimeseriesDataTypes] = None) -> None:
        if data_type is None:
            for key, value in day.values.items():
                self.set_value(key, value)
            return

        if type(data_type) is list:
            for key in data_type:
                self.set_value(key, day.values[key])
            return

        self.set_value(data_type, day.values[data_type])

    def get_value_from_key(self, value_key: str) -> str:
        """
        Extract the value_key from a days values.
        Search priority is values, calculated_values, result_values
        Unfound values return None
        """
        if value_key in self.values:
            return self.values[value_key]

        if value_key in self.calculated_values:
            return self.calculated_values[value_key]

        if value_key in self.result_values:
            return self.result_values[value_key]

        return None

    ### Calculations

    def is_between_dates(self, start_date: date, end_date: date) -> bool:
        if start_date is None:
            return self.date <= end_date

        if end_date is None:
            return start_date <= self.date

        if start_date is None and end_date is None:
            return True

        return start_date <= self.date <= end_date

    def is_date_equal(self, day: int, month: int) -> bool:
        """
        Check if the day and month of the TimeseriesDay is equal to the day and month provided
        """

        if self.date.month == month and self.date.day == day:
            return True
        return False

    def format_date(self, date_format: str = "%d/%m/%Y") -> str:
        """
        Parse the day's date into a string based on the date_format string
        """

        return self.date.strftime(date_format)

    def is_in_season(self, season: Season) -> bool:
        """
        Check if a the day's date is within a season (month/day) - year not relevent
        """

        months_in_season = []
        check_month = season.start_month

        while season.end_month not in months_in_season:
            months_in_season.append(check_month)
            check_month = check_month + 1 if check_month < 12 else 1

        if self.date.month in months_in_season:
            if self.date.month == season.start_month:
                if self.date.day < season.start_day:
                    return False
            if self.date.month == season.end_month:
                if self.date.day > season.end_day:
                    return False

            return True

        return False

    def get_season_for_day(
        self, seasons={"summer": [12, 1, 2], "autumn": [3, 4, 5], "winter": [6, 7, 8], "spring": [9, 10, 11]}
    ):
        """
        Get season (summer/winter/spring/autumn) for the day's date

        You can specify the seasonal months - default is australian seasons
        seasons = {"summer":[1,2,3],"winter":[4,5,6]} etc.
        """

        for season, months in seasons.items():
            if self.date.month in months:
                return season

        return None

    def set_velocity_from_flow(self, flow_key: str, velocity_key: str, slope, width, roughness) -> None:
        """
        Set the daily velocity calculated from the daily flow value
        """

        day_flow_value = self.get_value_from_key(flow_key)

        if day_flow_value is None:
            return

        ZrecCoef = roughness / (math.pow(slope, 0.5) * math.pow(width, 8 / 3))

        if day_flow_value > 0:
            Zrec = ZrecCoef * day_flow_value / 86400
            sinhThreeTheta = (143 - 42 * math.log(2) - 18 * math.log(Zrec)) / (
                -1 + 14 * math.log(2) + 6 * math.log(Zrec)
            )
            coshthreeTheta = math.sqrt(abs(1 + sinhThreeTheta * sinhThreeTheta))
            theta = (1.0 / 3.0) * math.log(abs(sinhThreeTheta + coshthreeTheta))
            sinhTheta = (math.exp(theta) - math.exp(-theta)) / 2.0
            alpha = 4.0 * math.sqrt(
                abs(1.0 + sinhTheta * math.sqrt(abs(-1.0 + 14.0 * math.log(2.0) + 6.0 * math.log(Zrec))))
            )
            depth = width * (math.exp(0.5 * -alpha + math.sqrt(abs(48.0 - alpha * alpha + 768.0 / alpha))))
            vel = (math.pow(((width * depth) / (width + 2.0 * depth)), (2.0 / 3.0)) * math.sqrt(slope)) / roughness
            self.set_calculated_value(velocity_key, vel)
        else:
            self.set_calculated_value(velocity_key, 0)
