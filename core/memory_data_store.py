from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes
from core.node_result import NodeResult


class MemoryDataStore(DataStore):
    def __init__(self) -> None:
        self.results: List[NodeResult] = []

    def create_result(self, result: NodeResult):
        self.results.append(result)

    def get_results(self, result_type: ResultTypes) -> List[NodeResult]:
        results_filtered = list(filter(lambda r: r.result_type == result_type, self.results))

        return results_filtered

    def cleanup(self) -> None:
        self.results = []
        pass
