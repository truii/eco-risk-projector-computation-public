# Entry method for model run
from core.model import Model
from core.sqlite_data_store import SQLiteDataStore
import helper.io as io
import os
import traceback
from helper.validation import remove_null_keys


def run(run_file, out_dir=None, result_dir_name=None, report_progress=False):
    """The entry to running the computation, called from run.py with the cli args

    Validates filepaths, imports the run file, create a model object and then runs the model object
    Handles any missed errors - This should never occur
    """

    # Import the settings and start run
    try:
        print("Starting Plugin Run")

        # Get abs file path
        run_file = io.get_abs_filepath(run_file)
        # Check file path
        if not (io.valid_filepath(run_file)):
            raise OSError("Run file does not exist at path: {0}".format(run_file))
        # Get out dir
        if out_dir is not None:
            # Get abs file path
            out_dir = io.get_abs_filepath(out_dir)
            # Check its a real directory
            if not (io.valid_dirpath(out_dir)):
                raise OSError("Out dir is not a valid directory: {0}".format(out_dir))
        else:  # If out_dir is not specified we use the same dir as the run.json
            out_dir = os.path.split(run_file)[0]

        # Import run settings
        run_settings = io.read_json(run_file)

        # io.debug_dump(run_file)

        # Clean run settings
        remove_null_keys(run_settings)

        # Create the DataStore, use an SQLite store for minimal memory usage
        data_store: SQLiteDataStore = SQLiteDataStore()

        # Create a model object from the run_settings file
        run_model = Model(run_settings, data_store, out_dir, result_dir_name, report_progress)

        # Run the model, passing the plugin.
        try:
            run_model.run()
            run_model.output()
        except Exception as e:
            write_error_file(e, run_model)
        finally:
            data_store.cleanup()

        print("Plugin Run Complete")

    except Exception as e:
        print("Error when running tool\n{0}".format(e))
        traceback.print_exc()


def write_error_file(error, run_model: Model):
    """Create a structures error file representing an error at a given location"""

    traceback.print_exc()
    try:
        message = getattr(error, "message")
    except Exception:
        message = str(error)
    print("FATAL ERROR: {0}".format(message))
    error_file = {
        "Code": error.__class__.__name__,
        "Message": message,
        "Model": run_model.plugin,
        "Trace": traceback.format_exc(),
    }

    run_model.create_result_directory()
    io.write_json(os.path.join(run_model.dir_path, "error.json"), error_file)
