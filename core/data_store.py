from abc import ABC, abstractmethod
from typing import List
from core.globals import ResultTypes
from core.node_result import NodeResult


class DataStore(ABC):
    @abstractmethod
    def __init__(self) -> None:
        pass

    @abstractmethod
    def create_result(self, result: NodeResult):
        pass

    @abstractmethod
    def get_results(self, result_type: ResultTypes) -> List[NodeResult]:
        pass

    @abstractmethod
    def cleanup(self) -> None:
        pass
