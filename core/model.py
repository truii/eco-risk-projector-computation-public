from typing import List
from core.data_store import DataStore
from core.globals import ModelSettings, PluginTypes, ResultTypes
from core.node import Node
from core.plugin import Plugin
from core.plugin_result import PluginResult
from helper.exception_utils import *
from core.exceptions import *
import helper.io as io
import gc
import importlib


class Model:
    """The class that captures a runfile and controls the computation run - including error handling"""

    def __init__(self, settings, data_store: DataStore, out_dir=None, result_dir_name=None, report_progress=False):
        """Create a model object from a json obj and and outdir path"""

        if self.validate_settings(settings):
            self.plugin: Plugin = getattr(
                importlib.import_module("plugins." + settings["plugin"]), settings["plugin"]
            )()
            self.data_store = data_store
            self.nodes = settings["nodes"]
            self.spatial_agg = None
            if "spatial_agg" in settings:
                self.spatial_agg = settings["spatial_agg"]
            self.run_period = None
            if "run_period" in settings:
                self.run_period = settings["run_period"]
            self.data_infill_settings = None
            if "data_infill_settings" in settings:
                self.data_infill_settings = settings["data_infill_settings"]
            self.out_dir = out_dir
            self.result_dir_name = (
                result_dir_name if result_dir_name is not None else io.get_dump_name(settings["plugin"])
            )
            self.report_progress = report_progress
            self.settings = settings

    def create_result_directory(self):
        dir_path = io.create_dir(self.out_dir, self.result_dir_name)
        self.dir_path = dir_path

    def run(self):
        """Function called to run the model

        This is where the plugin validate_args, run and output functions are called and thus must be implemented.
        This is also where errors thrown in the computation should be handled
        """

        # Validate the plugin args and run period
        try:
            validate_spatial_agg(self.spatial_agg, self.plugin.plugin_type)
            validate_run_period(self.run_period)
            validate_data_infill_settings(self.data_infill_settings)
        except Exception as e:
            handle_exception(e)

        # Validate & run each node
        for node in self.nodes:
            try:
                self.plugin.validate_node(node)
                node_data, node_default_data = self.plugin.load_data(node)
                if type(node_data) is list:
                    for data in node_data:
                        data.parse_run_period(self.run_period)
                        data.data_infill_settings = self.data_infill_settings
                else:
                    node_data.parse_run_period(self.run_period)
                    node_data.data_infill_settings = self.data_infill_settings

                parsed_node = Node(node, node_data, node_default_data)

                plugin_result = self.plugin.compute_assessment(parsed_node.assessment, self.plugin.run(parsed_node))

                self.save_result(plugin_result)

            except Exception as e:
                handle_exception(e, node)

            finally:
                gc.collect()
                if self.report_progress:
                    print("progress_ack")

        # Run the spatial agg
        self.plugin.compute_spatial(self.spatial_agg, self.data_store)

    def output(self):
        self.create_result_directory()

        self.plugin.write_intermediate_results(self.write_result)
        self.write_assessment_results()
        self.write_run_settings_log_json()

    def validate_settings(self, settings):
        """Validates that the runfile has all required general parameters"""

        parent_fields = ModelSettings.PARENTS.value
        for field in parent_fields:
            if field not in settings:
                raise ValueError("Model missing {0} field".format(field))
        return True

    def save_result(self, result: PluginResult) -> None:
        node_results = self.plugin.get_intermediate_results(result)
        node_results.extend(self.plugin.get_assessment_results(result))

        for node_result in node_results:
            self.data_store.create_result(node_result)

    def write_run_settings_log_json(self):
        formatted_data = {}
        for node in self.settings["nodes"]:
            node_name = node["meta_data"]["name"]
            formatted_data[node_name] = {
                "parameters": node["parameters"],
                "assessment": node["assessment"] if "assessment" in node else None,
            }
        formatted_data["spatial_assessment"] = self.settings["spatial_agg"] if "spatial_agg" in self.settings else None
        formatted_data["run_period"] = self.settings["run_period"] if "run_period" in self.settings else None

        io.write_json(self.dir_path + "\\" + "run_settings_log.json", formatted_data)

    def write_assessment_results(self):
        if self.plugin.plugin_type == PluginTypes.NONE:
            return

        # Generate Assessment Results
        if self.plugin.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY:
            self.write_to_csv(ResultTypes.DAILY_ASSESSMENT, ["node", "date", "success"])

        if (
            self.plugin.plugin_type == PluginTypes.TEMPORAL_YEARLY_DAILY
            or self.plugin.plugin_type == PluginTypes.TEMPORAL_YEARLY
        ):
            self.write_to_csv(ResultTypes.YEARLY_ASSESSMENT, ["node", "year", "success"])

        self.write_to_csv(ResultTypes.TEMPORAL_ASSESSMENT, ["node", "year", "success"])

        # Generate spatial results
        self.write_to_csv(ResultTypes.SPATIAL_ASSESSMENT, ["year", "risk"])

    def write_result(self, result_type: ResultTypes, headers: List[str] = []):
        if result_type == ResultTypes.PARAMETER_RESULTS:
            self.write_to_json(result_type)
            return

        self.write_to_csv(result_type, headers)

    def write_to_json(self, result_type: ResultTypes):
        results = self.data_store.get_results(result_type)
        result_json = {}
        for result in results:
            result_json.update(result.data)

        io.write_json(self.dir_path + "\\" + self.get_result_name(result_type) + ".json", result_json)

    def write_to_csv(self, result_type: ResultTypes, headers: List[str]):
        results = self.data_store.get_results(result_type)

        if result_type == ResultTypes.CUSTOM_RESULTS:
            rows = []
            for node in results:
                rows.extend(node.data)

            io.write_csv(self.dir_path + "\\" + self.get_result_name(result_type) + ".csv", rows)
            return

        node_data = list(map(lambda r: r.convert_data_to_list(headers), results))

        rows = [headers]
        for node in node_data:
            rows.extend(node)

        io.write_csv(self.dir_path + "\\" + self.get_result_name(result_type) + ".csv", rows)

    def get_result_name(self, result_type) -> str:
        # Intermediate Results
        if result_type == ResultTypes.DAILY_INTERMEDIATE:
            return "daily_intermediate_results"
        if result_type == ResultTypes.YEARLY_INTERMEDIATE:
            return "yearly_intermediate_results"
        if result_type == ResultTypes.SUMMARY_INTERMEDIATE:
            return "summary_intermediate_results"
        if result_type == ResultTypes.EVENTS_INTERMEDIATE:
            return "events_intermediate_results"

        # Assessment Results
        if result_type == ResultTypes.DAILY_ASSESSMENT:
            return "daily_results"
        if result_type == ResultTypes.YEARLY_ASSESSMENT:
            return "yearly_results"
        if result_type == ResultTypes.TEMPORAL_ASSESSMENT:
            return "temporal_results"
        if result_type == ResultTypes.SPATIAL_ASSESSMENT:
            return "spatial_results"

        # Spatial results
        if result_type == ResultTypes.DAILY_SPATIAL_INTERMEDIATE:
            return "daily_spatial_intermediate_results"
        if result_type == ResultTypes.YEARLY_SPATIAL_INTERMEDIATE:
            return "yearly_spatial_intermediate_results"
        if result_type == ResultTypes.EVENTS_SPATIAL_INTERMEDIATE:
            return "events_spatial_intermediate_results"
        if result_type == ResultTypes.SUMMARY_SPATIAL_INTERMEDIATE:
            return "summary_spatial_intermediate_results"

        # Other results
        if result_type == ResultTypes.PARAMETER_RESULTS:
            return "parameter_results"

        return "custom_results"
