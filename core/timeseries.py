from copy import deepcopy
from datetime import datetime, timedelta, timezone
import math
import statistics
from typing import List, Optional
from core.data import Data
from core.exceptions import InvalidDataException
from core.globals import DataInfillTypes, InvalidDataTypes, TimeseriesDataTypes
from core.node_result import NodeResult
from core.season import Season

from core.timeseries_day import TimeseriesDay
import helper.calculations as calcs


class Timeseries:
    """A class to handle a data timeseries (daily data values)

    This includes helper functions to add data, trim, aggregate etc
    """

    def __init__(self, data: Data = None, data_types_to_exclude: List[TimeseriesDataTypes] = []):
        self.contained_data_types: List[TimeseriesDataTypes] = []
        timeseries: List[TimeseriesDay] = (
            self.parse_timeseries_from_data(data, data_types_to_exclude) if data is not None else None
        )
        self.data: List[TimeseriesDay] = timeseries

    def set_timeseries(self, timeseries: List[TimeseriesDay]):
        self.data = timeseries

    def has_data_type(self, data_type: TimeseriesDataTypes) -> bool:
        return data_type in self.contained_data_types

    ### Parsing methods
    def parse_timeseries_from_data(
        self, data: Data, data_types_to_exclude: List[TimeseriesDataTypes] = [], concurrent_only: bool = True
    ) -> List[TimeseriesDay]:
        """
        Transform all avaliable timeseries data types in data into a single Timeseries

        concurrent_only refers to if the resulting Timeseries should only contain days that have data for all provided types (No missing data)
        """

        avaliable_data_types = filter(
            lambda f: getattr(data, f) is not None and f not in data_types_to_exclude, data.timeseries_fields
        )

        should_error_on_invalid_data = False
        should_infill_with_previous_value = False

        if data.data_infill_settings is not None:
            if data.data_infill_settings["should_infill_data"]:
                if data.data_infill_settings["infill_method"] == DataInfillTypes.PREVIOUS_VALUE:
                    should_infill_with_previous_value = True
            else:
                should_error_on_invalid_data = True

        parsed_data = list(
            map(
                lambda data_type: data.parse_csv_to_timeseries_days(
                    data_type,
                    infill_with_previous_value=should_infill_with_previous_value,
                    should_error_on_invalid_data=should_error_on_invalid_data,
                ),
                avaliable_data_types,
            )
        )

        merged_days = self.merge_timeseries_days(parsed_data, concurrent_only)

        if len(merged_days) == 0:
            raise InvalidDataException(InvalidDataTypes.NO_CONCURRENT_DATA)

        return merged_days

    def merge_timeseries_days(
        self, timeseries_days_lists: List[List[TimeseriesDay]], concurrent_only: bool = True
    ) -> List[TimeseriesDay]:
        """
        Merge multiple Timeseries into a single timeseries

        concurrent_only refers to if the resulting Timeseries should only contain days that have data for all provided types (No missing data)
        """

        timeseries_days_series = []
        start_day = None
        end_day = None

        for series in timeseries_days_lists:
            start = series[0].date
            end = series[-1].date
            timeseries_days_series.append({"start": start, "end": end, "data": series})

            if start_day is None or start < start_day:
                start_day = start

            if end_day is None or end > end_day:
                end_day = end

        length = (end_day - start_day).days + 1

        result_timeseries = []
        all_data_types = set()

        for date_index in range(length):
            date = start_day + timedelta(days=date_index)
            day = TimeseriesDay(date)

            date_values = {}
            date_calculated_values = {}
            date_result_values = {}
            for series in timeseries_days_series:
                if series["start"] <= date <= series["end"]:
                    adjusted_index = date_index - (series["start"] - start_day).days
                    date_values.update(series["data"][adjusted_index].values)
                    date_calculated_values.update(series["data"][adjusted_index].calculated_values)
                    date_result_values.update(series["data"][adjusted_index].result_values)

            all_data_types.update(date_values.keys())

            day.set_values(date_values)
            day.set_calculated_values(date_calculated_values)
            day.set_result_values(date_result_values)

            result_timeseries.append(day)

        self.contained_data_types = list(all_data_types)

        if not concurrent_only:
            return result_timeseries

        return list(
            filter(lambda d: all(data_type in d.values for data_type in list(all_data_types)), result_timeseries)
        )

    def add_timeseries_days(self, timeseries_days: List[TimeseriesDay], concurrent_only: bool = True) -> None:
        """
        Add the values from a list of TimeseriesDay onto the existing timeseries

        concurrent_only refers to if the resulting Timeseries should only contain days that have data for all provided types (No missing data)
        """

        if self.data is None:
            new_data = self.merge_timeseries_days([timeseries_days], concurrent_only)
        else:
            current_data = self.data
            new_data = self.merge_timeseries_days([current_data, timeseries_days], concurrent_only)

        self.set_timeseries(new_data)

    def convert_to_result_data(
        self,
        headers: List[str],
        date_index_list: Optional[List[int]] = None,
        extra_cols: Optional[dict[str, str]] = None,
    ) -> List[dict]:
        """
        Convert a timeseries object to data ready to store on a Result, by extracting specific fields from the timeseries

        Expects a list of header strings (case sensitive) that match keys in the timeseries
        Keys are matched on a day in the following priority: result_values, calculated_valeus, values

        You can specify a list of date index's (position in the headers list), this will format each of these index's as a date

        Can supply a dictionary in extra_cols of format {header:value,header:value} that will add a static value to all rows under the specified header.
        Header must be included in header list or extra_col will be ignored.

        Returns a list of dictionary's containing day values
        """

        result_data = []

        for day in self.data:
            day_result = {}
            for index, header in enumerate(headers):
                if extra_cols is not None and header in extra_cols:
                    day_result[header] = extra_cols[header]
                else:
                    if date_index_list is not None and index in date_index_list:
                        day_result[header] = day.format_date()
                    else:
                        day_result[header] = day.get_value_from_key(header)

            result_data.append(day_result)

        return result_data

    ### Statistical methods
    def get_data_type_value_median(
        self, data_type: str, data: List[TimeseriesDay] = None, dp: int = 3
    ) -> float or None:
        """
        Get the median of all values from a given data_type in the timeseries
        Returns None if no data is present for given data_type
        """

        if data is None:
            data = self.data

        values = list(filter(lambda v: v is not None, map(lambda v: v.get_value_from_key(data_type), data)))
        return round(statistics.median(values), dp) if len(values) > 0 else None

    def get_data_type_value_mean(self, data_type: str, data: List[TimeseriesDay] = None, dp: int = 3) -> float or None:
        """
        Get the mean of all values from a given data_type in the timeseries
        Returns None if no data is present for given data_type
        """

        if data is None:
            data = self.data

        values = list(filter(lambda v: v is not None, map(lambda v: v.get_value_from_key(data_type), data)))
        return round(statistics.mean(values), dp) if len(values) > 0 else None

    def get_data_type_value_sum(self, data_type: str, data: List[TimeseriesDay] = None, dp: int = 3) -> float or None:
        """
        Get the sum of all values from a given data_type in the timeseries
        Returns None if no data is present for given data_type
        """

        if data is None:
            data = self.data

        values = list(filter(lambda v: v is not None, map(lambda v: v.get_value_from_key(data_type), data)))
        return round(sum(values), dp) if len(values) > 0 else None

    def get_data_type_value_max(self, data_type: str, data: List[TimeseriesDay] = None) -> float or None:
        """
        Get the max of all values from a given data_type in the timeseries
        Returns None if no data is present for given data_type
        """

        if data is None:
            data = self.data

        values = list(filter(lambda v: v is not None, map(lambda v: v.get_value_from_key(data_type), data)))
        return max(values) if len(values) > 0 else None

    def get_data_type_value_min(self, data_type: str, data: List[TimeseriesDay] = None) -> float or None:
        """
        Get the min of all values from a given data_type in the timeseries
        Returns None if no data is present for given data_type
        """

        if data is None:
            data = self.data

        values = list(filter(lambda v: v is not None, map(lambda v: v.get_value_from_key(data_type), data)))
        return min(values) if len(values) > 0 else None

    def set_moving_avg(self, data_type: str, key_name: str, num_days: int) -> None:
        """
        Set a new value on each day, that is the moving average of a given data_type in the timeseries across a given num_days.
        The new value will be in the calculated_values field of the day

        When we don't have enough days to compute the moving avg for the num_days specified, the avaliable days will be used.
        I.E. 7 day moving avg for the 3rd day will be the avg of day 1, day 2 and day 3. From day 7 onwards the avg will always be for the full 7 days.
        """

        if num_days < 1:
            return

        start_index = -num_days
        for index, day in enumerate(self.data):
            start_index += 1
            day.set_calculated_value(
                key_name, self.get_data_type_value_mean(data_type, self.data[max(start_index, 0) : index + 1])
            )

        self.contained_data_types.append(key_name)

    def set_non_moving_sum(self, data_type: str, key_name: str, num_days: int) -> None:
        """
        Set a new value on each day, that is the non moving sum of a given data_type in the timeseries across a given num_days.
        I.E. if num_days = 7 then each block of 7 will have the same sum (the sum for that 7). Starting from the first day.
        The new value will be in the calculated_values field of the day

        When we dont have enough days to compute the sum of the requested num_days, the avaliable days will be used.
        I.E. If we have 3 days leftover at the end of the record, the value on these days will be the sum of these 3 days.
        """

        if num_days < 1:
            return

        index_list = []
        sum_ = 0
        for index, day in enumerate(self.data):
            sum_ += day.get_value_from_key(data_type)
            index_list.append(index)

            if len(index_list) == num_days:
                for index_ in index_list:
                    self.data[index_].set_calculated_value(key_name, sum_)
                index_list = []
                sum_ = 0

        if len(index_list) > 0:
            for index_ in index_list:
                self.data[index_].set_calculated_value(key_name, sum_)

        self.contained_data_types.append(key_name)

    def get_data_type_in_range_index_list(
        self, data_type: str, range_min: float = None, range_max: float = None
    ) -> List[int]:
        """
        Get a list of the data index's where the data_type value for that index is inclusivly within the provided range

        If the min or max is None, we only consider the alternative (No max = check if >= min, No min = check if <= max)
        """

        if not self.has_data_type(data_type):
            return []

        if range_min is None and range_max is None:
            return []

        result = []

        for index, day in enumerate(self.data):
            try:
                if range_min is None:
                    if day.get_value_from_key(data_type) <= range_max:
                        result.append(index)
                elif range_max is None:
                    if day.get_value_from_key(data_type) >= range_min:
                        result.append(index)
                else:
                    if (
                        day.get_value_from_key(data_type) >= range_min
                        and day.get_value_from_key(data_type) <= range_max
                    ):
                        result.append(index)
            except TypeError:
                pass  # This occurs when a day has a None value for the given data_type
        return result

    def is_data_type_variation_within_allowed_across_period(
        self, data_type: str, allowed_variation: float, data: List[TimeseriesDay] = None
    ) -> bool:
        """
        Check if the difference between the min and max values of a data_type is inclusivly within the allowed_variation
        """

        if not self.has_data_type(data_type):
            return False

        if data is None:
            data = self.data

        min_value, max_value = None, None
        for day in data:
            if min_value is None or day.get_value_from_key(data_type) < min_value:
                min_value = day.get_value_from_key(data_type)
            if max_value is None or day.get_value_from_key(data_type) > max_value:
                max_value = day.get_value_from_key(data_type)

        # Isclose is required due to the way python handles floats, allowing situations such as 0.5 > 0.5 to be true
        # isclose effectively checks for this edgecase and prevents incorrect conditionals
        difference = max_value - min_value
        if (difference > allowed_variation) and not (math.isclose(difference, allowed_variation, abs_tol=0.00001)):
            return False
        return True

    def is_data_type_variation_within_allowed_between_days(
        self, data_type: str, allowed_variation: float, data: List[TimeseriesDay] = None
    ) -> bool:
        """
        Check if the difference between the current and previous day values of a data_type is inclusivly within the allowed_variation
        """

        if not self.has_data_type(data_type):
            return False

        if data is None:
            data = self.data

        prev_day = None
        for day in data:
            if prev_day is not None:
                if abs(day.get_value_from_key(data_type) - prev_day.get_value_from_key(data_type)) > allowed_variation:
                    return False
            prev_day = day
        return True

    def is_data_type_variation_within_allowed_from_baseline(
        self, data_type: str, allowed_variation: float, baseline: float, data: List[TimeseriesDay] = None
    ) -> bool:
        """
        Check if each day in the data is within allowed_variation of the baseline
        """

        if not self.has_data_type(data_type):
            return False

        if data is None:
            data = self.data

        min_allowed = baseline - allowed_variation
        max_allowed = baseline + allowed_variation

        for day in data:
            if day.get_value(data_type) < min_allowed or day.get_value(data_type) > max_allowed:
                return False
        return True

    def is_data_type_variation_within_range(
        self, data_type: str, min_allowed: float, max_allowed: float, data: List[TimeseriesDay] = None
    ) -> bool:
        """
        Check if each day in the data is within allowed_variation of the baseline
        """

        if not self.has_data_type(data_type):
            return False

        if data is None:
            data = self.data

        for day in data:
            if day.get_value(data_type) < min_allowed or day.get_value(data_type) > max_allowed:
                return False
        return True

    def get_data_type_percentile(self, data_type: str, percentile: float, dp: int = 3) -> float:
        """
        Get the the specified percentile (1-99) of a given data type
        """

        if not self.has_data_type(data_type):
            return None

        values = list(map(lambda d: d.get_value_from_key(data_type), self.data))
        if not (len(values) > 0) or percentile < 1 or percentile > 99:
            return None
        percentiles = statistics.quantiles(values, n=100)
        return round(percentiles[percentile - 1], dp)

    def get_flood_level_POT(self, data_type: str, ARI: float, data: List[TimeseriesDay] = None, dp: int = 2) -> float:
        """
        Get flood flow threshold for a given ARI for a given data_type using the peak-over-threshold method
        """

        if not self.has_data_type(data_type):
            return None

        if data is None:
            data = self.data

        if ARI <= 0:
            return None

        # Get Monthly max dischage, and num years in record (n)
        monthly_max_discharge = self.get_monthly_max_value(data_type, data)
        num_years = (data[-1].date.year - data[0].date.year) + 1

        # Sort max discharge and take top n
        monthly_max_discharge.sort()
        monthly_max_discharge = monthly_max_discharge[-num_years:]

        annual_probabilities = []
        curve_points = []
        for index, value in enumerate(monthly_max_discharge):
            res = {"flow": value, "rank": index + 1}
            res["pr"] = (len(monthly_max_discharge) - res["rank"] + 1) / (len(monthly_max_discharge) + 1)
            res["rp"] = 1 / res["pr"]
            annual_probabilities.append(res)
            curve_points.append([res["rp"], res["flow"]])
        return round(calcs.get_curve_value(ARI, curve_points), dp)

    def get_monthly_max_value(self, data_type: str, data: List[TimeseriesDay] = None) -> List[float]:
        """
        Get the max value for each month in the data for a given data_type
        """

        if not self.has_data_type(data_type):
            return []

        if data is None:
            data = self.data

        yearly_data = self.sort_into_years(data)

        peak_values = []

        for days in yearly_data.values():
            months = {}

            for day in days:
                if day.date.month in months:
                    if day.get_value_from_key(data_type) > months[day.date.month]:
                        months[day.date.month] = day.get_value_from_key(data_type)
                else:
                    months[day.date.month] = day.get_value_from_key(data_type)

            peak_values.extend(months.values())

        return peak_values

    def get_RMSE_for_keys(
        self,
        actual_key: str,
        estimated_key: str,
        actual_max: float = None,
        dp: int = 3,
        data: List[TimeseriesDay] = None,
    ) -> float:
        """
        Calculate the RMSE for days given an actual value key and an estimated value key

        actual_max can be provided to adjust for situations where the estimated value might be larger than the possible max. The daily estimated value will be adjusted to be the actual max
        """

        if data is None:
            data = self.data

        sum_squared_error = 0
        count_usable_items = 0
        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                sum_squared_error += math.pow((actual - estimated), 2)
                count_usable_items += 1

        if count_usable_items == 0:
            return None

        return round(math.sqrt(sum_squared_error / count_usable_items), dp)

    def get_R2_for_keys(
        self,
        actual_key: str,
        estimated_key: str,
        actual_max: float = None,
        dp: int = 3,
        data: List[TimeseriesDay] = None,
    ) -> float:
        """
        Calculate the R2 for days given an actual value key and an estimated value key

        actual_max can be provided to adjust for situations where the estimated value might be larger than the possible max. The daily estimated value will be adjusted to be the actual max
        """

        if data is None:
            data = self.data

        # MSE
        mean_actual_key = 0
        sum_squared_error = 0
        count_usable_items = 0
        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                sum_squared_error += math.pow((actual - estimated), 2)
                count_usable_items += 1
                mean_actual_key += actual

        if count_usable_items == 0:
            return None

        mean_actual_key /= count_usable_items
        total_sum_of_squares = 0
        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                total_sum_of_squares += math.pow((actual - mean_actual_key), 2)

        r2 = 1 - (sum_squared_error / total_sum_of_squares) if total_sum_of_squares > 0 else 0

        return round(r2, dp)

    def get_NNSE_for_keys(
        self,
        actual_key: str,
        estimated_key: str,
        actual_max: float = None,
        dp: int = 3,
        data: List[TimeseriesDay] = None,
    ) -> float:
        """
        Calculate the NNSE for days given an actual value key and an estimated value key

        actual_max can be provided to adjust for situations where the estimated value might be larger than the possible max. The daily estimated value will be adjusted to be the actual max
        """

        if data is None:
            data = self.data

        r2 = self.get_R2_for_keys(actual_key, estimated_key, actual_max, dp, data)

        if r2 == None:
            return None

        nnse = 1 / (2 - r2)

        return round(nnse, dp)

    def get_PBIAS_for_keys(
        self,
        actual_key: str,
        estimated_key: str,
        actual_max: float = None,
        dp: int = 3,
        data: List[TimeseriesDay] = None,
    ) -> float:
        """
        Calculate the PBIAS for days given an actual value key and an estimated value key

        actual_max can be provided to adjust for situations where the estimated value might be larger than the possible max. The daily estimated value will be adjusted to be the actual max
        """

        if data is None:
            data = self.data

        # MSE
        sum_actual_key = 0
        sum_error = 0
        count_usable_items = 0
        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                sum_error += (actual - estimated) * 100
                sum_actual_key += actual

                count_usable_items += 1

        if count_usable_items == 0:
            return None

        pbias = sum_error / sum_actual_key

        return round(pbias, dp)

    def get_RSR_for_keys(
        self,
        actual_key: str,
        estimated_key: str,
        actual_max: float = None,
        dp: int = 3,
        data: List[TimeseriesDay] = None,
    ) -> float:
        """
        Calculate the RSR for days given an actual value key and an estimated value key

        actual_max can be provided to adjust for situations where the estimated value might be larger than the possible max. The daily estimated value will be adjusted to be the actual max
        """

        if data is None:
            data = self.data

        sum_squared_error = 0
        count_usable_items = 0
        mean_actual_key = 0
        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                sum_squared_error += math.pow((actual - estimated), 2)
                mean_actual_key += actual
                count_usable_items += 1

        if count_usable_items == 0:
            return None

        sqrt_sum_squared_error = math.sqrt(sum_squared_error)
        mean_actual_key /= count_usable_items

        stdev = 0

        for day in data:
            actual = day.get_value_from_key(actual_key)
            estimated = day.get_value_from_key(estimated_key)

            if actual is not None and estimated is not None:
                if actual_max is not None:
                    actual = min(actual, actual_max)

                stdev += math.pow((actual - mean_actual_key), 2)

        sqrt_stdev = math.sqrt(stdev)

        rsr = sqrt_sum_squared_error / sqrt_stdev if sqrt_stdev > 0 else math.inf

        return round(rsr, dp)

    ### Time/Season methods
    def get_season_index_list(
        self, season: Season, include_partial_seasons=False, data: List[TimeseriesDay] = None
    ) -> List[int]:
        """
        Get list of data index's that fall within the provided season

        Expects a season object, formatted as follows: {'start':{'day':1,'month':1},'end':{'day':31,'month':12}}

        include_partial_seasons to include data where the season starts but the ts ends before the season ends - I.E. incomplete data
        """

        if data is None:
            data = self.data

        season_indicies = [{"start": None, "end": None}]

        # Get indicies of seasons
        for index, day in enumerate(data):
            if day.is_date_equal(season.start_day, season.start_month):
                season_indicies.append({"start": index, "end": None})
            if day.is_date_equal(season.end_day, season.end_month):
                season_indicies[-1]["end"] = index

        # Remove partial seasons if include_partial_season is False
        if not include_partial_seasons:
            items_to_remove = []
            for index, item in enumerate(season_indicies):
                if item["start"] is None or item["end"] is None:
                    items_to_remove.append(index)
            items_to_remove.reverse()
            for index in items_to_remove:
                season_indicies.pop(index)

        # return full flat result list
        flat_result = []
        for season_index in season_indicies:
            if season_index["start"] is None and season_index["end"] is None:
                continue
            if season_index["start"] is None and season_index["end"] is not None:
                season_index["start"] = 0
            elif season_index["start"] is not None and season_index["end"] is None:
                season_index["end"] = len(data) - 1
            flat_result.extend(list(range(season_index["start"], season_index["end"] + 1)))
        return flat_result

    def split_into_seasons(
        self, season: Season, include_partial_seasons: bool = False, data: List[TimeseriesDay] = None
    ) -> List[List[TimeseriesDay]]:
        """
        Split timeseries days by season day/month (no year). I.E. 1/5 - 30/4

        Expects a season object, formatted as follows: {'start':{'day':1,'month':1},'end':{'day':31,'month':12}}

        include_partial_seasons to include data where the season starts but the ts ends before the season ends - I.E. incomplete data
        """

        if data is None:
            data = self.data

        season_indicies = [{"start": None, "end": None, "days": []}]

        # Get indicies of seasons
        for index, day in enumerate(data):
            if day.is_date_equal(season.start_day, season.start_month):
                season_indicies.append({"start": index, "end": None, "days": []})

            if day.is_date_equal(season.end_day, season.end_month):
                season_indicies[-1]["end"] = index + 1

            season_indicies[-1]["days"].append(index)

        # Remove any where both start and end is None
        empty_seasons = []
        for index, item in enumerate(season_indicies):
            if item["start"] is None and item["end"] is None:
                if len(item["days"]) == 0:
                    empty_seasons.append(index)
                else:
                    check_day_index = item["days"][0]
                    if not data[check_day_index].is_in_season(season):
                        empty_seasons.append(index)

        empty_seasons.reverse()
        for index in empty_seasons:
            season_indicies.pop(index)

        # Remove partial seasons if include_partial_season False
        if not include_partial_seasons:
            items_to_remove = []
            for index, item in enumerate(season_indicies):
                if item["start"] is None or item["end"] is None:
                    items_to_remove.append(index)
            items_to_remove.reverse()
            for index in items_to_remove:
                season_indicies.pop(index)

        # split list by index pairs
        return list(map(lambda pair: data[pair["start"] : pair["end"]], season_indicies))

    def trim_into_season(
        self, season: Season, include_partial_seasons: bool = False, data: List[TimeseriesDay] = None
    ) -> List[TimeseriesDay]:
        """
        Reduces the data into a single flat list of days within the provided season

        Expects a season object, formatted as follows: {'start':{'day':1,'month':1},'end':{'day':31,'month':12}}
        """

        if data is None:
            data = self.data

        index_list = self.get_season_index_list(season, include_partial_seasons, data)

        return [d for i, d in enumerate(data) if i in index_list]

    def trim_into_run_period(self, run_period, data: List[TimeseriesDay] = None) -> List[TimeseriesDay]:
        """
        Shortens days to only include those inclusivly within the provided run_period
        """

        if data is None:
            data = self.data

        new_ts = []
        if "start" in run_period:
            start_date = (
                datetime(run_period["start"]["year"], run_period["start"]["month"], run_period["start"]["day"])
                .replace(tzinfo=timezone.utc)
                .replace(hour=0, minute=0, second=0, microsecond=0)
            )
        else:
            start_date = None

        if "end" in run_period:
            end_date = (
                datetime(run_period["end"]["year"], run_period["end"]["month"], run_period["end"]["day"])
                .replace(tzinfo=timezone.utc)
                .replace(hour=0, minute=0, second=0, microsecond=0)
            )
        else:
            end_date = None

        for day in data:
            if day.is_between_dates(start_date, end_date):
                new_ts.append(day)

        return new_ts

    def split_by_value_key(self, data_type: str, data: List[TimeseriesDay] = None) -> List[List[TimeseriesDay]]:
        """
        Split timeseries days by key. Sequential days with the same key will be grouped together.
        """

        if not self.has_data_type(data_type):
            return []

        if data is None:
            data = self.data

        split_list = []
        current_data = data[0].get_value_from_key(data_type)
        split_list.append([])

        for day in data:
            if day.get_value_from_key(data_type) != current_data:
                current_data = day.get_value_from_key(data_type)
                split_list.append([])
            split_list[-1].append(day)

        return split_list

    def sort_into_months(self, data: List[TimeseriesDay] = None) -> dict[int, List[TimeseriesDay]]:
        """
        Seperate a list of days into a dictionary of form {"month": [days]} I.E. {1: [day1,day2,day3]}
        """

        if data is None:
            data = self.data

        monthly_split = {}
        for day in data:
            if day.date.month in monthly_split:
                monthly_split[day.date.month].append(day)
            else:
                monthly_split[day.date.month] = []
                monthly_split[day.date.month].append(day)
        return monthly_split

    def sort_into_years(self, data: List[TimeseriesDay] = None) -> dict[int, List[TimeseriesDay]]:
        """
        Seperate a list of days into a dictionary of form {"year": [days]} I.E. {2020: [day1,day2,day3]}
        """

        if data is None:
            data = self.data

        yearly_split = {}
        for day in data:
            if day.date.year in yearly_split:
                yearly_split[day.date.year].append(day)
            else:
                yearly_split[day.date.year] = []
                yearly_split[day.date.year].append(day)
        return yearly_split

    def sort_into_calender_seasons(self, data: List[TimeseriesDay] = None) -> dict[str, List[TimeseriesDay]]:
        """
        Seperate a list of days into a dictionary of form {"season": [days]} I.E. {"summer": [day1,day2,day3]}
        """

        if data is None:
            data = self.data

        season_split = {}
        for day in data:
            season = day.get_season_for_day()

            if season in season_split:
                season_split[season].append(day)
            else:
                season_split[season] = []
                season_split[season].append(day)

        return season_split

    def get_number_of_days_between_days(self, start_day: TimeseriesDay, end_day: TimeseriesDay) -> int:
        """
        Get the number of days between 2 TimeseriesDays

        It is assumed that start_day is > end_day
        """

        return (start_day.date - end_day.date).days

    def apply_lag_to_data_type(self, data_type: TimeseriesDataTypes, lag: int):
        """
        Apply a lag to a given data_type

        lag can be positive (move the value backwards in time) or negative (more the value forwards in time)

        This will move all values on that data_type back by lag days
        This means the first n (lag) days of the ts will be lost
        """

        if lag == 0 or abs(lag) > len(self.data) or not self.has_data_type(data_type):
            return

        if lag > 0:
            new_ts: List[TimeseriesDay] = deepcopy(self.data)

            for index, day in enumerate(self.data):
                lag_index = index + lag
                if lag_index < len(new_ts):
                    new_ts[lag_index].set_value(data_type, day.get_value(data_type))

            self.set_timeseries(new_ts[lag:])

        elif lag < 0:
            new_ts: List[TimeseriesDay] = deepcopy(self.data)

            for index, day in enumerate(self.data):
                lag_index = index + lag
                if lag_index >= 0:
                    new_ts[lag_index].set_value(data_type, day.get_value(data_type))

            self.set_timeseries(new_ts[:lag])
