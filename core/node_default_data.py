from core.data import Data


class NodeDefaultData(Data):
    """Class to handle node data"""

    def __init__(self, node):
        super().__init__()
        self.node = node
