# Global Variables
from enum import Enum


class ModelSettings(Enum):
    """Expected model settings fields, used for validation"""

    PARENTS = ["plugin", "nodes"]


class PluginSettings(Enum):
    """Expected plugin meta_data fields, used for validation"""

    PARENTS = ["name", "description", "version", "author"]


class ComparisonOptions(str, Enum):
    """Allowed aggregation comparison options"""

    GREATER_EQUAL = "gte"
    LESS_EQUAL = "lte"


class PluginTypes(str, Enum):
    """Plugin result types"""

    NONE = "none"
    TEMPORAL = "temporal"
    TEMPORAL_YEARLY = "temporal_yearly"
    TEMPORAL_YEARLY_DAILY = "temporal_yearly_daily"
    TEMPORAL_CONVERSION = "temporal_conversion"


class AggregationTypeOptions(str, Enum):
    """Allowed aggregation type options"""

    SUM = "sum"
    MAX = "max"
    MIN = "min"


class CriteriaOptions(str, Enum):
    """Allowed daily agg criteria options"""

    THRESHOLD = "threshold"
    MEDIAN = "median"


class WaterholeCalibrationType(str, Enum):
    """Allowed waterhole calibration types"""

    RUN_ONCE = "run_once"
    OPTIMISE = "optimise"


class WaterholeFitStatisticType(str, Enum):
    """Allowed waterhole fit statistic types"""

    RMSE = "rmse"
    R2 = "r2"
    NNSE = "nnse"
    PBIAS = "pbias"
    RSR = "rsr"


class WaterholeRunToEmptyEvaporationType(str, Enum):
    """Allowed waterhole run to empty evaporation types"""

    DATA = "data"
    MEAN = "mean"
    VALUE = "value"
    DATA_PLUS_PERCENT = "data_plus_percent"
    DATA_PLUS_VALUE = "data_plus_value"


class Months(str, Enum):
    """Valid months"""

    JANUARY = "january"
    FEBRUARY = "february"
    MARCH = "march"
    APRIL = "april"
    MAY = "may"
    JUNE = "june"
    JULY = "july"
    AUGUST = "august"
    SEPTEMBER = "september"
    OCTOBER = "october"
    NOVEMBER = "november"
    DECEMBER = "december"


class SpellAnalysisTypes(str, Enum):
    """Valid spell analysis types"""

    ABOVE_THRESHOLD = "above_threshold"
    BELOW_THRESHOLD = "below_threshold"
    RANGE = "range"


class SpellMetrics(str, Enum):
    """Valid spell metrics"""

    COUNT = "count"
    MEAN_MAGNITUDE = "mean_magnitude"
    TOTAL_DURATION = "total_duration"
    MEAN_DURATION = "mean_duration"
    SINGLE_LONGEST = "single_longest"
    TOTAL_DURATION_BETWEEN_SPELLS = "total_duration_between_spells"
    MEAN_DURATION_BETWEEN_SPELLS = "mean_duration_between_spells"
    SINGLE_LONGEST_BETWEEN_SPELLS = "single_longest_between_spells"


class TimeseriesDataTypes(str, Enum):
    """Valid timeseries data types"""

    FLOW = "flow"
    DEPTH = "depth"
    TEMPERATURE = "temperature"
    RAINFALL = "rainfall"
    EVAPORATION = "evaporation"
    COMPLIANCE = "compliance"
    SALINITY = "salinity"
    OTHER_TIMESERIES = "other_timeseries"


class OtherDataTypes(str, Enum):
    """Valid non-timeseries data types"""

    CLIMATE = "climate"
    BATHYMETRY = "bathymetry"


class ColwellsIndexTimePeriodTypes(str, Enum):
    """Valid colwells index time period types"""

    MONTHLY = "monthly"
    SEASONAL = "seasonal"


class ColwellsIndexSummaryMethodTypes(str, Enum):
    MEAN = "mean"
    MEDIAN = "median"
    MIN = "min"
    MAX = "max"


class ColwellsIndexClassBoundaryMethodTypes(str, Enum):
    TRANSFORM = "transform"
    EQUAL = "equal"
    LOG = "log"
    WEIGHTED_LOG = "weighted_log"
    GAN = "gan"


class DataInfillTypes(str, Enum):
    EMPTY = "empty"
    PREVIOUS_VALUE = "previous_value"


class InvalidDataTypes(str, Enum):
    UNKNOWN_EXCEPTION = "unknown_exception"
    UNKNOWN_DATE_FORMAT = "unknown_date_format"
    MISSING_DATE = "missing_date"
    INVALID_VALUE = "invalid_value"
    NO_CONCURRENT_DATA = "no_concurrent_data"


class ResultTypes(str, Enum):
    DAILY_ASSESSMENT = "daily_results"
    YEARLY_ASSESSMENT = "yearly_results"
    TEMPORAL_ASSESSMENT = "temporal_results"
    SPATIAL_ASSESSMENT = "spatial_results"
    DAILY_INTERMEDIATE = "daily_intermediate_results"
    YEARLY_INTERMEDIATE = "yearly_intermediate_results"
    SUMMARY_INTERMEDIATE = "summary_intermediate_results"
    EVENTS_INTERMEDIATE = "events_intermediate_results"
    PARAMETER_RESULTS = "parameter_results"
    DAILY_SPATIAL_INTERMEDIATE = "daily_spatial_intermediate_results"
    YEARLY_SPATIAL_INTERMEDIATE = "yearly_spatial_intermediate_results"
    SUMMARY_SPATIAL_INTERMEDIATE = "summary_spatial_intermediate_results"
    EVENTS_SPATIAL_INTERMEDIATE = "events_spatial_intermediate_results"
    RUN_SETTINGS_LOG = "run_settings_log"
    CUSTOM_RESULTS = "custom_results"
