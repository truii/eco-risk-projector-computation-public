import sqlite3
import json
from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes
from core.node_result import NodeResult
import uuid
import helper.io as io


class SQLiteDataStore(DataStore):
    def __init__(self) -> None:
        self.id = str(uuid.uuid4())
        self.path = io.get_abs_filepath("temp/{0}.db".format(self.id))
        self.connection = sqlite3.connect(self.path)
        self.channel = self.connection.cursor()
        self.__init_tables()

    def __init_tables(self) -> None:
        self.channel.execute("CREATE TABLE results(node, result_type, data)")

    def create_result(self, result: NodeResult):
        sql_str = """INSERT INTO results VALUES ('{0}', '{1}', '{2}')""".format(
            result.node, result.result_type, result.serialize_to_json()
        )
        self.channel.execute(sql_str)
        self.connection.commit()

    def get_results(self, result_type: ResultTypes) -> List[NodeResult]:
        sql_str = """SELECT * FROM results where result_type = '{0}'""".format(result_type)
        query = self.channel.execute(sql_str)
        raw_data = query.fetchall()
        results = list(map(lambda r: NodeResult(r[0], r[1], json.loads(r[2])), raw_data))

        return results

    def cleanup(self) -> None:
        try:
            self.connection.close()
        finally:
            io.delete_file(self.path)
