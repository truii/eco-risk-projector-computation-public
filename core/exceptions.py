# Custom exceptions
from core.globals import InvalidDataTypes


class MissingDataFileException(Exception):
    """Exception raised when a plugin is missing a required data file"""

    def __init__(self, file, node=None):
        self.file = file
        self.node = node
        self.set_message()

    def set_node(self, node):
        self.node = node
        self.set_message()

    def set_message(self):
        if self.node is None:
            self.message = "Could not find data file of type {0} in Plugin Args".format(self.file)
        else:
            self.message = "Could not find data file of type {0} in node {1}".format(self.file, self.node)
        super().__init__(self.message)


class MissingParameterException(Exception):
    """Exception raised when a plugin is missing a required parameter"""

    def __init__(self, parameter, node=None):
        self.parameter = parameter
        self.node = node
        self.set_message()

    def set_node(self, node):
        self.node = node
        self.set_message()

    def set_message(self):
        if self.node is None:
            self.message = "{0} parameter could not be found in Plugin Args".format(self.parameter)
        else:
            self.message = "{0} parameter could not be found in node {1}".format(self.parameter, self.node)

        super().__init__(self.message)


class InvalidParameterException(Exception):
    """Exception raised when a plugin parameter is invalid"""

    def __init__(self, parameter, reason, node=None):
        self.parameter = parameter
        self.node = node
        self.reason = reason
        self.set_message()

    def set_node(self, node):
        self.node = node
        self.set_message()

    def set_message(self):
        if self.node is None:
            self.message = "Parameter {0} is not valid for reason {1}".format(self.parameter, self.reason)
        else:
            self.message = "Parameter {0} in node {1} is not valid for reason {2}".format(
                self.parameter, self.node, self.reason
            )
        super().__init__(self.message)


class UnhandledValidationException(Exception):
    """Exception raised when an unhandled exception occurs during plugin validation"""

    def __init__(self, node=None):
        self.node = node
        if self.node is None:
            self.message = "Unhandled exception in node {0}".format(self.node)
        else:
            self.message = "Unhandled exception in Plugin Args"
        super().__init__(self.message)


class InvalidRunPeriodException(Exception):
    """Exception raised when an invalid run period is found"""

    def __init__(self, reason=None):
        if reason:
            message = "The run period provided is not valid because {0}".format(reason)
        else:
            message = "The run period provided is not valid"
        super().__init__(message)


class InvalidSpatialAggException(Exception):
    """Exception raised when invalid spatial aggregation paramaters are provided"""

    def __init__(self, param=None, reason=None):
        if param and reason:
            message = "The spatial aggregation parameter {0} is invalid because {1}".format(param, reason)
        else:
            message = "The spatial aggregation parameters provided are not valid"
        super().__init__(message)


class InvalidDataInfillSettingsException(Exception):
    """Exception raised when invalid infill settings are found"""

    def __init__(self, reason=None):
        if reason:
            message = "The data infill settings provided are not valid because {0}".format(reason)
        else:
            message = "The data infill settings provided are not valid"
        super().__init__(message)


class InvalidDataException(Exception):
    """Exception raised when the data provided is invalid for analysis"""

    def __init__(self, error_type: InvalidDataTypes, file=None, date=None, value=None, index=None, node=None):
        self.error_type = error_type
        self.file = file
        self.date = date
        self.value = value
        self.index = index
        self.node = node
        self.set_message()

    def set_node(self, node):
        self.node = node
        self.set_message()

    def set_message(self):
        if self.error_type == InvalidDataTypes.UNKNOWN_EXCEPTION:
            self.message = "Data file of type {0} in node {1} contains an invalid entry of {2} in row {3}".format(
                self.file, self.node, self.value, self.index + 1
            )
        elif self.error_type == InvalidDataTypes.UNKNOWN_DATE_FORMAT:
            self.message = "Data file of type {0} in node {1} contains an invalid date of '{2}'".format(
                self.file, self.node, self.date
            )
        elif self.error_type == InvalidDataTypes.MISSING_DATE:
            self.message = "Data file of type {0} in node {1} contains no value for date {2}".format(
                self.file, self.node, self.date.strftime("%d/%m/%Y")
            )
        elif self.error_type == InvalidDataTypes.INVALID_VALUE:
            self.message = "Data file of type {0} in node {1} contains an invalid value of '{2}' for date {3}".format(
                self.file, self.node, self.value, self.date.strftime("%d/%m/%Y")
            )
        elif self.error_type == InvalidDataTypes.NO_CONCURRENT_DATA:
            self.message = "There is no time period in which all required data types are avaliable in node {0}".format(
                self.node
            )

        super().__init__(self.message)
