import json
from typing import List
from core.globals import ResultTypes


class NodeResult:
    def __init__(self, node: str, result_type: ResultTypes, data: List[dict]):
        self.node = node
        self.result_type = result_type
        self.data: List[dict] = data

    def serialize_to_json(self):
        return json.dumps(self.data)

    def convert_data_to_list(self, headers: List[str]) -> List[List]:
        rows = []

        if len(headers) == 0 and len(self.data) > 0:
            headers = list(self.data[0].keys())

        for item in self.data:
            item_row: List = []
            for header in headers:
                if header == "node":
                    item_row.append(self.node)
                    continue

                item_header_value = item[header] if header in item else None
                item_row.append(item_header_value)

            rows.append(item_row)

        return rows
