from typing import List
import unittest

# Import test modules
# General
from tests import io_test
from tests import model_test
from tests import plugin_test
from tests import run_test
from tests import calc_test
from tests import timeseries_test

# Plugins
from tests import general_plugin_test
from tests import summary_plugin_test
from tests import rating_curve_plugin_test
from tests import ambassis_agassizii_test
from tests import mary_river_cod_test
from tests import mary_river_turtles_test
from tests import barramundi_growth_test
from tests import barramundi_juvenile_recruitment_test
from tests import barramundi_year_class_strength_test
from tests import king_threadfin_salmon_test
from tests import banana_prawn_growth_test
from tests import carp_recruitment_test
from tests import low_flow_spawning_fish_test
from tests import offshore_reef_fishery_test
from tests import tandanus_test
from tests import turtle_test
from tests import bass_test
from tests import northern_snake_necked_turtle_test
from tests import fish_barriers_and_connectivity_test
from tests import fish_resilience_test
from tests import lowflow_partial_season_test
from tests import freshes_partial_season_test
from tests import oversupply_partial_season_test
from tests import multiyear_freshes_partial_season_test
from tests import machrophyte_growth_test
from tests import waterhole_test
from tests import waterhole_calibration_test
from tests import waterhole_run_to_empty_test
from tests import spell_analysis_test
from tests import simultaneous_spell_analysis_test
from tests import baseflow_separation_test
from tests import colwells_index_test
from tests import australian_bass_test
from tests.test_context import DataStoreTypes, set_data_store_type

loader = unittest.TestLoader()


def get_core_test_suite():
    core_tests_suite = unittest.TestSuite()

    core_tests_suite.addTest(loader.loadTestsFromModule(io_test))
    core_tests_suite.addTest(loader.loadTestsFromModule(model_test))
    core_tests_suite.addTest(loader.loadTestsFromModule(plugin_test))
    core_tests_suite.addTest(loader.loadTestsFromModule(calc_test))
    core_tests_suite.addTest(loader.loadTestsFromModule(run_test))
    core_tests_suite.addTest(loader.loadTestsFromModule(timeseries_test))

    return core_tests_suite


def get_plugin_tests_suite():
    plugin_tests_suite = unittest.TestSuite()

    plugin_tests_suite.addTest(loader.loadTestsFromModule(general_plugin_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(summary_plugin_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(rating_curve_plugin_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(ambassis_agassizii_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(mary_river_cod_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(mary_river_turtles_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(barramundi_growth_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(barramundi_juvenile_recruitment_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(barramundi_year_class_strength_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(king_threadfin_salmon_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(banana_prawn_growth_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(carp_recruitment_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(low_flow_spawning_fish_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(offshore_reef_fishery_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(tandanus_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(turtle_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(bass_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(northern_snake_necked_turtle_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(fish_barriers_and_connectivity_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(fish_resilience_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(lowflow_partial_season_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(freshes_partial_season_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(oversupply_partial_season_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(multiyear_freshes_partial_season_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(machrophyte_growth_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(waterhole_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(waterhole_calibration_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(waterhole_run_to_empty_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(spell_analysis_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(simultaneous_spell_analysis_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(baseflow_separation_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(colwells_index_test))
    plugin_tests_suite.addTest(loader.loadTestsFromModule(australian_bass_test))

    return plugin_tests_suite


# Initialize a test runner
runner = unittest.TextTestRunner(verbosity=3)

# Run core tests
print("Running core test suite")
runner.run(get_core_test_suite())

# Run plugin tests
data_store_types: List[DataStoreTypes] = [DataStoreTypes.MEMORY, DataStoreTypes.SQLITE]
for data_store_type in data_store_types:
    print("Running plugin test suite with {0} data_store".format(data_store_type))
    set_data_store_type(data_store_type)
    runner.run(get_plugin_tests_suite())
