from typing import List
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from core.timeseries_day import TimeseriesDay
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class fish_barriers_and_connectivity(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Fish Barriers And Connectivity",
            "description": "Score Fish Barriers And Connectivity",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # global_params
        assert_wrapper(node_parameters["global_params"]["swim_speed"] > 0, "swim_speed", "must be > 0")
        assert_wrapper(
            node_parameters["global_params"]["upstream_start_AMTD_km"]
            > node_parameters["global_params"]["downstream_end_AMTD_km"],
            "upstream_start_AMTD_km/downstream_end_AMTD_km",
            "start distance must be > end distance",
        )
        if node_parameters["global_params"]["consider_downstream_movement"]:
            assert_wrapper(
                validation.valid_season(node_parameters["global_params"]["downstream_season"]) is True,
                "downstream_season",
                "must contain valid dates (day/month)",
            )
            node_parameters["global_params"]["downstream_season"] = Season(
                node_parameters["global_params"]["downstream_season"]
            )
        if node_parameters["global_params"]["consider_upstream_movement"]:
            assert_wrapper(
                validation.valid_season(node_parameters["global_params"]["upstream_season"]) is True,
                "upstream_season",
                "must contain valid dates (day/month)",
            )
            node_parameters["global_params"]["upstream_season"] = Season(
                node_parameters["global_params"]["upstream_season"]
            )
        assert_wrapper(
            not (
                not node_parameters["global_params"]["consider_downstream_movement"]
                and not node_parameters["global_params"]["consider_upstream_movement"]
            ),
            "consider_downstream_movement/consider_upstream_movement",
            "must consider at least 1 type of movement",
        )

        # barriers
        assert_wrapper(len(node_parameters["barriers"]) > 1, "barriers", "must have at least two barriers")
        for barrier in node_parameters["barriers"]:
            assert_wrapper(len(barrier["name"]) > 0, "name", "barrier cannot have an empty name")
            assert_wrapper(type(barrier["AMTD_km"]) is int, "AMTD_km", "barrier distance must be a number")
            assert_wrapper(barrier["velocity_params"]["slope"] >= 0, "slope", "must be >= 0")
            assert_wrapper(barrier["velocity_params"]["width_m"] >= 0, "width_m", "must be >= 0")
            assert_wrapper(barrier["velocity_params"]["roughness"] >= 0, "roughness", "must be >= 0")
            assert_wrapper(barrier["downstream_reqs"]["drownout"] >= 0, "drownout", "must be >= 0")
            assert_wrapper(barrier["downstream_reqs"]["dur"] >= 0, "dur", "must be >= 0")
            assert_wrapper(barrier["upstream_reqs"]["drownout"] >= 0, "drownout", "must be >= 0")
            assert_wrapper(barrier["upstream_reqs"]["dur"] >= 0, "dur", "must be >= 0")

    def load_data(self, node):
        data = []
        default_data = NodeDefaultData(node)

        for barrier in node["parameters"]["barriers"]:
            barrier_data = NodeData(node)
            barrier_data.flow = barrier_data.try_import_data(barrier["data"], TimeseriesDataTypes.FLOW)
            data.append(barrier_data)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        downstream_season_index_list, upstream_season_index_list = [], []
        if node["global_params"]["consider_downstream_movement"]:
            downstream_season_index_list = ts.get_season_index_list(node["global_params"]["downstream_season"])
        if node["global_params"]["consider_upstream_movement"]:
            upstream_season_index_list = ts.get_season_index_list(node["global_params"]["upstream_season"])

        for index, day in enumerate(ts.data):
            day.init_result_value_keys(
                [
                    "downstream_in_season",
                    "downstream_impassable_barrier",
                    "upstream_in_season",
                    "upstream_impassable_barrier",
                ]
            )

            if index in downstream_season_index_list:
                day.set_result_value("downstream_in_season", 1)
                downstream_success, downstream_impassable_barrier = self.check_downstream_passage(node, ts.data[index:])
                day.set_result_value("downstream_success", downstream_success)
                day.set_result_value("downstream_impassable_barrier", downstream_impassable_barrier)
            else:
                day.set_result_value("downstream_in_season", 0)
                day.set_result_value("downstream_success", None)

            if index in upstream_season_index_list:
                day.set_result_value("upstream_in_season", 1)
                upstream_success, upstream_impassable_barrier = self.check_upstream_passage(node, ts.data[index:])
                day.set_result_value("upstream_success", upstream_success)
                day.set_result_value("upstream_impassable_barrier", upstream_impassable_barrier)
            else:
                day.set_result_value("upstream_in_season", 0)
                day.set_result_value("upstream_success", None)

        return ts

    def check_downstream_passage(self, node, ts: List[TimeseriesDay]):
        fish_pos = node["global_params"]["upstream_start_AMTD_km"]
        fish_end_pos = node["global_params"]["downstream_end_AMTD_km"]
        swim_speed = node["global_params"]["swim_speed"]
        barriers = node["barriers"]

        skip_days = 0

        for index, day in enumerate(ts):
            # check if we need to skip the day to account for barrier transverse
            if skip_days > 0:
                skip_days -= 1
                continue

            # get the surrounding barriers to our current position
            closest_upstream_barrier_index = self.get_upstream_barrier(fish_pos, barriers)
            closest_downstream_barrier_index = self.get_downstream_barrier(fish_pos, barriers, fish_end_pos)
            upstream_barrier = barriers[closest_upstream_barrier_index]

            # check if fish has reached a barrier from the previous day
            if fish_pos == upstream_barrier["AMTD_km"]:
                # Transverse barrier
                for barrier_day in ts[index : index + upstream_barrier["downstream_reqs"]["dur"]]:
                    if (
                        barrier_day.get_value(str(closest_upstream_barrier_index) + "_flow")
                        < upstream_barrier["downstream_reqs"]["drownout"]
                    ):
                        # No Success - Couldn't pass the barrier
                        return 0, upstream_barrier["name"]
                # Move forwards to account for transversing the barrier
                skip_days = upstream_barrier["downstream_reqs"]["dur"] - 1
                # Need to slightly reduce the fish pos so we don't end up here again
                fish_pos -= 0.0000001
                continue

            # get the velocity
            velocity = day.get_calculated_value(str(closest_upstream_barrier_index) + "_vel")
            net_velocity = velocity + swim_speed

            # calculate the swim distance
            swim_distance = net_velocity * 86.4

            # Get the new fish position and check if we reached a barrier
            if closest_downstream_barrier_index is None:
                # We are past the final barrier
                if (fish_pos - swim_distance) <= fish_end_pos:
                    return 1, None  # Success - reached the end of the trip
                else:
                    fish_pos -= swim_distance
            else:
                downstream_barrier = barriers[closest_downstream_barrier_index]
                if (fish_pos - swim_distance) <= downstream_barrier["AMTD_km"]:
                    fish_pos = downstream_barrier["AMTD_km"]
                else:
                    fish_pos -= swim_distance

    def check_upstream_passage(self, node, ts: List[TimeseriesDay]):
        fish_pos = node["global_params"]["downstream_end_AMTD_km"]
        fish_end_pos = node["global_params"]["upstream_start_AMTD_km"]
        swim_speed = node["global_params"]["swim_speed"]
        barriers = node["barriers"]

        skip_days = 0

        for index, day in enumerate(ts):
            # check if we need to skip the day to account for barrier transverse
            if skip_days > 0:
                skip_days -= 1
                continue

            # get the surrounding barriers to our current position
            data_barrier_index = self.get_upstream_barrier(fish_pos, barriers)
            target_barrier_index = self.get_upstream_barrier_in_range(fish_pos, barriers, fish_end_pos)

            # check if fish has reached a barrier from the previous day
            if target_barrier_index is not None and fish_pos == barriers[target_barrier_index]["AMTD_km"]:
                upstream_barrier = barriers[target_barrier_index]
                # Transverse barrier
                for barrier_day in ts[index : index + upstream_barrier["upstream_reqs"]["dur"]]:
                    if (
                        barrier_day.get_value(str(target_barrier_index) + "_flow")
                        < upstream_barrier["upstream_reqs"]["drownout"]
                    ):
                        # No Success - Couldn't pass the barrier
                        return 0, upstream_barrier["name"]
                # Move forwards to account for transversing the barrier
                skip_days = upstream_barrier["upstream_reqs"]["dur"] - 1
                # Need to slightly increase the fish pos so we don't end up here again
                fish_pos += 0.0000001
                continue

            # get the velocity
            velocity = day.get_calculated_value(str(data_barrier_index) + "_vel")
            net_velocity = velocity - swim_speed
            if net_velocity < 0:  # If they can't move upstream they move to the side and stay still
                net_velocity = 0

            # calculate the swim distance
            swim_distance = net_velocity * 86.4

            # Get the new fish position and check if we reached a barrier
            if target_barrier_index is None:
                # We are past the final barrier
                if (fish_pos + swim_distance) >= fish_end_pos:
                    return 1, None  # Success - reached the end of the trip
                else:
                    fish_pos += swim_distance
            else:
                upstream_barrier = barriers[target_barrier_index]
                if (fish_pos + swim_distance) >= upstream_barrier["AMTD_km"]:
                    fish_pos = upstream_barrier["AMTD_km"]
                else:
                    fish_pos += swim_distance

    def get_downstream_barrier(self, pos, barriers, end_pos):
        res = None
        for index, barrier in enumerate(reversed(barriers)):
            if pos > barrier["AMTD_km"] and barrier["AMTD_km"] >= end_pos:
                res = index
        if res is None:
            return None
        return len(barriers) - 1 - res

    def get_upstream_barrier(self, pos, barriers):
        res = 0
        for index, barrier in enumerate(barriers):
            if pos <= barrier["AMTD_km"]:
                res = index
        return res

    def get_upstream_barrier_in_range(self, pos, barriers, end_pos):
        res = None
        for index, barrier in enumerate(barriers):
            if pos <= barrier["AMTD_km"] and barrier["AMTD_km"] <= end_pos:
                res = index
        return res

    def run(self, node: Node):
        # sort barriers - descending order by AMTD_km
        node.parameters["barriers"] = sorted(node.parameters["barriers"], key=lambda d: d["AMTD_km"], reverse=True)

        ts = Timeseries()
        result = PluginResult(node.name, ts)

        # get velocity at each barrier and add to list
        for index, barrier in enumerate(node.parameters["barriers"]):
            barrier_days = node.data[index].parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, str(index) + "_flow")
            for day in barrier_days:
                day.set_velocity_from_flow(
                    str(index) + "_flow",
                    str(index) + "_vel",
                    barrier["velocity_params"]["slope"],
                    barrier["velocity_params"]["width_m"],
                    barrier["velocity_params"]["roughness"],
                )
            ts.add_timeseries_days(barrier_days)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "downstream_in_season",
                "downstream_impassable_barrier",
                "downstream_success",
                "upstream_in_season",
                "upstream_impassable_barrier",
                "upstream_success",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "downstream_in_season",
                "downstream_impassable_barrier",
                "downstream_success",
                "upstream_in_season",
                "upstream_impassable_barrier",
                "upstream_success",
            ],
        )
