from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
import math


class banana_prawn_growth(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Banana Prawn Growth",
            "description": "Score Banana Prawn Growth growth",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # flow
        assert_wrapper(node_parameters["flow"]["flow0_dur"] >= 0, "flow0_dur", "must be >= 0")
        assert_wrapper(node_parameters["flow"]["flow4_dur"] >= 0, "flow4_dur", "must be >= 0")

        # constants
        assert_wrapper(
            validation.dict_contains_keys(
                node_parameters["constants"], ["days", "length1", "L", "a", "b", "kc", "kd", "ke", "kf", "kg"], []
            ),
            "constants",
            "must contain all required constants",
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        constants = node["constants"]

        # Calculate daily growth
        for index, day in enumerate(ts.data):
            if index + 1 < node["flow"]["flow0_dur"]:
                day.set_calculated_value("flow0", None)
            else:
                day.set_calculated_value(
                    "flow0",
                    ts.get_data_type_value_sum(
                        TimeseriesDataTypes.FLOW, ts.data[max(0, index + 1 - node["flow"]["flow0_dur"]) : index + 1]
                    ),
                )

            if index + 1 - node["flow"]["flow0_dur"] < node["flow"]["flow4_dur"]:
                day.set_calculated_value("flow4", None)
            else:
                day.set_calculated_value(
                    "flow4",
                    ts.get_data_type_value_sum(
                        TimeseriesDataTypes.FLOW,
                        ts.data[
                            max(0, index + 1 - node["flow"]["flow0_dur"] - node["flow"]["flow4_dur"]) : index
                            + 1
                            - node["flow"]["flow0_dur"]
                        ],
                    ),
                )

            # Calc prawn growth
            if (
                day.get_calculated_value("flow0") is not None
                and day.get_calculated_value("flow4") is not None
                and day.get_value(TimeseriesDataTypes.TEMPERATURE) is not None
            ):
                try:
                    length2 = constants["length1"] + (constants["L"] - constants["length1"]) * (
                        1
                        - math.exp(
                            -(
                                constants["a"]
                                + constants["kc"] * day.get_value(TimeseriesDataTypes.TEMPERATURE)
                                + constants["kd"] * math.pow(day.get_value(TimeseriesDataTypes.TEMPERATURE), 2)
                                + constants["ke"] * day.get_calculated_value("flow0")
                                + constants["kf"] * math.pow(day.get_calculated_value("flow0"), 2)
                                + constants["kg"] * day.get_calculated_value("flow4")
                            )
                            * constants["days"]
                            + constants["b"]
                        )
                    )
                    growth = (length2 - constants["length1"]) / 42
                    if growth < 0:
                        day.set_result_value("growth", 0)
                    else:
                        day.set_result_value("growth", growth)
                except OverflowError:
                    day.set_result_value("growth", None)
            else:
                day.set_result_value("growth", None)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_key = "growth"

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(["date", "growth"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["date", "growth"])
