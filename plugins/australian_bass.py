from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class australian_bass(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Australian Bass",
            "description": "Score Australian Bass spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True,
            "season",
            "season must contain valid dates (day/month)",
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # migration
        assert_wrapper(
            validation.valid_season(node_parameters["migration"]["season"]) is True,
            "migration",
            "season must contain valid dates (day/month)",
        )
        node_parameters["migration"]["season"] = Season(node_parameters["migration"]["season"])

        assert_wrapper(
            node_parameters["migration"]["flow_threshold"] >= 0,
            "flow_threshold",
            "must be >= 0",
        )
        assert_wrapper(
            validation.is_num(node_parameters["migration"]["duration"], int)
            and node_parameters["migration"]["duration"] >= 0,
            "duration",
            "must be int >= 0",
        )

        # spawning
        assert_wrapper(
            validation.valid_season(node_parameters["spawning"]["season"]) is True,
            "spawning",
            "season must contain valid dates (day/month)",
        )
        node_parameters["spawning"]["season"] = Season(node_parameters["spawning"]["season"])
        assert_wrapper(
            validation.valid_range(node_parameters["spawning"]["temperature_range"]),
            "spawning",
            "temperature_range is invalid",
        )
        assert_wrapper(
            validation.valid_range(node_parameters["spawning"]["salinity_range"]),
            "spawning",
            "salinity_range is invalid",
        )

        # hatching
        assert_wrapper(
            validation.valid_range(node_parameters["hatching"]["temperature_range"]),
            "hatching",
            "temperature_range is invalid",
        )
        assert_wrapper(
            validation.valid_range(node_parameters["hatching"]["salinity_range"]),
            "hatching",
            "salinity_range is invalid",
        )
        assert_wrapper(
            validation.valid_range(node_parameters["hatching"]["time_since_spawning_range"], int),
            "hatching",
            "time_since_spawning_range is invalid",
        )

        # recruitment
        assert_wrapper(
            validation.is_num(node_parameters["recruitment"]["time_since_hatching"], int)
            and node_parameters["recruitment"]["time_since_hatching"] >= 0,
            "time_since_hatching",
            "must be int >= 0",
        )
        assert_wrapper(
            node_parameters["recruitment"]["flow_threshold"] >= 0,
            "flow_threshold",
            "must be >= 0",
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        data.salinity = data.try_import_data(node["data"], TimeseriesDataTypes.SALINITY)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        seasonal_ts = ts.split_into_seasons(node["season"], True)

        for data in seasonal_ts:
            migration_season_index_list = ts.get_season_index_list(node["migration"]["season"], True, data)
            spawning_season_index_list = ts.get_season_index_list(node["spawning"]["season"], True, data)

            for index, day in enumerate(data):
                day.init_result_value_keys(
                    [
                        "migration_flows_provided",
                        "spawning_success",
                        "hatching_success",
                        "recruitment_success",
                        "failure_reason",
                    ]
                )
                day.set_result_value("success", 0)

                # Check migration
                migration_success = False
                if index in migration_season_index_list:
                    if day.get_value(TimeseriesDataTypes.FLOW) >= node["migration"]["flow_threshold"]:
                        num_days_to_check = node["migration"]["duration"]
                        flow_lasts_num_days = True

                        if (index + num_days_to_check) >= len(data):
                            day.set_result_value("migration_flows_provided", 0)
                            day.set_result_value("failure_reason", "Not enough days to check migration event")
                            continue

                        for check_index in range(index, index + num_days_to_check):
                            if (
                                data[check_index + 1].get_value(TimeseriesDataTypes.FLOW)
                                < node["migration"]["flow_threshold"]
                            ):
                                flow_lasts_num_days = False
                                break

                        if not flow_lasts_num_days:
                            day.set_result_value("failure_reason", "Migration flow does not last enough days")
                        else:
                            migration_success = True

                    else:
                        day.set_result_value("failure_reason", "Insufficient flows for migration")
                else:
                    day.set_result_value("failure_reason", "Not in migration season")

                if not migration_success:
                    day.set_result_value("migration_flows_provided", 0)
                    continue
                day.set_result_value("migration_flows_provided", 1)

                spawning_index = check_index + 2

                # Check spawning
                spawning_success = False
                any_spawning_temperature_success = False
                any_spawning_salinity_success = False
                while spawning_index in spawning_season_index_list:
                    spawning_temperature_success = False
                    spawning_salinity_success = False

                    if calcs.is_value_in_range(
                        node["spawning"]["temperature_range"]["min"],
                        data[spawning_index].get_value(TimeseriesDataTypes.TEMPERATURE),
                        node["spawning"]["temperature_range"]["max"],
                    ):
                        spawning_temperature_success = True
                        any_spawning_temperature_success = True

                    if calcs.is_value_in_range(
                        node["spawning"]["salinity_range"]["min"],
                        data[spawning_index].get_value(TimeseriesDataTypes.SALINITY),
                        node["spawning"]["salinity_range"]["max"],
                    ):
                        spawning_salinity_success = True
                        any_spawning_salinity_success = True

                    if spawning_temperature_success and spawning_salinity_success:
                        spawning_success = True
                        break

                    spawning_index += 1

                if not spawning_success:
                    day.set_result_value("spawning_success", 0)
                    if any_spawning_salinity_success and any_spawning_temperature_success:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient spawning day found in season - Temperature and salinity range both met, but not simultaneously",
                        )
                    elif not any_spawning_temperature_success:
                        day.set_result_value(
                            "failure_reason", "No sufficient spawning day found in season - Temperature range never met"
                        )
                    elif not any_spawning_salinity_success:
                        day.set_result_value(
                            "failure_reason", "No sufficient spawning day found in season - Salinity range never met"
                        )
                    else:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient spawning day found in season - Neither temperature or salinity range met at any time",
                        )
                    continue
                day.set_result_value("spawning_success", 1)

                # Check Hatching
                hatching_index = spawning_index + node["hatching"]["time_since_spawning_range"]["min"]

                hatching_success = False
                any_hatching_temperature_success = False
                any_hatching_salinity_success = False
                while hatching_index <= (spawning_index + node["hatching"]["time_since_spawning_range"]["max"]):
                    hatching_temperature_success = False
                    hatching_salinity_success = False

                    if calcs.is_value_in_range(
                        node["hatching"]["temperature_range"]["min"],
                        data[spawning_index].get_value(TimeseriesDataTypes.TEMPERATURE),
                        node["hatching"]["temperature_range"]["max"],
                    ):
                        hatching_temperature_success = True
                        any_hatching_temperature_success = True

                    if calcs.is_value_in_range(
                        node["hatching"]["salinity_range"]["min"],
                        data[spawning_index].get_value(TimeseriesDataTypes.SALINITY),
                        node["hatching"]["salinity_range"]["max"],
                    ):
                        hatching_salinity_success = True
                        any_hatching_salinity_success = True

                    if hatching_temperature_success and hatching_salinity_success:
                        hatching_success = True
                        break

                    hatching_index += 1

                if not hatching_success:
                    day.set_result_value("hatching_success", 0)
                    if any_hatching_salinity_success and any_hatching_temperature_success:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient hatching day found in hatching window - Temperature and salinity range both met, but not simultaneously",
                        )
                    elif any_hatching_temperature_success:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient hatching day found in hatching window - Temperature range never met",
                        )
                    elif any_hatching_salinity_success:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient hatching day found in hatching window - Salinity range never met",
                        )
                    else:
                        day.set_result_value(
                            "failure_reason",
                            "No sufficient hatching day found in hatching window - Neither temperature or salinity range met at any time",
                        )
                    continue
                day.set_result_value("hatching_success", 1)

                # Check recruitment
                recruitment_index = hatching_index + node["recruitment"]["time_since_hatching"]
                recruitment_success = False

                if recruitment_index >= len(data):
                    day.set_result_value("recruitment_success", 0)
                    day.set_result_value("failure_reason", "No days to check recruitment success")
                    continue

                while recruitment_index < len(data):
                    if (
                        data[recruitment_index].get_value(TimeseriesDataTypes.FLOW)
                        <= node["recruitment"]["flow_threshold"]
                    ):
                        recruitment_success = True
                        break
                    recruitment_index += 1

                if not recruitment_success:
                    day.set_result_value("recruitment_success", 0)
                    day.set_result_value(
                        "failure_reason", "No day in remainder of year meets recruitment flow requirements"
                    )
                    continue
                day.set_result_value("recruitment_success", 1)

                day.set_result_value("success", 1)

        return seasonal_ts

    def ts_aggregation(self, node, ts):
        yearly_results = []

        for year_ts in ts:
            year_success = 1 if sum(map(lambda d: d.get_result_value("success"), year_ts)) > 0 else 0
            result = {"year": year_ts[0].date.year, "success": year_success}
            yearly_results.append(result)

        return yearly_results

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Aggregate results to each year
        result.ts_agg = self.ts_aggregation(node.parameters, result.ts_gen)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(
            [
                "date",
                "migration_flows_provided",
                "spawning_success",
                "hatching_success",
                "recruitment_success",
                "success",
                "failure_reason",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        return [result_daily, result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "migration_flows_provided",
                "spawning_success",
                "hatching_success",
                "recruitment_success",
                "success",
                "failure_reason",
            ],
        )

        write_result(ResultTypes.YEARLY_INTERMEDIATE, ["node", "year", "success"])
