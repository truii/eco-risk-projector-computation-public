from typing import List
from core.data_store import DataStore
from core.globals import OtherDataTypes, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *
import math


class freshes_partial_season(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Freshes Partial Season",
            "description": "Score freshes occurances from flow data",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Season
        assert_wrapper(
            node_parameters["start_month"] >= 1 and node_parameters["start_month"] <= 12,
            "start_month",
            "must be between 1 and 12",
        )
        assert_wrapper(len(node_parameters["season"]) == 12, "season", "must contain a value for all 12 months")
        node_parameters["season"].sort()
        count = 1
        for season in node_parameters["season"]:
            assert_wrapper(season[0] == count, "season", "must be in ascending order")
            count += 1

        req_vars = ["mag", "dur", "ind", "count"]
        req_climates = ["Drought", "Very Dry", "Dry", "Average", "Wet"]
        for climate in req_climates:
            assert_wrapper(climate in node_parameters["rules"].keys(), "rules", "must only contain valid climates")

        # Lag
        assert_wrapper(validation.is_num(node_parameters["lag"], int), "lag", "must be an integer")

        # Rules
        for climate, rule in node_parameters["rules"].items():
            assert_wrapper(validation.valid_rule(rule, req_vars), "rules", "must contain only valid rules")

        # Partial Table
        for key in req_vars:
            assert_wrapper(
                validation.valid_curve(node_parameters["partial_tables"][key]) is True,
                "partial_tables",
                "must contain only valid curves",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.COMPLIANCE in node["data"]:
            data.compliance = data.try_import_data(node["data"], TimeseriesDataTypes.COMPLIANCE)

        data.climate = data.try_import_data(node["data"], OtherDataTypes.CLIMATE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Split by reporting year
        date_season = Season(start_month=node["start_month"])
        season_split_ts = ts.split_into_seasons(date_season, True)
        season_result_ts = []

        for season in season_split_ts:
            climates = ts.split_by_value_key(OtherDataTypes.CLIMATE, season)
            season_result_ts.append([])

            for climate_days in climates:
                climate = climate_days[0].get_value(OtherDataTypes.CLIMATE)

                # Get the climate specific rule
                rule = node["rules"][climate]

                # Get daily mag and comp score
                for day in climate_days:
                    day_score = day.get_value(TimeseriesDataTypes.FLOW) / rule["mag"]
                    day.set_result_value(
                        "mag_score", calcs.get_curve_value(day_score * 100, node["partial_tables"]["mag"]) / 100
                    )
                    if day.has_value(TimeseriesDataTypes.COMPLIANCE):
                        day.set_result_value(
                            "comp_score",
                            day.get_value(TimeseriesDataTypes.COMPLIANCE) / rule["mag"]
                            if day.get_value(TimeseriesDataTypes.COMPLIANCE) < rule["mag"]
                            else 1,
                        )

                    season_score = calcs.get_curve_value(day.date.month, node["season"]) / 100
                    day.set_result_value("season_score", season_score)
                    day.set_result_value(
                        "mag_score", day.get_result_value("mag_score") * day.get_result_value("season_score")
                    )
                    if day.has_value("comp_score"):
                        day.set_result_value(
                            "comp_score", day.set_result_value("comp_score") * day.set_result_value("season_score")
                        )

                # Get event oppurtunities
                events = self.get_event_opportunities("mag_score", climate_days)
                season_result_ts[-1].append({"data": climate_days, "events": events, "climate": climate})

        return season_result_ts

    def ts_aggregation(self, node, ts: Timeseries, seasonal_result_ts, should_calculate_compliance, node_name):
        seasonal_score = []
        for season in seasonal_result_ts:
            # Append season info
            seasonal_score.append({})
            seasonal_score[-1]["year"] = season[0]["data"][0].date.year
            seasonal_score[-1]["climates"] = []
            seasonal_score[-1]["event_log"] = []

            # Score each climate
            for climate_info in season:
                climate = climate_info["climate"]
                days = climate_info["data"]
                seasonal_score[-1]["climates"].append({})

                # Append climate info
                rule = node["rules"][climate]
                current_climate = seasonal_score[-1]["climates"][-1]
                current_climate["climate"] = climate
                current_climate["rule"] = rule
                current_climate["prop"] = len(days) / sum((len(climate_days["data"])) for climate_days in season)

                # Get best event from event ops
                events = []
                for event_opp in climate_info["events"]:
                    events.append(
                        self.get_best_event(
                            ts,
                            "mag_score",
                            calcs.round_to_positive_int(rule["dur"] * current_climate["prop"]),
                            event_opp,
                        )
                    )

                # Score each event
                scores = []
                for index, event in enumerate(events):
                    scores.append({})
                    scores[-1]["dur"] = (
                        max(len(event) / calcs.round_to_positive_int(rule["dur"] * current_climate["prop"]), 1)
                        if len(event) > 0
                        else 0
                    )
                    scores[-1]["mag"] = ts.get_data_type_value_mean("mag_score", event) or 0
                    scores[-1]["start"] = event[0]
                    scores[-1]["end"] = event[-1]

                    if should_calculate_compliance:
                        scores[-1]["compliance"] = ts.get_data_type_value_mean("comp_score", event) or 0

                    scores[-1]["summary"] = scores[-1]["dur"] * scores[-1]["mag"]

                # Remove illegal events (when independence is too close)
                min_ind = self.get_min_allowed_independence(rule["ind"], node["partial_tables"]["ind"])

                check_pos = 0
                while check_pos < len(scores):
                    if (check_pos + 1) < len(scores):
                        score = scores[check_pos]
                        next_score = scores[check_pos + 1]
                        diff = ts.get_number_of_days_between_days(next_score["start"], score["end"])
                        if diff < min_ind:
                            if score["summary"] > next_score["summary"]:
                                scores.pop(check_pos + 1)
                                check_pos = 0
                                continue
                            else:
                                scores.pop(check_pos)
                                check_pos = 0
                                continue
                    check_pos += 1

                target_count = math.ceil(rule["count"] * current_climate["prop"])

                # Take the top N scores (Where N is the desired count)
                if len(scores) > target_count:
                    score_index_pairs = [{"index": i, "score": s["summary"]} for i, s in enumerate(scores)]

                    score_index_pairs_sorted = sorted(score_index_pairs, key=lambda k: k["score"], reverse=True)

                    score_indexes_to_remove = map(lambda s: s["index"], score_index_pairs_sorted[target_count:])

                    for index in sorted(score_indexes_to_remove, reverse=True):
                        del scores[index]

                # Add scores to event log
                event_log = []
                for score in scores:
                    event_log.append(
                        {
                            "start_date": score["start"].format_date(),
                            "end_date": score["end"].format_date(),
                            "duration": ts.get_number_of_days_between_days(score["end"], score["start"]) + 1,
                        }
                    )
                seasonal_score[-1]["event_log"].extend(event_log)

                # Score independence
                scores_ind = []
                for index, score in enumerate(scores):
                    if (index + 1) < len(scores):
                        next_score = scores[index + 1]
                        diff = ts.get_number_of_days_between_days(next_score["start"], score["end"])
                        if diff >= min_ind:
                            score["ind"] = diff
                            scores_ind.append(score)
                    else:
                        score["ind"] = rule["ind"]
                        scores_ind.append(score)

                # Get ind score
                if len(scores_ind) > 1:
                    diffs = list(map(lambda s: s["ind"], scores_ind))
                    score = (sum(diffs) / len(diffs)) / rule["ind"]
                    ind_score = calcs.get_curve_value(score * 100, node["partial_tables"]["ind"]) / 100
                elif len(scores_ind) == 1:
                    ind_score = 1
                else:
                    ind_score = 0

                # Score climate
                current_climate["dur"] = calcs.get_dict_list_mean(scores, "dur") or 0
                current_climate["mag"] = calcs.get_dict_list_mean(scores, "mag") or 0
                current_climate["ind"] = ind_score
                current_climate["count"] = (
                    calcs.get_curve_value((len(scores) / target_count) * 100, node["partial_tables"]["count"]) / 100
                )

                if should_calculate_compliance:
                    current_climate["compliance"] = calcs.get_dict_list_mean(scores, "compliance") or 0

                if len(scores) > 0:
                    current_climate["summary"] = (
                        current_climate["dur"]
                        * current_climate["mag"]
                        * current_climate["count"]
                        * current_climate["ind"]
                    )
                else:
                    current_climate["summary"] = None

            # Score season
            seasonal_score[-1]["dur"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "dur", "prop")
            seasonal_score[-1]["mag"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "mag", "prop")
            seasonal_score[-1]["count"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "count", "prop")
            seasonal_score[-1]["ind"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "ind", "prop")
            if should_calculate_compliance:
                seasonal_score[-1]["compliance"] = calcs.get_weighted_average(
                    seasonal_score[-1]["climates"], "compliance", "prop"
                )
            else:
                seasonal_score[-1]["compliance"] = None
            seasonal_score[-1]["summary"] = (
                seasonal_score[-1]["dur"]
                * seasonal_score[-1]["mag"]
                * seasonal_score[-1]["count"]
                * seasonal_score[-1]["ind"]
            )

        return seasonal_score

    def get_min_allowed_independence(self, target_independence, curve):
        # Get the x value that related to a y score of 0 (minimum x % allowed)
        for datapoint in curve:
            if datapoint[1] == 0:
                returnval = datapoint[0]

        # Get the target_independence adjusted by this value
        val = math.ceil(((returnval / 100) * target_independence))

        # Independence can't be < 1, so return the max of 1 and val
        return max(1, val)

    def get_event_opportunities(self, key, data):
        split_data = [[]]
        in_event = False
        detect_peak = {"prev": None, "next": None, "peaked": False}
        for index, day in enumerate(data):
            try:
                detect_peak["prev"] = data[index - 1]
                detect_peak["next"] = data[index + 1]
            except Exception:
                detect_peak["peaked"] = False
            if in_event:
                if day.get_value_from_key(key) == 1:
                    detect_peak["peaked"] = True
                if (
                    detect_peak["peaked"]
                    and detect_peak["prev"].get_value_from_key(key) > day.get_value_from_key(key)
                    and detect_peak["next"].get_value_from_key(key) > day.get_value_from_key(key)
                ):
                    split_data.append([])
                    detect_peak["peaked"] = False
                if day.get_value_from_key(key) > 0:
                    split_data[-1].append(day)
                else:
                    split_data.append([])
                    in_event = False
            else:
                if day.get_value_from_key(key) > 0:
                    split_data[-1].append(day)
                    in_event = True

        return list(filter(lambda e: len(e) != 0, split_data))

    def get_best_event(self, ts: Timeseries, key, event_duration, data):
        event = data[:event_duration]
        if len(data) <= event_duration:
            return data
        for index, day in enumerate(data):
            try:
                test_event = data[index : (index + event_duration)]
                if len(test_event) < event_duration:
                    continue

                if ts.get_data_type_value_mean(key, test_event, 10) > ts.get_data_type_value_mean(key, event, 10):
                    event = test_event
            except Exception:
                continue

        return event

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Setup ts for analysis
        climate_ts_days = node.data.parse_csv_to_timeseries_days(
            OtherDataTypes.CLIMATE, is_number_values=False, infill_with_previous_value=True
        )
        ts.add_timeseries_days(climate_ts_days)

        # Apply lag if we have compliance
        if ts.has_data_type(TimeseriesDataTypes.COMPLIANCE):
            ts.apply_lag_to_data_type(TimeseriesDataTypes.COMPLIANCE, node.parameters["lag"])

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get summary results
        result.ts_agg = self.ts_aggregation(node.parameters, ts, result.ts_gen, node.data.compliance != None, node.name)

        # Set event log
        for season in result.ts_agg:
            result.event_log.extend(season["event_log"])

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["summary"]), result.ts_agg))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        result_events = NodeResult(result.node_name, ResultTypes.EVENTS_INTERMEDIATE, result.event_log)

        return [result_yearly, result_events]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "mag", "dur", "count", "ind", "compliance", "summary"],
        )

        write_result(ResultTypes.EVENTS_INTERMEDIATE, ["node", "start_date", "end_date", "duration"])
