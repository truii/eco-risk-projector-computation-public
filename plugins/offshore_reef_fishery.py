from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *
import math


class offshore_reef_fishery(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Offshore Reef Fishery",
            "description": "Score Offshore Reef Fishery catch rate",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # constants
        assert_wrapper(
            validation.dict_contains_keys(node_parameters["constants"], ["a", "b", "c", "d", "f", "m", "g"], []),
            "constants",
            "must contain all constants",
        )

        # flow_parameters
        assert_wrapper(
            node_parameters["flow_parameters"]["high_flow_year_threshold"] >= 0,
            "high_flow_year_threshold",
            "must be >= 0",
        )
        assert_wrapper(
            validation.valid_season(node_parameters["flow_parameters"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["flow_parameters"]["season"] = Season(node_parameters["flow_parameters"]["season"])

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Split ts by season
        seasonal_split_days = ts.split_into_seasons(node["flow_parameters"]["season"])

        yearly_result = []

        # Generate results for each year
        for season_days in seasonal_split_days:
            previous_result = (
                {"catch_rate": 0, "high_flow_threshold_met": 0} if len(yearly_result) == 0 else yearly_result[-1]
            )

            season_sum = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, season_days)
            constants = node["constants"]

            if season_sum >= node["flow_parameters"]["high_flow_year_threshold"]:
                highflow = 1
            else:
                highflow = 0

            if previous_result["high_flow_threshold_met"] == 1:
                catch_rate = (previous_result["catch_rate"] * 0.6473) + 40969
            else:
                catch_rate = (
                    constants["f"]
                    / (
                        constants["a"]
                        + constants["b"] * math.exp(-((constants["m"] * season_sum) + constants["c"]) / constants["g"])
                    )
                    + constants["d"]
                )

            yearly_result.append(
                {
                    "year": season_days[0].date.year,
                    "catch_rate": catch_rate,
                    "high_flow_threshold_met": highflow,
                }
            )

        return yearly_result

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["catch_rate"]), result.ts_gen))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_gen)

        return [result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "catch_rate", "high_flow_threshold_met"],
        )
