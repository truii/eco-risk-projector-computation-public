import copy
from typing import List
from core.data_store import DataStore
from core.globals import OtherDataTypes, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *
import math


class multiyear_freshes_partial_season(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Multiyear Freshes Partial Season",
            "description": "Score multiyear freshes occurances from flow data",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Season
        assert_wrapper(
            node_parameters["start_month"] >= 1 and node_parameters["start_month"] <= 12,
            "start_month",
            "must be between 1 and 12",
        )
        assert_wrapper(len(node_parameters["season"]) == 12, "season", "must contain a value for all 12 months")
        node_parameters["season"].sort()
        count = 1
        for season in node_parameters["season"]:
            assert_wrapper(season[0] == count, "season", "must be in ascending order")
            count += 1

        # Lag
        assert_wrapper(validation.is_num(node_parameters["lag"], int), "lag", "must be an integer")

        # Multiyear
        assert_wrapper(node_parameters["return_period"] > 0, "return_period", "must be > 0")
        assert_wrapper(node_parameters["max_time"] > 0, "max_time", "must be > 0")
        assert_wrapper(
            validation.valid_curve(node_parameters["partial_tables"]["max_time"]) is True,
            "partial_tables",
            "must contain max_time partial table",
        )

        req_vars = ["mag", "dur", "ind", "count"]
        req_climates = ["Drought", "Very Dry", "Dry", "Average", "Wet"]
        for climate in req_climates:
            assert_wrapper(climate in node_parameters["rules"].keys(), "rules", "must only contain valid climates")

        # Rules
        for climate, rule in node_parameters["rules"].items():
            assert_wrapper(validation.valid_rule(rule, req_vars), "rules", "must contain only valid rules")

        # Partial Table
        for key in req_vars:
            assert_wrapper(
                validation.valid_curve(node_parameters["partial_tables"][key]) is True,
                "partial_tables",
                "must contain only valid curves",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.COMPLIANCE in node["data"]:
            data.compliance = data.try_import_data(node["data"], TimeseriesDataTypes.COMPLIANCE)

        data.climate = data.try_import_data(node["data"], OtherDataTypes.CLIMATE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Split by reporting year
        date_season = Season(start_month=node["start_month"])
        season_split_ts = ts.split_into_seasons(date_season, True)
        season_result_ts = []

        # combine years
        extended_ts = []
        for index, season in enumerate(season_split_ts):
            extended_ts.append([])

            for season_num in range(index - node["return_period"] + 1, index + 1):
                if season_num >= 0:
                    extended_ts[-1].extend(copy.deepcopy(season_split_ts[season_num]))

        for year_list in extended_ts:
            climates = ts.split_by_value_key(OtherDataTypes.CLIMATE, year_list)
            season_result_ts.append([])

            # Get main climate in period
            climates_length_calculation = {}
            for climate_days in climates:
                climate = climate_days[0].get_value(OtherDataTypes.CLIMATE)

                if climate not in climates_length_calculation:
                    climates_length_calculation[climate] = 0

                climates_length_calculation[climate] += len(climate_days)

            main_climate = calcs.get_dict_key_with_max_value(climates_length_calculation)

            for climate_days in climates:
                # Get the climate specific rule
                rule = node["rules"][main_climate]

                # Get daily mag and comp score
                for day in climate_days:
                    day_score = day.get_value(TimeseriesDataTypes.FLOW) / rule["mag"]
                    day.set_result_value(
                        "mag_score", calcs.get_curve_value(day_score * 100, node["partial_tables"]["mag"]) / 100
                    )
                    if day.has_value(TimeseriesDataTypes.COMPLIANCE):
                        day.set_result_value(
                            "comp_score",
                            day.get_value(TimeseriesDataTypes.COMPLIANCE) / rule["mag"]
                            if day.get_value(TimeseriesDataTypes.COMPLIANCE) < rule["mag"]
                            else 1,
                        )

                    season_score = calcs.get_curve_value(day.date.month, node["season"]) / 100
                    day.set_result_value("season_score", season_score)
                    day.set_result_value(
                        "mag_score", day.get_result_value("mag_score") * day.get_result_value("season_score")
                    )
                    if day.has_result_value("comp_score"):
                        day.set_result_value(
                            "comp_score", day.get_result_value("comp_score") * day.get_result_value("season_score")
                        )
                    else:
                        day.set_result_value("comp_score", None)

                # Get event oppurtunities
                events = self.get_event_opportunities("mag_score", climate_days)
                season_result_ts[-1].append({"data": climate_days, "events": events, "main_climate": main_climate})

        return season_result_ts

    def ts_aggregation(self, node, ts: Timeseries, seasonal_result_ts, should_calculate_compliance, node_name):
        seasonal_score = []

        for year_list in seasonal_result_ts:
            prevailing_climate = year_list[-1]["main_climate"]
            prevailing_climate_rule = node["rules"][prevailing_climate]
            start_date = year_list[-1]["data"][-1]
            end_date = year_list[0]["data"][0]

            # Append season info
            seasonal_score.append({})
            seasonal_score[-1]["year"] = start_date.date.year
            seasonal_score[-1]["mag"] = 0
            seasonal_score[-1]["dur"] = 0
            seasonal_score[-1]["count"] = 0
            seasonal_score[-1]["ind"] = 0
            seasonal_score[-1]["compliance"] = None
            seasonal_score[-1]["max_time"] = 0
            seasonal_score[-1]["summary"] = 0
            seasonal_score[-1]["prevailing_climate"] = prevailing_climate
            seasonal_score[-1]["prevailing_climate_rule"] = prevailing_climate_rule
            seasonal_score[-1]["event_log"] = []

            events = []
            # Get best events in the data
            for climate_info in year_list:
                for event_opp in climate_info["events"]:
                    events.append(self.get_best_event(ts, "mag_score", prevailing_climate_rule["dur"], event_opp))

            # Score data
            if len(events) == 0:
                continue

            scores = []
            for event in events:
                mag = ts.get_data_type_value_mean("mag_score", event, 10)
                dur = (
                    calcs.get_curve_value(
                        (len(event) / prevailing_climate_rule["dur"]) * 100, node["partial_tables"]["dur"]
                    )
                    / 100
                )
                summary = mag * dur
                comp = ts.get_data_type_value_mean("comp_score", event, 10)
                scores.append(
                    {
                        "summary": summary,
                        "mag": mag,
                        "dur": dur,
                        "compliance": comp,
                        "start": event[0],
                        "end": event[-1],
                    }
                )

            # Remove illegal events (when independence is too close)
            min_ind = self.get_min_allowed_independence(prevailing_climate_rule["ind"], node["partial_tables"]["ind"])

            check_pos = 0
            while check_pos < len(scores):
                if (check_pos + 1) < len(scores):
                    score = scores[check_pos]
                    next_score = scores[check_pos + 1]
                    diff = ts.get_number_of_days_between_days(next_score["start"], score["end"])
                    if diff < min_ind:
                        if score["summary"] > next_score["summary"]:
                            scores.pop(check_pos + 1)
                            check_pos = 0
                            continue
                        else:
                            scores.pop(check_pos)
                            check_pos = 0
                            continue
                check_pos += 1

            if len(scores) == 0:
                continue

            seasonal_score[-1]["start_date"] = start_date.format_date()
            seasonal_score[-1]["end_date"] = end_date.format_date()

            # Calculate the time between each event, including the start and end of the period
            time_between_events = []
            check_pos = 0
            while check_pos < len(scores):
                if check_pos == 0:
                    score = scores[check_pos]
                    diff = ts.get_number_of_days_between_days(score["start"], start_date)
                    time_between_events.append(diff)
                if (check_pos + 1) < len(scores):
                    score = scores[check_pos]
                    next_score = scores[check_pos + 1]
                    diff = ts.get_number_of_days_between_days(next_score["start"], score["end"])
                    time_between_events.append(diff)
                else:
                    score = scores[check_pos]
                    diff = ts.get_number_of_days_between_days(end_date, score["end"])
                    time_between_events.append(diff)

                check_pos += 1

            # Determine the max time score
            min_time_between_events = min(time_between_events)
            max_time_days = node["max_time"] * 365
            max_time_allowed_days = max_time_days * 2
            if min_time_between_events > max_time_days:
                if min_time_between_events > max_time_allowed_days:
                    max_time_score = 0
                else:
                    days_exceeding = min_time_between_events - max_time_days
                    max_days_exceeding = max_time_allowed_days - max_time_days
                    days_exceeding_inverse = max_days_exceeding - days_exceeding
                    score = (days_exceeding_inverse / max_days_exceeding) * 100
                    max_time_score = calcs.get_curve_value(score, node["partial_tables"]["max_time"])
            else:
                max_time_score = 100

            seasonal_score[-1]["max_time"] = max_time_score / 100

            target_count = math.ceil(prevailing_climate_rule["count"])

            # Take the top N scores (Where N is the desired count)
            if len(scores) > target_count:
                score_index_pairs = [{"index": i, "score": s["summary"]} for i, s in enumerate(scores)]

                score_index_pairs_sorted = sorted(score_index_pairs, key=lambda k: k["score"], reverse=True)

                score_indexes_to_remove = map(lambda s: s["index"], score_index_pairs_sorted[target_count:])

                for index in sorted(score_indexes_to_remove, reverse=True):
                    del scores[index]

            # Add scores to event log
            event_log = []
            for score in scores:
                event_log.append(
                    {
                        "start_date": score["start"].format_date(),
                        "end_date": score["end"].format_date(),
                        "duration": ts.get_number_of_days_between_days(score["end"], score["start"]) + 1,
                    }
                )
            seasonal_score[-1]["event_log"].extend(event_log)

            # Score independence
            scores_ind = []
            for index, score in enumerate(scores):
                if (index + 1) < len(scores):
                    next_score = scores[index + 1]
                    diff = ts.get_number_of_days_between_days(next_score["start"], score["end"])
                    if diff >= min_ind:
                        score["ind"] = diff
                        scores_ind.append(score)
                else:
                    score["ind"] = prevailing_climate_rule["ind"]
                    scores_ind.append(score)

            # Get ind score
            if len(scores_ind) > 1:
                diffs = list(map(lambda s: s["ind"], scores_ind))
                score = (sum(diffs) / len(diffs)) / prevailing_climate_rule["ind"]
                ind_score = calcs.get_curve_value(score * 100, node["partial_tables"]["ind"]) / 100
            elif len(scores_ind) == 1:
                ind_score = 1
            else:
                ind_score = 0

            # Score climate
            seasonal_score[-1]["dur"] = calcs.get_dict_list_mean(scores, "dur", 10) or 0
            seasonal_score[-1]["mag"] = calcs.get_dict_list_mean(scores, "mag", 10) or 0
            seasonal_score[-1]["ind"] = ind_score
            seasonal_score[-1]["count"] = (
                calcs.get_curve_value((len(scores) / target_count) * 100, node["partial_tables"]["count"]) / 100
            )

            if should_calculate_compliance:
                seasonal_score[-1]["compliance"] = calcs.get_dict_list_mean(scores, "compliance", 10) or 0

            if len(scores) > 0:
                seasonal_score[-1]["summary"] = (
                    seasonal_score[-1]["dur"]
                    * seasonal_score[-1]["mag"]
                    * seasonal_score[-1]["count"]
                    * seasonal_score[-1]["ind"]
                    * seasonal_score[-1]["max_time"]
                )
            else:
                seasonal_score[-1]["summary"] = None

        return seasonal_score

    def get_min_allowed_independence(self, target_independence, curve):
        # Get the x value that related to a y score of 0 (minimum x % allowed)
        for datapoint in curve:
            if datapoint[1] == 0:
                returnval = datapoint[0]

        # Get the target_independence adjusted by this value
        val = math.ceil(((returnval / 100) * target_independence))

        # Independence can't be < 1, so return the max of 1 and val
        return max(1, val)

    def get_event_opportunities(self, key, data):
        split_data = [[]]
        in_event = False
        detect_peak = {"prev": None, "next": None, "peaked": False}
        for index, day in enumerate(data):
            try:
                detect_peak["prev"] = data[index - 1]
                detect_peak["next"] = data[index + 1]
            except Exception:
                detect_peak["peaked"] = False
            if in_event:
                if day.get_value_from_key(key) == 1:
                    detect_peak["peaked"] = True
                if (
                    detect_peak["peaked"]
                    and detect_peak["prev"].get_value_from_key(key) > day.get_value_from_key(key)
                    and detect_peak["next"].get_value_from_key(key) > day.get_value_from_key(key)
                ):
                    split_data.append([])
                    detect_peak["peaked"] = False
                if day.get_value_from_key(key) > 0:
                    split_data[-1].append(day)
                else:
                    split_data.append([])
                    in_event = False
            else:
                if day.get_value_from_key(key) > 0:
                    split_data[-1].append(day)
                    in_event = True

        return list(filter(lambda e: len(e) != 0, split_data))

    def get_best_event(self, ts: Timeseries, key, event_duration, data):
        event = data[:event_duration]
        if len(data) <= event_duration:
            return data
        for index, day in enumerate(data):
            try:
                test_event = data[index : (index + event_duration)]
                if len(test_event) < event_duration:
                    continue

                if ts.get_data_type_value_mean(key, test_event, 10) > ts.get_data_type_value_mean(key, event, 10):
                    event = test_event
            except Exception:
                continue

        return event

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Setup ts for analysis
        climate_ts_days = node.data.parse_csv_to_timeseries_days(
            OtherDataTypes.CLIMATE, is_number_values=False, infill_with_previous_value=True
        )
        ts.add_timeseries_days(climate_ts_days)

        # Apply lag if we have compliance
        if ts.has_data_type(TimeseriesDataTypes.COMPLIANCE):
            ts.apply_lag_to_data_type(TimeseriesDataTypes.COMPLIANCE, node.parameters["lag"])

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get summary results
        result.ts_agg = self.ts_aggregation(node.parameters, ts, result.ts_gen, node.data.compliance != None, node.name)

        # Set event log
        for season in result.ts_agg:
            result.event_log.extend(season["event_log"])

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["summary"]), result.ts_agg))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        result_events = NodeResult(result.node_name, ResultTypes.EVENTS_INTERMEDIATE, result.event_log)

        return [result_yearly, result_events]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            [
                "node",
                "year",
                "prevailing_climate",
                "start_date",
                "end_date",
                "mag",
                "dur",
                "count",
                "ind",
                "compliance",
                "max_time",
                "summary",
            ],
        )

        write_result(ResultTypes.EVENTS_INTERMEDIATE, ["node", "start_date", "end_date", "duration"])
