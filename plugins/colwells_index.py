import math
from typing import List
from core.data_store import DataStore
from core.globals import (
    ColwellsIndexClassBoundaryMethodTypes,
    ColwellsIndexSummaryMethodTypes,
    ColwellsIndexTimePeriodTypes,
    ResultTypes,
    TimeseriesDataTypes,
)
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
import statistics


class colwells_index(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Colwells Index",
            "description": "A statistic method to determine Constancy, Contingency and Predictability of timeseries data",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Data
        try:
            node_parameters["data"]["data_type"] = TimeseriesDataTypes(node_parameters["data"]["data_type"])
        except Exception as e:
            raise InvalidParameterException("data", "data_type is not a valid TimeseriesDataType")

        # Classes
        try:
            node_parameters["classes"]["time_period"] = ColwellsIndexTimePeriodTypes(
                node_parameters["classes"]["time_period"]
            )
        except Exception as e:
            raise InvalidParameterException("classes", "time_period is not a valid ColwellsIndexTimePeriodType")

        try:
            node_parameters["classes"]["summary_method"] = ColwellsIndexSummaryMethodTypes(
                node_parameters["classes"]["summary_method"]
            )
        except Exception as e:
            raise InvalidParameterException("classes", "summary_method is not a valid ColwellsIndexSummaryMethodType")

        assert_wrapper(
            validation.is_num(node_parameters["classes"]["number_of_classes"], int),
            "number_of_classes",
            "Must be an integer",
        )
        assert_wrapper((node_parameters["classes"]["number_of_classes"] > 0), "number_of_classes", "Must be > 0")

        try:
            node_parameters["classes"]["class_boundary_method"] = ColwellsIndexClassBoundaryMethodTypes(
                node_parameters["classes"]["class_boundary_method"]
            )
        except Exception as e:
            raise InvalidParameterException(
                "classes", "class_boundary_method is not a valid ColwellsIndexClassBoundaryMethodType"
            )

        if (
            node_parameters["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.LOG
            or node_parameters["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.WEIGHTED_LOG
        ):
            assert_wrapper(
                validation.is_num(node_parameters["classes"]["log_base"]),
                "log_base",
                "Must be a number",
            )

        if node_parameters["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.GAN:
            assert_wrapper(
                validation.is_num(node_parameters["classes"]["gan_from"]),
                "gan_from",
                "Must be a number",
            )
            assert_wrapper(
                validation.is_num(node_parameters["classes"]["gan_by"]),
                "gan_by",
                "Must be a number",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data_key = node["parameters"]["data"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Calculate Classes
        seasonal_split_data = []

        if node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.TRANSFORM:
            for day in ts.data:
                day.set_value(TimeseriesDataTypes.FLOW, math.log10(day.get_value(TimeseriesDataTypes.FLOW) + 1))

        if node["classes"]["time_period"] == ColwellsIndexTimePeriodTypes.MONTHLY:
            seasonal_split_data = ts.sort_into_months()
        elif node["classes"]["time_period"] == ColwellsIndexTimePeriodTypes.SEASONAL:
            seasonal_split_data = ts.sort_into_calender_seasons()

        mean_seasonal_discharge = statistics.mean(
            list(
                map(
                    lambda days: ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW, days),
                    seasonal_split_data.values(),
                )
            )
        )
        min_discharge = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW)
        max_discharge = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW)

        class_boundaries = []

        num_classes = node["classes"]["number_of_classes"]

        if (
            node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.EQUAL
            or node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.TRANSFORM
        ):
            data_range = max_discharge - min_discharge
            step = data_range / (num_classes - 1) if num_classes > 1 else data_range
            for i in range(0, num_classes):
                class_boundaries.append(min_discharge + (step * i))
        elif node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.LOG:
            for i in range(1, num_classes + 1):
                class_boundaries.append(math.pow(node["classes"]["log_base"], (i - 2)))
        elif node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.WEIGHTED_LOG:
            exp = math.ceil(0 - (num_classes - 1) / 2)
            for i in range(0, num_classes + 1):
                class_boundaries.append(math.pow(node["classes"]["log_base"], exp + i) * mean_seasonal_discharge)
        elif node["classes"]["class_boundary_method"] == ColwellsIndexClassBoundaryMethodTypes.GAN:
            for i in range(0, num_classes):
                class_boundaries.append(
                    node["classes"]["gan_from"] + (i * node["classes"]["gan_by"]) * mean_seasonal_discharge
                )

        counts = {}
        for item in seasonal_split_data:
            counts[item] = 0

        classes = {}
        for index, boundary in enumerate(class_boundaries):
            if index == 0:
                classes[index] = {"min": None, "max": boundary, "counts": counts.copy()}
            else:
                classes[index] = {"min": class_boundaries[index - 1], "max": boundary, "counts": counts.copy()}

        classes[len(class_boundaries)] = {"min": boundary, "max": None, "counts": counts.copy()}

        # Sort data into classes
        yearly_split = ts.sort_into_years()
        for year, data in yearly_split.items():
            if node["classes"]["time_period"] == ColwellsIndexTimePeriodTypes.MONTHLY:
                year_data = ts.sort_into_months(data)
            elif node["classes"]["time_period"] == ColwellsIndexTimePeriodTypes.SEASONAL:
                year_data = ts.sort_into_calender_seasons(data)

            for season, data in year_data.items():
                if node["classes"]["summary_method"] == ColwellsIndexSummaryMethodTypes.MEAN:
                    summary_value = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW, data)
                elif node["classes"]["summary_method"] == ColwellsIndexSummaryMethodTypes.MEDIAN:
                    summary_value = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW, data)
                elif node["classes"]["summary_method"] == ColwellsIndexSummaryMethodTypes.MIN:
                    summary_value = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW, data)
                elif node["classes"]["summary_method"] == ColwellsIndexSummaryMethodTypes.MAX:
                    summary_value = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, data)

                class_id = self.get_class_id_from_value(summary_value, classes)

                classes[class_id]["counts"][season] += 1

        return classes

    def get_class_id_from_value(self, value, classes):
        for class_id, class_data in classes.items():
            # Check for bottom class (just < max)
            if class_data["min"] is None:
                if value <= class_data["max"]:
                    return class_id
                continue

            # Check for top class (just > min)
            if class_data["max"] is None:
                if value >= class_data["min"]:
                    return class_id
                continue

            if value >= class_data["min"] and value < class_data["max"]:
                return class_id

        return None

    def ts_aggregation(self, node, classes):
        state_totals = {}
        time_totals = {}

        for state, data in classes.items():
            state_total = 0
            for time, count in data["counts"].items():
                if time not in time_totals:
                    time_totals[time] = 0

                time_totals[time] += count
                state_total += count

            state_totals[state] = state_total

        grand_total = sum(time_totals.values())

        hx = 0
        for total in time_totals.values():
            value = (total / grand_total) * math.log10(total / grand_total) if total != 0 else 0
            hx += value
        hx = -hx

        hy = 0
        for total in state_totals.values():
            value = (total / grand_total) * math.log10(total / grand_total) if total != 0 else 0
            hy += value
        hy = -hy

        hxy = 0
        for bin_data in classes.values():
            for count in bin_data["counts"].values():
                value = (count / grand_total) * math.log10(count / grand_total) if count != 0 else 0
                hxy += value
        hxy = -hxy

        p = round(1 - ((hxy - hx) / math.log10(len(state_totals))), 3)
        c = round(1 - (hy / math.log10(len(state_totals))), 3)
        m = round((hx + hy - hxy) / math.log10(len(state_totals)), 3)

        return {"predictability": p, "constancy": c, "contingency": m}

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get summary results
        result.ts_agg = self.ts_aggregation(node.parameters, result.ts_gen)

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_summary_intermediate = NodeResult(
            result.node_name,
            ResultTypes.SUMMARY_INTERMEDIATE,
            [
                {
                    "predictability": result.ts_agg["predictability"],
                    "constancy": result.ts_agg["constancy"],
                    "contingency": result.ts_agg["contingency"],
                }
            ],
        )

        result_custom_data = [["node", "predictability", "constancy", "contingency"]]

        result_custom_data.append(
            [
                result.node_name,
                result.ts_agg["predictability"],
                result.ts_agg["constancy"],
                result.ts_agg["contingency"],
            ]
        )

        node_class_table_row = ["Seasons"]
        node_class_table_row.extend(list(result.ts_gen[0]["counts"].keys()))
        result_custom_data.append(node_class_table_row)
        for bin in result.ts_gen.values():
            new_row = [str(bin["min"]) + " - " + str(bin["max"])]
            new_row.extend(list(bin["counts"].values()))
            result_custom_data.append(new_row)

        result_custom_data.append([])

        result_custom = NodeResult(result.node_name, ResultTypes.CUSTOM_RESULTS, result_custom_data)

        return [result_custom, result_summary_intermediate]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.CUSTOM_RESULTS)
