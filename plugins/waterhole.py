from typing import List
from core.globals import OtherDataTypes, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class waterhole(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Waterhole",
            "description": "Model Waterhole drying",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Evaporation
        assert_wrapper(node_parameters["evaporation"]["scaling"] >= 0, "scaling", "must be >= 0")

        # Seepage
        assert_wrapper(node_parameters["seepage"]["rate"] >= 0, "rate", "must be >= 0")

        # Rainfall
        assert_wrapper(node_parameters["rainfall"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(node_parameters["rainfall"]["max_infiltration"] >= 0, "max_infiltration", "must be >= 0")

        # Ground Water
        assert_wrapper(node_parameters["groundwater"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["inflow"], 0, 100),
            "inflow",
            "must be between 0 and 100",
        )
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["loss_to_deep_drainage"], 0, 100),
            "loss_to_deep_drainage",
            "must be between 0 and 100",
        )
        assert_wrapper(
            (node_parameters["groundwater"]["inflow"] + node_parameters["groundwater"]["loss_to_deep_drainage"]) <= 100,
            "groundwater",
            "inflow % + loss to deep drainage % must be <= 100%",
        )

        # Extraction
        assert_wrapper(
            node_parameters["extraction"]["commence_pumping_depth"]
            >= node_parameters["extraction"]["cease_pumping_depth"],
            "commence_pumping_depth/cease_pumping_depth",
            "commence depth must be >= cease depth",
        )
        assert_wrapper(node_parameters["extraction"]["extraction_rate"] >= 0, "extraction_rate", "must be >= 0")
        assert_wrapper(node_parameters["extraction"]["max_annual_take"] >= 0, "max_annual_take", "must be >= 0")
        assert_wrapper(
            node_parameters["extraction"]["start_month"] >= 1 and node_parameters["extraction"]["start_month"] <= 12,
            "start_month",
            "must be a valid month",
        )
        assert_wrapper(node_parameters["extraction"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(validation.is_num(node_parameters["extraction"]["lag"], int), "lag", "must be an integer")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.rainfall = data.try_import_data(node["data"], TimeseriesDataTypes.RAINFALL)

        data.evaporation = data.try_import_data(node["data"], TimeseriesDataTypes.EVAPORATION)

        data.bathymetry = data.try_import_data(node["data"], OtherDataTypes.BATHYMETRY)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries, bathymetry):
        # Setup general vars
        max_depth = bathymetry["volume"][-1][0]
        max_volume = bathymetry["volume"][-1][1]
        max_area = bathymetry["area"][-1][1]
        waterhole_volume = max_volume
        waterhole_depth = max_depth
        prev_month = ts.data[0].date.month
        GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        max_GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        annual_pump_take = 0

        for index, day in enumerate(ts.data):
            # Reset annual pump take if new water year
            if day.date.month != prev_month:
                if day.date.month == node["extraction"]["start_month"]:
                    annual_pump_take = 0
                prev_month = day.date.month

            # Only analyse waterhole level if no flow, else top up to full depth
            if day.get_value(TimeseriesDataTypes.FLOW) <= node["extraction"]["CTF"]:
                # Calculate daily inflow and outflow, apply this to prev depth to get new depth
                waterhole_inflow_m3 = 0
                waterhole_outflow_m3 = 0
                GW_inflow_m3 = 0
                GW_outflow_m3 = 0

                # Get the current waterhole area for rainfall and evaporation calculations
                waterhole_surface_area = calcs.get_curve_value(waterhole_depth, bathymetry["area"])

                # Rainfall
                if day.has_value(TimeseriesDataTypes.RAINFALL) and day.get_value(TimeseriesDataTypes.RAINFALL) > 0:
                    # Direct Rainfall
                    waterhole_inflow_m3 += (
                        day.get_value(TimeseriesDataTypes.RAINFALL) * waterhole_surface_area
                    ) * 0.001

                    # Runoff Rainfall
                    if node["rainfall"]["area"] > waterhole_surface_area:
                        local_rainfall_area = node["rainfall"]["area"] - waterhole_surface_area

                        if day.get_value(TimeseriesDataTypes.RAINFALL) > node["rainfall"]["max_infiltration"]:
                            local_rainfall_above_infiltration = (
                                day.get_value(TimeseriesDataTypes.RAINFALL) - node["rainfall"]["max_infiltration"]
                            )
                            waterhole_inflow_m3 += (local_rainfall_above_infiltration * local_rainfall_area) * 0.001

                            GW_inflow_m3 += (node["rainfall"]["max_infiltration"] * local_rainfall_area) * 0.001
                        else:
                            GW_inflow_m3 += (day.get_value(TimeseriesDataTypes.RAINFALL) * local_rainfall_area) * 0.001

                    # GW Rainfall
                    if (
                        node["groundwater"]["area"] > waterhole_surface_area
                        and node["groundwater"]["area"] > node["rainfall"]["area"]
                    ):
                        GW_rainfall_area = node["groundwater"]["area"] - node["rainfall"]["area"]

                        if day.get_value(TimeseriesDataTypes.RAINFALL) > node["rainfall"]["max_infiltration"]:
                            GW_inflow_m3 += (node["rainfall"]["max_infiltration"] * GW_rainfall_area) * 0.001
                        else:
                            GW_inflow_m3 += (day.get_value(TimeseriesDataTypes.RAINFALL) * GW_rainfall_area) * 0.001

                # Evaporation
                evaporation_adjusted = 0
                if day.has_value(TimeseriesDataTypes.EVAPORATION):
                    evaporation_adjusted = day.get_value(TimeseriesDataTypes.EVAPORATION) * (
                        node["evaporation"]["scaling"] / 100
                    )
                    waterhole_outflow_m3 += (evaporation_adjusted * waterhole_surface_area) * 0.001

                # Seepage
                waterhole_outflow_m3 += (node["seepage"]["rate"] * waterhole_surface_area) * 0.001

                # Pumping
                if (
                    waterhole_depth <= node["extraction"]["commence_pumping_depth"]
                    and waterhole_depth >= node["extraction"]["cease_pumping_depth"]
                ):
                    if (annual_pump_take + node["extraction"]["extraction_rate"]) <= node["extraction"][
                        "max_annual_take"
                    ]:
                        waterhole_outflow_m3 += node["extraction"]["extraction_rate"] * 1000
                        annual_pump_take += node["extraction"]["extraction_rate"]

                # GW
                # To waterhole
                waterhole_inflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)
                GW_outflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)

                # To Deep Drainage
                GW_outflow_m3 += GW_volume * (node["groundwater"]["loss_to_deep_drainage"] / 100)

                # To Evaporation
                if GW_volume > (max_GW_volume * 0.8) and day.has_value(TimeseriesDataTypes.EVAPORATION):
                    if (node["groundwater"]["area"] - node["rainfall"]["area"]) > 0:
                        GW_outflow_m3 += (
                            evaporation_adjusted * 0.5 * node["groundwater"]["area"] - node["rainfall"]["area"]
                        ) * 0.001

                # Apply all volume changes
                waterhole_volume -= waterhole_outflow_m3
                waterhole_volume += waterhole_inflow_m3

                if waterhole_volume > max_volume:  # Cant exceed max volume/depth
                    waterhole_volume = max_volume
                elif waterhole_volume < 0:
                    waterhole_volume = 0

                waterhole_depth = calcs.get_curve_value(waterhole_volume, bathymetry["volume"], True)

                GW_volume -= GW_outflow_m3
                GW_volume += GW_inflow_m3

                if GW_volume > max_GW_volume:  # Cant exceed max volume/depth
                    GW_volume = max_GW_volume

            else:
                waterhole_depth = max_depth
                waterhole_volume = max_volume
                waterhole_surface_area = max_area
                GW_volume = max_GW_volume

            day.set_result_value("modelled_depth", round(waterhole_depth, 5))
            day.set_result_value("modelled_volume", round(waterhole_volume, 5))
            day.set_result_value("modelled_area", round(waterhole_surface_area, 5))
            day.set_result_value("modelled_GW_volume", round(GW_volume, 5))

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, node.parameters["extraction"]["lag"])

        bathymetry = {
            "volume": calcs.extract_curve(node.data.bathymetry, 0, 2, True, True),
            "area": calcs.extract_curve(node.data.bathymetry, 0, 3, True, True),
            "height": calcs.extract_curve(node.data.bathymetry, 0, 1, True, True),
        }

        # Generate daily depth
        result.ts_gen = self.ts_generation(node.parameters, ts, bathymetry)

        # Set assessment results
        result.assessment_key = "modelled_depth"

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "modelled_depth",
                "modelled_volume",
                "modelled_area",
                "modelled_GW_volume",
                "flow",
                "rainfall",
                "evaporation",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "modelled_depth",
                "modelled_volume",
                "modelled_area",
                "modelled_GW_volume",
                "flow",
                "rainfall",
                "evaporation",
            ],
        )
