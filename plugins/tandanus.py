from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class tandanus(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Tandanus",
            "description": "Score Tandanus spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # season
        if node_parameters["season"]["use_temperature"] is False:
            assert_wrapper(
                validation.valid_season(node_parameters["season"]["season"]) is True,
                "season",
                "must contain valid dates (day/month)",
            )
            node_parameters["season"]["season"] = Season(node_parameters["season"]["season"])
            assert_wrapper(
                type(node_parameters["season"]["allow_partial_seasons"]) is bool,
                "allow_partial_seasons",
                "must be a boolean",
            )
        else:
            try:
                node_parameters["season"]["temperature_comparison"] = ComparisonOptions(
                    node_parameters["season"]["temperature_comparison"]
                )
            except Exception:
                raise InvalidParameterException("temperature_comparison", "comparison type is not valid")

            assert_wrapper(
                validation.is_num(node_parameters["season"]["temperature_threshold"]),
                "temperature_threshold",
                "must be a number",
            )
            assert_wrapper(
                "temperature" in node_data,
                "temperature",
                "must provide temperature data if using temperature based season",
            )
            assert_wrapper(
                type(node_parameters["season"]["use_temperature_moving_average"]) is bool,
                "use_temperature_moving_average",
                "must be a boolean",
            )
            if node_parameters["season"]["use_temperature_moving_average"]:
                assert_wrapper(
                    node_parameters["season"]["temperature_moving_average_duration"] >= 1,
                    "temperature_moving_average_duration",
                    "must be >= 1",
                )

        # flow_params
        if node_parameters["flow_params"]["use_median_flow"] is not True:
            assert_wrapper(node_parameters["flow_params"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")
        assert_wrapper(node_parameters["flow_params"]["flow_duration"] >= 0, "flow_duration", "must be >= 0")

        if "depth" in node_data:
            assert_wrapper(
                node_parameters["flow_params"]["depth_bounds"]["upper"]
                >= node_parameters["flow_params"]["depth_bounds"]["lower"],
                "depth_bounds",
                "upper bound must be >= lower bound",
            )
            assert_wrapper(node_parameters["flow_params"]["depth_duration"] >= 0, "depth_duration", "must be >= 0")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.DEPTH in node["data"]:
            data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        if TimeseriesDataTypes.TEMPERATURE in node["data"]:
            data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        if node["season"]["use_temperature"] is True:
            temperature_key = TimeseriesDataTypes.TEMPERATURE

            if node["season"]["use_temperature_moving_average"]:
                ts.set_moving_avg(
                    TimeseriesDataTypes.TEMPERATURE,
                    "temperature_avg",
                    node["season"]["temperature_moving_average_duration"],
                )
                temperature_key = "temperature_avg"

            if node["season"]["temperature_comparison"] is ComparisonOptions.GREATER_EQUAL:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, node["season"]["temperature_threshold"], None
                )
            else:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, None, node["season"]["temperature_threshold"]
                )

        else:
            season_index_list = ts.get_season_index_list(
                node["season"]["season"], node["season"]["allow_partial_seasons"]
            )

        for index, day in enumerate(ts.data):
            day.init_result_value_keys(
                [
                    "in_season",
                    "suitable_time_to_complete_flow_event",
                    "flow_threshold_not_exceeded_in_period",
                    "suitable_time_to_complete_depth_event",
                    "depth_stays_in_range_for_period",
                ]
            )

            if index not in season_index_list:
                day.set_result_value("success", 0)
                day.set_result_value("in_season", 0)
                continue
            else:
                day.set_result_value("in_season", 1)

            # Check if event occurs given that the day is suitable to start
            # Make sure there's enough time left to complete the event, if not it's not suitable
            flow_check_dur = node["flow_params"]["flow_duration"]
            flow_check_index = index + flow_check_dur
            if flow_check_index >= len(ts.data):
                day.set_result_value("success", 0)
                day.set_result_value("suitable_time_to_complete_flow_event", 0)
                continue
            else:
                day.set_result_value("suitable_time_to_complete_flow_event", 1)

            # get days to check
            flow_check_days = ts.data[index:flow_check_index]

            # Check if flow criteria is met
            flow_check_days_max = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, flow_check_days)

            if flow_check_days_max > node["flow_params"]["flow_threshold"]:
                day.set_result_value("success", 0)
                day.set_result_value("flow_threshold_not_exceeded_in_period", 0)
                continue
            else:
                day.set_result_value("flow_threshold_not_exceeded_in_period", 1)

            # Check if depth criteria is met
            if day.has_value(TimeseriesDataTypes.DEPTH):
                depth_check_dur = node["flow_params"]["depth_duration"]
                depth_check_index = flow_check_index + depth_check_dur

                if depth_check_index >= len(ts.data):
                    day.set_result_value("success", 0)
                    day.set_result_value("suitable_time_to_complete_depth_event", 0)
                    continue
                else:
                    day.set_result_value("suitable_time_to_complete_depth_event", 1)

                # get days to check
                depth_check_days = ts.data[flow_check_index:depth_check_index]

                # Check if depth criteria is met
                depth_check_days_max = ts.get_data_type_value_max(TimeseriesDataTypes.DEPTH, depth_check_days)
                depth_check_days_min = ts.get_data_type_value_min(TimeseriesDataTypes.DEPTH, depth_check_days)

                if (
                    depth_check_days_max > node["flow_params"]["depth_bounds"]["upper"]
                    or depth_check_days_min < node["flow_params"]["depth_bounds"]["lower"]
                ):
                    day.set_result_value("success", 0)
                    day.set_result_value("depth_stays_in_range_for_period", 0)
                    continue
                else:
                    day.set_result_value("depth_stays_in_range_for_period", 1)

            # If we are at this point, all criteria have been met
            day.set_result_value("success", 1)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Get lowflow threshhold
        if node.parameters["flow_params"]["use_median_flow"]:
            node.parameters["flow_params"]["flow_threshold"] = ts.get_data_type_value_median(
                TimeseriesDataTypes.FLOW,
            )

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "in_season",
                "suitable_time_to_complete_flow_event",
                "flow_threshold_not_exceeded_in_period",
                "suitable_time_to_complete_depth_event",
                "depth_stays_in_range_for_period",
                "success",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "in_season",
                "suitable_time_to_complete_flow_event",
                "flow_threshold_not_exceeded_in_period",
                "suitable_time_to_complete_depth_event",
                "depth_stays_in_range_for_period",
                "success",
            ],
        )
