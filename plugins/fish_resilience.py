from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class fish_resilience(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Fish Resilience",
            "description": "Score Fish Resilience",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # flow_params
        if not node_parameters["flow_params"]["use_median_CTF"]:
            assert_wrapper(node_parameters["flow_params"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(node_parameters["flow_params"]["water_stress_time"] >= 0, "water_stress_time", "must be >= 0")
        assert_wrapper(node_parameters["flow_params"]["drying_time"] >= 0, "drying_time", "must be >= 0")
        assert_wrapper(
            type(node_parameters["flow_params"]["reset_water_stress_after_flow"]) is bool,
            "reset_water_stress_after_flow",
            "must be a boolean",
        )

        # movement_params
        assert_wrapper(node_parameters["movement_params"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")
        assert_wrapper(node_parameters["movement_params"]["dur"] >= 0, "dur", "must be >= 0")
        if node_parameters["movement_params"]["use_temperature"]:
            try:
                node_parameters["movement_params"]["temperature_comparison"] = ComparisonOptions(
                    node_parameters["movement_params"]["temperature_comparison"]
                )
            except Exception:
                raise InvalidParameterException("temperature_comparison", "comparison type is not valid")

            assert_wrapper(
                "temperature" in node_data,
                "temperature",
                "must provide temperature data if using temperature based season",
            )
            assert_wrapper(
                validation.is_num(node_parameters["movement_params"]["temperature_threshold"]),
                "temperature_threshold",
                "must be a number",
            )

            assert_wrapper(
                type(node_parameters["movement_params"]["use_temperature_moving_average"]) is bool,
                "use_temperature_moving_average",
                "must be a boolean",
            )
            if node_parameters["movement_params"]["use_temperature_moving_average"]:
                assert_wrapper(
                    node_parameters["movement_params"]["temperature_moving_average_duration"] >= 1,
                    "temperature_moving_average_duration",
                    "must be >= 1",
                )
        else:
            assert_wrapper(
                validation.valid_season(node_parameters["movement_params"]["season"]) is True,
                "season",
                "must contain valid dates (day/month)",
            )
            node_parameters["movement_params"]["season"] = Season(node_parameters["movement_params"]["season"])
            assert_wrapper(
                type(node_parameters["movement_params"]["allow_partial_seasons"]) is bool,
                "allow_partial_seasons",
                "must be a boolean",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.TEMPERATURE in node["data"]:
            data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        if node["movement_params"]["use_temperature"] is True:
            temperature_key = TimeseriesDataTypes.TEMPERATURE

            if node["movement_params"]["use_temperature_moving_average"]:
                ts.set_moving_avg(
                    TimeseriesDataTypes.TEMPERATURE,
                    "temperature_avg",
                    node["movement_params"]["temperature_moving_average_duration"],
                )
                temperature_key = "temperature_avg"

            if node["movement_params"]["temperature_comparison"] is ComparisonOptions.GREATER_EQUAL:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, node["movement_params"]["temperature_threshold"], None
                )
            else:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, None, node["movement_params"]["temperature_threshold"]
                )

        else:
            season_index_list = ts.get_season_index_list(
                node["movement_params"]["season"], node["movement_params"]["allow_partial_seasons"]
            )

        stress_period_count = 0
        failure_period_count = 0
        stress = False
        fail = False
        fishless = False
        resilience = False
        above = False
        found_movement = False

        for index, day in enumerate(ts.data):
            resilience = False
            if day.get_value(TimeseriesDataTypes.FLOW) <= node["flow_params"]["CTF"]:
                if above:  # If flow has gone above, but now dropped
                    failure_period_count = 0
                    stress = False
                    fail = False
                    if node["flow_params"]["reset_water_stress_after_flow"]:
                        stress_period_count = 0
                    above = False
                    found_movement = False

                stress_period_count += 1
                failure_period_count += 1
                if stress_period_count >= node["flow_params"]["water_stress_time"]:
                    stress = True
                    if failure_period_count >= node["flow_params"]["drying_time"]:
                        fail = True
                        fishless = True
            else:
                if (
                    not found_movement
                    and index in season_index_list
                    and day.get_value(TimeseriesDataTypes.FLOW) >= node["movement_params"]["flow_threshold"]
                ):
                    # Make sure we have enough days left
                    if (index + node["movement_params"]["dur"]) <= len(ts.data):
                        dur_met = True
                        for index_, day_ in enumerate(ts.data[index : index + node["movement_params"]["dur"]]):
                            if day_.get_value(TimeseriesDataTypes.FLOW) < node["movement_params"]["flow_threshold"]:
                                dur_met = False
                        if dur_met:
                            # Score resilience on these days
                            if not fishless and not fail and stress:
                                for index_, day_ in enumerate(ts.data[index : index + node["movement_params"]["dur"]]):
                                    day_.set_result_value("resilience", 1)
                            stress_period_count = 0
                            fishless = False
                            found_movement = True
                above = True

            # Log results
            if not above:
                day.set_result_value("stress", int(stress))
                day.set_result_value("fail", int(fail))
                day.set_result_value("resilience", int(resilience))
            else:
                day.set_result_value("stress", 0)
                day.set_result_value("fail", 0)
                if not day.has_result_value("resilience"):
                    day.set_result_value("resilience", 0)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if node.parameters["flow_params"]["use_median_CTF"]:
            node.parameters["flow_params"]["CTF"] = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(["date", "stress", "fail", "resilience"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "stress", "fail", "resilience"])
