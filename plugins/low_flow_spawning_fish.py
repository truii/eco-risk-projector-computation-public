from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class low_flow_spawning_fish(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Low Flow Spawning Fish",
            "description": "Score Low Flow Spawning Fish spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # lowflow
        if node_parameters["lowflow"]["use_median_flow"] is not True:
            assert_wrapper(node_parameters["lowflow"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")

        # breeding season
        if node_parameters["breeding_season"]["use_temperature"] is False:
            assert_wrapper(
                validation.valid_season(node_parameters["breeding_season"]["season"]) is True,
                "season",
                "must contain valid dates (day/month)",
            )
            node_parameters["breeding_season"]["season"] = Season(node_parameters["breeding_season"]["season"])
            assert_wrapper(
                type(node_parameters["breeding_season"]["allow_partial_seasons"]) is bool,
                "allow_partial_seasons",
                "must be a boolean",
            )
        else:
            try:
                node_parameters["breeding_season"]["temperature_comparison"] = ComparisonOptions(
                    node_parameters["breeding_season"]["temperature_comparison"]
                )
            except Exception:
                raise InvalidParameterException("temperature_comparison", "comparison type is not valid")

            assert_wrapper(
                validation.is_num(node_parameters["breeding_season"]["temperature_threshold"]),
                "temperature_threshold",
                "must be a number",
            )
            assert_wrapper(
                "temperature" in node_data,
                "temperature",
                "must provide temperature data if using temperature based season",
            )

            assert_wrapper(
                type(node_parameters["breeding_season"]["use_temperature_moving_average"]) is bool,
                "use_temperature_moving_average",
                "must be a boolean",
            )
            if node_parameters["breeding_season"]["use_temperature_moving_average"]:
                assert_wrapper(
                    node_parameters["breeding_season"]["temperature_moving_average_duration"] >= 1,
                    "temperature_moving_average_duration",
                    "must be >= 1",
                )

        # life stages criteria
        assert_wrapper(node_parameters["life_stages_criteria"]["egg"]["max_change"] >= 0, "max_change", "must be >= 0")
        assert_wrapper(
            node_parameters["life_stages_criteria"]["egg"]["consecutive_days"] >= 0, "consecutive_days", "must be >= 0"
        )

        if node_parameters["life_stages_criteria"]["larvae"]["consider_larvae_criteria"] is True:
            assert_wrapper(
                node_parameters["life_stages_criteria"]["larvae"]["daily_change"] >= 0, "daily_change", "must be >= 0"
            )
            assert_wrapper(
                node_parameters["life_stages_criteria"]["larvae"]["consecutive_days"] >= 0,
                "consecutive_days",
                "must be >= 0",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.TEMPERATURE in node["data"]:
            data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        if node["breeding_season"]["use_temperature"] is True:
            temperature_key = TimeseriesDataTypes.TEMPERATURE

            if node["breeding_season"]["use_temperature_moving_average"]:
                ts.set_moving_avg(
                    TimeseriesDataTypes.TEMPERATURE,
                    "temperature_avg",
                    node["breeding_season"]["temperature_moving_average_duration"],
                )
                temperature_key = "temperature_avg"

            if node["breeding_season"]["temperature_comparison"] is ComparisonOptions.GREATER_EQUAL:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, node["breeding_season"]["temperature_threshold"], None
                )
            else:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, None, node["breeding_season"]["temperature_threshold"]
                )

        else:
            season_index_list = ts.get_season_index_list(
                node["breeding_season"]["season"], node["breeding_season"]["allow_partial_seasons"]
            )

        for index, day in enumerate(ts.data):
            # Check if day is suitable for event
            day.init_result_value_keys(
                [
                    "in_season",
                    "flow_threshold_not_exceeded",
                    "suitable_time_to_complete_event",
                    "egg_criteria_met",
                    "larvae_criteria_met",
                ]
            )

            if index not in season_index_list:
                day.set_result_value("success", 0)
                day.set_result_value("in_season", 0)
                continue
            else:
                day.set_result_value("in_season", 1)

            if (
                not day.has_value(TimeseriesDataTypes.FLOW)
                or day.get_value(TimeseriesDataTypes.FLOW) > node["lowflow"]["flow_threshold"]
            ):
                day.set_result_value("success", 0)
                day.set_result_value("flow_threshold_not_exceeded", 0)
                continue
            else:
                day.set_result_value("flow_threshold_not_exceeded", 1)

            # Check if event occurs given that the day is suitable to start
            # Make sure there's enough time left to complete the event, if not it's not suitable
            egg_dur = node["life_stages_criteria"]["egg"]["consecutive_days"]
            larvae_dur = 0

            if node["life_stages_criteria"]["larvae"]["consider_larvae_criteria"]:
                larvae_dur = node["life_stages_criteria"]["larvae"]["consecutive_days"]

            if index + egg_dur + larvae_dur >= len(ts.data):
                day.set_result_value("success", 0)
                day.set_result_value("suitable_time_to_complete_event", 0)
                continue
            else:
                day.set_result_value("suitable_time_to_complete_event", 1)

            # Check if egg criteria is met
            egg_days = ts.data[index : index + egg_dur]
            egg_res = ts.is_data_type_variation_within_allowed_from_baseline(
                TimeseriesDataTypes.DEPTH,
                node["life_stages_criteria"]["egg"]["max_change"],
                day.get_value(TimeseriesDataTypes.DEPTH),
                egg_days,
            )

            if egg_res is False:
                day.set_result_value("success", 0)
                day.set_result_value("egg_criteria_met", 0)
                continue
            else:
                day.set_result_value("egg_criteria_met", 1)

            if node["life_stages_criteria"]["larvae"]["consider_larvae_criteria"]:
                # Check if larvae criteria is met
                larvae_days = ts.data[index + egg_dur : index + egg_dur + larvae_dur]
                larvae_res = ts.is_data_type_variation_within_allowed_between_days(
                    TimeseriesDataTypes.DEPTH,
                    node["life_stages_criteria"]["larvae"]["daily_change"],
                    larvae_days,
                )
                if larvae_res is False:
                    day.set_result_value("success", 0)
                    day.set_result_value("larvae_criteria_met", 0)
                    continue
                else:
                    day.set_result_value("larvae_criteria_met", 1)

            # If we are at this point, all criteria have been met
            day.set_result_value("success", 1)

        return ts

    def ts_aggregation(self, node, ts: Timeseries):
        date_season = Season(start_month=calcs.get_month_number(node.assessment["yearly_agg"]["start_month"]))

        season_split_ts = ts.split_into_seasons(date_season, True)

        yearly_recruitment_result = []

        for year_days in season_split_ts:
            year = year_days[0].date.year
            total_recruitment_opportunities = 0

            for day in year_days:
                total_recruitment_opportunities += (
                    day.get_result_value("success") if day.has_result_value("success") else 0
                )

            yearly_recruitment_result.append(
                {"year": year, "total_recruitment_opportunities": total_recruitment_opportunities}
            )

        return yearly_recruitment_result

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Get lowflow threshhold
        if node.parameters["lowflow"]["use_median_flow"]:
            node.parameters["lowflow"]["flow_threshold"] = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        result.ts_agg = self.ts_aggregation(node, ts)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "in_season",
                "flow_threshold_not_exceeded",
                "suitable_time_to_complete_event",
                "egg_criteria_met",
                "larvae_criteria_met",
                "success",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        return [result_daily, result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "in_season",
                "flow_threshold_not_exceeded",
                "suitable_time_to_complete_event",
                "egg_criteria_met",
                "larvae_criteria_met",
                "success",
            ],
        )

        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "total_recruitment_opportunities"],
        )
