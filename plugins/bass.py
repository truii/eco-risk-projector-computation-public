from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class bass(Plugin):
    def __init__(self):
        meta_data = {
            "name": "SUPERSEDED - Bass",
            "description": "Score Bass spawning success - this model is superseded by the newer Australian Bass plugin",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # flow_params
        assert_wrapper(
            node_parameters["flow_params"]["flow_thresholds"][0]
            >= node_parameters["flow_params"]["flow_thresholds"][1],
            "flow_thresholds",
            "first flow threshold must be >= second flow threshold",
        )
        assert_wrapper(
            type(node_parameters["flow_params"]["use_flow_percentiles"]) is bool,
            "use_flow_percentiles",
            "must be a boolean",
        )
        if "depth" in node_data:
            assert_wrapper(node_parameters["flow_params"]["depth_threshold"] >= 0, "depth_threshold", "must be >= 0")
        # season
        if node_parameters["season"]["use_temperature"] is False:
            assert_wrapper(
                validation.valid_season(node_parameters["season"]["season"]) is True,
                "season",
                "must contain valid dates (day/month)",
            )
            node_parameters["season"]["season"] = Season(node_parameters["season"]["season"])
            assert_wrapper(
                type(node_parameters["season"]["allow_partial_seasons"]) is bool,
                "allow_partial_seasons",
                "must be a boolean",
            )

        else:
            try:
                node_parameters["season"]["temperature_comparison"] = ComparisonOptions(
                    node_parameters["season"]["temperature_comparison"]
                )
            except Exception:
                raise InvalidParameterException("temperature_comparison", "comparison type is not valid")

            assert_wrapper(
                node_parameters["season"]["temperature_threshold"] >= 0, "temperature_threshold", "must be >= 0"
            )
            assert_wrapper(
                "temperature" in node_data,
                "temperature",
                "must provide temperature data if using temperature based season",
            )
            assert_wrapper(
                type(node_parameters["season"]["use_temperature_moving_average"]) is bool,
                "use_temperature_moving_average",
                "must be a boolean",
            )
            if node_parameters["season"]["use_temperature_moving_average"]:
                assert_wrapper(
                    node_parameters["season"]["temperature_moving_average_duration"] >= 1,
                    "temperature_moving_average_duration",
                    "must be >= 1",
                )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.DEPTH in node["data"]:
            data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        if TimeseriesDataTypes.TEMPERATURE in node["data"]:
            data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        if node["season"]["use_temperature"] is True:
            temperature_key = TimeseriesDataTypes.TEMPERATURE

            if node["season"]["use_temperature_moving_average"]:
                ts.set_moving_avg(
                    TimeseriesDataTypes.TEMPERATURE,
                    "temperature_avg",
                    node["season"]["temperature_moving_average_duration"],
                )
                temperature_key = "temperature_avg"

            if node["season"]["temperature_comparison"] is ComparisonOptions.GREATER_EQUAL:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, node["season"]["temperature_threshold"], None
                )
            else:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, None, node["season"]["temperature_threshold"]
                )

        else:
            season_index_list = ts.get_season_index_list(
                node["season"]["season"], node["season"]["allow_partial_seasons"]
            )

        in_movement_season = False
        success_count = 0
        # Check each day for success
        for index, day in enumerate(ts.data):
            day.init_result_value_keys(
                [
                    "in_season",
                    "in_movement_season",
                    "depth_threshold_met",
                ]
            )

            # if day in season
            if index not in season_index_list:
                day.set_result_value("in_season", 0)
                day.set_result_value("success", 0)
                continue
            else:
                day.set_result_value("in_season", 1)

                # Check flow
                flow_success = True
                if in_movement_season:
                    if day.get_value(TimeseriesDataTypes.FLOW) < node["flow_params"]["flow_thresholds"][1]:
                        flow_success = False
                        in_movement_season = False
                else:
                    if day.get_value(TimeseriesDataTypes.FLOW) >= node["flow_params"]["flow_thresholds"][0]:
                        in_movement_season = True
                    else:
                        flow_success = False

                day.set_result_value("in_movement_season", 1 if in_movement_season else 0)

                # Check depth
                depth_success = True
                if day.has_value(TimeseriesDataTypes.DEPTH):
                    if day.get_value(TimeseriesDataTypes.DEPTH) < node["flow_params"]["depth_threshold"]:
                        depth_success = False
                        day.set_result_value("depth_threshold_met", 0)
                    else:
                        day.set_result_value("depth_threshold_met", 1)

                # Get success
                if flow_success and depth_success:
                    success_count += 1
                    day.set_result_value("success", success_count)
                else:
                    success_count = 0
                    day.set_result_value("success", 0)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Get flow percentiles
        if node.parameters["flow_params"]["use_flow_percentiles"]:
            node.parameters["flow_params"]["flow_thresholds"][0] = ts.get_data_type_percentile(
                TimeseriesDataTypes.FLOW, node.parameters["flow_params"]["flow_thresholds"][0]
            )
            node.parameters["flow_params"]["flow_thresholds"][1] = ts.get_data_type_percentile(
                TimeseriesDataTypes.FLOW, node.parameters["flow_params"]["flow_thresholds"][1]
            )

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_key = "success"

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(
            ["date", "in_season", "in_movement_season", "depth_threshold_met", "success"], [0]
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            ["node", "date", "in_season", "in_movement_season", "depth_threshold_met", "success"],
        )
