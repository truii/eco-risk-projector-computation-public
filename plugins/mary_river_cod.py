from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class mary_river_cod(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Mary River Cod",
            "description": "Score Mary River Cod spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # connectivity_event_1
        assert_wrapper(
            validation.valid_season(node_parameters["connectivity_event_1"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["connectivity_event_1"]["season"] = Season(node_parameters["connectivity_event_1"]["season"])
        assert_wrapper(node_parameters["connectivity_event_1"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(
            node_parameters["connectivity_event_1"]["amount_above_CTF"] >= 0, "amount_above_CTF", "must be >= 0"
        )

        # connectivity_event_2
        assert_wrapper(
            validation.valid_season(node_parameters["connectivity_event_2"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["connectivity_event_2"]["season"] = Season(node_parameters["connectivity_event_2"]["season"])
        assert_wrapper(
            node_parameters["connectivity_event_1"]["season"].start_day
            == node_parameters["connectivity_event_2"]["season"].start_day,
            "season",
            "event 1 and 2 must start on the same day",
        )
        assert_wrapper(
            node_parameters["connectivity_event_2"]["temperature_range"][0]
            <= node_parameters["connectivity_event_2"]["temperature_range"][1],
            "temperature_range",
            "lower bound must be <= upper bound",
        )
        assert_wrapper(node_parameters["connectivity_event_2"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(
            node_parameters["connectivity_event_2"]["amount_above_CTF"] >= 0, "amount_above_CTF", "must be >= 0"
        )

        # period without disturbance
        assert_wrapper(
            type(node_parameters["period_without_disturbance"]["use_default_data"]) is bool,
            "use_default_data",
            "must be a boolean",
        )
        if node_parameters["period_without_disturbance"]["use_ARI"]:
            assert_wrapper(node_parameters["period_without_disturbance"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(
                node_parameters["period_without_disturbance"]["flow_threshold"] > 0, "flow_threshold", "must be >= 0"
            )
        assert_wrapper(node_parameters["period_without_disturbance"]["duration"] > 0, "duration", "must be > 0")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        if node["parameters"]["period_without_disturbance"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        ts.set_moving_avg(TimeseriesDataTypes.TEMPERATURE, "temperature_avg", 7)
        temp_season_index_list = ts.get_data_type_in_range_index_list(
            "temperature_avg",
            node["connectivity_event_2"]["temperature_range"][0],
            node["connectivity_event_2"]["temperature_range"][1],
        )

        event_2_season_index_list = ts.get_season_index_list(node["connectivity_event_2"]["season"])
        event_1_season_index_list = ts.get_season_index_list(node["connectivity_event_1"]["season"])

        for index, day in enumerate(ts.data):
            day.init_result_value_keys(
                [
                    "in_connectivity_event_1_season",
                    "connectivity_event_1_depth_exceeded",
                    "connectivity_event_2_depth_exceeded",
                    "suitable_time_to_complete_period_without_disturbance",
                    "max_flow_remains_below_threshold_during_period_without_disturbance",
                ]
            )

            # Check if day is suitable for event
            if index not in event_1_season_index_list:  # Check day is in season
                day.set_result_value("success", 0)
                day.set_result_value("in_connectivity_event_1_season", 0)
                continue
            else:
                day.set_result_value("in_connectivity_event_1_season", 1)

            # Check if we have enough flow to start an event
            if day.get_value(TimeseriesDataTypes.DEPTH) < (
                node["connectivity_event_1"]["CTF"] + node["connectivity_event_1"]["amount_above_CTF"]
            ):
                day.set_result_value("connectivity_event_1_depth_exceeded", 0)
                day.set_result_value("success", 0)
                continue
            else:
                day.set_result_value("connectivity_event_1_depth_exceeded", 1)

            # Look for another day of success in the future
            continue_event = True
            check_index = index
            while continue_event:
                check_index += 1
                if check_index not in event_2_season_index_list:
                    continue_event = False
                else:
                    if check_index in temp_season_index_list and ts.data[check_index].get_value(
                        TimeseriesDataTypes.DEPTH
                    ) >= (node["connectivity_event_2"]["CTF"] + node["connectivity_event_2"]["amount_above_CTF"]):
                        break

            if continue_event is False:
                day.set_result_value("success", 0)
                day.set_result_value("connectivity_event_2_depth_exceeded", 0)
                continue
            else:
                day.set_result_value("connectivity_event_2_depth_exceeded", 1)

            # We now need to get the next dur # days to check for a flood
            if check_index + node["period_without_disturbance"]["duration"] >= len(ts.data):
                day.set_result_value("success", 0)
                day.set_result_value("suitable_time_to_complete_period_without_disturbance", 0)
                continue
            else:
                day.set_result_value("suitable_time_to_complete_period_without_disturbance", 1)

            # We have enough days so we can just extent our event list
            no_flood_period = ts.data[
                check_index + 1 : check_index + node["period_without_disturbance"]["duration"] + 1
            ]

            # Check for flood event
            max_flow = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, no_flood_period)

            # Check the period doesn't exceed the max flood flow
            if max_flow > node["period_without_disturbance"]["flow_threshold"]:
                day.set_result_value("success", 0)
                day.set_result_value("max_flow_remains_below_threshold_during_period_without_disturbance", 0)
            else:
                day.set_result_value("success", 1)
                day.set_result_value("max_flow_remains_below_threshold_during_period_without_disturbance", 1)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if node.parameters["period_without_disturbance"]["use_ARI"]:
            # Parse the multiyear flow and split by year, ready to calc flood flow
            if node.parameters["period_without_disturbance"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["period_without_disturbance"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["period_without_disturbance"]["ARI"]
            )

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "in_connectivity_event_1_season",
                "connectivity_event_1_depth_exceeded",
                "connectivity_event_2_depth_exceeded",
                "suitable_time_to_complete_period_without_disturbance",
                "max_flow_remains_below_threshold_during_period_without_disturbance",
                "success",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "in_connectivity_event_1_season",
                "connectivity_event_1_depth_exceeded",
                "connectivity_event_2_depth_exceeded",
                "suitable_time_to_complete_period_without_disturbance",
                "max_flow_remains_below_threshold_during_period_without_disturbance",
                "success",
            ],
        )
