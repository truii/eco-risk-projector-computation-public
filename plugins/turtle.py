from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class turtle(Plugin):
    def __init__(self):
        meta_data = {"name": "Turtle", "description": "Score Turtle", "version": "1.0.0", "author": "Zach Marsh"}
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # flow params
        assert_wrapper(
            type(node_parameters["flow_params"]["use_default_data"]) is bool, "use_default_data", "must be a boolean"
        )
        if node_parameters["flow_params"]["use_ARI"]:
            assert_wrapper(node_parameters["flow_params"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(node_parameters["flow_params"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")
        assert_wrapper(node_parameters["flow_params"]["drying_period"] >= 0, "drying_period", "must be >= 0")
        assert_wrapper(node_parameters["flow_params"]["aestivation_period"] >= 0, "aestivation_period", "must be >= 0")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if node["parameters"]["flow_params"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        dry_count = 0
        aestivation_days = 0

        for index, day in enumerate(ts.data):
            if day.get_value(TimeseriesDataTypes.FLOW) < node["flow_params"]["flow_threshold"]:
                dry_count += 1
                if dry_count > node["flow_params"]["drying_period"]:
                    day.set_result_value("success", 0.5)
                    aestivation_days += 1
                    if aestivation_days > node["flow_params"]["aestivation_period"]:
                        day.set_result_value("success", 0)
                else:
                    day.set_result_value("success", 1)

            else:
                dry_count = 0
                aestivation_days = 0
                day.set_result_value("success", 1)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if node.parameters["flow_params"]["use_ARI"]:
            # Parse the multiyear flow and split by year, ready to calc flood flow
            if node.parameters["flow_params"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["flow_params"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["flow_params"]["ARI"]
            )

        # Generate results
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_key = "success"

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(["date", "success"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "success"])
