from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from core.timeseries_day import TimeseriesDay
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class northern_snake_necked_turtle(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Northern Snake Necked Turtle",
            "description": "Score Northern Snake Necked Turtle spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True, "season", "must contain valid dates (day/month)"
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # flow_params
        assert_wrapper(
            validation.valid_season(node_parameters["flow_params"]["clutch_season"]) is True,
            "clutch_season",
            "must contain valid dates (day/month)",
        )
        node_parameters["flow_params"]["clutch_season"] = Season(node_parameters["flow_params"]["clutch_season"])
        assert_wrapper(
            type(node_parameters["flow_params"]["use_default_data"]) is bool, "use_default_data", "must be a boolean"
        )
        if node_parameters["flow_params"]["use_ARI"]:
            assert_wrapper(node_parameters["flow_params"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(node_parameters["flow_params"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")

        # egg_criteria
        assert_wrapper(
            node_parameters["egg_criteria"]["max_clutches_per_season"] >= 0, "max_clutches_per_season", "must be >= 0"
        )
        assert_wrapper(node_parameters["egg_criteria"]["incubation_period"] >= 0, "incubation_period", "must be >= 0")
        assert_wrapper(node_parameters["egg_criteria"]["max_diapause"] >= 0, "max_diapause", "must be >= 0")
        assert_wrapper(
            node_parameters["egg_criteria"]["time_between_clutches"] >= 0, "time_between_clutches", "must be >= 0"
        )
        assert_wrapper(
            node_parameters["egg_criteria"]["soil_moisture_drying"] >= 0, "soil_moisture_drying", "must be >= 0"
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if node["parameters"]["flow_params"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        seasonal_split_ts = ts.split_into_seasons(node["season"])

        for season_days in seasonal_split_ts:
            season_index_list = ts.get_season_index_list(node["flow_params"]["clutch_season"], data=season_days)

            prev_day_above_threshold = False
            time_since_last_clutch = 0
            clutch_attempts = 0
            for index, day in enumerate(season_days):
                day.init_result_value_keys(
                    [
                        "in_clutch_season",
                        "clutch_attempted",
                        "clutch_failure_reason",
                    ]
                )

                # Looking for first clutch - must be in season
                if clutch_attempts == 0:
                    # Check if were in season
                    if index in season_index_list:
                        day.set_result_value("in_clutch_season", 1)
                        # Were looking for flow that's been above the threshold and now dropped below
                        if (
                            day.get_value(TimeseriesDataTypes.FLOW) < node["flow_params"]["flow_threshold"]
                            and prev_day_above_threshold
                        ):
                            clutch_attempts += 1
                            time_since_last_clutch = 0

                            day.set_result_value("clutch_attempted", 1)
                            clutch_success, clutch_failure_reason = self.check_cluth_success(
                                node, ts, season_days[index:]
                            )
                            day.set_result_value("success", clutch_success)
                            day.set_result_value("clutch_failure_reason", clutch_failure_reason)

                            # Move to next day
                            continue
                        else:
                            day.set_result_value("success", 0)
                            day.set_result_value("clutch_attempted", 0)
                            if day.get_value(TimeseriesDataTypes.FLOW) >= node["flow_params"]["flow_threshold"]:
                                prev_day_above_threshold = True
                    else:
                        day.set_result_value("in_clutch_season", 0)
                        day.set_result_value("success", 0)

                else:
                    if (
                        clutch_attempts < node["egg_criteria"]["max_clutches_per_season"]
                        and time_since_last_clutch >= node["egg_criteria"]["time_between_clutches"]
                    ):
                        # Another clutch is layed
                        clutch_attempts += 1
                        time_since_last_clutch = 0

                        day.set_result_value("clutch_attempted", 1)
                        clutch_success, clutch_failure_reason = self.check_cluth_success(node, ts, season_days[index:])
                        day.set_result_value("success", clutch_success)
                        day.set_result_value("clutch_failure_reason", clutch_failure_reason)

                        # Move to next day
                        continue
                    else:
                        day.set_result_value("clutch_attempted", 0)
                        day.set_result_value("success", 0)

                time_since_last_clutch += 1

        return ts

    def check_cluth_success(self, node, ts: Timeseries, clutch_days: List[TimeseriesDay]):
        # Check diapause
        diapuase_count = 0
        total_days_count = 0
        check_index = 0

        for index, day in enumerate(clutch_days):
            check_index = index

            if day.get_value(TimeseriesDataTypes.FLOW) <= node["flow_params"]["flow_threshold"]:
                diapuase_count += 1
            else:
                diapuase_count = 0
            total_days_count += 1

            if diapuase_count >= node["egg_criteria"]["soil_moisture_drying"]:
                break

            if total_days_count >= node["egg_criteria"]["max_diapause"]:
                return 0, "Max diapause exceeded"

        if diapuase_count < node["egg_criteria"]["soil_moisture_drying"]:
            return 0, "Not enough days to check diapause"

        # Check incubation
        if check_index + 1 + node["egg_criteria"]["incubation_period"] >= len(clutch_days):
            return 0, "Not enough days to check incubation"

        incubation_period = clutch_days[check_index + 1 : check_index + 1 + node["egg_criteria"]["incubation_period"]]

        if (
            ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, incubation_period)
            >= node["flow_params"]["flow_threshold"]
        ):
            return 0, "Event occurs during incubation"

        # If we get here its a successful clutch
        return 1, None

    def ts_aggregation(self, node, ts: Timeseries):
        date_season = Season(start_month=calcs.get_month_number(node.assessment["yearly_agg"]["start_month"]))
        season_split_ts = ts.split_into_seasons(date_season, True)
        yearly_recruitment_result = []

        for year_days in season_split_ts:
            year = year_days[0].date.year

            number_of_successful_clutches = 0
            number_of_attempted_clutches = 0
            failure_reasons = []

            for day in year_days:
                number_of_successful_clutches += (
                    day.get_result_value("success") if day.get_result_value("success") is not None else 0
                )
                number_of_attempted_clutches += (
                    day.get_result_value("clutch_attempted")
                    if day.get_result_value("clutch_attempted") is not None
                    else 0
                )

                if day.get_result_value("clutch_failure_reason") is not None:
                    failure_reasons.append(day.get_result_value("clutch_failure_reason"))

            yearly_recruitment_result.append(
                {
                    "year": year,
                    "number_of_successful_clutches": number_of_successful_clutches,
                    "number_of_attempted_clutches": number_of_attempted_clutches,
                    "clutch_failure_reasons": ", ".join(failure_reasons),
                }
            )

        return yearly_recruitment_result

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if node.parameters["flow_params"]["use_ARI"]:
            # Parse the multiyear flow and split by year, ready to calc flood flow
            if node.parameters["flow_params"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["flow_params"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["flow_params"]["ARI"]
            )

        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get yearly intermediate results
        result.ts_agg = self.ts_aggregation(node, result.ts_gen)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(
            ["date", "in_clutch_season", "clutch_attempted", "clutch_failure_reason", "success"], [0]
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        return [result_daily, result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            ["node", "date", "in_clutch_season", "clutch_attempted", "clutch_failure_reason", "success"],
        )

        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "number_of_successful_clutches", "number_of_attempted_clutches", "clutch_failure_reasons"],
        )
