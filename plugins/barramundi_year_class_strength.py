from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *
import math


class barramundi_year_class_strength(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Barramundi Year Class Strength",
            "description": "Score Barramundi Year Class Strength",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["season"]["season"] = Season(node_parameters["season"]["season"])

        # constants
        assert_wrapper(
            validation.is_num(node_parameters["constants"]["a"]), "constants", "constant a must be a valid number"
        )
        assert_wrapper(
            validation.is_num(node_parameters["constants"]["b"]), "constants", "constant b must be a valid number"
        )

        # intermediate assessment
        assert_wrapper(
            validation.valid_categories(node_parameters["intermediate_assessment"]["annual_recruitment_strength"]),
            "annual_recruitment_strength",
            "must be valid categories",
        )
        assert_wrapper(
            validation.valid_categories(node_parameters["intermediate_assessment"]["temporal_risk"]),
            "temporal_risk",
            "must be valid categories",
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        seasonally_split_ts = ts.split_into_seasons(
            Season({"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}}), True
        )
        result_ts = []

        for year in seasonally_split_ts:
            summer_season = ts.trim_into_season(node["season"]["season"], True, year)
            total_flow = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, summer_season)

            try:
                ycs = node["constants"]["a"] * math.log10(total_flow) - node["constants"]["b"]
                strength = calcs.get_risk_category(
                    node["intermediate_assessment"]["annual_recruitment_strength"]["high"],
                    node["intermediate_assessment"]["annual_recruitment_strength"]["low"],
                    ycs,
                )

                success = 0
                strength_translated = None
                if strength == "high":
                    strength_translated = "strong"
                    success = 1
                elif strength == "low":
                    strength_translated = "poor"
                else:
                    strength_translated = strength

                result_ts.append(
                    {
                        "year": year[0].date.year,
                        "success": success,
                        "ycs": ycs,
                        "recruitment_strength": strength_translated,
                    }
                )

            # Taking the log of certain total_flow values can cause a math domain error (ValueError)
            except ValueError:
                result_ts.append(
                    {
                        "year": year[0].date.year,
                        "success": None,
                        "ycs": None,
                        "recruitment_strength": None,
                    }
                )

        return result_ts

    def ts_aggregation(self, node, ts):
        for index, year in enumerate(ts):
            check_index = index
            num_years = 0
            success_found = False

            while check_index >= 0:
                if (
                    ts[check_index]["recruitment_strength"] is not None
                    and ts[check_index]["recruitment_strength"] == "strong"
                ):
                    success_found = True
                    break

                num_years += 1
                check_index -= 1

            if success_found:
                if num_years >= node["intermediate_assessment"]["temporal_risk"]["high"]:
                    year["risk"] = "high"
                elif num_years <= node["intermediate_assessment"]["temporal_risk"]["low"]:
                    year["risk"] = "low"
                else:
                    year["risk"] = "moderate"

            else:
                year["risk"] = "high"

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results for each year
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Generate intermediate assessment results
        self.ts_aggregation(node.parameters, result.ts_gen)

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["success"]), result.ts_gen))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_gen)

        return [result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "ycs", "recruitment_strength", "risk"],
        )
