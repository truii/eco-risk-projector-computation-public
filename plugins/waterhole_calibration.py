from typing import List
from core.data_store import DataStore
from core.globals import (
    OtherDataTypes,
    ResultTypes,
    TimeseriesDataTypes,
    WaterholeCalibrationType,
    WaterholeFitStatisticType,
)
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper import genetic_algorithm as ga
from helper.exception_utils import *
import numpy as np


class waterhole_calibration(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Waterhole Calibration",
            "description": "Calibrate waterhole parameters",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Evaporation
        assert_wrapper(node_parameters["evaporation"]["scaling"] >= 0, "scaling", "must be >= 0")

        # Seepage
        assert_wrapper(node_parameters["seepage"]["rate"] >= 0, "rate", "must be >= 0")

        # Rainfall
        assert_wrapper(node_parameters["rainfall"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(node_parameters["rainfall"]["max_infiltration"] >= 0, "max_infiltration", "must be >= 0")

        # Ground Water
        assert_wrapper(node_parameters["groundwater"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["inflow"], 0, 100),
            "inflow",
            "must be between 0 and 100",
        )
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["loss_to_deep_drainage"], 0, 100),
            "loss_to_deep_drainage",
            "must be between 0 and 100",
        )
        assert_wrapper(
            (node_parameters["groundwater"]["inflow"] + node_parameters["groundwater"]["loss_to_deep_drainage"]) <= 100,
            "groundwater",
            "inflow % + loss to deep drainage % must be <= 100%",
        )

        # Extraction
        assert_wrapper(
            node_parameters["extraction"]["commence_pumping_depth"]
            >= node_parameters["extraction"]["cease_pumping_depth"],
            "commence_pumping_depth/cease_pumping_depth",
            "commence depth must be >= cease depth",
        )
        assert_wrapper(node_parameters["extraction"]["extraction_rate"] >= 0, "extraction_rate", "must be >= 0")
        assert_wrapper(node_parameters["extraction"]["max_annual_take"] >= 0, "max_annual_take", "must be >= 0")
        assert_wrapper(
            node_parameters["extraction"]["start_month"] >= 1 and node_parameters["extraction"]["start_month"] <= 12,
            "start_month",
            "must be a valid month",
        )
        assert_wrapper(node_parameters["extraction"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(validation.is_num(node_parameters["extraction"]["lag"], int), "lag", "must be an integer")

        # Optimisation
        try:
            node_parameters["optimisation"]["run_type"] = WaterholeCalibrationType(
                node_parameters["optimisation"]["run_type"]
            )
        except Exception:
            assert_wrapper(False, "run_type", "must be a valid run_type")

        if "season" in node_parameters["optimisation"]:
            assert_wrapper(
                validate_run_period(node_parameters["optimisation"]["season"]), "season", "must be a valid season"
            )

        # Optimise specific
        if node_parameters["optimisation"]["run_type"] == WaterholeCalibrationType.OPTIMISE:
            # Optimisation list
            allowed_optimisation_parameters = [
                "evaporation.scaling",
                "seepage.rate",
                "rainfall.area",
                "rainfall.max_infiltration",
                "groundwater.area",
                "groundwater.inflow",
                "groundwater.loss_to_deep_drainage",
            ]
            assert_wrapper(
                len(node_parameters["optimisation"]["predictors"]) > 0,
                "predictors",
                "must optimise at least 1 predictor",
            )
            for item in node_parameters["optimisation"]["predictors"]:
                assert_wrapper(item in allowed_optimisation_parameters, "predictors", "all predictors must be valid")

            # Optimisation params
            assert_wrapper(node_parameters["optimisation"]["population"] > 0, "population", "must be > 0")
            assert_wrapper(node_parameters["optimisation"]["generations"] > 0, "generations", "must be > 0")
            assert_wrapper(
                validation.value_in_range(node_parameters["optimisation"]["scaling"], 0, 100),
                "scaling",
                "must be between 0 and 100",
            )
            try:
                node_parameters["optimisation"]["fit_statistic"] = WaterholeFitStatisticType(
                    node_parameters["optimisation"]["fit_statistic"]
                )
            except Exception:
                assert_wrapper(False, "fit_statistic", "must be a valid fit_statistic")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.rainfall = data.try_import_data(node["data"], TimeseriesDataTypes.RAINFALL)

        data.evaporation = data.try_import_data(node["data"], TimeseriesDataTypes.EVAPORATION)

        data.bathymetry = data.try_import_data(node["data"], OtherDataTypes.BATHYMETRY)

        if (
            "depth" in node["data"]
            or node["parameters"]["optimisation"]["run_type"] == WaterholeCalibrationType.OPTIMISE
        ):
            data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries, bathymetry):
        # Setup general vars
        max_depth = bathymetry["volume"][-1][0]
        max_volume = bathymetry["volume"][-1][1]
        max_area = bathymetry["area"][-1][1]
        waterhole_volume = max_volume
        waterhole_depth = max_depth
        prev_month = ts.data[0].date.month if len(ts.data) > 0 else None
        GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        max_GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        annual_pump_take = 0

        for day in ts.data:
            # Reset annual pump take if new water year
            if day.date.month != prev_month:
                if day.date.month == node["extraction"]["start_month"]:
                    annual_pump_take = 0
                prev_month = day.date.month

            # Only analyse waterhole level if no flow, else top up to full depth
            if day.get_value(TimeseriesDataTypes.FLOW) <= node["extraction"]["CTF"]:
                # Calculate daily inflow and outflow, apply this to prev depth to get new depth
                waterhole_inflow_m3 = 0
                waterhole_outflow_m3 = 0
                GW_inflow_m3 = 0
                GW_outflow_m3 = 0

                # Get the current waterhole area for rainfall and evaporation calculations
                waterhole_surface_area = calcs.get_curve_value(waterhole_depth, bathymetry["area"])

                # Rainfall
                if day.has_value(TimeseriesDataTypes.RAINFALL) and day.get_value(TimeseriesDataTypes.RAINFALL) > 0:
                    # Direct Rainfall
                    waterhole_inflow_m3 += (
                        day.get_value(TimeseriesDataTypes.RAINFALL) * waterhole_surface_area
                    ) * 0.001

                    # Runoff Rainfall
                    if node["rainfall"]["area"] > waterhole_surface_area:
                        local_rainfall_area = node["rainfall"]["area"] - waterhole_surface_area

                        if day.get_value(TimeseriesDataTypes.RAINFALL) > node["rainfall"]["max_infiltration"]:
                            local_rainfall_above_infiltration = (
                                day.get_value(TimeseriesDataTypes.RAINFALL) - node["rainfall"]["max_infiltration"]
                            )
                            waterhole_inflow_m3 += (local_rainfall_above_infiltration * local_rainfall_area) * 0.001

                            GW_inflow_m3 += (node["rainfall"]["max_infiltration"] * local_rainfall_area) * 0.001
                        else:
                            GW_inflow_m3 += (day.get_value(TimeseriesDataTypes.RAINFALL) * local_rainfall_area) * 0.001

                    # GW Rainfall
                    if (
                        node["groundwater"]["area"] > waterhole_surface_area
                        and node["groundwater"]["area"] > node["rainfall"]["area"]
                    ):
                        GW_rainfall_area = node["groundwater"]["area"] - node["rainfall"]["area"]

                        if day.get_value(TimeseriesDataTypes.RAINFALL) > node["rainfall"]["max_infiltration"]:
                            GW_inflow_m3 += (node["rainfall"]["max_infiltration"] * GW_rainfall_area) * 0.001
                        else:
                            GW_inflow_m3 += (day.get_value(TimeseriesDataTypes.RAINFALL) * GW_rainfall_area) * 0.001

                # Evaporation
                evaporation_adjusted = 0
                if day.has_value(TimeseriesDataTypes.EVAPORATION):
                    evaporation_adjusted = day.get_value(TimeseriesDataTypes.EVAPORATION) * (
                        node["evaporation"]["scaling"] / 100
                    )
                    waterhole_outflow_m3 += (evaporation_adjusted * waterhole_surface_area) * 0.001

                # Seepage
                waterhole_outflow_m3 += (node["seepage"]["rate"] * waterhole_surface_area) * 0.001

                # Pumping
                if (
                    waterhole_depth <= node["extraction"]["commence_pumping_depth"]
                    and waterhole_depth >= node["extraction"]["cease_pumping_depth"]
                ):
                    if (annual_pump_take + node["extraction"]["extraction_rate"]) <= node["extraction"][
                        "max_annual_take"
                    ]:
                        waterhole_outflow_m3 += node["extraction"]["extraction_rate"] * 1000
                        annual_pump_take += node["extraction"]["extraction_rate"]

                # GW
                # To waterhole
                waterhole_inflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)
                GW_outflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)

                # To Deep Drainage
                GW_outflow_m3 += GW_volume * (node["groundwater"]["loss_to_deep_drainage"] / 100)

                # To Evaporation
                if GW_volume > (max_GW_volume * 0.8) and day.has_value(TimeseriesDataTypes.EVAPORATION):
                    if (node["groundwater"]["area"] - node["rainfall"]["area"]) > 0:
                        GW_outflow_m3 += (
                            evaporation_adjusted * 0.5 * node["groundwater"]["area"] - node["rainfall"]["area"]
                        ) * 0.001

                # Apply all volume changes
                waterhole_volume -= waterhole_outflow_m3
                waterhole_volume += waterhole_inflow_m3

                if waterhole_volume > max_volume:  # Cant exceed max volume/depth
                    waterhole_volume = max_volume
                elif waterhole_volume < 0:
                    waterhole_volume = 0

                waterhole_depth = calcs.get_curve_value(waterhole_volume, bathymetry["volume"], True)

                GW_volume -= GW_outflow_m3
                GW_volume += GW_inflow_m3

                if GW_volume > max_GW_volume:  # Cant exceed max volume/depth
                    GW_volume = max_GW_volume

            else:
                waterhole_depth = max_depth
                waterhole_volume = max_volume
                waterhole_surface_area = max_area
                GW_volume = max_GW_volume

            day.set_result_value("modelled_depth", round(waterhole_depth, 5))
            day.set_result_value("modelled_volume", round(waterhole_volume, 5))
            day.set_result_value("modelled_area", round(waterhole_surface_area, 5))
            day.set_result_value("modelled_GW_volume", round(GW_volume, 5))

        return ts

    def fitness_func(self, params, node):
        # Get params from genetic algorithm, match with what they are, generate dict to run model off, return the RMSE of the model
        data = node["optimisation"]["genetic_algorithm_data"]
        param_name_list = data["predictor_list"]
        params_to_run = {}
        for index, param in enumerate(params.tolist()):
            predictor = param_name_list[index]
            section = predictor[: predictor.find(".")]
            param_name = predictor[predictor.find(".") + 1 :]
            if section not in params_to_run:
                params_to_run[section] = {}
            params_to_run[section][param_name] = param

        # Check that all predictors are included - not all are optimised
        for predictor in data["vars_req_for_run"]:
            section = predictor[: predictor.find(".")]
            param_name = predictor[predictor.find(".") + 1 :]
            if section not in params_to_run:
                params_to_run[section] = {}
            if param_name not in params_to_run[section]:
                params_to_run[section][param_name] = node[section][param_name]

        ts = self.ts_generation(params_to_run, data["ts"], data["bathymetry"])

        # Calculate the fit statistic given in the params
        if node["optimisation"]["fit_statistic"] == WaterholeFitStatisticType.RMSE:
            rmse = ts.get_RMSE_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", data["bathymetry"]["height"][-1][0], 5
            )
            return rmse
        elif node["optimisation"]["fit_statistic"] == WaterholeFitStatisticType.R2:
            r2 = ts.get_R2_for_keys(TimeseriesDataTypes.DEPTH, "modelled_depth", data["bathymetry"]["height"][-1][0], 5)
            return 1 - r2
        elif node["optimisation"]["fit_statistic"] == WaterholeFitStatisticType.NNSE:
            nnse = ts.get_NNSE_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", data["bathymetry"]["height"][-1][0], 5
            )
            return 1 - nnse
        elif node["optimisation"]["fit_statistic"] == WaterholeFitStatisticType.PBIAS:
            pbias = ts.get_PBIAS_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", data["bathymetry"]["height"][-1][0], 5
            )
            return abs(pbias)
        elif node["optimisation"]["fit_statistic"] == WaterholeFitStatisticType.RSR:
            rsr = ts.get_RSR_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", data["bathymetry"]["height"][-1][0], 5
            )
            return rsr

        return 0

    def run(self, node: Node):
        ts = Timeseries(node.data, [TimeseriesDataTypes.DEPTH])
        result = PluginResult(node.name, ts)

        ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, node.parameters["extraction"]["lag"])

        if node.data.depth != None:
            depth_days = node.data.parse_csv_to_timeseries_days(TimeseriesDataTypes.DEPTH)
            ts.add_timeseries_days(depth_days, False)

        if "season" in node.parameters["optimisation"] and node.parameters["optimisation"]["season"] is not None:
            ts.data = ts.trim_into_run_period(node.parameters["optimisation"]["season"])

        bathymetry = {
            "volume": calcs.extract_curve(node.data.bathymetry, 0, 2, True, True),
            "area": calcs.extract_curve(node.data.bathymetry, 0, 3, True, True),
            "height": calcs.extract_curve(node.data.bathymetry, 0, 1, True, True),
        }

        if node.parameters["optimisation"]["run_type"] == WaterholeCalibrationType.OPTIMISE:
            # Setup genetic algorithm parameters
            algorithm_param = {
                "max_num_iteration": node.parameters["optimisation"]["generations"],
                "population_size": node.parameters["optimisation"]["population"],
                "mutation_probability": 0.1,
                "elit_ratio": 0.01,
                "crossover_probability": 0.5,
                "parents_portion": 0.3,
                "crossover_type": "uniform",
                "max_iteration_without_improv": None,
            }

            # Setup predictors to be optimised
            predictors = []
            predictor_list = node.parameters["optimisation"]["predictors"]
            varbound_list = []
            for predictor in predictor_list:
                section = predictor[: predictor.find(".")]
                param = predictor[predictor.find(".") + 1 :]
                predictors.append(node.parameters[section][param])

                # Get variable boundary (varbound) - 5% either side of the provided value. We also have to consider if this takes us out of the legal range for a value
                scaling = node.parameters["optimisation"]["scaling"] / 100

                predictor_varbound = [predictors[-1] * (1 - scaling), predictors[-1] * (1 + scaling)]
                if predictor == "evaporation.scaling":
                    if predictor_varbound[0] < 0:
                        predictor_varbound[0] = 0
                elif predictor == "groundwater.inflow" or predictor == "groundwater.loss_to_deep_drainage":
                    if predictor_varbound[1] > 100:
                        predictor_varbound[1] = 100
                    if predictor_varbound[0] < 0:
                        predictor_varbound[0] = 0

                varbound_list.append(predictor_varbound)

            node.parameters["optimisation"]["genetic_algorithm_data"] = {
                "ts": ts,
                "bathymetry": bathymetry,
                "predictor_list": predictor_list,
                "vars_req_for_run": [
                    "evaporation.scaling",
                    "seepage.rate",
                    "rainfall.area",
                    "rainfall.max_infiltration",
                    "groundwater.area",
                    "groundwater.inflow",
                    "groundwater.loss_to_deep_drainage",
                    "extraction.commence_pumping_depth",
                    "extraction.cease_pumping_depth",
                    "extraction.extraction_rate",
                    "extraction.max_annual_take",
                    "extraction.start_month",
                    "extraction.CTF",
                ],
            }
            varbound = np.array(varbound_list)
            model = ga.geneticalgorithm(
                node=node.parameters,
                function=self.fitness_func,
                dimension=len(varbound_list),
                variable_type="real",
                variable_boundaries=varbound,
                algorithm_parameters=algorithm_param,
                convergence_curve=False,
                progress_bar=False,
                function_timeout=100,
            )
            model.run()

            # Update the node with the optimised parameters
            optimised_predictors = {}
            solution = model.best_variable.tolist()
            for index, param in enumerate(solution):
                predictor = node.parameters["optimisation"]["genetic_algorithm_data"]["predictor_list"][index]
                section = predictor[: predictor.find(".")]
                param_name = predictor[predictor.find(".") + 1 :]
                node.parameters[section][param_name] = param

                if section not in optimised_predictors:
                    optimised_predictors[section] = {}
                optimised_predictors[section][param_name] = round(param, 3)

        # Generate daily depth
        result.ts_gen = self.ts_generation(node.parameters, ts, bathymetry)
        result.ts_agg = {}

        if ts.has_data_type(TimeseriesDataTypes.DEPTH):
            # Calculate RMSE for results
            result.ts_agg["rmse"] = ts.get_RMSE_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", bathymetry["height"][-1][0], 5
            )
            result.ts_agg["r2"] = ts.get_R2_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", bathymetry["height"][-1][0], 5
            )
            result.ts_agg["nnse"] = ts.get_NNSE_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", bathymetry["height"][-1][0], 5
            )
            result.ts_agg["pbias"] = ts.get_PBIAS_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", bathymetry["height"][-1][0], 5
            )
            result.ts_agg["rsr"] = ts.get_RSR_for_keys(
                TimeseriesDataTypes.DEPTH, "modelled_depth", bathymetry["height"][-1][0], 5
            )

        if node.parameters["optimisation"]["run_type"] == WaterholeCalibrationType.OPTIMISE:
            result.ts_agg["predictors"] = optimised_predictors

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "modelled_depth",
                "modelled_volume",
                "modelled_area",
                "modelled_GW_volume",
                "depth",
                "flow",
                "rainfall",
                "evaporation",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_parameters = NodeResult(result.node_name, ResultTypes.PARAMETER_RESULTS, result.ts_agg)

        return [result_daily, result_parameters]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "modelled_depth",
                "modelled_volume",
                "modelled_area",
                "modelled_GW_volume",
                "depth",
                "flow",
                "rainfall",
                "evaporation",
            ],
        )

        write_result(ResultTypes.PARAMETER_RESULTS)
