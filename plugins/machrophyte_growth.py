from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *
import math


class machrophyte_growth(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Machrophyte Growth",
            "description": "Score Machrophyte Growth",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Components
        assert_wrapper(len(node_parameters["components"]) > 0, "components", "must be > 0")
        for component in node_parameters["components"]:
            assert_wrapper(
                component["type"] == "growth" or component["type"] == "decay", "type", "must be either growth or decay"
            )
            assert_wrapper(len(component["name"]) > 0, "name", "component cannot have an empty name")

            # Curve
            assert_wrapper(
                validation.valid_curve(component["magnitude_curve"]) is True, "magnitude_curve", "must be a valid curve"
            )

    def load_data(self, node):
        data = []
        default_data = NodeDefaultData(node)

        for component in node["parameters"]["components"]:
            component_data = NodeData(node)
            component_data.flow = component_data.try_import_data(component["data"], "data")
            data.append(component_data)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Score each component for each day
        annual_score = []

        for index, day in enumerate(ts.data):
            new_total = 0
            previous_total = 0
            if index != 0:
                # Get prev day
                prev_day = ts.data[index - 1]

                # Check if new year
                if prev_day.date.year == day.date.year:
                    previous_total = prev_day.get_result_value("total")
                    new_total = prev_day.get_result_value("total")
                else:  # new year, save prev year total
                    annual_score.append({"year": prev_day.date.year, "total": prev_day.get_result_value("total")})

            # Get component scores
            growth_scores = []
            decay_scores = []
            for component in node["components"]:
                if day.has_value(component["id"]):
                    score = calcs.get_curve_value(day.get_value(component["id"]), component["magnitude_curve"])

                    if component["type"] == "decay":
                        decay_scores.append(score)
                    else:
                        growth_scores.append(score)

            # Combine scores
            growth_total = math.prod(growth_scores)
            decay_prop = sum(decay_scores)
            if decay_prop > 1:
                decay_prop = 1

            # Adjust total
            new_total += growth_total
            new_total -= new_total * decay_prop

            day.set_result_value("total", round(new_total, 5))
            day.set_result_value("change", round(new_total - previous_total, 5))

        return {"daily_res": ts, "annual_res": annual_score}

    def run(self, node: Node):
        ts = Timeseries()
        result = PluginResult(node.name, ts)

        # get velocity at each barrier and add to list
        for index, component in enumerate(node.parameters["components"]):
            component_id = component["type"] + "_" + component["name"]
            component["id"] = component_id
            component_days = node.data[index].parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, component_id)
            ts.add_timeseries_days(component_days)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        result.ts_agg = result.ts_gen["annual_res"]
        result.ts_gen = result.ts_gen["daily_res"]

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["total"]), result.ts_agg))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(["date", "total", "change"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        return [result_daily, result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "total", "change"])

        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "total"],
        )
