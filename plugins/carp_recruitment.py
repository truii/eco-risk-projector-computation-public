from typing import List
from core.data_store import DataStore
from core.globals import ComparisonOptions, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class carp_recruitment(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Carp Recruitment",
            "description": "Score Carp Recruitment",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # spawning_conditions
        assert_wrapper(
            type(node_parameters["spawning_conditions"]["use_default_data"]) is bool,
            "use_default_data",
            "must be a boolean",
        )
        if node_parameters["spawning_conditions"]["use_ARI"]:
            assert_wrapper(node_parameters["spawning_conditions"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(
                node_parameters["spawning_conditions"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0"
            )
        assert_wrapper(
            node_parameters["spawning_conditions"]["recruitment_period"] >= 0, "recruitment_period", "must be >= 0"
        )
        assert_wrapper(node_parameters["spawning_conditions"]["drying_time"] >= 0, "drying_time", "must be >= 0")

        # season
        if node_parameters["season"]["use_temperature"]:
            try:
                node_parameters["season"]["temperature_comparison"] = ComparisonOptions(
                    node_parameters["season"]["temperature_comparison"]
                )
            except Exception:
                raise InvalidParameterException("temperature_comparison", "comparison type is not valid")
            assert_wrapper(
                "temperature" in node_data,
                "temperature",
                "must provide temperature data if using temperature based season",
            )
            assert_wrapper(
                validation.is_num(node_parameters["season"]["temperature_threshold"]),
                "temperature_threshold",
                "must be a number",
            )
            assert_wrapper(
                type(node_parameters["season"]["use_temperature_moving_average"]) is bool,
                "use_temperature_moving_average",
                "must be a boolean",
            )
            if node_parameters["season"]["use_temperature_moving_average"]:
                assert_wrapper(
                    node_parameters["season"]["temperature_moving_average_duration"] >= 1,
                    "temperature_moving_average_duration",
                    "must be >= 1",
                )

        else:
            assert_wrapper(
                validation.valid_season(node_parameters["season"]["season"]) is True,
                "season",
                "must contain valid dates (day/month)",
            )
            node_parameters["season"]["season"] = Season(node_parameters["season"]["season"])
            assert_wrapper(
                type(node_parameters["season"]["allow_partial_seasons"]) is bool,
                "allow_partial_seasons",
                "must be a boolean",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        if TimeseriesDataTypes.TEMPERATURE in node["data"]:
            data.temperature = data.try_import_data(node["data"], TimeseriesDataTypes.TEMPERATURE)

        if node["parameters"]["spawning_conditions"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        if node["season"]["use_temperature"] is True:
            temperature_key = TimeseriesDataTypes.TEMPERATURE

            if node["season"]["use_temperature_moving_average"]:
                ts.set_moving_avg(
                    TimeseriesDataTypes.TEMPERATURE,
                    "temperature_avg",
                    node["season"]["temperature_moving_average_duration"],
                )
                temperature_key = "temperature_avg"

            if node["season"]["temperature_comparison"] is ComparisonOptions.GREATER_EQUAL:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, node["season"]["temperature_threshold"], None
                )
            else:
                season_index_list = ts.get_data_type_in_range_index_list(
                    temperature_key, None, node["season"]["temperature_threshold"]
                )

        else:
            season_index_list = ts.get_season_index_list(
                node["season"]["season"], node["season"]["allow_partial_seasons"]
            )

        for index, day in enumerate(ts.data):
            day.init_result_value_keys(
                [
                    "in_season",
                    "flow_threshold_exceeded",
                    "drying_time_not_exceeded",
                    "recruitment_days_exceeded",
                    "final_day_flow_threshold_exceeded",
                ]
            )

            if index not in season_index_list:
                day.set_result_value("success", 0)
                day.set_result_value("in_season", 0)
                continue
            else:
                day.set_result_value("in_season", 1)

            # Check if we have enough flow to start an event
            if day.get_value(TimeseriesDataTypes.FLOW) < node["spawning_conditions"]["flow_threshold"]:
                day.set_result_value("success", 0)
                day.set_result_value("flow_threshold_exceeded", 0)
                continue
            else:
                day.set_result_value("flow_threshold_exceeded", 1)

            # Look for event - Wait then check if movement is possible - without drying occuring
            dry_days = 0
            recruitment_days = 0
            for check_index, check_day in enumerate(ts.data[index:]):
                # Check drying
                if dry_days >= node["spawning_conditions"]["drying_time"]:
                    day.set_result_value("success", 0)
                    day.set_result_value("drying_time_not_exceeded", 0)
                    break
                elif check_day.get_value(TimeseriesDataTypes.FLOW) < node["spawning_conditions"]["flow_threshold"]:
                    dry_days += 1
                else:
                    dry_days = 0

                # Check movement
                if recruitment_days >= node["spawning_conditions"]["recruitment_period"]:
                    if check_day.get_value(TimeseriesDataTypes.FLOW) >= node["spawning_conditions"]["flow_threshold"]:
                        day.set_result_value("success", 1)
                        day.set_result_value("drying_time_not_exceeded", 1)
                        day.set_result_value("recruitment_days_exceeded", 1)
                        day.set_result_value("final_day_flow_threshold_exceeded", 1)
                        break
                    else:
                        day.set_result_value("success", 0)
                        day.set_result_value("recruitment_days_exceeded", 1)
                        day.set_result_value("final_day_flow_threshold_exceeded", 0)
                else:
                    recruitment_days += 1

            # If we haven't succeeded by this point were out of time
            if day.get_result_value("success") is None:
                day.set_result_value("drying_time_not_exceeded", 1)
                day.set_result_value("recruitment_days_exceeded", 0)
                day.set_result_value("success", 0)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if node.parameters["spawning_conditions"]["use_ARI"]:
            # Parse the multiyear flow and split by year, ready to calc flood flow
            if node.parameters["spawning_conditions"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["spawning_conditions"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["spawning_conditions"]["ARI"]
            )

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_daily = list(map(lambda d: (d.date, d.get_result_value("success")), ts.data))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(
            [
                "date",
                "in_season",
                "flow_threshold_exceeded",
                "drying_time_not_exceeded",
                "recruitment_days_exceeded",
                "final_day_flow_threshold_exceeded",
                "success",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "in_season",
                "flow_threshold_exceeded",
                "drying_time_not_exceeded",
                "recruitment_days_exceeded",
                "final_day_flow_threshold_exceeded",
                "success",
            ],
        )
