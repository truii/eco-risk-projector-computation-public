from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
import math


class barramundi_growth(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Barramundi Growth",
            "description": "Score Barramundi Growth spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # constants
        assert_wrapper(
            validation.dict_contains_keys(
                node_parameters["constants"],
                ["a", "b"],
                [["a", ["summer", "autumn", "winter", "spring"]], ["b", ["summer", "autumn", "winter", "spring"]]],
            ),
            "constants",
            "must contain all required constants",
        )
        # flow threshold
        assert_wrapper(node_parameters["flow_threshold"]["summer"] >= 0, "summer", "must be >= 0")
        assert_wrapper(node_parameters["flow_threshold"]["autumn"] >= 0, "autumn", "must be >= 0")
        assert_wrapper(node_parameters["flow_threshold"]["winter"] >= 0, "winter", "must be >= 0")
        assert_wrapper(node_parameters["flow_threshold"]["spring"] >= 0, "spring", "must be >= 0")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        for day in ts.data:
            season = day.get_season_for_day()
            threshold = node["flow_threshold"][season]

            if day.get_calculated_value("flow_sum") >= threshold:
                day.set_result_value(
                    "growth",
                    node["constants"]["a"][season]
                    + (node["constants"]["b"][season] * math.log(day.get_calculated_value("flow_sum"))),
                )
            else:
                day.set_result_value("growth", 0)

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Get weekly flow sum on each day
        ts.set_non_moving_sum(TimeseriesDataTypes.FLOW, "flow_sum", 7)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_key = "growth"
        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(["date", "growth"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "growth"])
