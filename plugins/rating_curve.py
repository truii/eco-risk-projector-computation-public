from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class rating_curve(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Rating Curve",
            "description": "Convert daily timeseries values from rating curve",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_YEARLY_DAILY
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Data
        try:
            node_parameters["data_type"] = TimeseriesDataTypes(node_parameters["data_type"])
        except Exception as e:
            raise InvalidParameterException("data_type", "data_type is not a valid TimeseriesDataType")

        # Curve Points
        assert_wrapper(
            validation.valid_curve(node_parameters["curve_points"]) is True, "curve_points", "must be a valid curve"
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        data_key = node["parameters"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        for day in ts.data:
            day.set_result_value(
                "value", calcs.get_curve_value(day.get_value(TimeseriesDataTypes.FLOW), node["curve_points"])
            )

        return ts

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_key = "value"

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(["date", "value"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        return [result_daily]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "value"])
