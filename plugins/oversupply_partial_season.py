from typing import List
from core.data_store import DataStore
from core.globals import OtherDataTypes, ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class oversupply_partial_season(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Oversupply Partial Season",
            "description": "Score oversupply occurances from flow data",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Season
        assert_wrapper(
            node_parameters["start_month"] >= 1 and node_parameters["start_month"] <= 12,
            "start_month",
            "must be between 1 and 12",
        )
        assert_wrapper(len(node_parameters["season"]) == 12, "season", "must contain a value for all 12 months")
        node_parameters["season"].sort()
        count = 1
        for season in node_parameters["season"]:
            assert_wrapper(season[0] == count, "season", "must be in ascending order")
            count += 1

        req_vars = ["mag", "dur"]
        req_climates = ["Drought", "Very Dry", "Dry", "Average", "Wet"]
        for climate in req_climates:
            assert_wrapper(climate in node_parameters["rules"].keys(), "rules", "must only contain valid climates")
        # Rules
        for climate, rule in node_parameters["rules"].items():
            assert_wrapper(validation.valid_rule(rule, req_vars), "rules", "must contain only valid rules")

        # Partial Table
        req_tables = ["dur"]
        for key in req_tables:
            assert_wrapper(
                validation.valid_curve(node_parameters["partial_tables"][key]) is True,
                "partial_tables",
                "must contain only valid curves",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        data.climate = data.try_import_data(node["data"], OtherDataTypes.CLIMATE)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        # Split by reporting year
        date_season = Season(start_month=node["start_month"])
        season_split_ts = ts.split_into_seasons(date_season)
        season_result_ts = []

        for season in season_split_ts:
            climates = ts.split_by_value_key(OtherDataTypes.CLIMATE, season)
            season_result_ts.append([])

            for climate_days in climates:
                climate = climate_days[0].get_value(OtherDataTypes.CLIMATE)
                rule = node["rules"][climate]

                for day in climate_days:
                    if day.get_value(TimeseriesDataTypes.FLOW) <= rule["mag"]:
                        day.set_result_value("mag_score", 1)
                    else:
                        day.set_result_value("mag_score", 0)

                    season_score = calcs.get_curve_value(day.date.month, node["season"]) / 100

                    day.set_result_value("season_score", season_score)
                    day.set_result_value(
                        "mag_score", day.get_result_value("mag_score") * day.get_result_value("season_score")
                    )

            season_result_ts[-1].append({"data": climate_days, "climate": climate})

        return season_result_ts

    def ts_aggregation(self, node, ts: Timeseries, seasonal_result_ts, node_name):
        seasonal_score = []

        for season in seasonal_result_ts:
            # Append season info
            seasonal_score.append({})
            seasonal_score[-1]["year"] = season[0]["data"][0].date.year
            seasonal_score[-1]["climates"] = []

            # Score each climate
            for climate_info in season:
                climate = climate_info["climate"]
                days = climate_info["data"]
                seasonal_score[-1]["climates"].append({})

                # Append climate info
                rule = node["rules"][climate]
                seasonal_score[-1]["climates"][-1]["climate"] = climate
                seasonal_score[-1]["climates"][-1]["rule"] = rule
                seasonal_score[-1]["climates"][-1]["prop"] = len(days) / sum(
                    (len(climate_days["data"])) for climate_days in season
                )

                # Calc and store climate scores
                event = []
                event_log = [{"start_date": None, "end_date": None}]
                for day in days:
                    if day.get_result_value("mag_score") > 0:
                        event.append((day.get_result_value("mag_score"), None))
                        if event_log[-1]["start_date"] is None:
                            event_log[-1]["start_date"] = day

                    elif event_log[-1]["end_date"] is None and event_log[-1]["start_date"] is not None:
                        event_log[-1]["end_date"] = day
                        event_log[-1]["duration"] = ts.get_number_of_days_between_days(
                            event_log[-1]["end_date"], event_log[-1]["start_date"]
                        )
                        event_log.append({"start_date": None, "end_date": None})

                if event_log[-1]["end_date"] is None and event_log[-1]["start_date"] is not None:
                    event_log[-1]["end_date"] = days[-1]
                    event_log[-1]["duration"] = ts.get_number_of_days_between_days(
                        event_log[-1]["end_date"], event_log[-1]["start_date"]
                    )

                seasonal_score[-1]["event_log"] = list(
                    map(
                        lambda e: {
                            **e,
                            "start_date": e["start_date"].format_date(),
                            "end_date": e["end_date"].format_date(),
                        },
                        filter(lambda entry: entry["start_date"] != None and entry["start_date"] != None, event_log),
                    )
                )

                event.sort(reverse=True)
                target_dur = calcs.round_to_positive_int((rule["dur"] / 100) * len(days))
                dur_score = max(len(event) / target_dur, 1) if len(event) > 0 else 0
                if len(event) < target_dur:
                    target_dur = len(event)

                seasonal_score[-1]["climates"][-1]["dur"] = (
                    calcs.get_curve_value(dur_score * 100, node["partial_tables"]["dur"]) / 100
                )
                seasonal_score[-1]["climates"][-1]["mag"] = calcs.get_dict_list_mean(event[0:target_dur], 0) or 0
                seasonal_score[-1]["climates"][-1]["summary"] = (
                    seasonal_score[-1]["climates"][-1]["dur"] * seasonal_score[-1]["climates"][-1]["mag"]
                )

            # Get season score by mean of climate score
            seasonal_score[-1]["dur"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "dur", "prop")
            seasonal_score[-1]["mag"] = calcs.get_weighted_average(seasonal_score[-1]["climates"], "mag", "prop")
            seasonal_score[-1]["summary"] = seasonal_score[-1]["dur"] * seasonal_score[-1]["mag"]

        return seasonal_score

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Setup ts for analysis
        climate_ts_days = node.data.parse_csv_to_timeseries_days(
            OtherDataTypes.CLIMATE, is_number_values=False, infill_with_previous_value=True
        )
        ts.add_timeseries_days(climate_ts_days)

        # Generate results for each day
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get summary results
        result.ts_agg = self.ts_aggregation(node.parameters, ts, result.ts_gen, node.name)

        # Set event log results
        for season in result.ts_agg:
            result.event_log.extend(season["event_log"])

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["summary"]), result.ts_agg))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        result_events = NodeResult(result.node_name, ResultTypes.EVENTS_INTERMEDIATE, result.event_log)

        return [result_yearly, result_events]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "mag", "dur", "summary"],
        )

        write_result(ResultTypes.EVENTS_INTERMEDIATE, ["node", "start_date", "end_date", "duration"])
