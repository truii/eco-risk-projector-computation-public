from typing import List
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class summary(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Summary",
            "description": "A variety of methods to summarise daily timeseries data",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Constants
        valid_options = ["mean", "median", "max", "min"]
        valid_summary = ["mean", "median", "max", "min", "1.5_year_ari", "2_year_ari", "5_year_ari", "10_year_ari"]

        # Data
        try:
            node_parameters["data"]["data_type"] = TimeseriesDataTypes(node_parameters["data"]["data_type"])
        except Exception as e:
            raise InvalidParameterException("data", "data_type is not a valid TimeseriesDataType")

        # Season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True, "season", "must contain valid dates (day/month)"
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # yearly summary statistics
        for option in node_parameters["yearly"]:
            assert_wrapper(option in valid_options, "yearly", "must be a valid option")

        # Total Summary
        for summary in node_parameters["summary"]:
            assert_wrapper(summary in valid_summary, "summary", "must be a valid summary option")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data_key = node["parameters"]["data"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        season_split_ts = ts.split_into_seasons(node["season"])

        results = []

        for season in season_split_ts:
            result = {"year": season[0].date.year}

            for measure in node["yearly"]:
                if measure == "mean":
                    result[measure] = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW, season)

                if measure == "median":
                    result[measure] = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW, season)

                if measure == "max":
                    result[measure] = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, season)

                if measure == "min":
                    result[measure] = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW, season)

            results.append(result)

        return results

    def calc_summary(self, node, ts: Timeseries):
        result = {}

        for measure in node["summary"]:
            if measure == "mean":
                result[measure] = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW)

            if measure == "median":
                result[measure] = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW)

            if measure == "max":
                result[measure] = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW)

            if measure == "min":
                result[measure] = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW)

            if measure == "1.5_year_ari":
                result[measure] = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 1.5)

            if measure == "2_year_ari":
                result[measure] = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 2)

            if measure == "5_year_ari":
                result[measure] = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 5)

            if measure == "10_year_ari":
                result[measure] = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 10)

        return [result]

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        result.ts_gen = self.ts_generation(node.parameters, ts)

        result.summary = self.calc_summary(node.parameters, ts)

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_gen)

        result_summary = NodeResult(result.node_name, ResultTypes.SUMMARY_INTERMEDIATE, result.summary)

        return [result_yearly, result_summary]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            [
                "node",
                "year",
                "mean",
                "median",
                "max",
                "min",
            ],
        )

        write_result(
            ResultTypes.SUMMARY_INTERMEDIATE,
            ["node", "mean", "median", "max", "min", "1.5_year_ari", "2_year_ari", "5_year_ari", "10_year_ari"],
        )
