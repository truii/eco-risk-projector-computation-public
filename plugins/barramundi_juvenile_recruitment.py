from typing import List
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *


class barramundi_juvenile_recruitment(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Barramundi Juvenile Recruitment",
            "description": "Score Barramundi Juvenile Recruitment spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # total flow volume
        assert_wrapper(
            validation.valid_season(node_parameters["total_flow_volume"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["total_flow_volume"]["season"] = Season(node_parameters["total_flow_volume"]["season"])
        assert_wrapper(node_parameters["total_flow_volume"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")

        # maximum_flows_timing
        assert_wrapper(
            validation.valid_season(node_parameters["maximum_flows_timing"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["maximum_flows_timing"]["season"] = Season(node_parameters["maximum_flows_timing"]["season"])

        # month_flow
        assert_wrapper(
            validation.valid_season(node_parameters["month_flow"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["month_flow"]["season"] = Season(node_parameters["month_flow"]["season"])
        assert_wrapper(node_parameters["month_flow"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0")
        assert_wrapper(node_parameters["month_flow"]["num_month_exceeding"] >= 0, "num_month_exceeding", "must be >= 0")

        # intermediate assessment
        assert_wrapper(
            validation.valid_categories(node_parameters["intermediate_assessment"]["annual_recruitment_strength"]),
            "annual_recruitment_strength",
            "must be valid categories",
        )
        assert_wrapper(
            validation.valid_categories(node_parameters["intermediate_assessment"]["temporal_risk"]),
            "temporal_risk",
            "must be valid categories",
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.flow = data.try_import_data(node["data"], TimeseriesDataTypes.FLOW)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        seasonally_split_ts = ts.split_into_seasons(
            Season({"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}}), True
        )
        result_ts = []

        for year in seasonally_split_ts:
            num_rule_successes = 0

            # Rule 1
            rule_1_season = ts.trim_into_season(node["total_flow_volume"]["season"], True, year)
            total_flow = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, rule_1_season)
            if total_flow >= node["total_flow_volume"]["flow_threshold"]:
                rule_1_success = 1
                num_rule_successes += 1
            else:
                rule_1_success = 0

            # Rule 2
            rule_2_success = 0
            max_flow = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, year)
            season_index_list = ts.get_season_index_list(node["maximum_flows_timing"]["season"], True, year)
            max_flow_day_index = list(map(lambda d: d.get_value(TimeseriesDataTypes.FLOW), year)).index(max_flow)
            if max_flow_day_index in season_index_list:
                rule_2_success = 1
                num_rule_successes += 1

            # Rule 3
            rule_3_season = ts.trim_into_season(node["month_flow"]["season"], True, year)
            monthly_ts = ts.sort_into_months(rule_3_season)
            num_successful_months = 0
            for month, days in monthly_ts.items():
                if ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, days) >= node["month_flow"]["flow_threshold"]:
                    num_successful_months += 1
            if num_successful_months >= node["month_flow"]["num_month_exceeding"]:
                rule_3_success = 1
                num_rule_successes += 1
            else:
                rule_3_success = 0

            # strength
            success = 0
            if num_rule_successes >= node["intermediate_assessment"]["annual_recruitment_strength"]["high"]:
                strength = "strong"
                success = 1
            elif num_rule_successes <= node["intermediate_assessment"]["annual_recruitment_strength"]["low"]:
                strength = "poor"
            else:
                strength = "moderate"

            result_ts.append(
                {
                    "year": year[0].date.year,
                    "success": success,
                    "rule_1": rule_1_success,
                    "rule_2": rule_2_success,
                    "rule_3": rule_3_success,
                    "recruitment_strength": strength,
                }
            )

        return result_ts

    def ts_aggregation(self, node, ts):
        for index, year in enumerate(ts):
            check_index = index
            num_years = 0
            success_found = False

            while check_index >= 0:
                if (
                    ts[check_index]["recruitment_strength"] is not None
                    and ts[check_index]["recruitment_strength"] == "strong"
                ):
                    success_found = True
                    break

                num_years += 1
                check_index -= 1

            if success_found:
                if num_years >= node["intermediate_assessment"]["temporal_risk"]["high"]:
                    year["risk"] = "high"
                elif num_years <= node["intermediate_assessment"]["temporal_risk"]["low"]:
                    year["risk"] = "low"
                else:
                    year["risk"] = "moderate"

            else:
                year["risk"] = "high"

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results for each year
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Generate intermediate assessment results
        self.ts_aggregation(node.parameters, result.ts_gen)

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["success"]), result.ts_gen))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_gen)

        return [result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "rule_1", "rule_2", "rule_3", "recruitment_strength", "risk"],
        )
