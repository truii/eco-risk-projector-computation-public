import copy
from typing import List
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
from core.node_data import NodeData


class baseflow_separation(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Baseflow Separation",
            "description": "Separate baseflow and quickflow from a given timeseries",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Data
        try:
            node_parameters["data"]["data_type"] = TimeseriesDataTypes(node_parameters["data"]["data_type"])
        except Exception as e:
            raise InvalidParameterException("data", "data_type is not a valid TimeseriesDataType")

        # Season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True, "season", "must contain valid dates (day/month)"
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # method
        assert_wrapper(
            validation.is_num(node_parameters["method"]["alpha"]), "method", "Alpha constant must be a number"
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data_key = node["parameters"]["data"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        return data, default_data

    def calc_filter_value(self, day_value, prev_day_value, prev_day_qf, alpha):
        value = (alpha * prev_day_qf) + (((1 + alpha) / 2) * (day_value - prev_day_value))
        return value

    def ts_generation(self, node, ts: Timeseries):
        # pad ts
        padded_ts_start = ts.data[1:31]
        padded_ts_start.reverse()

        padded_ts_end = ts.data[-2:-32:-1]

        padded_ts = copy.deepcopy(padded_ts_start) + copy.deepcopy(ts.data) + copy.deepcopy(padded_ts_end)

        alpha = node["method"]["alpha"]

        # Pass 1 - Forwards
        for index, day in enumerate(padded_ts):
            quickflow = day.get_value(TimeseriesDataTypes.FLOW)

            if index > 0:
                prev_day = padded_ts[index - 1]
                quickflow = self.calc_filter_value(
                    day.get_value(TimeseriesDataTypes.FLOW),
                    prev_day.get_value(TimeseriesDataTypes.FLOW),
                    prev_day.get_calculated_value("quickflow_pass_1"),
                    alpha,
                )

            day.set_calculated_value("quickflow_pass_1", quickflow)
            day.set_calculated_value(
                "baseflow_pass_1",
                day.get_value(TimeseriesDataTypes.FLOW) - quickflow
                if quickflow > 0
                else day.get_value(TimeseriesDataTypes.FLOW),
            )

        # Pass 2 - Backwards
        for index in range(len(padded_ts) - 1, -1, -1):
            day = padded_ts[index]
            quickflow = day.get_calculated_value("baseflow_pass_1")

            if index < len(padded_ts) - 1:
                prev_day = padded_ts[index + 1]
                quickflow = self.calc_filter_value(
                    day.get_calculated_value("baseflow_pass_1"),
                    prev_day.get_calculated_value("baseflow_pass_1"),
                    prev_day.get_calculated_value("quickflow_pass_2"),
                    alpha,
                )

            day.set_calculated_value("quickflow_pass_2", quickflow)
            day.set_calculated_value(
                "baseflow_pass_2",
                day.get_calculated_value("baseflow_pass_1") - quickflow
                if quickflow > 0
                else day.get_calculated_value("baseflow_pass_1"),
            )

        # Pass 3 - Forwards
        for index, day in enumerate(padded_ts):
            quickflow = day.get_calculated_value("baseflow_pass_2")

            if index > 0:
                prev_day = padded_ts[index - 1]
                quickflow = self.calc_filter_value(
                    day.get_calculated_value("baseflow_pass_2"),
                    prev_day.get_calculated_value("baseflow_pass_2"),
                    prev_day.get_calculated_value("quickflow_pass_3"),
                    alpha,
                )

            day.set_calculated_value("quickflow_pass_3", quickflow)
            day.set_calculated_value(
                "baseflow_pass_3",
                day.get_calculated_value("baseflow_pass_2") - quickflow
                if quickflow > 0
                else day.get_calculated_value("baseflow_pass_2"),
            )

        result_ts = padded_ts[30:-30]
        ts.set_timeseries(result_ts)
        return result_ts

    def ts_aggregation(self, node, ts: Timeseries):
        seasonal_split_ts = ts.split_into_seasons(node["season"], True)
        seasonal_results = []
        summary_results = {
            "baseflow_index": 0,
            "quickflow_index": 0,
            "mean_baseflow": 0,
            "mean_quickflow": 0,
        }

        for season_ts in seasonal_split_ts:
            baseflow_ts = list(
                map(
                    lambda d: d.get_calculated_value("baseflow_pass_3")
                    if d.get_calculated_value("baseflow_pass_3") > 0
                    else 0,
                    season_ts,
                )
            )
            flow_ts = list(map(lambda d: d.get_value(TimeseriesDataTypes.FLOW), season_ts))
            quickflow_ts = list(
                map(
                    lambda d: d.get_calculated_value("quickflow_pass_3")
                    if d.get_calculated_value("quickflow_pass_3") > 0
                    else 0,
                    season_ts,
                )
            )
            total_baseflow = sum(baseflow_ts)
            total_flow = sum(flow_ts)
            total_quickflow = sum(quickflow_ts)

            bfi = total_baseflow / total_flow if total_flow > 0 else 0
            qfi = total_quickflow / total_flow if total_flow > 0 else 0
            mean_baseflow = total_baseflow / len(season_ts) if len(season_ts) > 0 else 0
            mean_quickflow = total_quickflow / len(season_ts) if len(season_ts) > 0 else 0

            seasonal_results.append(
                {
                    "year": season_ts[0].date.year,
                    "baseflow_index": bfi,
                    "quickflow_index": qfi,
                    "mean_baseflow": mean_baseflow,
                    "mean_quickflow": mean_quickflow,
                }
            )

            summary_results["baseflow_index"] += bfi
            summary_results["quickflow_index"] += qfi
            summary_results["mean_baseflow"] += mean_baseflow
            summary_results["mean_quickflow"] += mean_quickflow

        summary_results["baseflow_index"] /= len(seasonal_split_ts)
        summary_results["quickflow_index"] /= len(seasonal_split_ts)
        summary_results["mean_baseflow"] /= len(seasonal_split_ts)
        summary_results["mean_quickflow"] /= len(seasonal_split_ts)

        return seasonal_results, [summary_results]

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Get summary results
        result.ts_agg, result.summary = self.ts_aggregation(node.parameters, ts)

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(
            [
                "date",
                "flow",
                "quickflow_pass_1",
                "baseflow_pass_1",
                "quickflow_pass_2",
                "baseflow_pass_2",
                "quickflow_pass_3",
                "baseflow_pass_3",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        result_summary = NodeResult(result.node_name, ResultTypes.SUMMARY_INTERMEDIATE, result.summary)

        return [result_daily, result_yearly, result_summary]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            [
                "node",
                "date",
                "flow",
                "quickflow_pass_1",
                "baseflow_pass_1",
                "quickflow_pass_2",
                "baseflow_pass_2",
                "quickflow_pass_3",
                "baseflow_pass_3",
            ],
        )

        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "baseflow_index", "quickflow_index", "mean_baseflow", "mean_quickflow"],
        )

        write_result(
            ResultTypes.SUMMARY_INTERMEDIATE,
            ["node", "baseflow_index", "quickflow_index", "mean_baseflow", "mean_quickflow"],
        )
