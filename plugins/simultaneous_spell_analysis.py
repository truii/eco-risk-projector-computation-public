from datetime import datetime, timezone
import statistics
from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes, SpellAnalysisTypes
from core.node import Node
from core.node_default_data import NodeDefaultData
from core.node_result import NodeResult
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.season import Season
from core.timeseries import Timeseries
from core.timeseries_day import TimeseriesDay
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
from core.node_data import NodeData


class simultaneous_spell_analysis(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Simultaneous Spell Analysis",
            "description": "Calculate simultaneous spell analysis metrics",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Data
        try:
            node_parameters["data"]["data_type"] = TimeseriesDataTypes(node_parameters["data"]["data_type"])
        except Exception as e:
            raise InvalidParameterException("data", "data_type is not a valid TimeseriesDataType")

        # Season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True, "season", "must contain valid dates (day/month)"
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # event
        try:
            node_parameters["event"]["type"] = SpellAnalysisTypes(node_parameters["event"]["type"])
        except Exception as e:
            raise InvalidParameterException("event", "type is not a valid SpellAnalysisType")

        # Thresholds
        assert_wrapper(
            type(node_parameters["event"]["upper_threshold"]["use_default_data"]) is bool,
            "use_default_data",
            "must be a boolean",
        )
        if node_parameters["event"]["upper_threshold"]["use_ARI"]:
            assert_wrapper(node_parameters["event"]["upper_threshold"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(
                node_parameters["event"]["upper_threshold"]["flow_threshold"] > 0, "flow_threshold", "must be >= 0"
            )

        if node_parameters["event"]["type"] is SpellAnalysisTypes.RANGE:
            assert_wrapper(
                type(node_parameters["event"]["lower_threshold"]["use_default_data"]) is bool,
                "use_default_data",
                "must be a boolean",
            )
            if node_parameters["event"]["lower_threshold"]["use_ARI"]:
                assert_wrapper(node_parameters["event"]["lower_threshold"]["ARI"] > 0, "ARI", "must be > 0")
            else:
                assert_wrapper(
                    node_parameters["event"]["lower_threshold"]["flow_threshold"] > 0, "flow_threshold", "must be >= 0"
                )

        # Length + Independence
        if node_parameters["event"]["max_spell_length"]["should_consider"]:
            assert_wrapper(
                validation.is_num(node_parameters["event"]["max_spell_length"]["value"]),
                "max_spell_length",
                "Value must be a number",
            )
            assert_wrapper(
                node_parameters["event"]["max_spell_length"]["value"] >= 0, "max_spell_length", "Value must be >= 0"
            )
        if node_parameters["event"]["min_spell_length"]["should_consider"]:
            assert_wrapper(
                validation.is_num(node_parameters["event"]["min_spell_length"]["value"]),
                "min_spell_length",
                "Value must be a number",
            )
            assert_wrapper(
                node_parameters["event"]["min_spell_length"]["value"] >= 0, "min_spell_length", "Value must be >= 0"
            )
        assert_wrapper(node_parameters["event"]["independence"] >= 0, "independence", "Must be >= 0")
        assert_wrapper(validation.is_num(node_parameters["event"]["lag"], int), "lag", "Must be an integer")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data_key = node["parameters"]["data"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        if node["parameters"]["event"]["upper_threshold"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], data_key)
        elif (
            node["parameters"]["event"]["type"] is SpellAnalysisTypes.RANGE
            and node["parameters"]["event"]["lower_threshold"]["use_default_data"]
        ):
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], data_key)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries, node_name):
        event_opportunities = []
        in_event = False
        event_key = node_name + "_event"

        # Find all possible events
        for index, day in enumerate(ts.data):
            day.set_calculated_value("index", index)
            day.set_calculated_value(node_name + "_flow", day.get_value(TimeseriesDataTypes.FLOW))

            day_meets_threshold = False
            if node["event"]["type"] is SpellAnalysisTypes.ABOVE_THRESHOLD:
                if day.get_value(TimeseriesDataTypes.FLOW) >= node["event"]["upper_threshold"]["flow_threshold"]:
                    day_meets_threshold = True
            elif node["event"]["type"] is SpellAnalysisTypes.BELOW_THRESHOLD:
                if day.get_value(TimeseriesDataTypes.FLOW) <= node["event"]["upper_threshold"]["flow_threshold"]:
                    day_meets_threshold = True
            elif node["event"]["type"] is SpellAnalysisTypes.RANGE:
                if (
                    day.get_value(TimeseriesDataTypes.FLOW) <= node["event"]["upper_threshold"]["flow_threshold"]
                    and day.get_value(TimeseriesDataTypes.FLOW) >= node["event"]["lower_threshold"]["flow_threshold"]
                ):
                    day_meets_threshold = True

            if day_meets_threshold:
                day.set_result_value(event_key, 1)

                if not in_event:
                    event_opportunities.append([])
                    in_event = True

                event_opportunities[-1].append(day)
            else:
                day.set_result_value(event_key, 0)
                in_event = False

        events_to_remove_indexes = []
        events = []
        # Remove events that dont meet min/max
        for event in event_opportunities:
            if node["event"]["min_spell_length"]["should_consider"]:
                if len(event) < node["event"]["min_spell_length"]["value"]:
                    events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                    continue

            if node["event"]["max_spell_length"]["should_consider"]:
                if len(event) > node["event"]["max_spell_length"]["value"]:
                    events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                    continue

            events.append(event)

        for index in events_to_remove_indexes:
            ts.data[index].set_result_value("event", 0)

        # Remove events that violate independence
        events_to_remove_indexes = []
        check_pos = 0
        while check_pos < len(events):
            if (check_pos + 1) < len(events):
                event = events[check_pos]
                next_event = events[check_pos + 1]

                diff = ts.get_number_of_days_between_days(next_event[0], event[-1])

                if diff < node["event"]["independence"]:
                    if len(event) > len(next_event):
                        events.pop(check_pos + 1)
                        events_to_remove_indexes.extend(
                            list(map(lambda d: d.get_calculated_value("index"), next_event))
                        )
                        check_pos = 0
                        continue
                    else:
                        events.pop(check_pos)
                        events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                        check_pos = 0
                        continue
            check_pos += 1

        for index in events_to_remove_indexes:
            ts.data[index].set_result_value("event", 0)

        # season_split_ts = ts.split_into_seasons(node["season"], True)
        # season_ts_flat = []
        # for year in season_split_ts:
        #     season_ts_flat.extend(year)

        return ts

    def get_simultaneous_events(self, ts, event_keys):
        events = []
        in_event = False

        for day in ts:
            day_event_success_list = list(map(lambda k: day.get_result_value(k), event_keys))

            if len(list(filter(lambda i: i == 1, day_event_success_list))) == len(day_event_success_list):
                if not in_event:
                    events.append([])
                    in_event = True

                events[-1].append(day)
            else:
                in_event = False

        return events

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, node.parameters["event"]["lag"])

        # Calculate ARI's
        if node.parameters["event"]["upper_threshold"]["use_ARI"]:
            if node.parameters["event"]["upper_threshold"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["event"]["upper_threshold"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["event"]["upper_threshold"]["ARI"]
            )

        if node.parameters["event"]["type"] is SpellAnalysisTypes.RANGE:
            if node.parameters["event"]["lower_threshold"]["use_ARI"]:
                if node.parameters["event"]["lower_threshold"]["use_default_data"]:
                    ari_ts = Timeseries(node.default_data)
                else:
                    ari_ts = ts

                # Calculate the flood level here as it's constant for the whole period
                node.parameters["event"]["lower_threshold"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                    TimeseriesDataTypes.FLOW, node.parameters["event"]["lower_threshold"]["ARI"]
                )

        # Generate results
        data = self.ts_generation(node.parameters, ts, node.name)
        result.ts_gen = list(
            map(
                lambda d: {
                    "date": d.format_date(),
                    "flow": d.get_calculated_value(node.name + "_flow"),
                    "event": d.get_result_value(node.name + "_event"),
                },
                data.data,
            )
        )

        return result

    def ts_aggregation(self, node, ts: Timeseries, event_keys, flow_keys):
        yearly_sorted_ts = ts.sort_into_years()
        events = self.get_simultaneous_events(ts.data, event_keys)

        results = []

        for year, year_days in yearly_sorted_ts.items():
            yearly_events = self.get_simultaneous_events(year_days, event_keys)

            result = {
                "year": year,
            }

            result["events_summary"] = list(
                map(
                    lambda e: {"start_date": e[0].format_date(), "end_date": e[-1].format_date(), "duration": len(e)},
                    yearly_events,
                )
            )

            # Generate result for each metric
            if len(yearly_events) > 0:
                result["count"] = len(yearly_events)
                total_magnitude_sum = 0
                for event in yearly_events:
                    node_peaks = []
                    for key in flow_keys:
                        node_peaks.append(ts.get_data_type_value_max(key, event))
                    total_magnitude_sum += sum(node_peaks) / len(node_peaks)

                result["mean_magnitude_of_peaks"] = round(total_magnitude_sum / len(yearly_events), 3)
                result["total_duration"] = sum(map(lambda e: len(e), yearly_events))
                result["mean_duration"] = round(sum(map(lambda e: len(e), yearly_events)) / len(yearly_events), 3)
                result["single_longest"] = max(map(lambda e: len(e), yearly_events))

                days_betwen_events = []
                for index, event in enumerate(yearly_events):
                    if index + 1 < len(yearly_events):
                        next_event = yearly_events[index + 1]
                        diff = ts.get_number_of_days_between_days(next_event[0], event[-1]) - 1
                        days_betwen_events.append(diff)

                if len(days_betwen_events) > 0:
                    result["total_duration_between_spells"] = sum(days_betwen_events)
                    result["mean_duration_between_spells"] = round(statistics.mean(days_betwen_events), 3)
                    result["single_longest_between_spells"] = max(days_betwen_events)
                else:
                    result["total_duration_between_spells"] = 0
                    result["mean_duration_between_spells"] = 0
                    result["single_longest_between_spells"] = 0

            else:
                result["count"] = 0
                result["mean_magnitude_of_peaks"] = 0
                result["total_duration"] = 0
                result["mean_duration"] = 0
                result["single_longest"] = 0
                result["total_duration_between_spells"] = 0
                result["mean_duration_between_spells"] = 0
                result["single_longest_between_spells"] = 0

            results.append(result)

        return results, events

    def compute_spatial(self, params, data_store: DataStore) -> None:
        daily_node_data = []

        if data_store is not None:
            node_results = data_store.get_results(ResultTypes.CUSTOM_RESULTS)
            for node in node_results:
                daily_node_data.append({"node": node.node, "data": node.data})

        if len(daily_node_data) > 0:
            event_keys = []
            flow_keys = []
            combined_ts = Timeseries()

            for node in daily_node_data:
                event_key = node["node"] + "_event"
                flow_key = node["node"] + "_flow"
                event_keys.append(event_key)
                flow_keys.append(flow_key)
                timeseries_days = list(
                    map(
                        lambda d: self.parse_to_timeseries_day(node["node"], d["date"], d["flow"], d["event"]),
                        node["data"],
                    )
                )
                combined_ts.add_timeseries_days(timeseries_days)

            for day in combined_ts.data:
                event_scores = list(
                    map(lambda k: day.get_result_value(k) if day.get_result_value(k) is not None else 0, event_keys)
                )
                sum_event_scores = sum(event_scores)
                day.set_result_value("event_sum", sum_event_scores)
                day.set_result_value("event_success", 1 if sum_event_scores == len(event_scores) else 0)

            yearly_spatial, events_spatial = self.ts_aggregation(None, combined_ts, event_keys, flow_keys)
            summary_spatial = self.calc_event_summary(combined_ts, events_spatial)
            spatial_result = {
                "daily": combined_ts,
                "yearly": yearly_spatial,
                "events": list(
                    map(
                        lambda e: {
                            "start_date": e[0].format_date(),
                            "end_date": e[-1].format_date(),
                            "duration": len(e),
                        },
                        events_spatial,
                    )
                ),
                "summary": summary_spatial,
                "event_keys": event_keys,
                "flow_keys": flow_keys,
            }
        else:
            spatial_result = {"daily": [], "yearly": [], "events": [], "summary": [], "event_keys": [], "flow_keys": []}

        self.store_spatial_results(data_store, spatial_result)

    def parse_to_timeseries_day(self, node, date_string, flow, event) -> TimeseriesDay:
        date = (
            datetime.strptime(date_string, "%d/%m/%Y")
            .replace(tzinfo=timezone.utc)
            .replace(hour=0, minute=0, second=0, microsecond=0)
        )
        day = TimeseriesDay(date)
        day.set_calculated_value(node + "_flow", flow)
        day.set_result_value(node + "_event", event)

        return day

    def store_spatial_results(self, data_store: DataStore, spatial_result) -> None:
        daily_headers = ["date"]
        for key in spatial_result["flow_keys"]:
            daily_headers.append(key)
        for key in spatial_result["event_keys"]:
            daily_headers.append(key)
        daily_headers.append("event_sum")
        daily_headers.append("event_success")
        daily_spatial_intermediate_result_data = spatial_result["daily"].convert_to_result_data(daily_headers, [0])
        daily_spatial_intermediate_result = NodeResult(
            "all", ResultTypes.DAILY_SPATIAL_INTERMEDIATE, daily_spatial_intermediate_result_data
        )
        data_store.create_result(daily_spatial_intermediate_result)

        yearly_spatial_intermediate_result = NodeResult(
            "all", ResultTypes.YEARLY_SPATIAL_INTERMEDIATE, spatial_result["yearly"]
        )
        data_store.create_result(yearly_spatial_intermediate_result)

        events_spatial_intermediate_result = NodeResult(
            "all", ResultTypes.EVENTS_SPATIAL_INTERMEDIATE, spatial_result["events"]
        )
        data_store.create_result(events_spatial_intermediate_result)

        summary_spatial_intermediate_result = NodeResult(
            "all", ResultTypes.SUMMARY_SPATIAL_INTERMEDIATE, spatial_result["summary"]
        )
        data_store.create_result(summary_spatial_intermediate_result)

    def calc_event_summary(self, ts: Timeseries, events):
        summary_results = {}

        if len(events) > 0:
            event_lengths = list(map(lambda x: len(x), events))

            days_betwen_events = []
            for index, event in enumerate(events):
                if index + 1 < len(events):
                    next_event = events[index + 1]
                    diff = ts.get_number_of_days_between_days(next_event[0], event[-1]) - 1
                    days_betwen_events.append(diff)

            summary_results.update(
                {
                    "total_duration": sum(event_lengths),
                    "mean_duration": sum(event_lengths) / len(events),
                    "max_duration": max(event_lengths),
                    "mean_duration_between_spells": sum(days_betwen_events) / len(days_betwen_events)
                    if len(days_betwen_events) > 0
                    else 0,
                    "max_duration_between_spells": max(days_betwen_events) if len(days_betwen_events) > 0 else 0,
                }
            )

        return [summary_results]

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_custom = NodeResult(result.node_name, ResultTypes.CUSTOM_RESULTS, result.ts_gen)

        return [result_custom]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_SPATIAL_INTERMEDIATE)
        write_result(
            ResultTypes.YEARLY_SPATIAL_INTERMEDIATE,
            [
                "year",
                "count",
                "mean_magnitude_of_peaks",
                "total_duration",
                "mean_duration",
                "single_longest",
                "total_duration_between_spells",
                "mean_duration_between_spells",
                "single_longest_between_spells",
            ],
        )
        write_result(ResultTypes.EVENTS_SPATIAL_INTERMEDIATE, ["start_date", "end_date", "duration"])
        write_result(
            ResultTypes.SUMMARY_SPATIAL_INTERMEDIATE,
            [
                "total_duration",
                "mean_duration",
                "max_duration",
                "mean_duration_between_spells",
                "max_duration_between_spells",
            ],
        )
