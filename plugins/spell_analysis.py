import statistics
from typing import List
from core.globals import ResultTypes, SpellAnalysisTypes, SpellMetrics, TimeseriesDataTypes
from core.node import Node
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import validation as validation
from helper.exception_utils import *
from core.node_data import NodeData


class spell_analysis(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Spell Analysis",
            "description": "Calculate spell analysis metrics",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL_CONVERSION
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Data
        try:
            node_parameters["data"]["data_type"] = TimeseriesDataTypes(node_parameters["data"]["data_type"])
        except Exception as e:
            raise InvalidParameterException("data", "data_type is not a valid TimeseriesDataType")

        # Season
        assert_wrapper(
            validation.valid_season(node_parameters["season"]) is True, "season", "must contain valid dates (day/month)"
        )
        node_parameters["season"] = Season(node_parameters["season"])

        # event
        try:
            node_parameters["event"]["type"] = SpellAnalysisTypes(node_parameters["event"]["type"])
        except Exception as e:
            raise InvalidParameterException("event", "type is not a valid SpellAnalysisType")

        # Thresholds
        assert_wrapper(
            type(node_parameters["event"]["upper_threshold"]["use_default_data"]) is bool,
            "use_default_data",
            "must be a boolean",
        )
        if node_parameters["event"]["upper_threshold"]["use_ARI"]:
            assert_wrapper(node_parameters["event"]["upper_threshold"]["ARI"] > 0, "ARI", "must be > 0")
        else:
            assert_wrapper(
                node_parameters["event"]["upper_threshold"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0"
            )

        if node_parameters["event"]["type"] is SpellAnalysisTypes.RANGE:
            assert_wrapper(
                type(node_parameters["event"]["lower_threshold"]["use_default_data"]) is bool,
                "use_default_data",
                "must be a boolean",
            )
            if node_parameters["event"]["lower_threshold"]["use_ARI"]:
                assert_wrapper(node_parameters["event"]["lower_threshold"]["ARI"] > 0, "ARI", "must be > 0")
            else:
                assert_wrapper(
                    node_parameters["event"]["lower_threshold"]["flow_threshold"] >= 0, "flow_threshold", "must be >= 0"
                )

        # Length + Independence
        if node_parameters["event"]["max_spell_length"]["should_consider"]:
            assert_wrapper(
                validation.is_num(node_parameters["event"]["max_spell_length"]["value"]),
                "max_spell_length",
                "Value must be a number",
            )
            assert_wrapper(
                node_parameters["event"]["max_spell_length"]["value"] >= 0, "max_spell_length", "Value must be >= 0"
            )
        if node_parameters["event"]["min_spell_length"]["should_consider"]:
            assert_wrapper(
                validation.is_num(node_parameters["event"]["min_spell_length"]["value"]),
                "min_spell_length",
                "Value must be a number",
            )
            assert_wrapper(
                node_parameters["event"]["min_spell_length"]["value"] >= 0, "min_spell_length", "Value must be >= 0"
            )
        assert_wrapper(node_parameters["event"]["independence"] >= 0, "independence", "Must be >= 0")

        # event assessment
        try:
            node_parameters["event_assessment"]["assessment_metric"] = SpellMetrics(
                node_parameters["event_assessment"]["assessment_metric"]
            )
        except Exception as e:
            raise InvalidParameterException("assessment_metric", "assessment_metric is not a valid metric")

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data_key = node["parameters"]["data"]["data_type"]

        data.flow = data.try_import_data(node["data"], data_key)

        if node["parameters"]["event"]["upper_threshold"]["use_default_data"]:
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], data_key)
        elif (
            node["parameters"]["event"]["type"] is SpellAnalysisTypes.RANGE
            and node["parameters"]["event"]["lower_threshold"]["use_default_data"]
        ):
            node["default_data"] = data.handle_null_paths(node["default_data"])
            default_data.flow = default_data.try_import_data(node["default_data"], data_key)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        event_opportunities = []
        in_event = False

        # Find all possible events
        for index, day in enumerate(ts.data):
            day.set_calculated_value("index", index)

            day_meets_threshold = False
            if node["event"]["type"] is SpellAnalysisTypes.ABOVE_THRESHOLD:
                if day.get_value(TimeseriesDataTypes.FLOW) >= node["event"]["upper_threshold"]["flow_threshold"]:
                    day_meets_threshold = True

            elif node["event"]["type"] is SpellAnalysisTypes.BELOW_THRESHOLD:
                if day.get_value(TimeseriesDataTypes.FLOW) <= node["event"]["upper_threshold"]["flow_threshold"]:
                    day_meets_threshold = True

            elif node["event"]["type"] is SpellAnalysisTypes.RANGE:
                if (
                    day.get_value(TimeseriesDataTypes.FLOW) <= node["event"]["upper_threshold"]["flow_threshold"]
                    and day.get_value(TimeseriesDataTypes.FLOW) >= node["event"]["lower_threshold"]["flow_threshold"]
                ):
                    day_meets_threshold = True

            if day_meets_threshold:
                day.set_result_value("event", 1)

                if not in_event:
                    event_opportunities.append([])
                    in_event = True

                event_opportunities[-1].append(day)
            else:
                day.set_result_value("event", 0)
                in_event = False

        events_to_remove_indexes = []
        events = []
        # Remove events that dont meet min/max
        for event in event_opportunities:
            if node["event"]["min_spell_length"]["should_consider"]:
                if len(event) < node["event"]["min_spell_length"]["value"]:
                    events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                    continue

            if node["event"]["max_spell_length"]["should_consider"]:
                if len(event) > node["event"]["max_spell_length"]["value"]:
                    events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                    continue

            events.append(event)

        for index in events_to_remove_indexes:
            ts.data[index].set_result_value("event", 0)

        # Remove events that violate independence
        events_to_remove_indexes = []
        check_pos = 0
        while check_pos < len(events):
            if (check_pos + 1) < len(events):
                event = events[check_pos]
                next_event = events[check_pos + 1]

                diff = ts.get_number_of_days_between_days(next_event[0], event[-1])

                if diff < node["event"]["independence"]:
                    if len(event) > len(next_event):
                        events.pop(check_pos + 1)
                        events_to_remove_indexes.extend(
                            list(map(lambda d: d.get_calculated_value("index"), next_event))
                        )
                        check_pos = 0
                        continue
                    else:
                        events.pop(check_pos)
                        events_to_remove_indexes.extend(list(map(lambda d: d.get_calculated_value("index"), event)))
                        check_pos = 0
                        continue
            check_pos += 1

        for index in events_to_remove_indexes:
            ts.data[index].set_result_value("event", 0)

        # Split data into seasons, include overlapping events in the previous season
        season_split_ts = ts.split_into_seasons(node["season"], True)
        season_split_data = []
        for year_days in season_split_ts:
            year_events = []
            in_event = False

            for day in year_days:
                if day.get_result_value("event") == 1:
                    if not in_event:
                        year_events.append([])
                        in_event = True

                    year_events[-1].append(day)
                else:
                    in_event = False

            season_split_data.append({"data": year_days, "events": year_events})

        return season_split_data, events

    def ts_aggregation(self, node, ts: Timeseries, yearly_results):
        results = []

        for year in yearly_results:
            result = {
                "year": year["data"][0].date.year,
            }
            events = year["events"]
            result["events_summary"] = list(
                map(
                    lambda e: {"start_date": e[0].format_date(), "end_date": e[-1].format_date(), "duration": len(e)},
                    events,
                )
            )

            # Generate result for each metric
            if len(events) > 0:
                result["count"] = len(events)
                result["mean_magnitude_of_peaks"] = round(
                    sum(map(lambda e: ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, e), events)) / len(events),
                    3,
                )
                result["total_duration"] = sum(map(lambda e: len(e), events))
                result["mean_duration"] = round(sum(map(lambda e: len(e), events)) / len(events), 3)
                result["max_duration"] = max(map(lambda e: len(e), events))

                days_betwen_events = []
                for index, event in enumerate(events):
                    if index + 1 < len(events):
                        next_event = events[index + 1]
                        diff = ts.get_number_of_days_between_days(next_event[0], event[-1]) - 1
                        days_betwen_events.append(diff)

                if len(days_betwen_events) > 0:
                    result["total_duration_between_spells"] = sum(days_betwen_events)
                    result["mean_duration_between_spells"] = round(statistics.mean(days_betwen_events), 3)
                    result["max_duration_between_spells"] = max(days_betwen_events)
                else:
                    result["total_duration_between_spells"] = 0
                    result["mean_duration_between_spells"] = 0
                    result["max_duration_between_spells"] = 0

            else:
                result["count"] = 0
                result["mean_magnitude_of_peaks"] = 0
                result["total_duration"] = 0
                result["mean_duration"] = 0
                result["max_duration"] = 0
                result["total_duration_between_spells"] = 0
                result["mean_duration_between_spells"] = 0
                result["max_duration_between_spells"] = 0

            results.append(result)

        return results

    def calc_event_summary(self, node, ts: Timeseries, events):
        summary_results = {
            "spell_type": node["event"]["type"],
            "threshold": node["event"]["upper_threshold"]["flow_threshold"]
            if node["event"]["type"] != SpellAnalysisTypes.RANGE
            else f"{node['event']['lower_threshold']['flow_threshold']} - {node['event']['upper_threshold']['flow_threshold']}",
        }

        if len(events) > 0:
            event_lengths = list(map(lambda x: len(x), events))

            days_betwen_events = []
            for index, event in enumerate(events):
                if index + 1 < len(events):
                    next_event = events[index + 1]
                    diff = ts.get_number_of_days_between_days(next_event[0], event[-1]) - 1
                    days_betwen_events.append(diff)

            summary_results.update(
                {
                    "total_duration": sum(event_lengths),
                    "mean_duration": sum(event_lengths) / len(events),
                    "max_duration": max(event_lengths),
                    "mean_duration_between_spells": sum(days_betwen_events) / len(days_betwen_events)
                    if len(days_betwen_events) > 0
                    else 0,
                    "max_duration_between_spells": max(days_betwen_events) if len(days_betwen_events) > 0 else 0,
                }
            )

        return [summary_results]

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Calculate ARI's
        if node.parameters["event"]["upper_threshold"]["use_ARI"]:
            if node.parameters["event"]["upper_threshold"]["use_default_data"]:
                ari_ts = Timeseries(node.default_data)
            else:
                ari_ts = ts

            # Calculate the flood level here as it's constant for the whole period
            node.parameters["event"]["upper_threshold"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                TimeseriesDataTypes.FLOW, node.parameters["event"]["upper_threshold"]["ARI"]
            )

        if node.parameters["event"]["type"] is SpellAnalysisTypes.RANGE:
            if node.parameters["event"]["lower_threshold"]["use_ARI"]:
                if node.parameters["event"]["lower_threshold"]["use_default_data"]:
                    ari_ts = Timeseries(node.default_data)
                else:
                    ari_ts = ts

                # Calculate the flood level here as it's constant for the whole period
                node.parameters["event"]["lower_threshold"]["flow_threshold"] = ari_ts.get_flood_level_POT(
                    TimeseriesDataTypes.FLOW, node.parameters["event"]["lower_threshold"]["ARI"]
                )

        # Generate results
        result.ts_gen, events = self.ts_generation(node.parameters, ts)

        result.event_log = list(
            map(
                lambda e: {"start_date": e[0].format_date(), "end_date": e[-1].format_date(), "duration": len(e)},
                events,
            )
        )

        # Get yearly results
        result.ts_agg = self.ts_aggregation(node.parameters, ts, result.ts_gen)

        result.summary = self.calc_event_summary(node.parameters, ts, events)

        # Set assessment results
        assessment_metric = node.parameters["event_assessment"]["assessment_metric"].value
        if assessment_metric == SpellMetrics.MEAN_MAGNITUDE:
            assessment_metric = "mean_magnitude_of_peaks"

        result.assessment_yearly = list(map(lambda d: (d["year"], d[assessment_metric]), result.ts_agg))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts.convert_to_result_data(["date", "flow", "event"], [0])
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_agg)

        result_events = NodeResult(result.node_name, ResultTypes.EVENTS_INTERMEDIATE, result.event_log)

        result_summary = NodeResult(result.node_name, ResultTypes.SUMMARY_INTERMEDIATE, result.summary)

        return [result_daily, result_yearly, result_events, result_summary]

    def write_intermediate_results(self, write_result):
        write_result(ResultTypes.DAILY_INTERMEDIATE, ["node", "date", "flow", "event"])

        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            [
                "node",
                "year",
                "count",
                "mean_magnitude_of_peaks",
                "total_duration",
                "mean_duration",
                "max_duration",
                "total_duration_between_spells",
                "mean_duration_between_spells",
                "max_duration_between_spells",
            ],
        )

        write_result(ResultTypes.EVENTS_INTERMEDIATE, ["node", "start_date", "end_date", "duration"])

        write_result(
            ResultTypes.SUMMARY_INTERMEDIATE,
            [
                "node",
                "spell_type",
                "threshold",
                "total_duration",
                "mean_duration",
                "max_duration",
                "mean_duration_between_spells",
                "max_duration_between_spells",
            ],
        )
