from typing import List
from core.data_store import DataStore
from core.globals import ResultTypes, TimeseriesDataTypes
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.node_result import NodeResult
from core.season import Season
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class mary_river_turtles(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Mary River Turtles",
            "description": "Score Mary Turtles spawning success",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.TEMPORAL
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # partnering_event
        assert_wrapper(
            validation.valid_season(node_parameters["partnering_event"]["season"]) is True,
            "season",
            "must contain valid dates (day/month)",
        )
        node_parameters["partnering_event"]["season"] = Season(node_parameters["partnering_event"]["season"])
        assert_wrapper(node_parameters["partnering_event"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(node_parameters["partnering_event"]["amount_above_CTF"] >= 0, "amount_above_CTF", "must be >= 0")

        # rainfall_event
        assert_wrapper(node_parameters["rainfall_event"]["rainfall"] >= 0, "rainfall", "must be >= 0")
        assert_wrapper(
            node_parameters["rainfall_event"]["days_since_partnering"] >= 0, "days_since_partnering", "must be >= 0"
        )

        # Stable Water level
        assert_wrapper(node_parameters["stable_water_level"]["duration_days"] >= 0, "duration_days", "must be >= 0")
        assert_wrapper(
            validation.valid_curve(node_parameters["stable_water_level"]["nests_inundated_curve"]) is True,
            "nests_inundated_curve",
            "must be a valid curve",
        )
        assert_wrapper(
            validation.valid_risk(node_parameters["stable_water_level"]["inundation_risk_curve"]) is True,
            "inundation_risk_curve",
            "must be valid risks",
        )
        assert_wrapper(
            validation.value_in_range(node_parameters["stable_water_level"]["inundation_risk_threshold"], 0, 100),
            "inundation_risk_threshold",
            "must be a valid %",
        )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])
        data.depth = data.try_import_data(node["data"], TimeseriesDataTypes.DEPTH)

        data.rainfall = data.try_import_data(node["data"], TimeseriesDataTypes.RAINFALL)

        return data, default_data

    def ts_generation(self, node, ts: Timeseries):
        yearly_sorted_ts = ts.sort_into_years()
        yearly_results = []

        for year, year_days in yearly_sorted_ts.items():
            yearly_results.append({"year": year})

            partnering_season_index_list = ts.get_season_index_list(node["partnering_event"]["season"], data=year_days)
            partnering_event = []

            # Check for partnering event
            for index, day in enumerate(year_days):
                # Check if day is in partnering season
                if index in partnering_season_index_list:  # Check day is in season
                    # Check if we have enough flow to start an event
                    if day.get_value(TimeseriesDataTypes.DEPTH) >= (
                        node["partnering_event"]["CTF"] + node["partnering_event"]["amount_above_CTF"]
                    ):
                        partnering_event.append(day)

            # Rainfall events
            event = []
            event_found = False
            for index, day in enumerate(year_days):
                if event_found:
                    break  # stop when event is found
                if (
                    day.get_value(TimeseriesDataTypes.RAINFALL) is not None
                    and day.get_value(TimeseriesDataTypes.RAINFALL) >= node["rainfall_event"]["rainfall"]
                ):
                    for check_day in partnering_event:
                        if (
                            ts.get_number_of_days_between_days(day, check_day)
                            >= node["rainfall_event"]["days_since_partnering"]
                        ):
                            event.append(check_day)
                            day.set_calculated_value("index", index)
                            event.append(day)
                            event_found = True
                            break

            # Check if we have an event - if not can stop here
            if len(event) == 0:
                yearly_results[-1] = {
                    **yearly_results[-1],
                    "success": 0,
                    "inundation": None,
                    "risk": None,
                    "attempted_recruitment": 0,
                    "failure_reason": "No potential events found",
                }
                continue

            # Check for stable flow
            rainfall_day = event[1]
            # If we don't have the full period we fail
            if (
                len(year_days)
                < rainfall_day.get_calculated_value("index") + 1 + node["stable_water_level"]["duration_days"]
            ):
                yearly_results[-1] = {
                    **yearly_results[-1],
                    "success": 0,
                    "inundation": None,
                    "risk": None,
                    "attempted_recruitment": 0,
                    "failure_reason": "Not enough time to complete event",
                }
                continue
            else:
                stable_flow_ts = year_days[
                    rainfall_day.get_calculated_value("index")
                    + 1 : rainfall_day.get_calculated_value("index")
                    + 1
                    + node["stable_water_level"]["duration_days"]
                ]

            # Get the max daily depth in this period
            max_depth = ts.get_data_type_value_max(TimeseriesDataTypes.DEPTH, stable_flow_ts)
            depth_above_rainfall_day = max_depth - rainfall_day.get_value(TimeseriesDataTypes.DEPTH)

            # Get inundation %
            inundation = calcs.get_curve_value(
                depth_above_rainfall_day, node["stable_water_level"]["nests_inundated_curve"]
            )

            # Get risk %
            risk = None
            for category, range in node["stable_water_level"]["inundation_risk_curve"].items():
                if inundation >= range[0] and inundation <= range[1]:
                    risk = category
                    break

            # Get overall success
            if inundation <= node["stable_water_level"]["inundation_risk_threshold"]:
                yearly_results[-1] = {
                    **yearly_results[-1],
                    "success": 1,
                    "inundation": inundation,
                    "risk": risk,
                    "attempted_recruitment": 1,
                    "failure_reason": None,
                }
                continue
            else:
                yearly_results[-1] = {
                    **yearly_results[-1],
                    "success": 0,
                    "inundation": inundation,
                    "risk": risk,
                    "attempted_recruitment": 1,
                    "failure_reason": "Inundation above max threshold",
                }
                continue

        return yearly_results

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        # Generate results for each year
        result.ts_gen = self.ts_generation(node.parameters, ts)

        # Set assessment results
        result.assessment_yearly = list(map(lambda d: (d["year"], d["success"]), result.ts_gen))

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_yearly = NodeResult(result.node_name, ResultTypes.YEARLY_INTERMEDIATE, result.ts_gen)

        return [result_yearly]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.YEARLY_INTERMEDIATE,
            ["node", "year", "success", "inundation", "risk", "attempted_recruitment", "failure_reason"],
        )
