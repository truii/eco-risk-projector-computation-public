from typing import List
from core.data_store import DataStore
from core.globals import OtherDataTypes, ResultTypes, TimeseriesDataTypes, WaterholeRunToEmptyEvaporationType
from core.node import Node
from core.node_data import NodeData
from core.node_default_data import NodeDefaultData
from core.node_result import NodeResult
from core.plugin import Plugin
from core.plugin_result import PluginResult
from core.timeseries import Timeseries
from helper import io as io
from helper import calculations as calcs
from helper import validation as validation
from helper.exception_utils import *


class waterhole_run_to_empty(Plugin):
    def __init__(self):
        meta_data = {
            "name": "Waterhole Run To Empty",
            "description": "Simulate waterhole run to empty",
            "version": "1.0.0",
            "author": "Zach Marsh",
        }
        plugin_type = globals.PluginTypes.NONE
        super().__init__(meta_data, plugin_type)

    def validate_parameters(self, node_parameters, node_data):
        # Evaporation
        assert_wrapper(node_parameters["evaporation"]["scaling"] >= 0, "scaling", "must be >= 0")

        # Seepage
        assert_wrapper(node_parameters["seepage"]["rate"] >= 0, "rate", "must be >= 0")

        # Rainfall
        assert_wrapper(node_parameters["rainfall"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(node_parameters["rainfall"]["max_infiltration"] >= 0, "max_infiltration", "must be >= 0")

        # Ground Water
        assert_wrapper(node_parameters["groundwater"]["area"] >= 0, "area", "must be >= 0")
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["inflow"], 0, 100),
            "inflow",
            "must be between 0 and 100",
        )
        assert_wrapper(
            validation.value_in_range(node_parameters["groundwater"]["loss_to_deep_drainage"], 0, 100),
            "loss_to_deep_drainage",
            "must be between 0 and 100",
        )
        assert_wrapper(
            (node_parameters["groundwater"]["inflow"] + node_parameters["groundwater"]["loss_to_deep_drainage"]) <= 100,
            "groundwater",
            "inflow % + loss to deep drainage % must be <= 100%",
        )

        # Extraction
        assert_wrapper(
            node_parameters["extraction"]["commence_pumping_depth"]
            >= node_parameters["extraction"]["cease_pumping_depth"],
            "commence_pumping_depth/cease_pumping_depth",
            "commence depth must be >= cease depth",
        )
        assert_wrapper(node_parameters["extraction"]["extraction_rate"] >= 0, "extraction_rate", "must be >= 0")
        assert_wrapper(node_parameters["extraction"]["max_annual_take"] >= 0, "max_annual_take", "must be >= 0")
        assert_wrapper(
            node_parameters["extraction"]["start_month"] >= 1 and node_parameters["extraction"]["start_month"] <= 12,
            "start_month",
            "must be a valid month",
        )
        assert_wrapper(node_parameters["extraction"]["CTF"] >= 0, "CTF", "must be >= 0")
        assert_wrapper(validation.is_num(node_parameters["extraction"]["lag"], int), "lag", "must be an integer")

        # Optimisation
        # run to empty depth
        assert_wrapper(
            validation.is_num(node_parameters["optimisation"]["run_to_empty_depth"]),
            "run_to_empty_depth",
            "must be a number",
        )

        try:
            node_parameters["optimisation"]["run_to_empty_type"] = WaterholeRunToEmptyEvaporationType(
                node_parameters["optimisation"]["run_to_empty_type"]
            )
        except Exception:
            assert_wrapper(False, "run_to_empty_type", "must be a valid run_to_empty_type")

        if "season" in node_parameters["optimisation"]:
            assert_wrapper(
                validate_run_period(node_parameters["optimisation"]["season"]), "season", "must be a valid season"
            )

        if (
            node_parameters["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.VALUE
            or node_parameters["optimisation"]["run_to_empty_type"]
            == WaterholeRunToEmptyEvaporationType.DATA_PLUS_VALUE
        ):
            assert_wrapper(
                validation.is_num(node_parameters["optimisation"]["run_to_empty_value"]),
                "run_to_empty_value",
                "must be a number",
            )

        if node_parameters["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.DATA_PLUS_PERCENT:
            assert_wrapper(
                validation.value_in_range(node_parameters["optimisation"]["run_to_empty_value"], 0, 100),
                "run_to_empty_value",
                "must be between 0 and 100",
            )

    def load_data(self, node):
        data = NodeData(node)
        default_data = NodeDefaultData(node)

        node["data"] = data.handle_null_paths(node["data"])

        data.evaporation = data.try_import_data(node["data"], TimeseriesDataTypes.EVAPORATION)

        data.bathymetry = data.try_import_data(node["data"], OtherDataTypes.BATHYMETRY)

        return data, default_data

    def run_to_empty(self, node, ts: Timeseries, bathymetry):
        # Setup general vars
        max_depth = bathymetry["volume"][-1][0]
        max_volume = bathymetry["volume"][-1][1]
        waterhole_volume = max_volume
        waterhole_depth = max_depth
        prev_month = ts.data[0].date.month
        GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        max_GW_volume = node["groundwater"]["area"] * max_depth * 0.4
        annual_pump_take = 0
        run_to_empty_days = 0
        is_empty = False
        evaporation_scaling = node["evaporation"]["scaling"] / 100

        evapotation_value = None
        if node["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.MEAN:
            evapotation_value = ts.get_data_type_value_mean(TimeseriesDataTypes.EVAPORATION)
        elif node["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.VALUE:
            evapotation_value = node["optimisation"]["run_to_empty_value"]

        for day in ts.data:
            # Reset annual pump take if new water year
            if day.date.month != prev_month:
                if day.date.month == node["extraction"]["start_month"]:
                    annual_pump_take = 0
                prev_month = day.date.month

            # Calculate daily inflow and outflow, apply this to prev depth to get new depth
            waterhole_inflow_m3 = 0
            waterhole_outflow_m3 = 0
            GW_inflow_m3 = 0
            GW_outflow_m3 = 0

            # Get the current waterhole area for rainfall and evaporation calculations
            waterhole_surface_area = calcs.get_curve_value(waterhole_depth, bathymetry["area"])

            # Evaporation
            if node["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.DATA:
                if day.has_value(TimeseriesDataTypes.EVAPORATION):
                    evapotation_value = day.get_value(TimeseriesDataTypes.EVAPORATION) * evaporation_scaling
                else:
                    evapotation_value = 0

            if node["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.DATA_PLUS_VALUE:
                if day.has_value(TimeseriesDataTypes.EVAPORATION):
                    evapotation_value = (day.get_value(TimeseriesDataTypes.EVAPORATION) * evaporation_scaling) + node[
                        "optimisation"
                    ]["run_to_empty_value"]
                else:
                    evapotation_value = 0

            if node["optimisation"]["run_to_empty_type"] == WaterholeRunToEmptyEvaporationType.DATA_PLUS_PERCENT:
                if day.has_value(TimeseriesDataTypes.EVAPORATION):
                    evapotation_value = day.get_value(TimeseriesDataTypes.EVAPORATION) * evaporation_scaling
                    evapotation_value += evapotation_value * (node["optimisation"]["run_to_empty_value"] / 100)
                else:
                    evapotation_value = 0

            waterhole_outflow_m3 += (evapotation_value * waterhole_surface_area) * 0.001

            # Seepage
            waterhole_outflow_m3 += (node["seepage"]["rate"] * waterhole_surface_area) * 0.001

            # Pumping
            if (
                waterhole_depth <= node["extraction"]["commence_pumping_depth"]
                and waterhole_depth >= node["extraction"]["cease_pumping_depth"]
            ):
                if (annual_pump_take + node["extraction"]["extraction_rate"]) <= node["extraction"]["max_annual_take"]:
                    waterhole_outflow_m3 += node["extraction"]["extraction_rate"] * 1000
                    annual_pump_take += node["extraction"]["extraction_rate"]

            # GW
            # To waterhole
            waterhole_inflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)
            GW_outflow_m3 += GW_volume * (node["groundwater"]["inflow"] / 100)

            # To Deep Drainage
            GW_outflow_m3 += GW_volume * (node["groundwater"]["loss_to_deep_drainage"] / 100)

            # To Evaporation
            if GW_volume > (max_GW_volume * 0.8):
                if (node["groundwater"]["area"] - node["rainfall"]["area"]) > 0:
                    GW_outflow_m3 += (
                        evapotation_value * 0.5 * node["groundwater"]["area"] - node["rainfall"]["area"]
                    ) * 0.001

            # Apply all volume changes
            waterhole_volume -= waterhole_outflow_m3
            waterhole_volume += waterhole_inflow_m3

            if waterhole_volume > max_volume:  # Cant exceed max volume/depth
                waterhole_volume = max_volume
            elif waterhole_volume < 0:
                waterhole_volume = 0

            waterhole_depth = calcs.get_curve_value(waterhole_volume, bathymetry["volume"], True)

            GW_volume -= GW_outflow_m3
            GW_volume += GW_inflow_m3

            if GW_volume > max_GW_volume:  # Cant exceed max volume/depth
                GW_volume = max_GW_volume

            day.set_result_value("modelled_depth", round(waterhole_depth, 5))
            day.set_result_value("modelled_volume", round(waterhole_volume, 5))
            day.set_result_value("modelled_area", round(waterhole_surface_area, 5))
            day.set_result_value("modelled_GW_volume", round(GW_volume, 5))

            if not is_empty and waterhole_depth > node["optimisation"]["run_to_empty_depth"]:
                run_to_empty_days += 1
            elif not is_empty:
                is_empty = True

        if is_empty:
            return run_to_empty_days
        return None

    def run(self, node: Node):
        ts = Timeseries(node.data)
        result = PluginResult(node.name, ts)

        if "season" in node.parameters["optimisation"] and node.parameters["optimisation"]["season"] is not None:
            ts.data = ts.trim_into_run_period(node.parameters["optimisation"]["season"])

        bathymetry = {
            "volume": calcs.extract_curve(node.data.bathymetry, 0, 2, True, True),
            "area": calcs.extract_curve(node.data.bathymetry, 0, 3, True, True),
            "height": calcs.extract_curve(node.data.bathymetry, 0, 1, True, True),
        }

        run_to_empty_days = self.run_to_empty(node.parameters, ts, bathymetry)

        result.ts_gen = ts
        result.ts_agg = {"days": run_to_empty_days}

        return result

    def get_intermediate_results(self, result: PluginResult) -> List[NodeResult]:
        result_daily_data = result.ts_gen.convert_to_result_data(
            [
                "date",
                "modelled_depth",
                "modelled_volume",
                "modelled_area",
                "modelled_GW_volume",
                "evaporation",
            ],
            [0],
        )
        result_daily = NodeResult(result.node_name, ResultTypes.DAILY_INTERMEDIATE, result_daily_data)

        result_parameters = NodeResult(result.node_name, ResultTypes.PARAMETER_RESULTS, result.ts_agg)

        return [result_daily, result_parameters]

    def write_intermediate_results(self, write_result):
        write_result(
            ResultTypes.DAILY_INTERMEDIATE,
            ["node", "date", "modelled_depth", "modelled_volume", "modelled_area", "modelled_GW_volume", "evaporation"],
        )

        write_result(ResultTypes.PARAMETER_RESULTS)
