import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class OffshoreReefFisheryPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "offshore_reef_fishery"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_offshoreReefFishery_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_offshoreReefFishery_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\NA\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_offshoreReefFishery_validation_invalidConstants(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "aa": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_offshoreReefFishery_validation_invalidFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": -100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_offshoreReefFishery_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": -1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_offshoreReefFishery_run_dailyResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["catch_rate"], 16.586544091182752)
        self.assertEqual(yearly_intermediate[1]["catch_rate"], 40979.736469990225)

    def test_offshoreReefFishery_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["catch_rate"], 16.586544091182752)
        self.assertEqual(yearly_intermediate[0]["high_flow_threshold_met"], 1)

    def test_offshoreReefFishery_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1890)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_offshoreReefFishery_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "constants": {
                        "a": 12120.44,
                        "b": 3312455.74,
                        "c": -210028.55,
                        "d": 16.53,
                        "f": 190548.16,
                        "m": 1.08,
                        "g": 1083205.75,
                    },
                    "flow_parameters": {
                        "high_flow_year_threshold": 100000,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1890)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
