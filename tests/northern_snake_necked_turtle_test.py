import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import calculations as calcs
from tests.test_context import get_data_store


class NorthernSnakeNeckedTurtle(unittest.TestCase):
    def setUp(self):
        self.plugin = "northern_snake_necked_turtle"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_northernSnakeNeckedTurtle_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\NA\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": -1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": -10,
                        "use_ARI": False,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidUseDefaultData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 10,
                        "use_ARI": False,
                        "ARI": 5,
                        "use_default_data": "invalid",
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidUseDefaultDataMissingData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 10,
                        "use_ARI": False,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_validation_invalidEggCriteria(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria_NA": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 120,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_northernSnakeNeckedTurtle_run_setThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 5}},
                        "flow_threshold": 1000,
                        "use_ARI": False,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 40,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 2)
        self.assertEqual(yearly_intermediate[1]["number_of_successful_clutches"], 2)
        self.assertEqual(yearly_intermediate[1]["number_of_attempted_clutches"], 4)

    def test_northernSnakeNeckedTurtle_run_ARIThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 2)
        self.assertEqual(yearly_intermediate[1]["number_of_successful_clutches"], 2)
        self.assertEqual(yearly_intermediate[1]["number_of_attempted_clutches"], 4)

    def test_northernSnakeNeckedTurtle_run_UseDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 2)
        self.assertEqual(yearly_intermediate[1]["number_of_successful_clutches"], 2)
        self.assertEqual(yearly_intermediate[1]["number_of_attempted_clutches"], 4)

    def test_northernSnakeNeckedTurtle_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_clutch_season = sum(
            map(
                lambda d: d["in_clutch_season"] if d["in_clutch_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_clutch_attempted = sum(
            map(
                lambda d: d["clutch_attempted"] if d["clutch_attempted"] is not None else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 2)
        self.assertEqual(num_in_clutch_season, 213)
        self.assertEqual(num_clutch_attempted, 4)

        clutch_failure_reasons = calcs.get_all_values_for_key_from_dict_list(
            daily_intermediate, "clutch_failure_reason"
        )
        self.assertEqual(clutch_failure_reasons[None], 363)
        self.assertEqual(clutch_failure_reasons["Not enough days to check incubation"], 2)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_clutch_season"], 1)
        self.assertEqual(daily_intermediate[0]["clutch_attempted"], 0)
        self.assertEqual(daily_intermediate[0]["clutch_failure_reason"], None)
        self.assertEqual(daily_intermediate[0]["success"], 0)

    def test_northernSnakeNeckedTurtle_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[1]["number_of_successful_clutches"], 2)
        self.assertEqual(yearly_intermediate[1]["number_of_attempted_clutches"], 4)
        self.assertEqual(
            yearly_intermediate[1]["clutch_failure_reasons"],
            "Not enough days to check incubation, Not enough days to check incubation",
        )

        self.assertEqual(yearly_intermediate[0]["year"], 2018)
        self.assertEqual(yearly_intermediate[0]["number_of_successful_clutches"], 0)
        self.assertEqual(yearly_intermediate[0]["number_of_attempted_clutches"], 0)
        self.assertEqual(yearly_intermediate[0]["clutch_failure_reasons"], "")

    def test_northernSnakeNeckedTurtle_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_northernSnakeNeckedTurtle_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "flow_params": {
                        "clutch_season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "flow_threshold": 1000,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                    },
                    "egg_criteria": {
                        "max_clutches_per_season": 4,
                        "incubation_period": 60,
                        "max_diapause": 90,
                        "time_between_clutches": 30,
                        "soil_moisture_drying": 42,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
