import datetime
import math
import unittest
from core.exceptions import InvalidDataException
from core.globals import OtherDataTypes, TimeseriesDataTypes
from core.node_data import NodeData
from core.season import Season
from core.timeseries import Timeseries

FLOW_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"
DEPTH_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"
TEMPERATURE_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Temperature 10 Years.csv"
RAINFALL_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Rainfall 10 Years.csv"
SALINITY_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv"

DEPTH_1_YEAR_DATA_PATH = ".\\tests\\test_data\\Site 1 Depth Single Year.csv"
FLOW_5_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 5 Years.csv"
FLOW_10_YEARS_MISSING_DAYS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 10 Years Missing Days.csv"
CLIMATE_DATA_PATH = ".\\tests\\test_data\\Climate.csv"


class TimeseriesTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_singleDataTypeFlow = NodeData(None)
        cls.data_singleDataTypeFlow.flow = cls.data_singleDataTypeFlow.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_singleDataTypeFlowShort = NodeData(None)
        cls.data_singleDataTypeFlowShort.flow = cls.data_singleDataTypeFlowShort.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_5_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_singleDataTypeDepth = NodeData(None)
        cls.data_singleDataTypeDepth.depth = cls.data_singleDataTypeDepth.try_import_data(
            {TimeseriesDataTypes.DEPTH: DEPTH_10_YEARS_DATA_PATH}, TimeseriesDataTypes.DEPTH
        )

        cls.data_multipleDataTypes = NodeData(None)
        cls.data_multipleDataTypes.flow = cls.data_multipleDataTypes.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )
        cls.data_multipleDataTypes.depth = cls.data_multipleDataTypes.try_import_data(
            {TimeseriesDataTypes.DEPTH: DEPTH_10_YEARS_DATA_PATH}, TimeseriesDataTypes.DEPTH
        )

        cls.data_multipleDataTypesMismatchedLength = NodeData(None)
        cls.data_multipleDataTypesMismatchedLength.flow = cls.data_multipleDataTypesMismatchedLength.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_5_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )
        cls.data_multipleDataTypesMismatchedLength.depth = cls.data_multipleDataTypesMismatchedLength.try_import_data(
            {TimeseriesDataTypes.DEPTH: DEPTH_10_YEARS_DATA_PATH}, TimeseriesDataTypes.DEPTH
        )

        cls.data_multipleDataTypesNoOverlappingDates = NodeData(None)
        cls.data_multipleDataTypesNoOverlappingDates.flow = (
            cls.data_multipleDataTypesNoOverlappingDates.try_import_data(
                {TimeseriesDataTypes.FLOW: FLOW_5_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
            )
        )
        cls.data_multipleDataTypesNoOverlappingDates.depth = (
            cls.data_multipleDataTypesNoOverlappingDates.try_import_data(
                {TimeseriesDataTypes.DEPTH: DEPTH_1_YEAR_DATA_PATH}, TimeseriesDataTypes.DEPTH
            )
        )

        cls.data_dataWithMissingDays = NodeData(None)
        cls.data_dataWithMissingDays.flow = cls.data_dataWithMissingDays.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_MISSING_DAYS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_climate = NodeData(None)
        cls.data_climate.climate = cls.data_climate.try_import_data(
            {OtherDataTypes.CLIMATE: CLIMATE_DATA_PATH}, OtherDataTypes.CLIMATE
        )

        cls.days_singleDataTypeFlow = Timeseries(cls.data_singleDataTypeFlow).data
        cls.days_singleDataTypeFlowShort = Timeseries(cls.data_singleDataTypeFlowShort).data
        cls.days_singleDataTypeDepth = Timeseries(cls.data_singleDataTypeDepth).data
        cls.days_multipleDataTypes = Timeseries(cls.data_multipleDataTypes).data
        cls.days_infilledData = Timeseries(cls.data_dataWithMissingDays).data
        cls.days_climate = cls.data_climate.parse_csv_to_timeseries_days(
            OtherDataTypes.CLIMATE, is_number_values=False, infill_with_previous_value=True
        )

        cls.annual_season = Season(start_month=1)
        cls.water_year_season = Season(start_month=7)
        cls.one_month_season = Season({"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 1}})

        cls.full_run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
            "end": {"day": 31, "month": 12, "year": 1895},
        }
        cls.no_start_run_period = {"end": {"day": 31, "month": 12, "year": 1895}}
        cls.no_end_run_period = {"start": {"day": 1, "month": 1, "year": 1890}}

    def test_setTimeseries(self):
        empty_ts = Timeseries()
        self.assertIsNone(empty_ts.data)
        empty_ts.set_timeseries(self.days_multipleDataTypes)
        self.assertIsNotNone(empty_ts.data)

    def test_initialiseTimeseriesWithData_singleDataType(self):
        data = Timeseries(self.data_singleDataTypeFlow)
        self.assertIsNotNone(data)
        self.assertEqual(len(data.data), 3652)
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertFalse(data.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = data.data[0]
        last_day = data.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

    def test_initialiseTimeseriesWithData_multipleDataTypes(self):
        data = Timeseries(self.data_multipleDataTypes)
        self.assertIsNotNone(data)
        self.assertEqual(len(data.data), 3652)
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = data.data[0]
        last_day = data.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_initialiseTimeseriesWithData_multipleDataTypesMismatchedLength(self):
        data = Timeseries(self.data_multipleDataTypesMismatchedLength)
        self.assertIsNotNone(data)
        self.assertEqual(len(data.data), 1826)
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = data.data[0]
        last_day = data.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1894, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_initialiseTimeseriesWithData_multipleDataTypesMismatchedLengthExcludeShorter(self):
        data = Timeseries(self.data_multipleDataTypesMismatchedLength, [TimeseriesDataTypes.FLOW])
        self.assertIsNotNone(data)
        self.assertEqual(len(data.data), 3652)
        self.assertFalse(data.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(data.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = data.data[0]
        last_day = data.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.DEPTH: 0.89})

    def test_initialiseTimeseriesWithData_noDatesWithAllDataThrowsError(self):
        with self.assertRaisesRegex(
            InvalidDataException, "There is no time period in which all required data types are avaliable in node None"
        ):
            data = Timeseries(self.data_multipleDataTypesNoOverlappingDates)

    def test_parseTimeseriesFromData_singleDataType(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(self.data_singleDataTypeFlow)

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 3652)

        first_day = parsed_data[0]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

    def test_parseTimeseriesFromData_multipleDataTypes(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(self.data_multipleDataTypes)

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 3652)

        first_day = parsed_data[0]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_parseTimeseriesFromData_multipleDataTypesMismatchedLength(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(self.data_multipleDataTypesMismatchedLength)

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 1826)

        first_day = parsed_data[0]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1894, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_parseTimeseriesFromData_multipleDataTypesMismatchedLengthNotConcurrentOnly(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(self.data_multipleDataTypesMismatchedLength, concurrent_only=False)

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 3652)

        first_day = parsed_data[0]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})
        self.assertEqual(last_day.values, {TimeseriesDataTypes.DEPTH: 1})

    def test_parseTimeseriesFromData_multipleDataTypesExcludeOne(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(self.data_multipleDataTypes, [TimeseriesDataTypes.DEPTH])

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 3652)

        first_day = parsed_data[0]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

    def test_parseTimeseriesFromData_infilledData(self):
        ts = Timeseries()
        parsed_data = ts.parse_timeseries_from_data(
            self.data_dataWithMissingDays,
        )

        self.assertIsNotNone(parsed_data)
        self.assertEqual(len(parsed_data), 3652)

        first_day = parsed_data[0]
        second_day = parsed_data[1]
        last_day = parsed_data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})
        self.assertEqual(second_day.values, {TimeseriesDataTypes.FLOW: None})

    def test_parseTimeseriesFromData_noDatesWithAllDataThrowsError(self):
        ts = Timeseries()
        with self.assertRaisesRegex(
            InvalidDataException, "There is no time period in which all required data types are avaliable in node None"
        ):
            parsed_data = ts.parse_timeseries_from_data(self.data_multipleDataTypesNoOverlappingDates)

    def test_mergeTimeseriesDays_singleList(self):
        ts = Timeseries()

        merged_days = ts.merge_timeseries_days([self.days_singleDataTypeFlow])

        self.assertEqual(len(merged_days), 3652)

        first_day = merged_days[0]
        last_day = merged_days[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

    def test_mergeTimeseriesDays_multipleLists(self):
        ts = Timeseries()

        merged_days = ts.merge_timeseries_days([self.days_singleDataTypeFlow, self.days_singleDataTypeDepth])

        self.assertEqual(len(merged_days), 3652)

        first_day = merged_days[0]
        last_day = merged_days[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_mergeTimeseriesDays_multipleListsMismatchedLengthConcurrentOnly(self):
        ts = Timeseries()

        merged_days = ts.merge_timeseries_days([self.days_singleDataTypeFlowShort, self.days_singleDataTypeDepth])

        self.assertEqual(len(merged_days), 1826)

        first_day = merged_days[0]
        last_day = merged_days[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1894, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_mergeTimeseriesDays_multipleListsMismatchedLengthNotConcurrentOnly(self):
        ts = Timeseries()

        merged_days = ts.merge_timeseries_days(
            [self.days_singleDataTypeFlowShort, self.days_singleDataTypeDepth], False
        )

        self.assertEqual(len(merged_days), 3652)

        first_day = merged_days[0]
        last_day = merged_days[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})
        self.assertEqual(last_day.values, {TimeseriesDataTypes.DEPTH: 1})

    def test_addTimeseriesDays_noExistingData(self):
        ts = Timeseries()

        ts.add_timeseries_days(self.days_singleDataTypeFlow)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertFalse(ts.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

    def test_addTimeseriesDays_existingData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertFalse(ts.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33})

        ts.add_timeseries_days(self.days_singleDataTypeDepth)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.FLOW: 158.33, TimeseriesDataTypes.DEPTH: 0.89})

    def test_addTimeseriesDays_existingDataMismatchedConcurrentOnly(self):
        ts = Timeseries(self.data_singleDataTypeDepth)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.DEPTH))
        self.assertFalse(ts.has_data_type(TimeseriesDataTypes.FLOW))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.DEPTH: 0.89})

        ts.add_timeseries_days(self.days_singleDataTypeFlowShort)

        self.assertEqual(len(ts.data), 1826)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1894, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.DEPTH: 0.89, TimeseriesDataTypes.FLOW: 158.33})

    def test_addTimeseriesDays_existingDataMismatchedNotConcurrentOnly(self):
        ts = Timeseries(self.data_singleDataTypeDepth)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.DEPTH))
        self.assertFalse(ts.has_data_type(TimeseriesDataTypes.FLOW))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.DEPTH: 0.89})

        ts.add_timeseries_days(self.days_singleDataTypeFlowShort, False)

        self.assertEqual(len(ts.data), 3652)
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.FLOW))
        self.assertTrue(ts.has_data_type(TimeseriesDataTypes.DEPTH))

        first_day = ts.data[0]
        last_day = ts.data[-1]
        self.assertEqual(
            first_day.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            last_day.date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

        self.assertEqual(first_day.calculated_values, {})
        self.assertEqual(first_day.result_values, {})
        self.assertEqual(first_day.values, {TimeseriesDataTypes.DEPTH: 0.89, TimeseriesDataTypes.FLOW: 158.33})
        self.assertEqual(last_day.values, {TimeseriesDataTypes.DEPTH: 1})

    def test_convertToCsv_singleDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        csvData = ts.convert_to_result_data(["date", "flow"], [0])
        self.assertEqual(len(csvData), 3652)

        self.assertEqual(csvData[0]["date"], "01/07/1889")
        self.assertEqual(csvData[0]["flow"], 158.33)
        self.assertEqual(csvData[-1]["date"], "30/06/1899")
        self.assertEqual(csvData[-1]["flow"], 260.31)

    def test_convertToCsv_singleDataTypeExtraColumn(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        csvData = ts.convert_to_result_data(["date", "flow", "staticValue"], [0], {"staticValue": "same"})
        self.assertEqual(len(csvData), 3652)

        self.assertEqual(csvData[0]["date"], "01/07/1889")
        self.assertEqual(csvData[0]["flow"], 158.33)
        self.assertEqual(csvData[0]["staticValue"], "same")
        self.assertEqual(csvData[-1]["date"], "30/06/1899")
        self.assertEqual(csvData[-1]["flow"], 260.31)
        self.assertEqual(csvData[-1]["staticValue"], "same")

    def test_convertToCsv_multipleDataTypes(self):
        ts = Timeseries(self.data_multipleDataTypes)
        csvData = ts.convert_to_result_data(["date", "flow", "depth"], [0])
        self.assertEqual(len(csvData), 3652)

        self.assertEqual(csvData[0]["date"], "01/07/1889")
        self.assertEqual(csvData[0]["flow"], 158.33)
        self.assertEqual(csvData[0]["depth"], 0.89)
        self.assertEqual(csvData[-1]["date"], "30/06/1899")
        self.assertEqual(csvData[-1]["flow"], 260.31)
        self.assertEqual(csvData[-1]["depth"], 1)

    def test_getDataTypeValueMedian_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 177.955)

    def test_getDataTypeValueMedian_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:100]
        result = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW, data)

        self.assertEqual(result, 91.81)

    def test_getDataTypeValueMedian_roundShort(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW, dp=1)

        self.assertEqual(result, 178)

    def test_getDataTypeValueMedian_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 181.26)

    def test_getDataTypeValueMedian_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.DEPTH)

        self.assertEqual(result, None)

    def test_getDataTypeValueMean_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 420.431)

    def test_getDataTypeValueMean_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:100]
        result = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW, data)

        self.assertEqual(result, 97.721)

    def test_getDataTypeValueMean_roundShort(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW, dp=1)

        self.assertEqual(result, 420.4)

    def test_getDataTypeValueMean_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_value_mean(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 433.409)

    def test_getDataTypeValueMean_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_mean(TimeseriesDataTypes.DEPTH)

        self.assertEqual(result, None)

    def test_getDataTypeValueSum_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 1535413.39)

    def test_getDataTypeValueSum_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:100]
        result = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, data)

        self.assertEqual(result, 9772.08)

    def test_getDataTypeValueSum_roundShort(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW, dp=1)

        self.assertEqual(result, 1535413.4)

    def test_getDataTypeValueSum_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_value_sum(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 1504362.88)

    def test_getDataTypeValueMSum_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.DEPTH)

        self.assertEqual(result, None)

    def test_getDataTypeValueMax_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 21258.05)

    def test_getDataTypeValueMax_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:100]
        result = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW, data)

        self.assertEqual(result, 158.33)

    def test_getDataTypeValueMax_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_value_max(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 21258.05)

    def test_getDataTypeValueMax_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.DEPTH)

        self.assertEqual(result, None)

    def test_getDataTypeValueMin_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 35.4)

    def test_getDataTypeValueMin_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:100]
        result = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW, data)

        self.assertEqual(result, 60.6)

    def test_getDataTypeValueMin_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_value_min(TimeseriesDataTypes.FLOW)

        self.assertEqual(result, 35.4)

    def test_getDataTypeValueMin_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_value_median(TimeseriesDataTypes.DEPTH)

        self.assertEqual(result, None)

    def test_setMovingAverage_7day(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        avg = ts.get_data_type_value_mean("flow_average")

        self.assertEqual(avg, 420.341)

    def test_setMovingAverage_1day(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 1)

        avg = ts.get_data_type_value_mean("flow_average")

        self.assertEqual(avg, 420.431)

    def test_setMovingAverage_invalidDays(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", -10)

        self.assertEqual(ts.data[0].get_calculated_value("flow_average"), None)

    def test_setNonMovingSum_7day(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_non_moving_sum(TimeseriesDataTypes.FLOW, "flow_sum", 7)

        avg = ts.get_data_type_value_mean("flow_sum")

        self.assertEqual(avg, 2942.284)

    def test_setNonMovingSum_1day(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_non_moving_sum(TimeseriesDataTypes.FLOW, "flow_sum", 1)

        sum = ts.get_data_type_value_sum("flow_sum")

        self.assertEqual(sum, 1535413.39)

    def test_setNonMovingSum_invalidDays(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_non_moving_sum(TimeseriesDataTypes.FLOW, "flow_sum", -10)

        self.assertEqual(ts.data[0].get_calculated_value("flow_sum"), None)

    def test_getDataTypeInRange_onlyMin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_in_range_index_list(TimeseriesDataTypes.FLOW, 1)

        self.assertEqual(len(result), 3652)

    def test_getDataTypeInRange_onlyMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_in_range_index_list(TimeseriesDataTypes.FLOW, None, 1000000)

        self.assertEqual(len(result), 3652)

    def test_getDataTypeInRange_minAndMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_in_range_index_list(TimeseriesDataTypes.FLOW, 1, 1000000)

        self.assertEqual(len(result), 3652)

    def test_getDataTypeInRange_dataWithMissingValues(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_in_range_index_list(TimeseriesDataTypes.FLOW, 1, 1000000)

        self.assertEqual(len(result), 3471)

    def test_getDataTypeInRange_invalidDataType(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        result = ts.get_data_type_in_range_index_list(TimeseriesDataTypes.DEPTH, 1, 1000000)

        self.assertEqual(len(result), 0)

    def test_isDataTypeVariationWithinAllowedAcrossPeriod_isWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_across_period(TimeseriesDataTypes.FLOW, 100000)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedAcrossPeriod_isNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_across_period(TimeseriesDataTypes.FLOW, 1)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedAcrossPeriod_isExactlyWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_across_period(TimeseriesDataTypes.FLOW, 16.22, data)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedAcrossPeriod_isExactlyNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_across_period(TimeseriesDataTypes.FLOW, 16.21, data)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedAcrossPeriod_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_across_period(TimeseriesDataTypes.DEPTH, 100000)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedBetweenDays_isWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_between_days(TimeseriesDataTypes.FLOW, 100000)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedBetweenDays_isNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_between_days(TimeseriesDataTypes.FLOW, 1)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedBetweenDays_isExactlyWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_between_days(TimeseriesDataTypes.FLOW, 3.66, data)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedBetweenDays_isExactlyNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_between_days(TimeseriesDataTypes.FLOW, 3.65, data)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedBetweenDays_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_between_days(TimeseriesDataTypes.DEPTH, 100000)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedFromBaseline_isWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_from_baseline(TimeseriesDataTypes.FLOW, 100000, 5000)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedFromBaseline_isNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_from_baseline(TimeseriesDataTypes.FLOW, 1, 100)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedFromBaseline_isExactlyWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_from_baseline(TimeseriesDataTypes.FLOW, 8.208, 150.318, data)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinAllowedFromBaseline_isExactlyNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_allowed_from_baseline(TimeseriesDataTypes.FLOW, 8.207, 150.318, data)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinAllowedFromBaseline_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_allowed_from_baseline(TimeseriesDataTypes.DEPTH, 100000, 5000)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinRange_isWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_range(TimeseriesDataTypes.FLOW, 0, 100000)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinRange_isNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_range(TimeseriesDataTypes.FLOW, 0, 100)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinRange_isExactlyWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_range(TimeseriesDataTypes.FLOW, 142.11, 158.33, data)

        self.assertTrue(result)

    def test_isDataTypeVariationWithinRange_isExactlyNotWithin(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.is_data_type_variation_within_range(TimeseriesDataTypes.FLOW, 142.12, 158.32, data)

        self.assertFalse(result)

    def test_isDataTypeVariationWithinRange_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.is_data_type_variation_within_range(TimeseriesDataTypes.DEPTH, 0, 100000)

        self.assertFalse(result)

    def test_getDataTypePercentiles_1Percentile(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_percentile(TimeseriesDataTypes.FLOW, 1)

        self.assertEqual(result, 40.471)

    def test_getDataTypePercentiles_99Percentile(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_percentile(TimeseriesDataTypes.FLOW, 99)

        self.assertEqual(result, 3574.26)

    def test_getDataTypePercentiles_invalidPercentile(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_percentile(TimeseriesDataTypes.FLOW, -10)

        self.assertEqual(result, None)

    def test_getDataTypePercentiles_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_data_type_percentile(TimeseriesDataTypes.DEPTH, 99)

        self.assertEqual(result, None)

    def test_getFloodLevelPOT_point5Year(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 0.5)

        self.assertEqual(result, 2818.39)

    def test_getFloodLevelPOT_1Year(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 1)

        self.assertEqual(result, 2818.39)

    def test_getFloodLevelPOT_2point5Year(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 2.5)

        self.assertEqual(result, 4362.24)

    def test_getFloodLevelPOT_5Year(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 5)

        self.assertEqual(result, 9431.2)

    def test_getFloodLevelPOT_10Year(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, 10)

        self.assertEqual(result, 18687.79)

    def test_getFloodLevelPOT_invalidYear(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.FLOW, -1)

        self.assertEqual(result, None)

    def test_getFloodLevelPOT_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_flood_level_POT(TimeseriesDataTypes.DEPTH, 1)

        self.assertEqual(result, None)

    def test_getMonthlyMaxValue_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        result = ts.get_monthly_max_value(TimeseriesDataTypes.FLOW)

        mean = round(sum(result) / len(result), 3)

        self.assertEqual(len(result), 120)
        self.assertEqual(mean, 1057.497)
        self.assertEqual(result[0], 158.33)

    def test_getMonthlyMaxValue_overwriteDataShortPeriod(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        data = ts.data[:10]
        result = ts.get_monthly_max_value(TimeseriesDataTypes.FLOW, data)

        mean = round(sum(result) / len(result), 3)

        self.assertEqual(len(result), 1)
        self.assertEqual(mean, 158.33)
        self.assertEqual(result[0], 158.33)

    def test_getMonthlyMaxValue_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_monthly_max_value(TimeseriesDataTypes.DEPTH)

        self.assertEqual(len(result), 0)

    def test_getRMSEforkeys_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 477.344)

    def test_getRMSEforkeys_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        data = ts.data[:100]
        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average", data=data)

        self.assertEqual(result, 3.145)

    def test_getRMSEforkeys_setMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average", 5)

        self.assertEqual(result, 768.069)

    def test_getRMSEforkeys_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 489.616)

    def test_getRMSEforkeys_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.DEPTH, "flow_average")

        self.assertEqual(result, None)

    def test_getRMSEforkeys_invalidEstimatedKey(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_RMSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, None)

    def test_getR2forkeys_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_R2_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.616)

    def test_getR2forkeys_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        data = ts.data[:100]
        result = ts.get_R2_for_keys(TimeseriesDataTypes.FLOW, "flow_average", data=data)

        self.assertEqual(result, 0.987)

    def test_getR2forkeys_setMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_R2_for_keys(TimeseriesDataTypes.FLOW, "flow_average", 5)

        self.assertEqual(result, 0)

    def test_getR2forkeys_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_R2_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.614)

    def test_getR2forkeys_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_R2_for_keys(TimeseriesDataTypes.DEPTH, "flow_average")

        self.assertEqual(result, None)

    def test_getR2forkeys_invalidEstimatedKey(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_R2_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, None)

    def test_getNNSEforkeys_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.723)

    def test_getNNSEforkeys_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        data = ts.data[:100]
        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average", data=data)

        self.assertEqual(result, 0.987)

    def test_getNNSEforkeys_setMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average", 5)

        self.assertEqual(result, 0.5)

    def test_getNNSEforkeys_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.722)

    def test_getNNSEforkeys_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.DEPTH, "flow_average")

        self.assertEqual(result, None)

    def test_getNNSEforkeys_invalidEstimatedKey(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_NNSE_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, None)

    def test_getPBIASforkeys_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.021)

    def test_getPBIASforkeys_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        data = ts.data[:100]
        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.FLOW, "flow_average", data=data)

        self.assertEqual(result, -2.938)

    def test_getPBIASforkeys_setMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.FLOW, "flow_average", 5)

        self.assertEqual(result, -8306.825)

    def test_getPBIASforkeys_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.071)

    def test_getPBIASforkeys_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.DEPTH, "flow_average")

        self.assertEqual(result, None)

    def test_getPBIASforkeys_invalidEstimatedKey(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_PBIAS_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, None)

    def test_getRSRforkeys_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RSR_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.619)

    def test_getRSRforkeys_dataOverwrite(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        data = ts.data[:100]
        result = ts.get_RSR_for_keys(TimeseriesDataTypes.FLOW, "flow_average", data=data)

        self.assertEqual(result, 0.113)

    def test_getRSRforkeys_setMax(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RSR_for_keys(TimeseriesDataTypes.FLOW, "flow_average", 5)

        self.assertEqual(result, math.inf)

    def test_getRSRforkeys_tsWithMissingDays(self):
        ts = Timeseries(self.data_dataWithMissingDays)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RSR_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, 0.621)

    def test_getRSRforkeys_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.set_moving_avg(TimeseriesDataTypes.FLOW, "flow_average", 7)

        result = ts.get_RSR_for_keys(TimeseriesDataTypes.DEPTH, "flow_average")

        self.assertEqual(result, None)

    def test_getRSRforkeys_invalidEstimatedKey(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_RSR_for_keys(TimeseriesDataTypes.FLOW, "flow_average")

        self.assertEqual(result, None)

    def test_getSeasonIndexList_annualSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.annual_season)

        self.assertEqual(len(result), 3287)
        self.assertEqual(result[0], 184)
        self.assertEqual(
            ts.data[184].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getSeasonIndexList_waterYearSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.water_year_season)

        self.assertEqual(len(result), 3652)
        self.assertEqual(result[0], 0)
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getSeasonIndexList_oneMonthSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.one_month_season)

        self.assertEqual(len(result), 310)
        self.assertEqual(result[0], 184)
        self.assertEqual(result[-1], 3501)
        self.assertEqual(
            ts.data[184].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[3501].date,
            datetime.datetime(1899, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getSeasonIndexList_annualSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.annual_season, True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(result[0], 0)
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getSeasonIndexList_waterYearSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.water_year_season, True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(result[0], 0)
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getSeasonIndexList_oneMonthSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.get_season_index_list(self.one_month_season, True)

        self.assertEqual(len(result), 310)
        self.assertEqual(result[0], 184)
        self.assertEqual(result[-1], 3501)
        self.assertEqual(
            ts.data[184].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[3501].date,
            datetime.datetime(1899, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_annualSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.annual_season)

        self.assertEqual(len(result), 9)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_waterYearSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.water_year_season)

        self.assertEqual(len(result), 10)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1890, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_oneMonthSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.one_month_season)

        self.assertEqual(len(result), 10)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1890, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_annualSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.annual_season, True)

        self.assertEqual(len(result), 11)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1889, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_waterYearSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.water_year_season, True)

        self.assertEqual(len(result), 10)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1890, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitIntoSeasons_oneMonthSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_into_seasons(self.one_month_season, True)

        self.assertEqual(len(result), 10)
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1890, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_annualSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.annual_season)

        self.assertEqual(len(result), 3287)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1898, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_waterYearSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.water_year_season)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_oneMonthSeason(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.one_month_season)

        self.assertEqual(len(result), 310)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_annualSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.annual_season, True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_waterYearSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.water_year_season, True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoSeasons_oneMonthSeasonPartialSeasonAllowed(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_season(self.one_month_season, True)

        self.assertEqual(len(result), 310)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 1, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoRunPeriod_fullRunPeriod(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_run_period(self.full_run_period)

        self.assertEqual(len(result), 2191)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1895, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoRunPeriod_noStartRunPeriod(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_run_period(self.no_start_run_period)

        self.assertEqual(len(result), 2375)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1895, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )

    def test_trimIntoRunPeriod_noEndRunPeriod(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.trim_into_run_period(self.no_end_run_period)

        self.assertEqual(len(result), 3468)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitByValueKey_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        ts.add_timeseries_days(self.days_climate)

        result = ts.split_by_value_key(OtherDataTypes.CLIMATE)
        self.assertEqual(len(result), 1)
        self.assertEqual(len(result[0]), 3652)
        self.assertEqual(result[0][0].get_value(OtherDataTypes.CLIMATE), "Average")
        self.assertEqual(result[0][-1].get_value(OtherDataTypes.CLIMATE), "Average")
        self.assertEqual(
            result[0][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[0][-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )

    def test_splitByValueKey_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.split_by_value_key(OtherDataTypes.CLIMATE)
        self.assertEqual(len(result), 0)

    def test_sortIntoMonths_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.sort_into_months()
        self.assertEqual(len(result.items()), 12)
        self.assertEqual(len(result[7]), 310)
        self.assertEqual(
            result[7][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_sortIntoMonths_overwriteWithShorterData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        data = ts.data[:365]

        result = ts.sort_into_months(data)
        self.assertEqual(len(result.items()), 12)
        self.assertEqual(len(result[7]), 31)
        self.assertEqual(
            result[7][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_sortIntoYears_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.sort_into_years()
        self.assertEqual(len(result.items()), 11)
        self.assertEqual(len(result[1890]), 365)
        self.assertEqual(
            result[1889][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_sortIntoYears_overwriteWithShorterData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        data = ts.data[:365]

        result = ts.sort_into_years(data)
        self.assertEqual(len(result.items()), 2)
        self.assertEqual(len(result[1890]), 181)
        self.assertEqual(
            result[1889][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_sortIntoCalenderSeasons_tsData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        result = ts.sort_into_calender_seasons()
        self.assertEqual(len(result.items()), 4)
        self.assertEqual(len(result["winter"]), 920)
        self.assertEqual(
            result["winter"][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_sortIntoCalenderSeasons_overwriteWithShorterData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        data = ts.data[:365]

        result = ts.sort_into_calender_seasons(data)
        self.assertEqual(len(result.items()), 4)
        self.assertEqual(len(result["winter"]), 92)
        self.assertEqual(
            result["winter"][0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_getNumberOfDaysBetweenDays_startDatePreceedsEndDate(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        start_day = ts.data[-1]
        end_day = ts.data[0]

        result = ts.get_number_of_days_between_days(start_day, end_day)

        self.assertEqual(result, 3651)

    def test_getNumberOfDaysBetweenDays_endDatePreceedsStartDate(self):
        ts = Timeseries(self.data_singleDataTypeFlow)

        start_day = ts.data[-1]
        end_day = ts.data[0]

        result = ts.get_number_of_days_between_days(end_day, start_day)

        self.assertEqual(result, -3651)

    def test_applyLagToDataType_positiveLag(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        lag_ts = Timeseries(self.data_singleDataTypeFlow)

        lag_ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, 1)

        self.assertEqual(len(lag_ts.data), len(ts.data) - 1)
        self.assertEqual(
            lag_ts.data[0].date,
            datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            lag_ts.data[0].get_value(TimeseriesDataTypes.FLOW), ts.data[0].get_value(TimeseriesDataTypes.FLOW)
        )

    def test_applyLagToDataType_negativeLag(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        lag_ts = Timeseries(self.data_singleDataTypeFlow)

        lag_ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, -1)

        self.assertEqual(len(lag_ts.data), len(ts.data) - 1)
        self.assertEqual(
            lag_ts.data[-1].date,
            datetime.datetime(1899, 6, 29).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            lag_ts.data[-1].get_value(TimeseriesDataTypes.FLOW), ts.data[-1].get_value(TimeseriesDataTypes.FLOW)
        )

    def test_applyLagToDataType_zeroLag(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        lag_ts = Timeseries(self.data_singleDataTypeFlow)

        lag_ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, 0)

        self.assertEqual(len(lag_ts.data), len(ts.data))
        self.assertEqual(
            lag_ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            lag_ts.data[0].get_value(TimeseriesDataTypes.FLOW), ts.data[0].get_value(TimeseriesDataTypes.FLOW)
        )

    def test_applyLagToDataType_lagLongerThanData(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        lag_ts = Timeseries(self.data_singleDataTypeFlow)

        data = lag_ts.data[:10]
        lag_ts.data = data

        lag_ts.apply_lag_to_data_type(TimeseriesDataTypes.FLOW, 100)

        self.assertEqual(
            lag_ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            lag_ts.data[0].get_value(TimeseriesDataTypes.FLOW), ts.data[0].get_value(TimeseriesDataTypes.FLOW)
        )

    def test_applyLagToDataType_invalidDataType(self):
        ts = Timeseries(self.data_singleDataTypeFlow)
        lag_ts = Timeseries(self.data_singleDataTypeFlow)

        lag_ts.apply_lag_to_data_type(TimeseriesDataTypes.DEPTH, 1)

        self.assertEqual(
            lag_ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            ts.data[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            lag_ts.data[0].get_value(TimeseriesDataTypes.FLOW), ts.data[0].get_value(TimeseriesDataTypes.FLOW)
        )
