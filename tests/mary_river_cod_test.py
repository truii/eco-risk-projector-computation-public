import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class MaryRiverCodPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "mary_river_cod"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(
        self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}, run_period=None
    ):
        if run_period is None:
            return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}
        else:
            return {
                "plugin": self.plugin,
                "run_period": run_period,
                "nodes": nodes,
                "spatial_agg": spatial_agg,
            }

    def test_maryRiverCod_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\data\\NA\\sample\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 51, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [26, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": -28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": -5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidUseDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": "invalid",
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_validation_invalidUseDefaultFlowMissingData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverCod_run_ARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 62)

    def test_maryRiverCod_run_ARIUseDefaultDataWithRunPeriod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 8, "year": 2018}, "end": {"day": 1, "month": 9, "year": 2018}}
        settings = self.createSettings(nodes, spatial_agg, run_period)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        self.assertEqual(daily_intermediate[0]["date"], "01/08/2018")
        self.assertEqual(daily_intermediate[-1]["date"], "01/09/2018")

    def test_maryRiverCod_run_ARIUseDefaultData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 62)

    def test_maryRiverCod_run_NoARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": False,
                        "flow_threshold": 200,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 36)

    def test_maryRiverCod_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": False,
                        "flow_threshold": 200,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_connectivity_event_1_season = sum(
            map(
                lambda d: d["in_connectivity_event_1_season"] if d["in_connectivity_event_1_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_connectivity_event_1_depth_exceeded = sum(
            map(
                lambda d: d["connectivity_event_1_depth_exceeded"]
                if d["connectivity_event_1_depth_exceeded"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_connectivity_event_2_depth_exceeded = sum(
            map(
                lambda d: d["connectivity_event_2_depth_exceeded"]
                if d["connectivity_event_2_depth_exceeded"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_suitable_time_to_complete_period_without_disturbance = sum(
            map(
                lambda d: d["suitable_time_to_complete_period_without_disturbance"]
                if d["suitable_time_to_complete_period_without_disturbance"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_max_flow_remains_below_threshold_during_period_without_disturbance = sum(
            map(
                lambda d: d["max_flow_remains_below_threshold_during_period_without_disturbance"]
                if d["max_flow_remains_below_threshold_during_period_without_disturbance"] is not None
                else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 36)
        self.assertEqual(num_in_connectivity_event_1_season, 62)
        self.assertEqual(num_connectivity_event_1_depth_exceeded, 62)
        self.assertEqual(num_connectivity_event_2_depth_exceeded, 62)
        self.assertEqual(num_suitable_time_to_complete_period_without_disturbance, 62)
        self.assertEqual(num_max_flow_remains_below_threshold_during_period_without_disturbance, 36)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_connectivity_event_1_season"], 1)
        self.assertEqual(daily_intermediate[0]["connectivity_event_1_depth_exceeded"], 1)
        self.assertEqual(daily_intermediate[0]["connectivity_event_2_depth_exceeded"], 1)
        self.assertEqual(daily_intermediate[0]["suitable_time_to_complete_period_without_disturbance"], 1)
        self.assertEqual(daily_intermediate[0]["max_flow_remains_below_threshold_during_period_without_disturbance"], 0)
        self.assertEqual(daily_intermediate[0]["success"], 0)

    def test_maryRiverCod_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": False,
                        "flow_threshold": 200,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2018)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_maryRiverCod_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": False,
                        "flow_threshold": 200,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_maryRiverCod_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "connectivity_event_1": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 8}},
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "connectivity_event_2": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 12}},
                        "temperature_range": [18, 25],
                        "CTF": 0,
                        "amount_above_CTF": 0.25,
                    },
                    "period_without_disturbance": {
                        "use_ARI": False,
                        "flow_threshold": 200,
                        "use_default_data": False,
                        "duration": 28,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
