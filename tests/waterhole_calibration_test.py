import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from helper import calculations as calcs
from tests.test_context import get_data_store


class WaterholeCalibrationPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "waterhole_calibration"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(
        self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}, run_period=None
    ):
        if run_period is None:
            return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}
        else:
            return {
                "plugin": self.plugin,
                "run_period": run_period,
                "nodes": nodes,
                "spatial_agg": spatial_agg,
            }

    def test_waterhole_calibration_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\NA\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_noBathymetry(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidPumping(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 0,
                        "cease_pumping_depth": 5,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_noDepth(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidGroundwater(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 60, "loss_to_deep_drainage": 60},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_noPredictors(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidPredictors(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": -1.5,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidOptimisationScaling(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": -10,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 10,
                        "run_type": "optimise",
                        "season": {"start": {}},
                        "fit_statistic": "nnse",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidRunPeriodException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_validation_invalidFitStatistic(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 10,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "bad",
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_calibration_run_Optimise(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_RunOnce(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "run_once",
                        "season": None,
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        mean_depth = calcs.get_dict_list_mean(daily_intermediate, "modelled_depth")
        median_depth = calcs.get_dict_list_median(daily_intermediate, "modelled_depth")
        min_depth = min(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        max_depth = max(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        self.assertEqual(mean_depth, 4.698)
        self.assertEqual(median_depth, 4.995)
        self.assertEqual(min_depth, 3.40959)
        self.assertEqual(max_depth, 5.0)

        self.assertEqual(daily_intermediate[35]["modelled_depth"], 4.99718)

    def test_waterhole_calibration_run_WithSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_type": "run_once",
                        "season": {"start": {"day": 1, "month": 1, "year": 2009}},
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        self.assertEqual(daily_intermediate[0]["date"], "01/01/2009")

    def test_waterhole_calibration_run_OptimiseNNSE(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_OptimiseRMSE(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "rmse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_OptimiseR2(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "r2",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_OptimisePBIAS(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "pbias",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_OptimiseRSR(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "rsr",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_run_OptimiseScalingAbove100(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
        self.assertIsNotNone(parameter_results["predictors"]["evaporation"]["scaling"])
        self.assertTrue(parameter_results["predictors"]["evaporation"]["scaling"] > 100)
        self.assertIsNotNone(parameter_results["predictors"]["seepage"]["rate"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["rainfall"]["max_infiltration"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["area"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["inflow"])
        self.assertIsNotNone(parameter_results["predictors"]["groundwater"]["loss_to_deep_drainage"])

    def test_waterhole_calibration_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "18/06/2009")
        self.assertIsNotNone(daily_intermediate[0]["modelled_depth"])
        self.assertIsNotNone(daily_intermediate[0]["modelled_volume"])
        self.assertIsNotNone(daily_intermediate[0]["modelled_area"])
        self.assertIsNotNone(daily_intermediate[0]["modelled_GW_volume"])
        self.assertIsNotNone(daily_intermediate[0]["depth"])
        self.assertIsNotNone(daily_intermediate[0]["flow"])
        self.assertIsNotNone(daily_intermediate[0]["rainfall"])
        self.assertIsNotNone(daily_intermediate[0]["evaporation"])

    def test_waterhole_calibration_results_parameterResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                    "depth": ".\\tests\\test_data\\waterhole\\depth.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "predictors": [
                            "evaporation.scaling",
                            "seepage.rate",
                            "rainfall.area",
                            "rainfall.max_infiltration",
                            "groundwater.area",
                            "groundwater.inflow",
                            "groundwater.loss_to_deep_drainage",
                        ],
                        "generations": 1,
                        "population": 1,
                        "scaling": 5,
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "run_type": "optimise",
                        "season": {
                            "start": {"day": 18, "month": 6, "year": 2009},
                            "end": {"day": 10, "month": 10, "year": 2009},
                        },
                        "fit_statistic": "nnse",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data
        self.assertTrue("rmse" in parameter_results)
        self.assertTrue("r2" in parameter_results)
        self.assertTrue("nnse" in parameter_results)
        self.assertTrue("pbias" in parameter_results)
        self.assertTrue("rsr" in parameter_results)
        self.assertTrue("predictors" in parameter_results)
