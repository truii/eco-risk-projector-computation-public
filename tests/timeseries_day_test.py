import datetime
import unittest
from core.globals import TimeseriesDataTypes
from core.season import Season

from core.timeseries_day import TimeseriesDay


class TimeseriesDayTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def get_new_day_empty_day(self, date: datetime.date = None) -> TimeseriesDay:
        if date is None:
            date = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        return TimeseriesDay(date)

    def test_initialiseTimeseriesDay_justDate(self):
        date = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        day = TimeseriesDay(date)

        self.assertEqual(day.date, date)
        self.assertEqual(day.values, {})
        self.assertEqual(day.calculated_values, {})
        self.assertEqual(day.result_values, {})

    def test_initialiseTimeseriesDay_dateAndField(self):
        date = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        day = TimeseriesDay(date, TimeseriesDataTypes.FLOW)

        self.assertEqual(day.date, date)
        self.assertEqual(day.values, {TimeseriesDataTypes.FLOW: None})
        self.assertEqual(day.calculated_values, {})
        self.assertEqual(day.result_values, {})

    def test_initialiseTimeseriesDay_dateAndFieldAndValue(self):
        date = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        day = TimeseriesDay(date, TimeseriesDataTypes.FLOW, 1)

        self.assertEqual(day.date, date)
        self.assertEqual(day.values, {TimeseriesDataTypes.FLOW: 1})
        self.assertEqual(day.calculated_values, {})
        self.assertEqual(day.result_values, {})

    def test_setValues(self):
        day = self.get_new_day_empty_day()
        new_values = {TimeseriesDataTypes.FLOW: 1}
        day.set_values(new_values)

        self.assertEqual(day.values, new_values)

    def test_setValue(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)
        self.assertEqual(day.values, {TimeseriesDataTypes.FLOW: 1})

    def test_setCalculatedValues(self):
        day = self.get_new_day_empty_day()
        new_values = {"flow_average": 1}
        day.set_calculated_values(new_values)

        self.assertEqual(day.calculated_values, new_values)

    def test_setCalculatedValue(self):
        day = self.get_new_day_empty_day()
        day.set_calculated_value("flow_average", 1)
        self.assertEqual(day.calculated_values, {"flow_average": 1})

    def test_setResultValues(self):
        day = self.get_new_day_empty_day()
        new_values = {"flow_result": 1}
        day.set_result_values(new_values)

        self.assertEqual(day.result_values, new_values)

    def test_setResultValue(self):
        day = self.get_new_day_empty_day()
        day.set_result_value("flow_result", 1)
        self.assertEqual(day.result_values, {"flow_result": 1})

    def test_getValue(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)
        self.assertEqual(day.get_value(TimeseriesDataTypes.FLOW), 1)

    def test_getCalculatedValue(self):
        day = self.get_new_day_empty_day()
        day.set_calculated_value("flow_average", 1)
        self.assertEqual(day.get_calculated_value("flow_average"), 1)

    def test_getResultValue(self):
        day = self.get_new_day_empty_day()
        day.set_result_value("flow_result", 1)
        self.assertEqual(day.get_result_value("flow_result"), 1)

    def test_hasValue(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)
        self.assertTrue(day.has_value(TimeseriesDataTypes.FLOW))
        self.assertFalse(day.has_value(TimeseriesDataTypes.DEPTH))

    def test_hasCalculatedValue(self):
        day = self.get_new_day_empty_day()
        day.set_calculated_value("flow_average", 1)
        self.assertTrue(day.has_calculated_value("flow_average"))
        self.assertFalse(day.has_calculated_value("depth_average"))

    def test_hasResultValue(self):
        day = self.get_new_day_empty_day()
        day.set_result_value("flow_result", 1)
        self.assertTrue(day.has_result_value("flow_result"))
        self.assertFalse(day.has_result_value("depth_result"))

    def test_initResultValueKeys_noDefault(self):
        day = self.get_new_day_empty_day()
        day.init_result_value_keys(["flow_result", "depth_result"])
        self.assertEqual(day.get_result_value("flow_result"), None)
        self.assertEqual(day.get_result_value("depth_result"), None)

    def test_initResultValueKeys_withDefault(self):
        day = self.get_new_day_empty_day()
        day.init_result_value_keys(["flow_result", "depth_result"], 10)
        self.assertEqual(day.get_result_value("flow_result"), 10)
        self.assertEqual(day.get_result_value("depth_result"), 10)

    def test_addDay_addASingleDataType(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)

        self.assertFalse(day.has_value(TimeseriesDataTypes.DEPTH))

        addDay = self.get_new_day_empty_day()
        addDay.set_value(TimeseriesDataTypes.DEPTH, 2)

        day.add_day(addDay, TimeseriesDataTypes.DEPTH)

        self.assertTrue(day.has_value(TimeseriesDataTypes.DEPTH))
        self.assertEqual(day.get_value(TimeseriesDataTypes.DEPTH), 2)

    def test_addDay_addMultipleDataTypes(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)

        self.assertFalse(day.has_value(TimeseriesDataTypes.DEPTH))
        self.assertFalse(day.has_value(TimeseriesDataTypes.RAINFALL))

        addDay = self.get_new_day_empty_day()
        addDay.set_value(TimeseriesDataTypes.DEPTH, 2)
        addDay.set_value(TimeseriesDataTypes.RAINFALL, 3)

        day.add_day(addDay, [TimeseriesDataTypes.DEPTH, TimeseriesDataTypes.RAINFALL])

        self.assertTrue(day.has_value(TimeseriesDataTypes.DEPTH))
        self.assertTrue(day.has_value(TimeseriesDataTypes.RAINFALL))
        self.assertEqual(day.get_value(TimeseriesDataTypes.DEPTH), 2)
        self.assertEqual(day.get_value(TimeseriesDataTypes.RAINFALL), 3)

    def test_addDay_addAllDataTypes(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)

        self.assertFalse(day.has_value(TimeseriesDataTypes.DEPTH))
        self.assertFalse(day.has_value(TimeseriesDataTypes.RAINFALL))
        self.assertFalse(day.has_value(TimeseriesDataTypes.EVAPORATION))

        addDay = self.get_new_day_empty_day()
        addDay.set_value(TimeseriesDataTypes.DEPTH, 2)
        addDay.set_value(TimeseriesDataTypes.RAINFALL, 3)
        addDay.set_value(TimeseriesDataTypes.EVAPORATION, 4)

        day.add_day(addDay)

        self.assertTrue(day.has_value(TimeseriesDataTypes.DEPTH))
        self.assertTrue(day.has_value(TimeseriesDataTypes.RAINFALL))
        self.assertTrue(day.has_value(TimeseriesDataTypes.EVAPORATION))
        self.assertEqual(day.get_value(TimeseriesDataTypes.DEPTH), 2)
        self.assertEqual(day.get_value(TimeseriesDataTypes.RAINFALL), 3)
        self.assertEqual(day.get_value(TimeseriesDataTypes.EVAPORATION), 4)

    def test_getValueFromKey_keyIsValue(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)

        result = day.get_value_from_key(TimeseriesDataTypes.FLOW)
        self.assertEqual(result, 1)

    def test_getValueFromKey_keyIsCalculatedValue(self):
        day = self.get_new_day_empty_day()
        day.set_calculated_value("flow_average", 1)

        result = day.get_value_from_key("flow_average")
        self.assertEqual(result, 1)

    def test_getValueFromKey_keyIsResultValue(self):
        day = self.get_new_day_empty_day()
        day.set_result_value("flow_result", 1)

        result = day.get_value_from_key("flow_result")
        self.assertEqual(result, 1)

    def test_getValueFromKey_keyPriorityIsFollowed(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 1)
        day.set_calculated_value(TimeseriesDataTypes.FLOW, 2)
        day.set_result_value(TimeseriesDataTypes.FLOW, 3)

        result = day.get_value_from_key(TimeseriesDataTypes.FLOW)
        self.assertEqual(result, 1)

    def test_getValueFromKey_unfoundReturnsNone(self):
        day = self.get_new_day_empty_day()

        result = day.get_value_from_key(TimeseriesDataTypes.FLOW)
        self.assertEqual(result, None)

    def test_isBetweenDates_isBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        start_day = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        end_day = datetime.datetime(1889, 7, 3).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(start_day, end_day)
        self.assertTrue(result)

    def test_isBetweenDates_isNotBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 5).replace(tzinfo=datetime.timezone.utc))
        start_day = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)
        end_day = datetime.datetime(1889, 7, 3).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(start_day, end_day)
        self.assertFalse(result)

    def test_isBetweenDates_justStartDayIsBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        start_day = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(start_day, None)
        self.assertTrue(result)

    def test_isBetweenDates_justStartDayIsNotBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 6, 30).replace(tzinfo=datetime.timezone.utc))
        start_day = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(start_day, None)
        self.assertFalse(result)

    def test_isBetweenDates_justEndDayIsBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        end_day = datetime.datetime(1889, 7, 5).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(None, end_day)
        self.assertTrue(result)

    def test_isBetweenDates_justEndDayIsNotBetween(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        end_day = datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc)

        result = day.is_between_dates(None, end_day)
        self.assertFalse(result)

    def test_isDateEqual_isEqual(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.is_date_equal(1, 7)

        self.assertTrue(result)

    def test_isDateEqual_isNotEqualBecauseDay(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.is_date_equal(2, 7)

        self.assertFalse(result)

    def test_isDateEqual_isNotEqualBecauseMonth(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.is_date_equal(1, 8)

        self.assertFalse(result)

    def test_formatDate_default(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.format_date()

        self.assertEqual(result, "01/07/1889")

    def test_formatDate_overwriteToDifferentFormat(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.format_date("%d-%m-%Y")

        self.assertEqual(result, "01-07-1889")

    def test_isDayInSeason_isInSeasonOnStartDay(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 7}, "end": {"day": 3, "month": 7}})

        result = day.is_in_season(season)

        self.assertTrue(result)

    def test_isDayInSeason_isInSeasonOnEndDay(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 3).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 7}, "end": {"day": 3, "month": 7}})

        result = day.is_in_season(season)

        self.assertTrue(result)

    def test_isDayInSeason_isInSeasonInMiddle(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 7}, "end": {"day": 3, "month": 7}})

        result = day.is_in_season(season)

        self.assertTrue(result)

    def test_isDayInSeason_isInSeasonLargeSeason(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 2).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 6}, "end": {"day": 31, "month": 3}})

        result = day.is_in_season(season)

        self.assertTrue(result)

    def test_isDayInSeason_isNotInSeasonBefore(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 6, 30).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 7}, "end": {"day": 3, "month": 7}})

        result = day.is_in_season(season)

        self.assertFalse(result)

    def test_isDayInSeason_isNotInSeasonAfter(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 7, 4).replace(tzinfo=datetime.timezone.utc))
        season = Season({"start": {"day": 1, "month": 7}, "end": {"day": 3, "month": 7}})

        result = day.is_in_season(season)

        self.assertFalse(result)

    def test_getSeasonForDay_summer(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 12, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day()

        self.assertEqual(result, "summer")

    def test_getSeasonForDay_autumn(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 3, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day()

        self.assertEqual(result, "autumn")

    def test_getSeasonForDay_winter(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 6, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day()

        self.assertEqual(result, "winter")

    def test_getSeasonForDay_spring(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 9, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day()

        self.assertEqual(result, "spring")

    def test_getSeasonForDay_overwriteSeason(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 6, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day({"summer": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})

        self.assertEqual(result, "summer")

    def test_getSeasonForDay_overwriteSeasonMonthDoesntExist(self):
        day = self.get_new_day_empty_day(datetime.datetime(1889, 6, 1).replace(tzinfo=datetime.timezone.utc))

        result = day.get_season_for_day({"summer": [1]})

        self.assertEqual(result, None)

    def test_setVelocityFromFlow_baseUse(self):
        day = self.get_new_day_empty_day()
        day.set_value(TimeseriesDataTypes.FLOW, 10)

        self.assertFalse(day.has_calculated_value("velocity"))

        day.set_velocity_from_flow(TimeseriesDataTypes.FLOW, "velocity", 0.0005, 50, 0.035)

        self.assertTrue(day.has_calculated_value("velocity"))
        self.assertEqual(day.get_calculated_value("velocity"), 1.2450586509299535)

    def test_setVelocityFromFlow_invalidDataType(self):
        day = self.get_new_day_empty_day()

        day.set_velocity_from_flow(TimeseriesDataTypes.FLOW, "velocity", 0.0005, 50, 0.035)

        self.assertFalse(day.has_calculated_value("velocity"))
