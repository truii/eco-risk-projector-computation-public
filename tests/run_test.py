import unittest
import os
import glob
import shutil
import helper.io as io


class RunTest(unittest.TestCase):
    def tearDown(self):
        res_dir = glob.glob(os.path.join(os.getcwd(), "tests/test_results") + "/*")
        for item in res_dir:
            if item.endswith("error.json"):
                os.remove(item)
            else:
                shutil.rmtree(item)

    @classmethod
    def tearDownClass(cls):
        os.rmdir(cls.test_result_path)

    @classmethod
    def setUpClass(cls):
        cls.test_result_path = os.path.join(os.getcwd(), "tests/test_results")
        os.mkdir(cls.test_result_path)

    def test_run_relativeRun(self):
        os.system("python3 run.py tests/test_data/sample_run.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_run_absRun(self):
        abs_path_file = os.path.join(os.getcwd(), "tests/test_data/sample_run.json")
        abs_path_dir = os.path.join(os.getcwd(), "tests/test_results")
        os.system("python3 run.py {0} -out_dir {1}".format(abs_path_file, abs_path_dir))

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_resultDirName_nameProvided(self):
        os.system("python3 run.py tests/test_data/sample_run.json -out_dir tests/test_results -result_dir_name test")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        self.assertEqual(len([f for f in file_dir if f.endswith("test")]), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_resultDirName_nameNotProvided(self):
        os.system("python3 run.py tests/test_data/sample_run.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        self.assertEqual(len([f for f in file_dir if f.endswith("test")]), 0)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_errorFile_missingDataError(self):
        os.system("python3 run.py tests/test_data/sample_run_missing_data.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        error_file = glob.glob(self.test_result_path + "/*/error.json")
        self.assertEqual(len(error_file), 1)
        error_file_data = io.read_json(error_file[0])
        self.assertEqual(error_file_data["Code"], "MissingDataFileException")

    def test_errorFile_missingParamError(self):
        os.system("python3 run.py tests/test_data/sample_run_missing_param.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        error_file = glob.glob(self.test_result_path + "/*/error.json")
        self.assertEqual(len(error_file), 1)
        error_file_data = io.read_json(error_file[0])
        self.assertEqual(error_file_data["Code"], "MissingParameterException")

    def test_errorFile_invalidParamError(self):
        os.system("python3 run.py tests/test_data/sample_run_invalid_param.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        error_file = glob.glob(self.test_result_path + "/*/error.json")
        self.assertEqual(len(error_file), 1)
        error_file_data = io.read_json(error_file[0])
        self.assertEqual(error_file_data["Code"], "InvalidParameterException")

    def test_runSettingsLog_matchesSettings(self):
        os.system("python3 run.py tests/test_data/runs/ambassis_agassizii.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)

        run_settings_file = glob.glob(self.test_result_path + "/*/run_settings_log.json")
        self.assertEqual(len(run_settings_file), 1)

        run_settings_file_data = io.read_json(run_settings_file[0])

        expected_settings = {
            "node 1": {
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 357.43},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start_day": 1, "start_month": 7, "end_day": 30, "end_month": 6},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
            "spatial_assessment": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
            "run_period": None,
        }

        self.assertEqual(run_settings_file_data, expected_settings)

    def test_runAll_ambassisAgassizii(self):
        os.system("python3 run.py tests/test_data/runs/ambassis_agassizii.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_bananaPrawnGrowth(self):
        os.system("python3 run.py tests/test_data/runs/banana_prawn_growth.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_barramundiGrowth(self):
        os.system("python3 run.py tests/test_data/runs/barramundi_growth.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_barramundiJuvenileRecruitment(self):
        os.system(
            "python3 run.py tests/test_data/runs/barramundi_juvenile_recruitment.json -out_dir tests/test_results"
        )

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_barramundiYearClassStrength(self):
        os.system("python3 run.py tests/test_data/runs/barramundi_year_class_strength.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_bass(self):
        os.system("python3 run.py tests/test_data/runs/bass.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_carpRecruitment(self):
        os.system("python3 run.py tests/test_data/runs/carp_recruitment.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_fishBarriersAndConnectivity(self):
        os.system("python3 run.py tests/test_data/runs/fish_barriers_and_connectivity.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 2)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_fishResilience(self):
        os.system("python3 run.py tests/test_data/runs/fish_resilience.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 2)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_freshesPartialSeason(self):
        os.system("python3 run.py tests/test_data/runs/freshes_partial_season.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_kingThreadfinSalmon(self):
        os.system("python3 run.py tests/test_data/runs/king_threadfin_salmon.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_lowFlowSpawningFish(self):
        os.system("python3 run.py tests/test_data/runs/low_flow_spawning_fish.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_lowflow_partial_season(self):
        os.system("python3 run.py tests/test_data/runs/lowflow_partial_season.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_machrophyteGrowth(self):
        os.system("python3 run.py tests/test_data/runs/machrophyte_growth.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_maryRiverCod(self):
        os.system("python3 run.py tests/test_data/runs/mary_river_cod.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_maryRiverTurtles(self):
        os.system("python3 run.py tests/test_data/runs/mary_river_turtles.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_multiyearFreshesPartialSeason(self):
        os.system(
            "python3 run.py tests/test_data/runs/multiyear_freshes_partial_season.json -out_dir tests/test_results"
        )

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_northernSnakeNeckedTurtle(self):
        os.system("python3 run.py tests/test_data/runs/northern_snake_necked_turtle.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_offshoreReefFishery(self):
        os.system("python3 run.py tests/test_data/runs/offshore_reef_fishery.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_oversupplyPartialSeason(self):
        os.system("python3 run.py tests/test_data/runs/oversupply_partial_season.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_ratingCurve(self):
        os.system("python3 run.py tests/test_data/runs/rating_curve.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_summary(self):
        os.system("python3 run.py tests/test_data/runs/summary.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 3)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("summary_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_tandanus(self):
        os.system("python3 run.py tests/test_data/runs/tandanus.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_turtle(self):
        os.system("python3 run.py tests/test_data/runs/turtle.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_waterhole(self):
        os.system("python3 run.py tests/test_data/runs/waterhole.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("daily_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_waterholeCalibration(self):
        os.system("python3 run.py tests/test_data/runs/waterhole_calibration.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 3)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("parameter_results.json")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_waterholeRunToEmpty(self):
        os.system("python3 run.py tests/test_data/runs/waterhole_run_to_empty.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 3)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("parameter_results.json")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_spellAnalysis(self):
        os.system("python3 run.py tests/test_data/runs/spell_analysis.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 7)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("summary_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_simultaneousSpellAnalysis(self):
        os.system("python3 run.py tests/test_data/runs/simultaneous_spell_analysis.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 5)
        self.assertEqual(len([f for f in files if f.endswith("daily_spatial_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_spatial_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("summary_spatial_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("events_spatial_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_colwellsIndex(self):
        os.system("python3 run.py tests/test_data/runs/colwells_index.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 2)
        self.assertEqual(len([f for f in files if f.endswith("custom_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_baseflowSeperation(self):
        os.system("python3 run.py tests/test_data/runs/baseflow_separation.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 4)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("summary_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)

    def test_runAll_australianBass(self):
        os.system("python3 run.py tests/test_data/runs/australian_bass.json -out_dir tests/test_results")

        file_dir = glob.glob(self.test_result_path + "/*")
        self.assertEqual(len(file_dir), 1)
        files = glob.glob(self.test_result_path + "/*/*")
        self.assertEqual(len(files), 6)
        self.assertEqual(len([f for f in files if f.endswith("daily_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_intermediate_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("yearly_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("temporal_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("spatial_results.csv")]), 1)
        self.assertEqual(len([f for f in files if f.endswith("run_settings_log.json")]), 1)


if __name__ == "__main__":
    unittest.main()
