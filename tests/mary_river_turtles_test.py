import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import calculations as calcs
from tests.test_context import get_data_store


class MaryRiverTurtlesPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "mary_river_turtles"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_maryRiverTurtles_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 5}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverTurtles_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\NA\\Site 1 Depth.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 5}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverTurtles_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 2}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverTurtles_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 5}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": -168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverTurtles_validation_invalidRiskThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 5}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 110,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_maryRiverTurtles_run_elseyaAlbagula(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 3}, "end": {"day": 31, "month": 5}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 168,
                        "nests_inundated_curve": [[0, 0], [1.4, 20], [2.1, 50], [3.3, 80], [18, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 100,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}

        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(len(yearly_intermediate), 11)
        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["success"], 0)
        self.assertEqual(yearly_intermediate[0]["inundation"], None)
        self.assertEqual(yearly_intermediate[0]["risk"], None)
        self.assertEqual(yearly_intermediate[5]["year"], 1894)
        self.assertEqual(yearly_intermediate[5]["success"], 1)
        self.assertEqual(yearly_intermediate[5]["inundation"], 50.5)
        self.assertEqual(yearly_intermediate[5]["risk"], "moderate")
        self.assertEqual(yearly_intermediate[10]["year"], 1899)
        self.assertEqual(yearly_intermediate[10]["success"], 0)
        self.assertEqual(yearly_intermediate[10]["inundation"], None)
        self.assertEqual(yearly_intermediate[10]["risk"], None)

    def test_maryRiverTurtles_run_elusorMacrurus(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 10}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 52,
                        "nests_inundated_curve": [[0, 0], [2.5, 20], [3.8, 50], [5.7, 80], [14, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(len(yearly_intermediate), 11)
        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["success"], 0)
        self.assertEqual(yearly_intermediate[0]["inundation"], None)
        self.assertEqual(yearly_intermediate[0]["risk"], None)
        self.assertEqual(yearly_intermediate[5]["year"], 1894)
        self.assertEqual(yearly_intermediate[5]["success"], 1)
        self.assertEqual(yearly_intermediate[5]["inundation"], 4)
        self.assertEqual(yearly_intermediate[5]["risk"], "low")
        self.assertEqual(yearly_intermediate[10]["year"], 1899)
        self.assertEqual(yearly_intermediate[10]["success"], 0)
        self.assertEqual(yearly_intermediate[10]["inundation"], None)
        self.assertEqual(yearly_intermediate[10]["risk"], None)

    def test_maryRiverTurtles_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 10}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 52,
                        "nests_inundated_curve": [[0, 0], [2.5, 20], [3.8, 50], [5.7, 80], [14, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        yearly_failure_reason = calcs.get_all_values_for_key_from_dict_list(yearly_intermediate, "failure_reason")
        self.assertEqual(yearly_failure_reason[None], 9)
        self.assertEqual(yearly_failure_reason["No potential events found"], 2)

        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["success"], 1)
        self.assertEqual(yearly_intermediate[2]["inundation"], 0.08000000000000007)
        self.assertEqual(yearly_intermediate[2]["risk"], "low")
        self.assertEqual(yearly_intermediate[2]["attempted_recruitment"], 1)
        self.assertEqual(yearly_intermediate[2]["failure_reason"], None)

    def test_maryRiverTurtles_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 10}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 52,
                        "nests_inundated_curve": [[0, 0], [2.5, 20], [3.8, 50], [5.7, 80], [14, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_maryRiverTurtles_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "rainfall": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "partnering_event": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 10}},
                        "CTF": 0,
                        "amount_above_CTF": 0.1,
                    },
                    "rainfall_event": {"rainfall": 10, "days_since_partnering": 14},
                    "stable_water_level": {
                        "duration_days": 52,
                        "nests_inundated_curve": [[0, 0], [2.5, 20], [3.8, 50], [5.7, 80], [14, 100]],
                        "inundation_risk_curve": {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
                        "inundation_risk_threshold": 50,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
