import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class BarramundiYearClassStrengthPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "barramundi_year_class_strength"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_barramundiYearClassStrength_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 4.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiYearClassStrength_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\BAD\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 4.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiYearClassStrength_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"BAD": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 4.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiYearClassStrength_validation_invalidConstant(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": "bad"},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiYearClassStrength_validation_invalidCategories(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 0.1},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": -0.51, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiYearClassStrength_run_dailyResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 4.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        temporal_assessment_result = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0]
        self.assertEqual(yearly_result.data[0]["ycs"], -0.5541775563989715)
        self.assertEqual(yearly_result.data[0]["recruitment_strength"], "poor")
        self.assertEqual(yearly_result.data[0]["risk"], "high")
        self.assertEqual(temporal_assessment_result.data[0], {"year": 2018, "success": None})

    def test_barramundiYearClassStrength_run_dailyResultsSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 2.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        temporal_assessment_result = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0]
        self.assertEqual(yearly_result.data[0]["ycs"], 1.445822443601028)
        self.assertEqual(yearly_result.data[0]["recruitment_strength"], "strong")
        self.assertEqual(yearly_result.data[0]["risk"], "low")
        self.assertEqual(temporal_assessment_result.data[0], {"year": 2018, "success": None})

    def test_barramundiYearClassStrength_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 2.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["ycs"], 1.3797201863915145)
        self.assertEqual(yearly_intermediate[2]["recruitment_strength"], "strong")
        self.assertEqual(yearly_intermediate[2]["risk"], "low")

    def test_barramundiYearClassStrength_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 2.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)

    def test_barramundiYearClassStrength_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7227, "b": 2.1011},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
