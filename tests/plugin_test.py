import unittest
from core.plugin import *


class PluginTest(unittest.TestCase):
    # This should include:
    #   meta_data validation

    # meta_data validation
    # Case: Correct fields
    # Expected: Returns True
    def test_metadataValidation_correctTemporal(self):
        meta_data = {"name": "Example Name", "description": "Example Description", "version": 1, "author": "dev"}
        plugin_type = PluginTypes.TEMPORAL
        new_plugin = Plugin(meta_data, plugin_type)
        self.assertTrue(new_plugin.meta_data == meta_data)
        self.assertTrue(new_plugin.plugin_type == plugin_type)

    def test_metadataValidation_correctTEMPORAL_YEARLY(self):
        meta_data = {"name": "Example Name", "description": "Example Description", "version": 1, "author": "dev"}
        plugin_type = PluginTypes.TEMPORAL_YEARLY
        new_plugin = Plugin(meta_data, plugin_type)
        self.assertTrue(new_plugin.meta_data == meta_data)
        self.assertTrue(new_plugin.plugin_type == plugin_type)

    def test_metadataValidation_correctTEMPORAL_YEARLY_DAILY(self):
        meta_data = {"name": "Example Name", "description": "Example Description", "version": 1, "author": "dev"}
        plugin_type = PluginTypes.TEMPORAL_YEARLY_DAILY
        new_plugin = Plugin(meta_data, plugin_type)
        self.assertTrue(new_plugin.meta_data == meta_data)
        self.assertTrue(new_plugin.plugin_type == plugin_type)

    # meta_data validation
    # Case: Missing field - no name
    # Expected: Raises value error
    def test_metadataValidation_missingField(self):
        meta_data = {"description": "Example Description", "version": 1, "author": "dev"}
        plugin_type = PluginTypes.TEMPORAL_YEARLY_DAILY
        with self.assertRaises(Exception):
            new_plugin = Plugin(meta_data, plugin_type)


if __name__ == "__main__":
    unittest.main()
