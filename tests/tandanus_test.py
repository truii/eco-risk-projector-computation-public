import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class TandanusPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "tandanus"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_tandanus_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\NA\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": -7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": -22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidTempComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "bad",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": -1,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_validation_invalidDepthBounds(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 0, "lower": 10},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_tandanus_run_seasonDefinedMedianFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 134)

    def test_tandanus_run_seasonDefinedAllowPartialSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 134)

    def test_tandanus_run_tempSeasonMedianFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 50)

    def test_tandanus_run_tempSeasonMovingAvg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 51)

    def test_tandanus_run_tempSeasonLTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 84)

    def test_tandanus_run_seasonDefinedSetFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 148)

    def test_tandanus_run_tempSeasonSetFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 64)

    def test_tandanus_run_strictDepthRange(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": True,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 1, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 20)

    def test_tandanus_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_season = sum(
            map(
                lambda d: d["in_season"] if d["in_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_suitable_time_to_complete_flow_event = sum(
            map(
                lambda d: d["suitable_time_to_complete_flow_event"]
                if d["suitable_time_to_complete_flow_event"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_flow_threshold_not_exceeded_in_period = sum(
            map(
                lambda d: d["flow_threshold_not_exceeded_in_period"]
                if d["flow_threshold_not_exceeded_in_period"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_suitable_time_to_complete_depth_event = sum(
            map(
                lambda d: d["suitable_time_to_complete_depth_event"]
                if d["suitable_time_to_complete_depth_event"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_depth_stays_in_range_for_period = sum(
            map(
                lambda d: d["depth_stays_in_range_for_period"]
                if d["depth_stays_in_range_for_period"] is not None
                else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 64)
        self.assertEqual(num_in_season, 206)
        self.assertEqual(num_suitable_time_to_complete_flow_event, 206)
        self.assertEqual(num_flow_threshold_not_exceeded_in_period, 64)
        self.assertEqual(num_suitable_time_to_complete_depth_event, 64)
        self.assertEqual(num_depth_stays_in_range_for_period, 64)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_season"], 0)
        self.assertEqual(daily_intermediate[0]["suitable_time_to_complete_flow_event"], None)
        self.assertEqual(daily_intermediate[0]["flow_threshold_not_exceeded_in_period"], None)
        self.assertEqual(daily_intermediate[0]["suitable_time_to_complete_depth_event"], None)
        self.assertEqual(daily_intermediate[0]["depth_stays_in_range_for_period"], None)
        self.assertEqual(daily_intermediate[0]["success"], 0)

    def test_tandanus_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2018)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_tandanus_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_tandanus_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": True,
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "flow_params": {
                        "use_median_flow": False,
                        "flow_threshold": 500,
                        "depth_bounds": {"upper": 10, "lower": 0},
                        "flow_duration": 30,
                        "depth_duration": 30,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
