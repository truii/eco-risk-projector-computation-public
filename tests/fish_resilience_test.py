import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from tests.test_context import get_data_store


class FishResiliencePluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "fish_resilience"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_fishResilience_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\NA\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 32, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": "abc",
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidTempComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "bad",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": -365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishResilience_run_dateSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 33217)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 4)

    def test_fishResilience_run_dateSeasonAllowPartialSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 33217)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 4)

    def test_fishResilience_run_tempSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "temperature_threshold": 23,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 33217)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 0)

    def test_fishResilience_run_tempSeasonMovingAverage(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "temperature_threshold": 23,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 33217)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 2)

    def test_fishResilience_run_tempSeasonLTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": True,
                        "temperature_threshold": 23,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 33217)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 0)

    def test_fishResilience_run_noReset(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": False,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": False,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 42590)
        self.assertEqual(fail_success, 23794)
        self.assertEqual(resilience_success, 4)

    def test_fishResilience_run_useMedianCTF(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": True,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        stress_success = sum(day["stress"] for day in daily_intermediate)
        fail_success = sum(day["fail"] for day in daily_intermediate)
        resilience_success = sum(day["resilience"] for day in daily_intermediate)
        self.assertEqual(stress_success, 0)
        self.assertEqual(fail_success, 0)
        self.assertEqual(resilience_success, 0)

    def test_fishResilience_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "flow_params": {
                        "use_median_CTF": True,
                        "CTF": 8000,
                        "water_stress_time": 365,
                        "reset_water_stress_after_flow": True,
                        "drying_time": 730,
                    },
                    "movement_params": {
                        "flow_threshold": 10000,
                        "dur": 2,
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate[0]["stress"], 0)
        self.assertEqual(daily_intermediate[0]["fail"], 0)
        self.assertEqual(daily_intermediate[0]["resilience"], 0)
