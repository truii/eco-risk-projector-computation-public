import unittest
import helper.validation as validation


class ValidationTest(unittest.TestCase):
    # This should include:
    #   valid_season
    #   valid_date
    #   valid_rule
    #   valid_curve
    #   dict_contains_keys
    #   value_in_range
    #   data_preceeds_date
    #   is_num

    # valid_season
    # Case: season is valid - year
    # Expected: True
    def test_validSeason_validYear(self):
        season = {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}}
        self.assertTrue(validation.valid_season(season))

    # valid_season
    # Case: season is valid - water year
    # Expected: True
    def test_validSeason_validWaterYear(self):
        season = {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}}
        self.assertTrue(validation.valid_season(season))

    # valid_season
    # Case: season is not valid - month
    # Expected: False
    def test_validSeason_invalidMonth(self):
        season = {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 13}}
        self.assertFalse(validation.valid_season(season))

    # valid_season
    # Case: season is not valid - day
    # Expected: False
    def test_validSeason_invalidDay(self):
        season = {"start": {"day": 32, "month": 1}, "end": {"day": 31, "month": 12}}
        self.assertFalse(validation.valid_season(season))

    # valid_season
    # Case: season is not valid - format
    # Expected: False
    def test_validSeason_invalidFormat(self):
        season = {"start": {"day": 1, "month": 1}, "end": {"month": 13}}
        self.assertFalse(validation.valid_season(season))

    # valid_date
    # Case: date is valid - no year
    # Expected: True
    def test_validDate_validNoYear(self):
        self.assertTrue(validation.valid_date(28, 10))

    # valid_date
    # Case: date is valid - inc year not leap
    # Expected: True
    def test_validDate_validNotLeapYear(self):
        self.assertTrue(validation.valid_date(28, 10, 2001))

    # valid_date
    # Case: date is valid - inc year leap
    # Expected: True
    def test_validDate_validLeapYear(self):
        self.assertTrue(validation.valid_date(29, 2, 2000))

    # valid_date
    # Case: date is invalid - invalid leap day
    # Expected: False
    def test_validDate_invalidLeapDay(self):
        self.assertFalse(validation.valid_date(29, 2, 2001))

    # valid_date
    # Case: date is invalid - invalid year
    # Expected: False
    def test_validDate_invalidYear(self):
        self.assertFalse(validation.valid_date(29, 2, "abc"))

    # valid_date
    # Case: date is invalid - invalid month
    # Expected: False
    def test_validDate_invalidMonth(self):
        self.assertFalse(validation.valid_date(29, 13))

    # valid_date
    # Case: date is invalid - invalid day
    # Expected: False
    def test_validDate_invalidDay(self):
        self.assertFalse(validation.valid_date(32, 1))

    # valid_rule
    # Case: rule is valid
    # Expected: True
    def test_validRule_valid(self):
        rule = {"mag": 10, "dur": 1}
        keys = ["mag", "dur"]
        self.assertTrue(validation.valid_rule(rule, keys))

    # valid_rule
    # Case: rule is invalid - value < 0
    # Expected: False
    def test_validRule_invalidValue(self):
        rule = {"mag": 10, "dur": -1}
        keys = ["mag", "dur"]
        self.assertFalse(validation.valid_rule(rule, keys))

    # valid_rule
    # Case: rule is invalid - missing key
    # Expected: False
    def test_validRule_invalidMissingKey(self):
        rule = {"mag": 10, "dur": 1}
        keys = ["mag", "dur", "ind"]
        self.assertFalse(validation.valid_rule(rule, keys))

    # valid_curve
    # Case: curve is valid
    # Expected: True
    def test_validCurve_valid(self):
        curve = [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]
        self.assertTrue(validation.valid_curve(curve))

    # valid_curve
    # Case: curve is valid and sorted
    # Expected: True
    def test_validCurve_validSort(self):
        curve = [[0, 0], [0.5, 0.4], [0.1, 0.3], [0.8, 0.9], [1, 1]]
        self.assertTrue(validation.valid_curve(curve, True))
        self.assertEqual(curve, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]])

    # valid_curve
    # Case: curve is valid - 2 points
    # Expected: True
    def test_validCurve_valid2Points(self):
        curve = [[0, 0], [1, 1]]
        self.assertTrue(validation.valid_curve(curve))

    # valid_curve
    # Case: curve is invalid - no points
    # Expected: False
    def test_validCurve_invalidNoPoints(self):
        curve = []
        self.assertFalse(validation.valid_curve(curve))

    # valid_curve
    # Case: curve is invalid - not values - None
    # Expected: False
    def test_validCurve_invalidNotValuesNone(self):
        curve = [[None, 0], [1, 1]]
        self.assertFalse(validation.valid_curve(curve, False))

    # valid_curve
    # Case: curve is invalid - not values - String
    # Expected: False
    def test_validCurve_invalidNotValuesString(self):
        curve = [[0, 0], ["1", 1]]
        self.assertFalse(validation.valid_curve(curve, False))

    # valid_curve
    # Case: curve is invalid - format
    # Expected: False
    def test_validCurve_invalidFormat(self):
        curve = {"values": [[0, 0], [1, 1]]}
        self.assertFalse(validation.valid_curve(curve, False))

    # dict_contains_keys
    # Case: dict is valid (contains keys) - only parents
    # Expected: True
    def test_dictContainsKeys_validOnlyParents(self):
        test_dict = {"parent_1": 1, "parent_2": 2, "parent_3": 3}
        parent_keys = ["parent_1", "parent_2", "parent_3"]
        self.assertTrue(validation.dict_contains_keys(test_dict, parent_keys))

    # dict_contains_keys
    # Case: dict is valid (contains keys) - both parents and children
    # Expected: True
    def test_dictContainsKeys_validAllKeys(self):
        test_dict = {
            "parent_1": {"child_1": 1.1, "child_2": 1.2},
            "parent_2": {"child_1": 2.1, "child_2": 2.2},
            "parent_3": 3,
        }
        parent_keys = ["parent_1", "parent_2", "parent_3"]
        child_keys = [["parent_1", ["child_1", "child_2"]], ["parent_2", ["child_1", "child_2"]]]
        self.assertTrue(validation.dict_contains_keys(test_dict, parent_keys, child_keys))

    # dict_contains_keys
    # Case: dict is invalid (doesn't contains keys) - only parents
    # Expected: False
    def test_dictContainsKeys_invalidOnlyParents(self):
        test_dict = {"parent_1": 1, "parent_2": 2, "parent_3": 3}
        parent_keys = ["parent_1", "parent_2", "parent_3", "parent_4"]
        self.assertFalse(validation.dict_contains_keys(test_dict, parent_keys))

    # dict_contains_keys
    # Case: dict is invalid (doesn't contains keys) - both parents and children
    # Expected: False
    def test_dictContainsKeys_invalidAllKeys(self):
        test_dict = {
            "parent_1": {"child_1": 1.1, "child_2": 1.2},
            "parent_2": {"child_1": 2.1, "child_2": 2.2},
            "parent_3": 3,
        }
        parent_keys = ["parent_1", "parent_2", "parent_3"]
        child_keys = [
            ["parent_1", ["child_1", "child_2"]],
            ["parent_2", ["child_1", "child_2"]],
            ["parent_3", ["child_1", "child_2"]],
        ]
        self.assertFalse(validation.dict_contains_keys(test_dict, parent_keys, child_keys))

    # value_in_range
    # Case: value is in range (5 between 1-10)
    # Expected: True
    def test_valueInRange_validPositive(self):
        self.assertTrue(validation.value_in_range(5, 1, 10))

    # value_in_range
    # Case: value is in range (-5 between -1--10)
    # Expected: True
    def test_valueInRange_validNegative(self):
        self.assertTrue(validation.value_in_range(-5, -10, -1))

    # value_in_range
    # Case: value is not in range (below range)
    # Expected: False
    def test_valueInRange_invalidBelow(self):
        self.assertFalse(validation.value_in_range(0, 1, 10))

    # value_in_range
    # Case: value is not in range (above range)
    # Expected: False
    def test_valueInRange_invalidAbove(self):
        self.assertFalse(validation.value_in_range(11, 1, 10))

    # value_in_range
    # Case: value is not in range (invalid value)
    # Expected: False
    def test_valueInRange_invalidValue(self):
        self.assertFalse(validation.value_in_range("a", 1, 10))

    # date_preceeds_date
    # Case: valid - close dates
    # Expected: True
    def test_datePreceedsDate_validClose(self):
        date_1 = {"day": 1, "month": 1, "year": 2020}
        date_2 = {"day": 2, "month": 1, "year": 2020}
        self.assertTrue(validation.date_preceeds_date(date_1, date_2))

    # date_preceeds_date
    # Case: valid - not close dates
    # Expected: True
    def test_datePreceedsDate_validNotClose(self):
        date_1 = {"day": 1, "month": 1, "year": 2020}
        date_2 = {"day": 2, "month": 1, "year": 2050}
        self.assertTrue(validation.date_preceeds_date(date_1, date_2))

    # date_preceeds_date
    # Case: invalid
    # Expected: False
    def test_datePreceedsDate_invalid(self):
        date_1 = {"day": 1, "month": 1, "year": 2020}
        date_2 = {"day": 15, "month": 5, "year": 2019}
        self.assertFalse(validation.date_preceeds_date(date_1, date_2))

    # date_preceeds_date
    # Case: invalid - date
    # Expected: False
    def test_datePreceedsDate_invalidDate(self):
        date_1 = {"day": 32, "month": 1, "year": 2020}
        date_2 = {"day": 15, "month": 5, "year": 2019}
        self.assertFalse(validation.date_preceeds_date(date_1, date_2))

    # date_preceeds_date
    # Case: invalid - format
    # Expected: False
    def test_datePreceedsDate_invalidFormat(self):
        date_1 = {"day": 1, "month": 1, "year": 2020}
        date_2 = {"day": 15, "month": 5}
        self.assertFalse(validation.date_preceeds_date(date_1, date_2))

    # is_num
    # Case: valid - int
    # Expected: True
    def test_isNum_validInt(self):
        self.assertTrue(validation.is_num(1))

    # is_num
    # Case: valid - float
    # Expected: True
    def test_isNum_validFloat(self):
        self.assertTrue(validation.is_num(1.5))

    # is_num
    # Case: valid - negative int
    # Expected: True
    def test_isNum_validNegInt(self):
        self.assertTrue(validation.is_num(-1))

    # is_num
    # Case: valid - negative float
    # Expected: True
    def test_isNum_validNegFloat(self):
        self.assertTrue(validation.is_num(-1.5))

    # is_num
    # Case: valid - 0
    # Expected: True
    def test_isNum_valid0(self):
        self.assertTrue(validation.is_num(0))

    # is_num
    # Case: invalid - string
    # Expected: False
    def test_isNum_invalidString(self):
        self.assertFalse(validation.is_num("abc"))

    # remove_null_keys
    # Case: single level dict, no null keys
    # Expected: dict remain unchanged
    def test_removeNullKeys_singleLevelNoChange(self):
        input_dict = {"a": 1, "b": 2, "c": 3}
        validated_dict = {"a": 1, "b": 2, "c": 3}
        validation.remove_null_keys(validated_dict)
        self.assertEqual(validated_dict, input_dict)

    # remove_null_keys
    # Case: multi level dict, no null keys
    # Expected: dict remain unchanged
    def test_removeNullKeys_multiLevelNoChange(self):
        input_dict = {"a": 1, "b": {"a": 1, "b": 2}, "c": [{"a": 1, "b": 2}]}
        validated_dict = {"a": 1, "b": {"a": 1, "b": 2}, "c": [{"a": 1, "b": 2}]}
        validation.remove_null_keys(validated_dict)
        self.assertEqual(validated_dict, input_dict)

    # remove_null_keys
    # Case: single level dict, null keys
    # Expected: null keys are removed
    def test_removeNullKeys_singleLevelChange(self):
        input_dict = {"a": 1, "b": None, "c": 3}
        validated_dict = {"a": 1, "b": None, "c": 3}
        expected_dict = {"a": 1, "c": 3}
        validation.remove_null_keys(validated_dict)
        self.assertNotEqual(validated_dict, input_dict)
        self.assertEqual(validated_dict, expected_dict)

    # remove_null_keys
    # Case: multi level dict, null keys
    # Expected: null keys are removed
    def test_removeNullKeys_multiLevelChange(self):
        input_dict = {"a": None, "b": {"a": 1, "b": None}, "c": [{"a": 1, "b": None}]}
        validated_dict = {"a": None, "b": {"a": 1, "b": None}, "c": [{"a": 1, "b": None}]}
        expected_dict = {"b": {"a": 1}, "c": [{"a": 1}]}
        validation.remove_null_keys(validated_dict)
        self.assertNotEqual(validated_dict, input_dict)
        self.assertEqual(validated_dict, expected_dict)


if __name__ == "__main__":
    unittest.main()
