import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from helper import calculations as calcs
from tests.test_context import get_data_store


class WaterholePluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "waterhole"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_waterhole_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_validation_noBathymetry(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_validation_invalidGroundwater(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 60, "loss_to_deep_drainage": 60},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_validation_invalidPumping(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 0,
                        "cease_pumping_depth": 5,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_validation_invalidLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": -1.5,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_run_noLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        mean_depth = calcs.get_dict_list_mean(daily_intermediate, "modelled_depth")
        median_depth = calcs.get_dict_list_median(daily_intermediate, "modelled_depth")
        min_depth = min(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        max_depth = max(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        self.assertEqual(mean_depth, 4.698)
        self.assertEqual(median_depth, 4.995)
        self.assertEqual(min_depth, 3.40959)
        self.assertEqual(max_depth, 5.0)

        self.assertEqual(daily_intermediate[35]["modelled_depth"], 4.99718)

    def test_waterhole_run_NagativeLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": -10,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        mean_depth = calcs.get_dict_list_mean(daily_intermediate, "modelled_depth")
        median_depth = calcs.get_dict_list_median(daily_intermediate, "modelled_depth")
        min_depth = min(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        max_depth = max(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        self.assertEqual(mean_depth, 4.697)
        self.assertEqual(median_depth, 4.995)
        self.assertEqual(min_depth, 3.36385)
        self.assertEqual(max_depth, 5.0)

        self.assertEqual(daily_intermediate[35]["modelled_depth"], 4.99718)

    def test_waterhole_run_failureBasedAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {
                        "comparison": "gte",
                        "num_successes": 10,
                        "start_month": "january",
                        "is_failure_based": True,
                    },
                    "daily_agg": {
                        "comparison": "lte",
                        "criteria": "threshold",
                        "threshold": 4,
                        "is_failure_based": True,
                    },
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1, "is_failure_based": True},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_assessment = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0].data
        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data

        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment)), 2584)
        self.assertEqual(sum(map(lambda d: d["success"], yearly_assessment)), 7)
        self.assertEqual(sum(map(lambda d: d["success"], temporal_assessment)), 7)

    def test_waterhole_run_scalingAbove100(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        mean_depth = calcs.get_dict_list_mean(daily_intermediate, "modelled_depth")
        median_depth = calcs.get_dict_list_median(daily_intermediate, "modelled_depth")
        min_depth = min(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        max_depth = max(list(map(lambda d: d["modelled_depth"], daily_intermediate)))
        self.assertEqual(mean_depth, 4.537)
        self.assertEqual(median_depth, 4.98)
        self.assertEqual(min_depth, 2.62906)
        self.assertEqual(max_depth, 5.0)

        self.assertEqual(daily_intermediate[35]["modelled_depth"], 4.98597)

    def test_waterhole_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "21/07/2006")
        self.assertEqual(daily_intermediate[0]["modelled_depth"], 5.0)
        self.assertEqual(daily_intermediate[0]["modelled_volume"], 85634.46095)
        self.assertEqual(daily_intermediate[0]["modelled_area"], 51918.80719)
        self.assertEqual(daily_intermediate[0]["modelled_GW_volume"], 179792.5)
        self.assertEqual(daily_intermediate[0]["flow"], 0)
        self.assertEqual(daily_intermediate[0]["rainfall"], 0)
        self.assertEqual(daily_intermediate[0]["evaporation"], 3.700000048)

    def test_waterhole_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2006)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_waterhole_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2006)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_waterhole_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\waterhole\\flow.csv",
                    "rainfall": ".\\tests\\test_data\\waterhole\\rainfall.csv",
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2006)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
