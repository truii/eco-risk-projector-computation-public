import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class MachrophyteGrowthPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "machrophyte_growth"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_machrophyteGrowth_validation_invalidNoName(self):
        nodes = [
            {
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "decay_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_2",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_machrophyteGrowth_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "decay_driver_1",
                            "data": {"data": ".\\dat\\a\\sample\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_2",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_machrophyteGrowth_validation_invalidType(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "reduction",
                            "name": "decay_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_2",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_machrophyteGrowth_validation_invalidCurve(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "decay_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_1",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1, 1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "growth_driver_2",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_machrophyteGrowth_run_oneOfEachComponent(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["total"], 3.79281)
        self.assertEqual(daily_intermediate[0]["total"], 0.1425)

    def test_machrophyteGrowth_run_multipleGrowth(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["total"], 2.18195)
        self.assertEqual(daily_intermediate[0]["total"], 0.01222)

    def test_machrophyteGrowth_run_multipleBoth(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "decay",
                            "name": "depth",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"},
                            "magnitude_curve": [[0.5, 0.1], [1, 0.8], [1.5, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["total"], 0.0)
        self.assertEqual(daily_intermediate[0]["total"], 0.00345)

    def test_machrophyteGrowth_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "decay",
                            "name": "depth",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"},
                            "magnitude_curve": [[0.5, 0.1], [1, 0.8], [1.5, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate[0]["total"], 0.00345)
        self.assertEqual(daily_intermediate[0]["change"], 0.00345)

    def test_machrophyteGrowth_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "decay",
                            "name": "depth",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"},
                            "magnitude_curve": [[0.5, 0.1], [1, 0.8], [1.5, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["total"], 0.04228)

    def test_machrophyteGrowth_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "decay",
                            "name": "depth",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"},
                            "magnitude_curve": [[0.5, 0.1], [1, 0.8], [1.5, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_machrophyteGrowth_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {
                    "components": [
                        {
                            "type": "decay",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[1000, 0.1], [5000, 0.5], [10000, 1]],
                        },
                        {
                            "type": "decay",
                            "name": "depth",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"},
                            "magnitude_curve": [[0.5, 0.1], [1, 0.8], [1.5, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "flow",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                            "magnitude_curve": [[100, 0.1], [500, 0.5], [1000, 1]],
                        },
                        {
                            "type": "growth",
                            "name": "temperature",
                            "data": {"data": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv"},
                            "magnitude_curve": [[5, 0.01], [20, 0.1], [30, 1], [35, 1], [36, 0]],
                        },
                    ]
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
