import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class BassPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "bass"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_bass_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                "season": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "temperature_threshold": 15,
                    "use_temperature": False,
                    "use_temperature_moving_average": False,
                    "temperature_moving_average_duration": 7,
                    "allow_partial_seasons": False,
                    "temperature_comparison": "lte",
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\BAD\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "temperature_threshold": 15,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "season": {"start": {"day": -1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 15,
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "temperature_threshold": 15,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "temperature_threshold": 15,
                        "use_temperature": True,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidTempComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "temperature_threshold": 15,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "bad",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_validation_invalidDepth(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": -1},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 15,
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bass_run_dateSeasonAllData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 15,
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 11581)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 177)

    def test_bass_run_dateSeasonAllDataAllowPartial(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 15,
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 11581)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 177)

    def test_bass_run_tempSeasonAllData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1.5},
                    "season": {
                        "temperature_threshold": 23,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 3)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 2)

    def test_bass_run_tempSeasonAllDataGTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1.5},
                    "season": {
                        "temperature_threshold": 23,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 222)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 31)

    def test_bass_run_tempSeasonAllDataMovingAvg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50], "depth_threshold": 1.5},
                    "season": {
                        "temperature_threshold": 23,
                        "use_temperature": True,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 4,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 1)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 1)

    def test_bass_run_tempSeasonNoDepth(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow_params": {"use_flow_percentiles": True, "flow_thresholds": [90, 50]},
                    "season": {
                        "temperature_threshold": 23,
                        "use_temperature": True,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 3160)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 79)

    def test_bass_run_setThresholdAllData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {"use_flow_percentiles": False, "flow_thresholds": [100, 0]},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        self.assertEqual(num_success, 66795)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 365)

    def test_bass_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {"use_flow_percentiles": False, "flow_thresholds": [100, 0]},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_season = sum(
            map(
                lambda d: d["in_season"] if d["in_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_movement_season = sum(
            map(
                lambda d: d["in_movement_season"] if d["in_movement_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_depth_threshold_met = sum(
            map(
                lambda d: d["depth_threshold_met"] if d["depth_threshold_met"] is not None else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 66795)
        self.assertEqual(num_in_season, 365)
        self.assertEqual(num_in_movement_season, 365)
        self.assertEqual(num_depth_threshold_met, 0)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_season"], 1)
        self.assertEqual(daily_intermediate[0]["in_movement_season"], 1)
        self.assertEqual(daily_intermediate[0]["depth_threshold_met"], None)
        self.assertEqual(daily_intermediate[0]["success"], 1)

    def test_bass_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "flow_params": {"use_flow_percentiles": False, "flow_thresholds": [100, 0]},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 1889)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_bass_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "flow_params": {"use_flow_percentiles": False, "flow_thresholds": [100, 0]},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_bass_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "flow_params": {"use_flow_percentiles": False, "flow_thresholds": [100, 0]},
                    "season": {
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature": False,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "gte", "criteria": "threshold", "threshold": 1},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
