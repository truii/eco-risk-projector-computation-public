import unittest
from core.globals import Months
import helper.calculations as calculations


class CalcTest(unittest.TestCase):
    def test_getCurveValue_linearPoint(self):
        self.assertEqual(calculations.get_curve_value(0.5, [[0, 0], [1, 1]]), 0.5)

    def test_getCurveValue_complexCurve1(self):
        self.assertEqual(calculations.get_curve_value(0.05, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 0.15)

    def test_getCurveValue_complexCurve2(self):
        self.assertEqual(calculations.get_curve_value(0.2, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 0.325)

    def test_getCurveValue_complexCurve3(self):
        self.assertEqual(calculations.get_curve_value(0.65, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 0.65)

    def test_getCurveValue_complexCurve4(self):
        self.assertEqual(calculations.get_curve_value(0.9, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 0.95)

    def test_getCurveValue_pointLessThenCurve(self):
        self.assertEqual(calculations.get_curve_value(-10, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 0)

    def test_getCurveValue_pointLessThenCurve(self):
        self.assertEqual(calculations.get_curve_value(10, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]]), 1)

    def test_getCurveValue_inverseCurve(self):
        self.assertEqual(
            calculations.get_curve_value(0.3, [[0, 0], [0.1, 0.3], [0.5, 0.4], [0.8, 0.9], [1, 1]], True), 0.1
        )

    def test_getCurveValue_invalidCurveEmpty(self):
        with self.assertRaises(ValueError):
            calculations.get_curve_value(5, [])

    def test_getCurveValue_invalidCurveNone(self):
        with self.assertRaises(ValueError):
            calculations.get_curve_value(5, [[[0, 0], [0.1, 0.3], [None, 0.4], [0.8, 0.9], [1, 1]]])

    def test_getDictListMean_expectedUse(self):
        self.assertEqual(
            calculations.get_dict_list_mean(
                [{"value": 1}, {"value": 2}, {"value": 3}, {"value": 4}, {"value": 5}], "value"
            ),
            3,
        )

    def test_getDictListMean_noneValues(self):
        self.assertEqual(
            calculations.get_dict_list_mean(
                [{"value": None}, {"value": 2}, {"value": 3}, {"value": 4}, {"value": 5}], "value"
            ),
            3.5,
        )

    def test_getDictListMean_emptyList(self):
        self.assertEqual(calculations.get_dict_list_mean([], "value"), None)

    def test_getDictListMedian_expectedUse(self):
        self.assertEqual(
            calculations.get_dict_list_median(
                [{"value": 1}, {"value": 2}, {"value": 3}, {"value": 4}, {"value": 5}], "value"
            ),
            3,
        )

    def test_getDictListMedian_noneValues(self):
        self.assertEqual(
            calculations.get_dict_list_median(
                [{"value": None}, {"value": 2}, {"value": 3}, {"value": 4}, {"value": 5}], "value"
            ),
            3.5,
        )

    def test_getDictListMedian_emptyList(self):
        self.assertEqual(calculations.get_dict_list_median([], "value"), None)

    def test_getWeightedAverage_normalUse(self):
        res = calculations.get_weighted_average(
            [{"value": 10, "weight": 0.5}, {"value": 10, "weight": 0.5}], "value", "weight"
        )
        self.assertEqual(res, 10)

    def test_getWeightedAverage_oneItem(self):
        res = calculations.get_weighted_average([{"value": 10, "weight": 0.5}], "value", "weight")
        self.assertEqual(res, 5)

    def test_getWeightedAverage_noData(self):
        res = calculations.get_weighted_average([], "value", "weight")
        self.assertEqual(res, 0)

    def test_getWeightedAverage_wrongKey(self):
        invalid_args = ([{"value": 10, "weight": 0.5}, {"value": 10, "weight": 0.5}], "value1", "weight1")
        self.assertRaises(Exception, calculations.get_weighted_average, invalid_args)

    def test_getRiskCategory_highRisk(self):
        self.assertEqual(calculations.get_risk_category(50, 20, 60), "high")

    def test_getRiskCategory_lowRisk(self):
        self.assertEqual(calculations.get_risk_category(50, 20, 10), "low")

    def test_getRiskCategory_moderateRisk(self):
        self.assertEqual(calculations.get_risk_category(50, 20, 30), "moderate")

    def test_getRiskCategory_verySmallValue(self):
        self.assertEqual(calculations.get_risk_category(50, 20, -1000), "low")

    def test_getRiskCategory_veryLargeValue(self):
        self.assertEqual(calculations.get_risk_category(50, 20, 1000), "high")

    def test_getFlattenedList_lists(self):
        test_list = [[1, 2], [3, 4, 5], [6, 7, 8]]
        res = calculations.get_flattened_list(test_list)
        self.assertEqual(res, [1, 2, 3, 4, 5, 6, 7, 8])

    def test_getFlattenedList_values(self):
        test_list = [1, 2, 3, 4, 5, 6, 7, 8]
        res = calculations.get_flattened_list(test_list)
        self.assertEqual(res, [1, 2, 3, 4, 5, 6, 7, 8])

    def test_getFlattenedList_valuesLists(self):
        test_list = [1, 2, [3, 4, 5], [6, 7, 8]]
        res = calculations.get_flattened_list(test_list)
        self.assertEqual(res, [1, 2, 3, 4, 5, 6, 7, 8])

    def test_getFlattenedList_listListsLists(self):
        test_list = [1, 2, [3, [4, 5]], [6, 7, 8]]
        res = calculations.get_flattened_list(test_list)
        self.assertEqual(res, [1, 2, 3, [4, 5], 6, 7, 8])

    def test_roundToPositiveInt_posIntUp(self):
        self.assertEqual(calculations.round_to_positive_int(4.7), 5)

    def test_roundToPositiveInt_posIntHalf(self):
        self.assertEqual(calculations.round_to_positive_int(4.5), 4)

    def test_roundToPositiveInt_posIntDown(self):
        self.assertEqual(calculations.round_to_positive_int(4.3), 4)

    def test_roundToPositiveInt_posInt0(self):
        self.assertEqual(calculations.round_to_positive_int(0.3), 1)

    def test_roundToPositiveInt_posIntNegative(self):
        self.assertEqual(calculations.round_to_positive_int(-1.1), 1)

    def test_extractCurve_noFloatOrValidation(self):
        test_list = [[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]]
        res = calculations.extract_curve(test_list, 0, 1)
        self.assertEqual(res, [[1, 2], [1, 2], [1, 2], [1, 2]])

    def test_extractCurve_parseFloatNoValidation(self):
        test_list = [["1", "2", "3", "4"], ["1", "2", "3", "4"], ["1", "2", "3", "4"], ["1", "2", "3", "4"]]
        res = calculations.extract_curve(test_list, 0, 1, True)
        self.assertEqual(res, [[1, 2], [1, 2], [1, 2], [1, 2]])

    def test_extractCurve_parseFloatValid(self):
        test_list = [["1", "2", "3", "4"], ["1", "2", "3", "4"], ["1", "2", "3", "4"], ["1", "2", "3", "4"]]
        res = calculations.extract_curve(test_list, 0, 1, True, True)
        self.assertEqual(res, [[1, 2], [1, 2], [1, 2], [1, 2]])

    def test_extractCurve_parseFloatInvalid(self):
        invalid_args = [[], 0, 1, True, True]
        self.assertRaises(Exception, calculations.extract_curve, invalid_args)

    def test_extractValuesFromDictList_allValues(self):
        data = [
            {"actual": 1, "modelled": 1},
            {"actual": 2, "modelled": 2},
            {"actual": 3, "modelled": 3},
            {"actual": 4, "modelled": 4},
        ]
        res = calculations.extract_values_from_dict_list(data, "actual")
        self.assertEqual(res, [1, 2, 3, 4])

    def test_extractValuesFromDictList_someNones(self):
        data = [
            {"actual": 1, "modelled": 1},
            {"actual": 2, "modelled": 2},
            {"actual": None, "modelled": 3},
            {"actual": 4, "modelled": 4},
        ]
        res = calculations.extract_values_from_dict_list(data, "actual")
        self.assertEqual(res, [1, 2, 4])

    def test_extractValuesFromDictList_allNones(self):
        data = [
            {"actual": None, "modelled": 1},
            {"actual": None, "modelled": 2},
            {"actual": None, "modelled": 3},
            {"actual": None, "modelled": 4},
        ]
        res = calculations.extract_values_from_dict_list(data, "actual")
        self.assertEqual(res, [])

    def test_getAllValuesForKeyFromDictList_expectedUse1Value(self):
        data = [{"value": 1}, {"value": 1}, {"value": 1}, {"value": 1}, {"value": 1}]
        res = calculations.get_all_values_for_key_from_dict_list(data, "value")
        self.assertEqual(res, {1: 5})

    def test_getAllValuesForKeyFromDictList_expectedUse2Values(self):
        data = [{"value": 1}, {"value": 1}, {"value": 2}, {"value": 2}, {"value": 1}]
        res = calculations.get_all_values_for_key_from_dict_list(data, "value")
        self.assertEqual(res, {1: 3, 2: 2})

    def test_getAllValuesForKeyFromDictList_expectedUse5Values(self):
        data = [{"value": 1}, {"value": 2}, {"value": 3}, {"value": 4}, {"value": 5}]
        res = calculations.get_all_values_for_key_from_dict_list(data, "value")
        self.assertEqual(res, {1: 1, 2: 1, 3: 1, 4: 1, 5: 1})

    def test_getMonthNumber_january(self):
        self.assertEqual(calculations.get_month_number(Months.JANUARY), 1)

    def test_getMonthNumber_february(self):
        self.assertEqual(calculations.get_month_number(Months.FEBRUARY), 2)

    def test_getMonthNumber_march(self):
        self.assertEqual(calculations.get_month_number(Months.MARCH), 3)

    def test_getMonthNumber_april(self):
        self.assertEqual(calculations.get_month_number(Months.APRIL), 4)

    def test_getMonthNumber_may(self):
        self.assertEqual(calculations.get_month_number(Months.MAY), 5)

    def test_getMonthNumber_june(self):
        self.assertEqual(calculations.get_month_number(Months.JUNE), 6)

    def test_getMonthNumber_july(self):
        self.assertEqual(calculations.get_month_number(Months.JULY), 7)

    def test_getMonthNumber_august(self):
        self.assertEqual(calculations.get_month_number(Months.AUGUST), 8)

    def test_getMonthNumber_september(self):
        self.assertEqual(calculations.get_month_number(Months.SEPTEMBER), 9)

    def test_getMonthNumber_october(self):
        self.assertEqual(calculations.get_month_number(Months.OCTOBER), 10)

    def test_getMonthNumber_november(self):
        self.assertEqual(calculations.get_month_number(Months.NOVEMBER), 11)

    def test_getMonthNumber_december(self):
        self.assertEqual(calculations.get_month_number(Months.DECEMBER), 12)

    def test_getDictKeyWithMaxValue_usualCase(self):
        self.assertEqual(
            calculations.get_dict_key_with_max_value({"key_with_max_value": 10, "1": 1, "2": 2, "3": 3, "4": 4}),
            "key_with_max_value",
        )

    def test_getDictKeyWithMaxValue_onlyOneKey(self):
        self.assertEqual(
            calculations.get_dict_key_with_max_value({"key_with_max_value": 10}),
            "key_with_max_value",
        )

    def test_getDictKeyWithMaxValue_noKeys(self):
        self.assertEqual(calculations.get_dict_key_with_max_value({}), None)

    def test_getDictKeyWithMaxValue_multipleKeysWithMax(self):
        self.assertEqual(
            calculations.get_dict_key_with_max_value({"key_with_max_value": 10, "1": 10, "2": 10, "3": 3, "4": 4}),
            "key_with_max_value",
        )

    def test_isValueInRange_allValuesTrue(self):
        self.assertEqual(calculations.is_value_in_range(1, 5, 10), True)

    def test_isValueInRange_allValuesFalse(self):
        self.assertEqual(calculations.is_value_in_range(1, 0, 10), False)

    def test_isValueInRange_minNone(self):
        self.assertEqual(calculations.is_value_in_range(None, 0, 10), False)

    def test_isValueInRange_valueNone(self):
        self.assertEqual(calculations.is_value_in_range(1, None, 10), False)

    def test_isValueInRange_maxNone(self):
        self.assertEqual(calculations.is_value_in_range(1, 0, None), False)


if __name__ == "__main__":
    unittest.main()
