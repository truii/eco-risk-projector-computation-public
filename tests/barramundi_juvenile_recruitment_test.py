import unittest
from core.globals import ResultTypes
from core.exceptions import *
from core.model import Model
from tests.test_context import get_data_store


class BarramundiJuvenileRecruitmentPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "barramundi_juvenile_recruitment"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_barramundiJuvenileRecruitment_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\BAD\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": -5}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_validation_invalidFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": -10,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_validation_invalidNumMonths(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": -1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_validation_invalidTemporalRisk(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 15},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiJuvenileRecruitment_run_dailyResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500000,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400000,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        temporal_assessment_result = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0]
        self.assertEqual(yearly_result.data[0]["rule_1"], 0)
        self.assertEqual(yearly_result.data[0]["rule_2"], 1)
        self.assertEqual(yearly_result.data[0]["rule_3"], 0)
        self.assertEqual(yearly_result.data[0]["success"], 0)
        self.assertEqual(yearly_result.data[0]["recruitment_strength"], "poor")
        self.assertEqual(yearly_result.data[0]["risk"], "high")
        self.assertEqual(temporal_assessment_result.data[0], {"year": 2018, "success": None})

    def test_barramundiJuvenileRecruitment_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500000,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400000,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["rule_1"], 0)
        self.assertEqual(yearly_intermediate[2]["rule_2"], 1)
        self.assertEqual(yearly_intermediate[2]["rule_3"], 0)
        self.assertEqual(yearly_intermediate[2]["recruitment_strength"], "poor")
        self.assertEqual(yearly_intermediate[2]["risk"], "high")

    def test_barramundiJuvenileRecruitment_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500000,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400000,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_barramundiJuvenileRecruitment_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "total_flow_volume": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "flow_threshold": 1500000,
                    },
                    "maximum_flows_timing": {
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 28, "month": 2}}
                    },
                    "month_flow": {
                        "season": {"start": {"day": 1, "month": 11}, "end": {"day": 31, "month": 3}},
                        "num_month_exceeding": 1,
                        "flow_threshold": 400000,
                    },
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 3, "low": 1},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
