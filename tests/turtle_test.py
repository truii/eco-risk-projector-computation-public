import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from helper import calculations as calcs
from tests.test_context import get_data_store


class TurtlePluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "turtle"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_turtle_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\NA\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_validation_invalidARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": -5,
                        "use_default_data": False,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_validation_invalidPeriod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                        "drying_period": -30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_validation_invalidUseDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": "invalid",
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_validation_invalidUseDefaultFlowMissingData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_turtle_run_singleYearNoARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": False,
                        "ARI": 5,
                        "use_default_data": False,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        success_count = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "success")
        self.assertEqual(success_count[1], 365)

    def test_turtle_run_singleYearARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": False,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        success_count = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "success")
        self.assertEqual(success_count[1], 61)
        self.assertEqual(success_count[0.5], 60)
        self.assertEqual(success_count[0], 244)

    def test_turtle_run_useDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        success_count = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "success")
        self.assertEqual(success_count[1], 61)
        self.assertEqual(success_count[0.5], 60)
        self.assertEqual(success_count[0], 244)

    def test_turtle_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["success"], 1)

    def test_turtle_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2018)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_turtle_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_turtle_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "flow_params": {
                        "flow_threshold": 5,
                        "use_ARI": True,
                        "ARI": 5,
                        "use_default_data": True,
                        "drying_period": 30,
                        "aestivation_period": 30,
                    }
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
