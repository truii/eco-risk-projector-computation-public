from enum import Enum
from core.data_store import DataStore
from core.memory_data_store import MemoryDataStore
from core.sqlite_data_store import SQLiteDataStore


class DataStoreTypes(str, Enum):
    MEMORY = "memory"
    SQLITE = "sqlite"


test_data_store_type = DataStoreTypes.MEMORY


def get_data_store() -> DataStore:
    global test_data_store_type

    if test_data_store_type == DataStoreTypes.SQLITE:
        return SQLiteDataStore()

    return MemoryDataStore()


def set_data_store_type(data_store_type: DataStoreTypes):
    global test_data_store_type
    test_data_store_type = data_store_type
