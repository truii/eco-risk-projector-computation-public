import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class CarpRecruitmentPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "carp_recruitment"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_carpRecruitment_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\data\\NA\\sample\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 0}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidTempComparisonType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "bad",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": -30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidUseDefaultFlowMissingData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_validation_invalidUseDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": "invalid",
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_carpRecruitment_run_dateSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 335)

    def test_carpRecruitment_run_dateSeasonAllowPartialSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 335)

    def test_carpRecruitment_run_tempSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 335)

    def test_carpRecruitment_run_tempSeasonUseMovingAvg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 335)

    def test_carpRecruitment_run_tempSeasonLTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": False,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 0)

    def test_carpRecruitment_run_tempSeasonARI(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": False,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 0)

    def test_carpRecruitment_run_useDefaultFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 0)

    def test_carpRecruitment_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow Single Year.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_season = sum(
            map(
                lambda d: d["in_season"] if d["in_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_flow_threshold_exceeded = sum(
            map(
                lambda d: d["flow_threshold_exceeded"] if d["flow_threshold_exceeded"] is not None else 0,
                daily_intermediate,
            )
        )
        num_drying_time_not_exceeded = sum(
            map(
                lambda d: d["drying_time_not_exceeded"] if d["drying_time_not_exceeded"] is not None else 0,
                daily_intermediate,
            )
        )
        num_recruitment_days_exceeded = sum(
            map(
                lambda d: d["recruitment_days_exceeded"] if d["recruitment_days_exceeded"] is not None else 0,
                daily_intermediate,
            )
        )
        num_final_day_flow_threshold_exceeded = sum(
            map(
                lambda d: d["final_day_flow_threshold_exceeded"]
                if d["final_day_flow_threshold_exceeded"] is not None
                else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 0)
        self.assertEqual(num_in_season, 365)
        self.assertEqual(num_flow_threshold_exceeded, 1)
        self.assertEqual(num_drying_time_not_exceeded, 0)
        self.assertEqual(num_recruitment_days_exceeded, 1)
        self.assertEqual(num_final_day_flow_threshold_exceeded, 0)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_season"], 1)
        self.assertEqual(daily_intermediate[0]["flow_threshold_exceeded"], 0)
        self.assertEqual(daily_intermediate[0]["drying_time_not_exceeded"], None)
        self.assertEqual(daily_intermediate[0]["recruitment_days_exceeded"], None)
        self.assertEqual(daily_intermediate[0]["final_day_flow_threshold_exceeded"], None)
        self.assertEqual(daily_intermediate[0]["success"], 0)

    def test_carpRecruitment_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 1889)
        self.assertEqual(yearly_assessment[0]["success"], 0)

    def test_carpRecruitment_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_carpRecruitment_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "spawning_conditions": {
                        "use_ARI": True,
                        "ARI": 5,
                        "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                        "flow_threshold": 5,
                        "use_default_data": True,
                        "recruitment_period": 30,
                        "drying_time": 30,
                    },
                    "season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
