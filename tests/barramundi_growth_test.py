import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class BarramundiGrowthPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "barramundi_growth"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_barramundiGrowth_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiGrowth_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\BAD\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiGrowth_validation_invalidConstants(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "BAD": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiGrowth_validation_invalidFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": -1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiGrowth_validation_invalidDailyAggThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_barramundiGrowth_run_dailyResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        self.assertEqual(daily_result.data[0]["growth"], 0.01698542121549145)
        growth_sum = round(
            sum(
                list(
                    map(
                        lambda d: d["growth"],
                        filter(lambda d: d["growth"] is not None, daily_result.data),
                    )
                )
            ),
            3,
        )
        self.assertEqual(growth_sum, 77.288)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 183)

    def test_barramundiGrowth_run_thresholdDailyAgg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold", "threshold": 10},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        self.assertEqual(daily_result.data[0]["growth"], 0.01698542121549145)
        growth_sum = round(
            sum(
                list(
                    map(
                        lambda d: d["growth"],
                        filter(lambda d: d["growth"] is not None, daily_result.data),
                    )
                )
            ),
            3,
        )
        self.assertEqual(growth_sum, 77.288)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 365)

    def test_barramundiGrowth_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold", "threshold": 10},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["growth"], 0.01698542121549145)

    def test_barramundiGrowth_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold", "threshold": 10},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2018)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_barramundiGrowth_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold", "threshold": 10},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_barramundiGrowth_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "constants": {
                        "a": {"summer": -0.31159, "autumn": -0.17263, "winter": -0.01014, "spring": -0.1511},
                        "b": {"summer": 0.107743, "autumn": 0.059681, "winter": 0.003506, "spring": 0.052243},
                    },
                    "flow_threshold": {"summer": 1300, "autumn": 1300, "winter": 1300, "spring": 1300},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "threshold", "threshold": 10},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
