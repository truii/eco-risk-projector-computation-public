import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class RatingCurvePluginTest(unittest.TestCase):
    def setUp(self):
        self.plugin = "rating_curve"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_ratingCurve_validation_invalidDataType(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "bad", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_ratingCurve_validation_noData(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
            {
                "meta_data": {"name": "node_2"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_ratingCurve_run_normalRun(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        self.assertEqual(daily_intermediate[0]["value"], 0.007916500000000002)

    def test_ratingCurve_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate[0]["value"], 0.007916500000000002)

    def test_ratingCurve_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 1889)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_ratingCurve_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_ratingCurve_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {"data_type": "flow", "curve_points": [[0, 0], [10000, 0.5], [100000, 1]]},
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
