import unittest
import helper.io as io
import os


class IOTest(unittest.TestCase):
    # This should include:
    #   File path validation
    #   Dir path validation
    #   Get abs file path

    # File Path Validation
    # Case: File Path Exists
    # Expected: Returns True
    def test_filePathValidation_filePathExists(self):
        cwd = os.getcwd()
        test_path = io.format_path("tests\\io_test.py")
        self.assertTrue(io.valid_filepath(os.path.join(cwd, test_path)), "{0} should be a valid file path".format(cwd))

        # Case: File Path Does Not Exist
        # Expected: Returns False

    def test_filePathValidation_filePathDoesNotExists(self):
        file_path = "doesnt exist.txt"
        self.assertFalse(io.valid_filepath(file_path), "{0} should NOT be a valid file path".format(file_path))

        # Case: File Path is Invalid (Empty String, None, Int)
        # Expected: Returns False

    def test_filePathValidation_filePathIsInvalid(self):
        file_path = ["", None, 5]
        for path in file_path:
            self.assertFalse(io.valid_filepath(path), "{0} should be NOT a valid file path".format(str(path)))

    # Dir Path Validation
    # Case: Dir Path Exists
    # Expected: Returns True
    def test_dirPathValidation_dirPathExists(self):
        cwd = os.getcwd()
        self.assertTrue(io.valid_dirpath(cwd), "{0} should be a valid dir path".format(cwd))

        # Case: Dir Path Does Not Exist
        # Expected: Returns False

    def test_dirPathValidation_dirPathDoesNotExists(self):
        dir_path = "doesnt exist"
        self.assertFalse(io.valid_dirpath(dir_path), "{0} should NOT be a valid dir path".format(dir_path))

        # Case: Dir Path is Invalid (Empty String, None, Int)
        # Expected: Returns False

    def test_dirPathValidation_dirPathIsInvalid(self):
        dir_path = ["", None, 5]
        for path in dir_path:
            self.assertFalse(io.valid_dirpath(path), "{0} should be NOT a valid dir path".format(str(path)))

    # Get Abs Filepath
    # Case: Get abs path from relative path
    # Expected: Returns valid abs path from cwd + relative
    def test_absFilepath_getPathRelative(self):
        test_path = io.format_path("data\\Sample_File.csv")
        abs_path = io.get_abs_filepath(test_path)
        cwd = os.getcwd()
        self.assertEqual(abs_path, os.path.join(cwd, test_path))

    # Get Abs Filepath
    # Case: Get abs path from abs path
    # Expected: Returns abs path it was passed
    def test_absFilepath_getPathAbs(self):
        test_path = os.getcwd()
        abs_path = io.get_abs_filepath(test_path)
        self.assertEqual(abs_path, test_path)


if __name__ == "__main__":
    unittest.main()
