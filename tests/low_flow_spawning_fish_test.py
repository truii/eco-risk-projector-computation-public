import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class LowFlowSpawningFishPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "low_flow_spawning_fish"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_lowflowSpawningFish_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\DOESNTEXIST\\sample\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 35, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": "wrong",
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidTempComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "bad",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": -5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_validation_invalidConsiderLarvaeCriteria(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowSpawningFish_run_dailySuccessNoTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 111)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 111)

    def test_lowflowSpawningFish_run_dailySuccessNoTempAllowPartialSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 111)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 111)

    def test_lowflowSpawningFish_run_dailySuccessUseTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 81)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 81)

    def test_lowflowSpawningFish_run_dailySuccessUseTempWithMovingAverage(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 80)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 80)

    def test_lowflowSpawningFish_run_dailySuccessUseTempWithGTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 30)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 30)

    def test_lowflowSpawningFish_run_noLarvaeCriteria(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": False},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        num_success = sum(day["success"] for day in daily_intermediate)
        self.assertEqual(num_success, 81)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 81)

    def test_lowflowSpawningFish_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_season = sum(
            map(
                lambda d: d["in_season"] if d["in_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_flow_threshold_not_exceeded = sum(
            map(
                lambda d: d["flow_threshold_not_exceeded"] if d["flow_threshold_not_exceeded"] is not None else 0,
                daily_intermediate,
            )
        )
        num_suitable_time_to_complete_event = sum(
            map(
                lambda d: d["suitable_time_to_complete_event"]
                if d["suitable_time_to_complete_event"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_egg_criteria_met = sum(
            map(
                lambda d: d["egg_criteria_met"] if d["egg_criteria_met"] is not None else 0,
                daily_intermediate,
            )
        )
        num_larvae_criteria_met = sum(
            map(
                lambda d: d["larvae_criteria_met"] if d["larvae_criteria_met"] is not None else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 81)
        self.assertEqual(num_in_season, 159)
        self.assertEqual(num_flow_threshold_not_exceeded, 89)
        self.assertEqual(num_suitable_time_to_complete_event, 84)
        self.assertEqual(num_egg_criteria_met, 81)
        self.assertEqual(num_larvae_criteria_met, 81)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["in_season"], 1)
        self.assertEqual(daily_intermediate[0]["flow_threshold_not_exceeded"], 1)
        self.assertEqual(daily_intermediate[0]["suitable_time_to_complete_event"], 1)
        self.assertEqual(daily_intermediate[0]["egg_criteria_met"], 1)
        self.assertEqual(daily_intermediate[0]["larvae_criteria_met"], 1)
        self.assertEqual(daily_intermediate[0]["success"], 1)

    def test_lowflowSpawningFish_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 2018)
        self.assertEqual(yearly_intermediate[0]["total_recruitment_opportunities"], 81)

    def test_lowflowSpawningFish_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_lowflowSpawningFish_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"consider_larvae_criteria": True, "daily_change": 0.05, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
