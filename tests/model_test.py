import unittest
import core.model as model


class ModelTest(unittest.TestCase):
    # This should include:
    #   Model validation
    def setUp(self):
        self.model = model.Model.__new__(model.Model)

    # Model validation
    # Case: Correct Settings
    # Expected: Returns True
    def test_modelValidation_correctSettings(self):
        settings = {
            "plugin": "example_plugin",
            "spatial_agg": {},
            "nodes": [
                {"flow_data_path": "data\\runs\\example_run.csv", "magnitude": 40, "duration": 5},
                {"flow_data_path": "data\\runs\\example_run.csv", "magnitude": 40, "duration": 5},
            ],
        }
        self.assertTrue(self.model.validate_settings(settings))

        # Model validation
        # Case: Incorrect Settings - no nodes
        # Expected: Raises value error

    def test_modelValidation_noNodes(self):
        settings = {
            "plugin": "example_plugin.py",
            "spatial_agg": {"flow_data_path": "", "magnitude": 10, "duradtion": 5},
        }
        self.assertRaises(ValueError, self.model.validate_settings, settings)

    # Model validation
    # Case: Incorrect Settings - no plugin
    # Expected: Raises value error
    def test_modelValidation_noPlugin(self):
        settings = {
            "nodes": [
                {"flow_data_path": "data\\runs\\example_run.csv", "magnitude": 40, "duration": 5},
                {"flow_data_path": "data\\runs\\example_run.csv", "magnitude": 40, "duration": 5},
            ],
        }
        self.assertRaises(ValueError, self.model.validate_settings, settings)


if __name__ == "__main__":
    unittest.main()
