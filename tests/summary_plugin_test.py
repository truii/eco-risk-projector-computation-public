import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class SummaryPluginTest(unittest.TestCase):
    def setUp(self):
        self.plugin = "summary"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_summary_validation_noData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["median", "mean", "max", "min"],
                    "summary": ["mean", "median"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_summary_validation_invalidYearlyType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["median", "mean", "max", "min", "Invalid"],
                    "summary": ["mean", "median"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_summary_validation_invalidSummaryType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["median", "mean", "max", "min"],
                    "summary": ["mean", "median", "Invalid"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_summary_run_yearSeasonMean(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": ["mean"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["mean"], 495.45)
        self.assertEqual(yearly_intermediate[1]["year"], 1891)
        self.assertEqual(yearly_intermediate[1]["mean"], 533.612)

    def test_summary_run_interyearSeasonMean(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["mean"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["mean"], 509.638)
        self.assertEqual(yearly_intermediate[1]["year"], 1890)
        self.assertEqual(yearly_intermediate[1]["mean"], 516.003)

    def test_summary_run_yearSeasonMedian(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": ["median"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["median"], 243.44)
        self.assertEqual(yearly_intermediate[1]["year"], 1891)
        self.assertEqual(yearly_intermediate[1]["median"], 231.36)

    def test_summary_run_interyearSeasonMedian(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["median"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["median"], 277.76)
        self.assertEqual(yearly_intermediate[1]["year"], 1890)
        self.assertEqual(yearly_intermediate[1]["median"], 180.22)

    def test_summary_run_yearSeasonMax(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": ["max"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["max"], 2818.39)
        self.assertEqual(yearly_intermediate[1]["year"], 1891)
        self.assertEqual(yearly_intermediate[1]["max"], 4621.26)

    def test_summary_run_interyearSeasonMax(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["max"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["max"], 2818.39)
        self.assertEqual(yearly_intermediate[1]["year"], 1890)
        self.assertEqual(yearly_intermediate[1]["max"], 4621.26)

    def test_summary_run_yearSeasonMin(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": ["min"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["min"], 60.65)
        self.assertEqual(yearly_intermediate[1]["year"], 1891)
        self.assertEqual(yearly_intermediate[1]["min"], 58.41)

    def test_summary_run_interyearSeasonMin(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "yearly": ["min"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["min"], 47.25)
        self.assertEqual(yearly_intermediate[1]["year"], 1890)
        self.assertEqual(yearly_intermediate[1]["min"], 58.41)

    def test_summary_run_summaryMean(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["mean"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["mean"], 388.462)

    def test_summary_run_summaryMedian(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["median"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["median"], 165.12)

    def test_summary_run_summaryMax(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["max"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["max"], 58985.92)

    def test_summary_run_summaryMin(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["min"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["min"], 0)

    def test_summary_run_1point5YearAri(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["1.5_year_ari"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["1.5_year_ari"], 4878.050)

    def test_summary_run_2YearAri(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["2_year_ari"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["2_year_ari"], 6205.47)

    def test_summary_run_5YearAri(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["5_year_ari"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["5_year_ari"], 11939.37)

    def test_summary_run_10YearAri(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": ["10_year_ari"],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data

        self.assertEqual(summary_intermediate[0]["10_year_ari"], 18670.01)

    def test_summary_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": ["mean", "median", "max", "min"],
                    "summary": [],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["mean"], 495.45)
        self.assertEqual(yearly_intermediate[0]["median"], 243.44)
        self.assertEqual(yearly_intermediate[0]["max"], 2818.39)
        self.assertEqual(yearly_intermediate[0]["min"], 60.65)

    def test_summary_results_summaryIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "yearly": [],
                    "summary": [
                        "mean",
                        "median",
                        "max",
                        "min",
                        "1.5_year_ari",
                        "2_year_ari",
                        "5_year_ari",
                        "10_year_ari",
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data
        self.assertEqual(summary_intermediate[0]["mean"], 420.431)
        self.assertEqual(summary_intermediate[0]["median"], 177.955)
        self.assertEqual(summary_intermediate[0]["max"], 21258.05)
        self.assertEqual(summary_intermediate[0]["min"], 35.4)
        self.assertEqual(summary_intermediate[0]["1.5_year_ari"], 3562.83)
        self.assertEqual(summary_intermediate[0]["2_year_ari"], 3957.75)
        self.assertEqual(summary_intermediate[0]["5_year_ari"], 9431.2)
        self.assertEqual(summary_intermediate[0]["10_year_ari"], 18687.79)
