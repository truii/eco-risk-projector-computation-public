import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class KingThreadfinSalmonPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "king_threadfin_salmon"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_kingThreadfinSalmon_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 4.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_kingThreadfinSalmon_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\NA//Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 4.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_kingThreadfinSalmon_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"end": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 4.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_kingThreadfinSalmon_validation_invalidConstant(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 20, "month": 2}}},
                    "constants": {"a": 0.7291, "b": "wrong"},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_kingThreadfinSalmon_validation_invalidCategories(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 20, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 0.1},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 2, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_kingThreadfinSalmon_run_dailyResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 4.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data

        self.assertEqual(yearly_intermediate[0]["ycs"], -0.8142671459395192)
        self.assertEqual(yearly_intermediate[0]["recruitment_strength"], "poor")
        self.assertEqual(yearly_intermediate[0]["risk"], "high")
        self.assertEqual(temporal_assessment[0], {"year": 2018, "success": 0})

    def test_kingThreadfinSalmon_run_dailyResultsSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 2.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data

        self.assertEqual(yearly_intermediate[0]["ycs"], 1.1857328540604808)
        self.assertEqual(yearly_intermediate[0]["recruitment_strength"], "strong")
        self.assertEqual(yearly_intermediate[0]["risk"], "low")
        self.assertEqual(temporal_assessment[0], {"year": 2018, "success": 1})

    def test_kingThreadfinSalmon_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 2.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 2018)
        self.assertEqual(yearly_intermediate[0]["ycs"], 1.1857328540604808)
        self.assertEqual(yearly_intermediate[0]["recruitment_strength"], "strong")
        self.assertEqual(yearly_intermediate[0]["risk"], "low")

    def test_kingThreadfinSalmon_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 2.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_kingThreadfinSalmon_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                "parameters": {
                    "season": {"season": {"start": {"day": 1, "month": 12}, "end": {"day": 28, "month": 2}}},
                    "constants": {"a": 0.7291, "b": 2.3926},
                    "intermediate_assessment": {
                        "annual_recruitment_strength": {"high": 0.5, "low": -0.5},
                        "temporal_risk": {"high": 12, "low": 5},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
