import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class LowflowPartialSeasonPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "lowflow_partial_season"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_lowflowPartialSeason_validation_noFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
            {
                "meta_data": {"name": "node_2"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowPartialSeason_validation_noLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_lowflowPartialSeason_run_1YearComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.081)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.081)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.081)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.081)

    def test_lowflowPartialSeason_run_1YearNoComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.081)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.081)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_lowflowPartialSeason_run_smallSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.027)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.027)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.027)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.027)

    def test_lowflowPartialSeason_run_noMagSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [99, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 0)
        self.assertEqual(yearly_intermediate[0]["mag"], 0)
        self.assertEqual(yearly_intermediate[0]["summary"], 0)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_lowflowPartialSeason_run_noMagCompSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [99, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 0)
        self.assertEqual(yearly_intermediate[0]["mag"], 0)
        self.assertEqual(yearly_intermediate[0]["summary"], 0)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0)

    def test_lowflowPartialSeason_run_positiveLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["event_log"][0]["duration"], 118)
        self.assertEqual(yearly_intermediate[0]["event_log"][1]["duration"], 90)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.027)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.027)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.029)

    def test_lowflowPartialSeason_run_negativeLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["event_log"][0]["duration"], 123)
        self.assertEqual(yearly_intermediate[0]["event_log"][1]["duration"], 85)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.027)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.027)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.025)

    def test_lowflowPartialSeason_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 2018)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.027)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.025)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.027)

    def test_lowflowPartialSeason_results_eventsIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_intermediate = self.data_store.get_results(ResultTypes.EVENTS_INTERMEDIATE)[0].data
        self.assertEqual(events_intermediate[0]["start_date"], "01/07/2018")
        self.assertEqual(events_intermediate[0]["end_date"], "01/11/2018")
        self.assertEqual(events_intermediate[0]["duration"], 123)

    def test_lowflowPartialSeason_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_lowflowPartialSeason_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"mag": [[0, 0], [100, 100]], "dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
