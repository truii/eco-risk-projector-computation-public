import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class MultiyearFreshesPartialSeasonPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "multiyear_freshes_partial_season"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_multiyearFreshesPartialSeason_validation_noFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_multiyearFreshesPartialSeason_validation_noLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_multiyearFreshesPartialSeason_validation_invalidFlowRules(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_multiyearFreshesPartialSeason_validation_noPartialTables(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_multiyearFreshesPartialSeason_run_5YearComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.0001270158)
        self.assertEqual(yearly_intermediate[0]["max_time"], 1)

    def test_multiyearFreshesPartialSeason_run_1YearNoComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.0001270158)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_multiyearFreshesPartialSeason_run_eventlessSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [90, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["dur"], 0)
        self.assertEqual(yearly_intermediate[0]["mag"], 0)
        self.assertEqual(yearly_intermediate[0]["count"], 0)
        self.assertEqual(yearly_intermediate[0]["ind"], 0)
        self.assertEqual(yearly_intermediate[0]["summary"], 0)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_multiyearFreshesPartialSeason_run_positiveLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": 5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.0001270158)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.0001809312)

    def test_multiyearFreshesPartialSeason_run_negativeLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.0001270158)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.000181997)

    def test_multiyearFreshesPartialSeason_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 1890)
        self.assertEqual(yearly_intermediate[0]["prevailing_climate"], "Average")
        self.assertEqual(yearly_intermediate[0]["start_date"], "30/06/1890")
        self.assertEqual(yearly_intermediate[0]["end_date"], "01/07/1889")
        self.assertEqual(yearly_intermediate[0]["mag"], 0.0002540316)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.000181997)
        self.assertEqual(yearly_intermediate[0]["max_time"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.0001270158)

    def test_multiyearFreshesPartialSeason_results_eventsIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_intermediate = self.data_store.get_results(ResultTypes.EVENTS_INTERMEDIATE)[0].data
        self.assertEqual(events_intermediate[0]["start_date"], "09/02/1890")
        self.assertEqual(events_intermediate[0]["end_date"], "13/02/1890")
        self.assertEqual(events_intermediate[0]["duration"], 5)

    def test_multiyearFreshesPartialSeason_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1890)
        self.assertEqual(temporal_assessment[0]["success"], 0)

    def test_multiyearFreshesPartialSeason_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 1],
                        [2, 1],
                        [3, 1],
                        [4, 1],
                        [5, 1],
                        [6, 1],
                        [7, 1],
                        [8, 1],
                        [9, 1],
                        [10, 1],
                        [11, 1],
                        [12, 1],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "return_period": 5,
                    "max_time": 3,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 100000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                        "max_time": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1890)
        self.assertEqual(spatial_assessment[0]["risk"], "high")
