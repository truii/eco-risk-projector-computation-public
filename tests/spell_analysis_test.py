import unittest
from core.globals import ResultTypes, SpellAnalysisTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class SpellAnalysisPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "spell_analysis"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_spellAnalysis_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\NA\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": -1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "bad",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidMissingThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidSpellLength(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": None},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidEventAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "bad"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_validation_invalidDataType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "bad"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "count"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_spellAnalysis_run_aboveThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)
        result = res[0].data[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 3)
        self.assertEqual(result["mean_magnitude_of_peaks"], 12072.1)
        self.assertEqual(result["total_duration"], 10)
        self.assertEqual(result["mean_duration"], 3.333)
        self.assertEqual(result["max_duration"], 6)
        self.assertEqual(result["total_duration_between_spells"], 4)
        self.assertEqual(result["mean_duration_between_spells"], 2)
        self.assertEqual(result["max_duration_between_spells"], 2)

    def test_spellAnalysis_run_belowThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "below_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)
        result = res[0].data[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 4)
        self.assertEqual(result["mean_magnitude_of_peaks"], 3875.62)
        self.assertEqual(result["total_duration"], 355)
        self.assertEqual(result["mean_duration"], 88.75)
        self.assertEqual(result["max_duration"], 256)
        self.assertEqual(result["total_duration_between_spells"], 10)
        self.assertEqual(result["mean_duration_between_spells"], 3.333)
        self.assertEqual(result["max_duration_between_spells"], 6)

    def test_spellAnalysis_run_range(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "use_default_data": True},
                        "lower_threshold": {"use_ARI": True, "ARI": 2.5, "use_default_data": True},
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)
        result = res[0].data[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 4)
        self.assertEqual(result["mean_magnitude_of_peaks"], 5834.41)
        self.assertEqual(result["total_duration"], 7)
        self.assertEqual(result["mean_duration"], 1.75)
        self.assertEqual(result["max_duration"], 4)
        self.assertEqual(result["total_duration_between_spells"], 5)
        self.assertEqual(result["mean_duration_between_spells"], 1.667)
        self.assertEqual(result["max_duration_between_spells"], 2)

    def test_spellAnalysis_run_multipleDataTypes(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)
        self.assertEqual(len(res[0].data), 2)
        result = res[0].data[0]
        self.assertEqual(result["year"], 2018)

    def test_spellAnalysis_run_subSeasonDataLength(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\waterhole\\depth.csv",
                },
                "data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\waterhole\\depth.csv",
                },
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)
        self.assertEqual(len(res[0].data), 1)
        result = res[0].data[0]
        self.assertEqual(result["year"], 2009)

    def test_spellAnalysis_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate[0]["flow"], 158.33)
        self.assertEqual(daily_intermediate[0]["event"], 0)

    def test_spellAnalysis_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["count"], 1)
        self.assertEqual(yearly_intermediate[2]["mean_magnitude_of_peaks"], 4621.26)
        self.assertEqual(yearly_intermediate[2]["total_duration"], 1)
        self.assertEqual(yearly_intermediate[2]["mean_duration"], 1)
        self.assertEqual(yearly_intermediate[2]["max_duration"], 1)
        self.assertEqual(yearly_intermediate[2]["total_duration_between_spells"], 0)
        self.assertEqual(yearly_intermediate[2]["mean_duration_between_spells"], 0)
        self.assertEqual(yearly_intermediate[2]["max_duration_between_spells"], 0)

    def test_spellAnalysis_results_eventsIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_intermediate = self.data_store.get_results(ResultTypes.EVENTS_INTERMEDIATE)[0].data
        self.assertEqual(events_intermediate[3]["start_date"], "14/04/1894")
        self.assertEqual(events_intermediate[3]["end_date"], "19/04/1894")
        self.assertEqual(events_intermediate[3]["duration"], 6)

    def test_spellAnalysis_results_summaryIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data
        self.assertEqual(summary_intermediate[0]["spell_type"], SpellAnalysisTypes.ABOVE_THRESHOLD)
        self.assertEqual(summary_intermediate[0]["threshold"], 4362.24)
        self.assertEqual(summary_intermediate[0]["total_duration"], 16)
        self.assertEqual(summary_intermediate[0]["mean_duration"], 2.6666666666666665)
        self.assertEqual(summary_intermediate[0]["max_duration"], 6)
        self.assertEqual(summary_intermediate[0]["mean_duration_between_spells"], 510.4)
        self.assertEqual(summary_intermediate[0]["max_duration_between_spells"], 1137)

    def test_spellAnalysis_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_spellAnalysis_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": None},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                    },
                    "event_assessment": {"assessment_metric": "mean_duration"},
                },
                "assessment": {
                    "yearly_agg": None,
                    "daily_agg": None,
                    "yearly_conversion": {"comparison": "gte", "criteria": "median", "threshold": None},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
