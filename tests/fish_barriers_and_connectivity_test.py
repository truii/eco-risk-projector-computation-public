import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from helper import calculations as calcs
from tests.test_context import get_data_store


class FishBarrierAndConnectivityPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "fish_barriers_and_connectivity"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_fishBarriersAndRecruitment_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 1000, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishBarriersAndRecruitment_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 1000, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishBarriersAndRecruitment_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": -1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 1000, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishBarriersAndRecruitment_validation_oneBarriers(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        }
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishBarriersAndRecruitment_validation_invalidBarrierDur(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": -3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 1000, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_fishBarriersAndRecruitment_run_3BarriersUpAndDown(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 250, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 245)
        self.assertEqual(upstream_success[0], 108)
        self.assertEqual(upstream_success[1], 12)
        self.assertEqual(downstream_success[None], 242)
        self.assertEqual(downstream_success[0], 113)
        self.assertEqual(downstream_success[1], 10)

    def test_fishBarriersAndRecruitment_run_3BarriersUpOnly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": False,
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 250, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 245)
        self.assertEqual(upstream_success[0], 108)
        self.assertEqual(upstream_success[1], 12)
        self.assertEqual(downstream_success[None], 365)

    def test_fishBarriersAndRecruitment_run_3BarriersDownOnly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": False,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 2",
                            "AMTD_km": 30,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 250, "dur": 3},
                            "upstream_reqs": {"drownout": 1500, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 365)
        self.assertEqual(downstream_success[None], 242)
        self.assertEqual(downstream_success[0], 113)
        self.assertEqual(downstream_success[1], 10)

    def test_fishBarriersAndRecruitment_run_2BarriersUpAndDown(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 245)
        self.assertEqual(upstream_success[1], 120)
        self.assertEqual(downstream_success[None], 242)
        self.assertEqual(downstream_success[1], 123)

    def test_fishBarriersAndRecruitment_run_2BarriersUpOnly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": False,
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 245)
        self.assertEqual(upstream_success[1], 120)
        self.assertEqual(downstream_success[None], 365)

    def test_fishBarriersAndRecruitment_run_2BarriersDownOnly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": False,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 365)
        self.assertEqual(downstream_success[None], 242)
        self.assertEqual(downstream_success[1], 123)

    def test_fishBarriersAndRecruitment_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "parameters": {
                    "global_params": {
                        "swim_speed": 0.1,
                        "upstream_start_AMTD_km": 50,
                        "downstream_end_AMTD_km": 0,
                        "consider_downstream_movement": True,
                        "downstream_season": {"start": {"day": 1, "month": 7}, "end": {"day": 31, "month": 10}},
                        "consider_upstream_movement": True,
                        "upstream_season": {"start": {"day": 1, "month": 11}, "end": {"day": 28, "month": 2}},
                    },
                    "barriers": [
                        {
                            "name": "Barrier 1",
                            "AMTD_km": 60,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                        {
                            "name": "Barrier 3",
                            "AMTD_km": 10,
                            "data": {"flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv"},
                            "velocity_params": {"slope": 0.0005, "width_m": 50, "roughness": 0.035},
                            "downstream_reqs": {"drownout": 10, "dur": 3},
                            "upstream_reqs": {"drownout": 15, "dur": 5},
                        },
                    ],
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        upstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "upstream_success")
        downstream_success = calcs.get_all_values_for_key_from_dict_list(daily_intermediate, "downstream_success")
        self.assertEqual(upstream_success[None], 245)
        self.assertEqual(upstream_success[1], 120)
        self.assertEqual(downstream_success[None], 242)
        self.assertEqual(downstream_success[1], 123)

        num_downstream_in_season = sum(
            map(lambda d: d["downstream_in_season"] if d["downstream_in_season"] is not None else 0, daily_intermediate)
        )
        num_upstream_in_season = sum(
            map(lambda d: d["upstream_in_season"] if d["upstream_in_season"] is not None else 0, daily_intermediate)
        )
        self.assertEqual(num_downstream_in_season, 123)
        self.assertEqual(num_upstream_in_season, 120)
        downstream_impassable_barriers = calcs.get_all_values_for_key_from_dict_list(
            daily_intermediate, "downstream_impassable_barrier"
        )
        upstream_impassable_barriers = calcs.get_all_values_for_key_from_dict_list(
            daily_intermediate, "upstream_impassable_barrier"
        )
        self.assertEqual(downstream_impassable_barriers[None], 365)
        self.assertEqual(upstream_impassable_barriers[None], 365)

        self.assertEqual(daily_intermediate[0]["date"], "01/07/2018")
        self.assertEqual(daily_intermediate[0]["downstream_in_season"], 1)
        self.assertEqual(daily_intermediate[0]["downstream_impassable_barrier"], None)
        self.assertEqual(daily_intermediate[0]["downstream_success"], 1)
        self.assertEqual(daily_intermediate[0]["upstream_in_season"], 0)
        self.assertEqual(daily_intermediate[0]["upstream_impassable_barrier"], None)
        self.assertEqual(daily_intermediate[0]["upstream_success"], None)
