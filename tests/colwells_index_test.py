import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class ColwellsIndexPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "colwells_index"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_colwellsIndex_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\NA\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidDataType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "bad"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_missingDataOfType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidTimePeriod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "bad",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidSummaryMethod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "bad",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidNumberOfClasses(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 0,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidClassBoundaryMethod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "bad",
                    },
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidGan(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "gan",
                    },
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidLogClassSize(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "log",
                    },
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_validation_invalidWeightedLogClassSize(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "weighted_log",
                    },
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_colwellsIndex_run_monthly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.564)
        self.assertEqual(summary_intermediate["constancy"], 0.289)
        self.assertEqual(summary_intermediate["contingency"], 0.275)

    def test_colwellsIndex_run_seasonal(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "seasonal",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.573)
        self.assertEqual(summary_intermediate["constancy"], 0.351)
        self.assertEqual(summary_intermediate["contingency"], 0.222)

    def test_colwellsIndex_run_equal(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "equal",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.967)
        self.assertEqual(summary_intermediate["constancy"], 0.953)
        self.assertEqual(summary_intermediate["contingency"], 0.014)

    def test_colwellsIndex_run_transform(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.564)
        self.assertEqual(summary_intermediate["constancy"], 0.289)
        self.assertEqual(summary_intermediate["contingency"], 0.275)

    def test_colwellsIndex_run_logClassSize(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "log",
                        "log_base": 2,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.639)
        self.assertEqual(summary_intermediate["constancy"], 0.391)
        self.assertEqual(summary_intermediate["contingency"], 0.248)

    def test_colwellsIndex_run_weightedLogClassSize(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "weighted_log",
                        "log_base": 2,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.573)
        self.assertEqual(summary_intermediate["constancy"], 0.32)
        self.assertEqual(summary_intermediate["contingency"], 0.253)

    def test_colwellsIndex_run_gan(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "gan",
                        "gan_from": 0.25,
                        "gan_by": 0.25,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.509)
        self.assertEqual(summary_intermediate["constancy"], 0.177)
        self.assertEqual(summary_intermediate["contingency"], 0.333)

    def test_colwellsIndex_run_mean(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "mean",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.564)
        self.assertEqual(summary_intermediate["constancy"], 0.289)
        self.assertEqual(summary_intermediate["contingency"], 0.275)

    def test_colwellsIndex_run_median(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "median",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.568)
        self.assertEqual(summary_intermediate["constancy"], 0.28)
        self.assertEqual(summary_intermediate["contingency"], 0.289)

    def test_colwellsIndex_run_max(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "max",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.461)
        self.assertEqual(summary_intermediate["constancy"], 0.187)
        self.assertEqual(summary_intermediate["contingency"], 0.274)

    def test_colwellsIndex_run_min(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "min",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data[0]
        self.assertEqual(summary_intermediate["predictability"], 0.56)
        self.assertEqual(summary_intermediate["constancy"], 0.327)
        self.assertEqual(summary_intermediate["contingency"], 0.233)

    def test_colwellsIndex_results_summaryIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "min",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data
        self.assertEqual(summary_intermediate[0]["predictability"], 0.56)
        self.assertEqual(summary_intermediate[0]["constancy"], 0.327)
        self.assertEqual(summary_intermediate[0]["contingency"], 0.233)

    def test_colwellsIndex_results_custom(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "classes": {
                        "time_period": "monthly",
                        "summary_method": "min",
                        "number_of_classes": 11,
                        "class_boundary_method": "transform",
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        custom = self.data_store.get_results(ResultTypes.CUSTOM_RESULTS)[0].data
        self.assertTrue(len(custom) > 0)
