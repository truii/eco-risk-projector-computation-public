import statistics
import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class BananaPrawnGrowthPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "banana_prawn_growth"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_bananaPrawnGrowth_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bananaPrawnGrowth_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\BAD\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bananaPrawnGrowth_validation_invalidFlowDur(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": -1, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bananaPrawnGrowth_validation_invalidConstants(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_bananaPrawnGrowth_run_avgGrowth(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        growth_mean = round(
            statistics.mean(
                list(
                    map(
                        lambda d: d["growth"],
                        filter(lambda d: d["growth"] is not None, daily_result.data),
                    )
                )
            ),
            3,
        )
        self.assertEqual(growth_mean, 0.105)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 162)

    def test_bananaPrawnGrowth_run_sumGrowth(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        daily_assessment_result = self.data_store.get_results(ResultTypes.DAILY_ASSESSMENT)[0]
        growth_sum = round(
            sum(
                list(
                    map(
                        lambda d: d["growth"],
                        filter(lambda d: d["growth"] is not None, daily_result.data),
                    )
                )
            ),
            3,
        )
        self.assertEqual(growth_sum, 34.079)
        self.assertEqual(sum(map(lambda d: d["success"], daily_assessment_result.data)), 162)

    def test_bananaPrawnGrowth_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[41]["date"], "11/08/2018")
        self.assertEqual(daily_intermediate[41]["growth"], 0.029176411988114086)

    def test_bananaPrawnGrowth_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 2018)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_bananaPrawnGrowth_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_bananaPrawnGrowth_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "flow": {"flow0_dur": 14, "flow4_dur": 28},
                    "constants": {
                        "days": 42,
                        "length1": 5,
                        "L": 38,
                        "a": -0.0344,
                        "b": -0.0249,
                        "kc": 0.00297,
                        "kd": -0.0000604,
                        "ke": 0.0000000932,
                        "kf": -0.000000000000579,
                        "kg": 0.0000000557,
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
