import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store
from helper import calculations as calcs


class AustralianBassPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "australian_bass"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_australianBass_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\NA\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": -1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 1.4,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidTemperature(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 25, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidTimeSinceSpawning(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 10, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3.5, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_validation_invalidTimeSinceHatching(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 1814,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 10, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": -1, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_australianBass_run_lowSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 10,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 40},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 6, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        num_success = sum(day["success"] for day in daily_result.data)
        self.assertEqual(num_success, 230)
        num_yearly_success = sum(year["success"] for year in yearly_result.data)
        self.assertEqual(num_yearly_success, 2)

    def test_australianBass_run_highSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 100,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        num_success = sum(day["success"] for day in daily_result.data)
        self.assertEqual(num_success, 648)
        num_yearly_success = sum(year["success"] for year in yearly_result.data)
        self.assertEqual(num_yearly_success, 10)

    def test_australianBass_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 100,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 8, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate_result = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0]
        num_success = sum(day["success"] for day in daily_intermediate_result.data)
        num_migration_flows_provided = sum(
            map(
                lambda d: d["migration_flows_provided"] if d["migration_flows_provided"] is not None else 0,
                daily_intermediate_result.data,
            )
        )
        num_spawning_success = sum(
            map(
                lambda d: d["spawning_success"] if d["spawning_success"] is not None else 0,
                daily_intermediate_result.data,
            )
        )
        num_hatching_success = sum(
            map(
                lambda d: d["hatching_success"] if d["hatching_success"] is not None else 0,
                daily_intermediate_result.data,
            )
        )
        num_recruitment_success = sum(
            map(
                lambda d: d["recruitment_success"] if d["recruitment_success"] is not None else 0,
                daily_intermediate_result.data,
            )
        )
        self.assertEqual(num_success, 888)
        self.assertEqual(num_migration_flows_provided, 954)
        self.assertEqual(num_spawning_success, 911)
        self.assertEqual(num_hatching_success, 911)
        self.assertEqual(num_recruitment_success, 888)

        self.assertEqual(daily_intermediate_result.data[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate_result.data[0]["migration_flows_provided"], 1)
        self.assertEqual(daily_intermediate_result.data[0]["spawning_success"], 1)
        self.assertEqual(daily_intermediate_result.data[0]["hatching_success"], 1)
        self.assertEqual(daily_intermediate_result.data[0]["recruitment_success"], 1)
        self.assertEqual(daily_intermediate_result.data[0]["success"], 1)
        self.assertEqual(daily_intermediate_result.data[0]["failure_reason"], None)

        daily_failure_reason = calcs.get_all_values_for_key_from_dict_list(
            daily_intermediate_result.data, "failure_reason"
        )
        self.assertEqual(daily_failure_reason["Migration flow does not last enough days"], 31)
        self.assertEqual(daily_failure_reason["Insufficient flows for migration"], 229)
        self.assertEqual(daily_failure_reason["Not in migration season"], 2432)
        self.assertEqual(
            daily_failure_reason["No sufficient spawning day found in season - Temperature range never met"], 42
        )
        self.assertEqual(
            daily_failure_reason["No sufficient spawning day found in season - Salinity range never met"], 1
        )
        self.assertEqual(daily_failure_reason["No days to check recruitment success"], 23)
        self.assertEqual(daily_failure_reason["Not enough days to check migration event"], 6)

    def test_australianBass_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 100,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["success"], 1)

    def test_australianBass_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 100,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_australianBass_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "salinity": ".\\tests\\test_data\\Site 1 Salinity 10 Years.csv",
                },
                "parameters": {
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "migration": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "flow_threshold": 100,
                        "duration": 6,
                    },
                    "spawning": {
                        "season": {"start": {"day": 1, "month": 6}, "end": {"day": 30, "month": 9}},
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 14},
                    },
                    "hatching": {
                        "temperature_range": {"min": 11, "max": 24},
                        "salinity_range": {"min": 0, "max": 35},
                        "time_since_spawning_range": {"min": 3, "max": 10},
                    },
                    "recruitment": {"time_since_hatching": 40, "flow_threshold": 432},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
