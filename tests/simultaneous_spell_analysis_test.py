import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class SimultaneousSpellAnalysisPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "simultaneous_spell_analysis"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_simultaneousSpellAnalysis_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\NA\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": -1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "bad",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidMissingThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidSpellLength(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": -5},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidEventLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": "bad",
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_validation_invalidDataType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "bad"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": True, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 5,
                        "lag": 0,
                    },
                },
                "assessment": None,
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_simultaneousSpellAnalysis_run_aboveThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_spatial_intermediate = self.data_store.get_results(ResultTypes.YEARLY_SPATIAL_INTERMEDIATE)[0].data

        result = yearly_spatial_intermediate[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 3)
        self.assertEqual(result["mean_magnitude_of_peaks"], 12072.1)
        self.assertEqual(result["total_duration"], 10)
        self.assertEqual(result["mean_duration"], 3.333)
        self.assertEqual(result["single_longest"], 6)
        self.assertEqual(result["total_duration_between_spells"], 4)
        self.assertEqual(result["mean_duration_between_spells"], 2)
        self.assertEqual(result["single_longest_between_spells"], 2)

    def test_simultaneousSpellAnalysis_run_belowThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "below_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "below_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_spatial_intermediate = self.data_store.get_results(ResultTypes.YEARLY_SPATIAL_INTERMEDIATE)[0].data

        result = yearly_spatial_intermediate[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 4)
        self.assertEqual(result["mean_magnitude_of_peaks"], 3875.62)
        self.assertEqual(result["total_duration"], 355)
        self.assertEqual(result["mean_duration"], 88.75)
        self.assertEqual(result["single_longest"], 256)
        self.assertEqual(result["total_duration_between_spells"], 10)
        self.assertEqual(result["mean_duration_between_spells"], 3.333)
        self.assertEqual(result["single_longest_between_spells"], 6)

    def test_simultaneousSpellAnalysis_run_range(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "range",
                        "upper_threshold": {"use_ARI": True, "ARI": 5, "flow_threshold": 2.5, "use_default_data": True},
                        "lower_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_spatial_intermediate = self.data_store.get_results(ResultTypes.YEARLY_SPATIAL_INTERMEDIATE)[0].data

        result = yearly_spatial_intermediate[5]
        self.assertEqual(result["year"], 1894)
        self.assertEqual(result["count"], 4)
        self.assertEqual(result["mean_magnitude_of_peaks"], 5834.41)
        self.assertEqual(result["total_duration"], 7)
        self.assertEqual(result["mean_duration"], 1.75)
        self.assertEqual(result["single_longest"], 4)
        self.assertEqual(result["total_duration_between_spells"], 5)
        self.assertEqual(result["mean_duration_between_spells"], 1.667)
        self.assertEqual(result["single_longest_between_spells"], 2)

    def test_simultaneousSpellAnalysis_run_multipleDataTypes(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "data": {
                    "flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv",
                    "other_timeseries": ".\\data\\sample\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_spatial_intermediate = self.data_store.get_results(ResultTypes.YEARLY_SPATIAL_INTERMEDIATE)[0].data

        self.assertEqual(len(yearly_spatial_intermediate), 2)
        result = yearly_spatial_intermediate[0]
        self.assertEqual(result["year"], 2018)

    def test_simultaneousSpellAnalysis_results_custom(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        custom = self.data_store.get_results(ResultTypes.CUSTOM_RESULTS)[0].data
        self.assertEqual(custom[0]["date"], "01/07/1889")
        self.assertEqual(custom[0]["flow"], 158.33)
        self.assertEqual(custom[0]["event"], 0)

    def test_simultaneousSpellAnalysis_results_dailySpatialIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_spatial_intermediate = self.data_store.get_results(ResultTypes.DAILY_SPATIAL_INTERMEDIATE)[0].data
        self.assertEqual(daily_spatial_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_spatial_intermediate[0]["node 1_flow"], 158.33)
        self.assertEqual(daily_spatial_intermediate[0]["node 2_flow"], 158.33)
        self.assertEqual(daily_spatial_intermediate[0]["node 1_event"], 0)
        self.assertEqual(daily_spatial_intermediate[0]["node 2_event"], 0)
        self.assertEqual(daily_spatial_intermediate[0]["event_sum"], 0)
        self.assertEqual(daily_spatial_intermediate[0]["event_success"], 0)

    def test_simultaneousSpellAnalysis_results_yearlySpatialIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_spatial_intermediate = self.data_store.get_results(ResultTypes.YEARLY_SPATIAL_INTERMEDIATE)[0].data
        self.assertEqual(yearly_spatial_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_spatial_intermediate[2]["count"], 1)
        self.assertEqual(yearly_spatial_intermediate[2]["mean_magnitude_of_peaks"], 4621.26)
        self.assertEqual(yearly_spatial_intermediate[2]["total_duration"], 1)
        self.assertEqual(yearly_spatial_intermediate[2]["mean_duration"], 1)
        self.assertEqual(yearly_spatial_intermediate[2]["single_longest"], 1)
        self.assertEqual(yearly_spatial_intermediate[2]["total_duration_between_spells"], 0)
        self.assertEqual(yearly_spatial_intermediate[2]["mean_duration_between_spells"], 0)
        self.assertEqual(yearly_spatial_intermediate[2]["single_longest_between_spells"], 0)

    def test_simultaneousSpellAnalysis_results_eventsSpatialIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_spatial_intermediate = self.data_store.get_results(ResultTypes.EVENTS_SPATIAL_INTERMEDIATE)[0].data
        self.assertEqual(events_spatial_intermediate[0]["start_date"], "23/02/1891")
        self.assertEqual(events_spatial_intermediate[0]["end_date"], "23/02/1891")
        self.assertEqual(events_spatial_intermediate[0]["duration"], 1)

    def test_simultaneousSpellAnalysis_results_summarySpatialIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
            {
                "meta_data": {"name": "node 2"},
                "default_data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "event": {
                        "type": "above_threshold",
                        "upper_threshold": {
                            "use_ARI": True,
                            "ARI": 2.5,
                            "flow_threshold": 2.5,
                            "use_default_data": True,
                        },
                        "max_spell_length": {"should_consider": False, "value": 1},
                        "min_spell_length": {"should_consider": True, "value": 1},
                        "independence": 1,
                        "lag": 0,
                    },
                },
                "assessment": None,
            },
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        summary_spatial_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_SPATIAL_INTERMEDIATE)[0].data
        self.assertEqual(summary_spatial_intermediate[0]["total_duration"], 16)
        self.assertEqual(summary_spatial_intermediate[0]["mean_duration"], 2.6666666666666665)
        self.assertEqual(summary_spatial_intermediate[0]["max_duration"], 6)
        self.assertEqual(summary_spatial_intermediate[0]["mean_duration_between_spells"], 510.4)
        self.assertEqual(summary_spatial_intermediate[0]["max_duration_between_spells"], 1137)
