import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
import os

from tests.test_context import get_data_store


class GeneralPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "ambassis_agassizii"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(
        self,
        nodes,
        spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
        run_period=None,
        data_infill_settings=None,
    ):
        if run_period is None and data_infill_settings is None:
            return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}
        else:
            return {
                "plugin": self.plugin,
                "run_period": run_period,
                "data_infill_settings": data_infill_settings,
                "nodes": nodes,
                "spatial_agg": spatial_agg,
            }

    def test_general_runPeriod_invalidStartDate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 13, "year": 2018}, "end": {"day": 1, "month": 9, "year": 2018}}
        with self.assertRaisesRegex(
            InvalidRunPeriodException, "The run period provided is not valid because the start date is invalid"
        ):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_runPeriod_invalidEndDate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 12, "year": 2018}, "end": {"day": 1, "year": 2018}}
        with self.assertRaisesRegex(
            InvalidRunPeriodException, "The run period provided is not valid because the end date is invalid"
        ):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_runPeriod_invalidBothNone(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": None, "end": None}
        with self.assertRaisesRegex(InvalidRunPeriodException, "The run period provided is not valid"):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_runPeriod_invalidOrder(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 8, "year": 2019}, "end": {"day": 1, "month": 9, "year": 2018}}
        with self.assertRaisesRegex(
            InvalidRunPeriodException,
            "The run period provided is not valid because the start date must preceed the end date",
        ):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_runPeriod_startAndEnd(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 8, "year": 2018}, "end": {"day": 1, "month": 9, "year": 2018}}
        settings = self.createSettings(nodes, spatial_agg, run_period)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        self.assertEqual(res[0].data[0]["date"], "01/08/2018")
        self.assertEqual(res[0].data[-1]["date"], "01/09/2018")

    def test_general_runPeriod_start(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {
            "start": {"day": 1, "month": 8, "year": 2018},
        }
        settings = self.createSettings(nodes, spatial_agg, run_period)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        self.assertEqual(res[0].data[0]["date"], "01/08/2018")

    def test_general_runPeriod_end(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"end": {"day": 1, "month": 9, "year": 2018}}
        settings = self.createSettings(nodes, spatial_agg, run_period)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        self.assertEqual(res[0].data[-1]["date"], "01/09/2018")

    def test_general_runPeriod_noPeriod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        self.assertEqual(res[0].data[0]["date"], "01/07/2018")
        self.assertEqual(res[0].data[-1]["date"], "30/06/2019")

    def test_general_runPeriod_noDataInPeriod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 8, "year": 2020}, "end": {"day": 1, "month": 9, "year": 2020}}
        with self.assertRaisesRegex(
            InvalidRunPeriodException,
            "The run period provided is not valid because node node 1 has no data in the period 1/8/2020 - 1/9/2020",
        ):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_runPeriod_noDataInPeriodStartOnly(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        run_period = {"start": {"day": 1, "month": 8, "year": 2020}}
        with self.assertRaisesRegex(
            InvalidRunPeriodException,
            "The run period provided is not valid because node node 1 has no data in the period 1/8/2020 - N/A",
        ):
            settings = self.createSettings(nodes, spatial_agg, run_period)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_invalidShouldInfill(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": "invalid", "infill_method": "empty"}
        with self.assertRaisesRegex(
            InvalidDataInfillSettingsException,
            "The data infill settings provided are not valid because should_infill_data must be a boolean",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_invalidInfillMethod(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "invalid"}
        with self.assertRaisesRegex(
            InvalidDataInfillSettingsException,
            "The data infill settings provided are not valid because infill_method must be a valid DataInfillType",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_validNullInfill(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=None)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_validInfillEmpty(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "empty"}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_validInfillPreviousValue(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "previous_value"}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_validDontInfillGoodData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": False}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_dontInfillMissingDataRaisesError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years Missing Days.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": False}

        with self.assertRaisesRegex(
            InvalidDataException,
            "Data file of type flow in node node 1 contains no value for date 02/07/1889",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_dontInfillInvalidDayValueRaisesError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years Invalid Days.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": False}

        with self.assertRaisesRegex(
            InvalidDataException,
            "Data file of type flow in node node 1 contains an invalid value of 'bad_value' for date 02/07/1889",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_dontInfillInvalidDateFormatRaisesError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years Bad Date Format.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": False}

        with self.assertRaisesRegex(
            InvalidDataException,
            "Data file of type flow in node node 1 contains an invalid date of '02/-/07/-/1889'",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_dataInfillSettings_infillMissingDataDoesntRaiseError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "previous_value"}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_infillInvalidDayValueDoesntRaiseError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years Invalid Days.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "previous_value"}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_infillInvalidFileValueDoesntRaiseError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years Bad Date Format.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": True, "infill_method": "previous_value"}

        settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
        Model(settings, self.data_store).run()

        self.assertTrue(len(self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)) > 0)

    def test_general_dataInfillSettings_dataWithNoOverlapRaisesError(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        data_infill_settings = {"should_infill_data": False}

        with self.assertRaisesRegex(
            InvalidDataException,
            "There is no time period in which all required data types are avaliable in node node 1",
        ):
            settings = self.createSettings(nodes, spatial_agg, data_infill_settings=data_infill_settings)
            Model(settings, self.data_store).run()

    def test_general_run_relative(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        num_success = sum(map(lambda d: d["success"], res[0].data))
        self.assertEqual(num_success, 26)

    def test_general_run_absolute(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}

        nodes[0]["data"]["depth"] = os.path.join(os.getcwd(), nodes[0]["data"]["depth"])
        nodes[0]["data"]["temperature"] = os.path.join(os.getcwd(), nodes[0]["data"]["temperature"])
        nodes[0]["data"]["flow"] = os.path.join(os.getcwd(), nodes[0]["data"]["flow"])

        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        num_success = sum(map(lambda d: d["success"], res[0].data))
        self.assertEqual(num_success, 26)

    def test_general_run_nullPaths(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": None,
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}

        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        res = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)
        num_success = sum(map(lambda d: d["success"], res[0].data))
        self.assertEqual(num_success, 106)

    def test_general_runSettingsLog_matchesRunSettings(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 1, "month": 8}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.05, "consecutive_days": 14},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "threshold": None},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [50, 100], "moderate": [20, 49], "low": [0, 19]}
        run_period = {"start": {"day": 1, "month": 1, "year": 2010}, "end": {"day": 1, "month": 12, "year": 2019}}
        settings = self.createSettings(nodes, spatial_agg, run_period)
        run_model = Model(settings, self.data_store)
        run_model.run()
        self.assertEqual(run_model.settings, settings)
