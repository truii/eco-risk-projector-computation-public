import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from helper import validation as validation
from tests.test_context import get_data_store


class WaterholeRunToEmptyPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "waterhole_run_to_empty"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(
        self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}, run_period=None
    ):
        if run_period is None:
            return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}
        else:
            return {
                "plugin": self.plugin,
                "run_period": run_period,
                "nodes": nodes,
                "spatial_agg": spatial_agg,
            }

    def test_waterhole_runToEmpty_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\NA\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_noBathymetry(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv"},
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidGroundwater(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 60, "loss_to_deep_drainage": 60},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidPumping(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 0,
                        "cease_pumping_depth": 5,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidLag(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": -1.5,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidEvaporationRunType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "invalid",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidEvaporationValue(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "value",
                        "run_to_empty_value": "invalid",
                        "season": None,
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_validation_invalidSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\tests\\test_data\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\tests\\test_data\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0.5,
                        "run_to_empty_type": "value",
                        "run_to_empty_value": "mean",
                        "season": {"start": {"day": 1, "month": 1}},
                    },
                },
                "assessment": {},
            }
        ]
        with self.assertRaises(InvalidRunPeriodException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_waterhole_runToEmpty_NullRunToEmpty(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(
            nodes,
            run_period={"start": {"day": 1, "month": 6, "year": 2009}, "end": {"day": 1, "month": 7, "year": 2009}},
        )
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], None)

    def test_waterhole_runToEmpty_run_DataEvaporation(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 775)

    def test_waterhole_runToEmpty_run_MeanEvaporation(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "mean",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 780)

    def test_waterhole_runToEmpty_run_ValueEvaporation(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "value",
                        "run_to_empty_value": 10,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 484)

    def test_waterhole_runToEmpty_run_DataPlusValueEvaporation(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data_plus_value",
                        "run_to_empty_value": 10,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 328)

    def test_waterhole_runToEmpty_run_DataPlusPercentEvaporation(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data_plus_percent",
                        "run_to_empty_value": 50,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 574)

    def test_waterhole_runToEmpty_run_WithSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 100},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": {"start": {"day": 1, "month": 1, "year": 2009}},
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        self.assertEqual(daily_intermediate[0]["date"], "01/01/2009")

    def test_waterhole_runToEmpty_run_ScalingAbove100(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data

        self.assertEqual(parameter_results["days"], 574)

    def test_waterhole_runToEmpty_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/01/2005")
        self.assertEqual(daily_intermediate[0]["modelled_depth"], 5)
        self.assertEqual(daily_intermediate[0]["modelled_volume"], 85634.46095)
        self.assertEqual(daily_intermediate[0]["modelled_area"], 51918.80719)
        self.assertEqual(daily_intermediate[0]["modelled_GW_volume"], 179424.99997)
        self.assertEqual(daily_intermediate[0]["evaporation"], 8.600000381)

    def test_waterhole_runToEmpty_results_parameterResults(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "evaporation": ".\\data\\sample\\waterhole\\evaporation.csv",
                    "bathymetry": ".\\data\\sample\\waterhole\\bathymetry.csv",
                },
                "parameters": {
                    "evaporation": {"scaling": 150},
                    "seepage": {"rate": 1},
                    "rainfall": {"area": 70000, "max_infiltration": 10},
                    "groundwater": {"area": 100000, "inflow": 5, "loss_to_deep_drainage": 5},
                    "extraction": {
                        "commence_pumping_depth": 5,
                        "cease_pumping_depth": 0,
                        "extraction_rate": 0,
                        "max_annual_take": 0,
                        "start_month": 7,
                        "CTF": 0,
                        "lag": 0,
                    },
                    "optimisation": {
                        "run_to_empty_depth": 0,
                        "run_to_empty_type": "data",
                        "run_to_empty_value": None,
                        "season": None,
                    },
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        parameter_results = self.data_store.get_results(ResultTypes.PARAMETER_RESULTS)[0].data
        self.assertEqual(parameter_results["days"], 574)
