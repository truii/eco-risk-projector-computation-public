import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class AmbassisAgassiziiPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "ambassis_agassizii"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(
        self,
        nodes,
        spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]},
        run_period=None,
        data_infill_settings=None,
    ):
        if run_period is None and data_infill_settings is None:
            return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}
        else:
            return {
                "plugin": self.plugin,
                "run_period": run_period,
                "data_infill_settings": data_infill_settings,
                "nodes": nodes,
                "spatial_agg": spatial_agg,
            }

    def test_ambassisAgassizii_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(MissingParameterException, "name parameter could not be found in node unknown"):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\data\\DOESNTEXIST\\sample\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(MissingDataFileException, "Could not find data file of type depth in node node 1"):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidSeasonDates(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": -1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": "WRONG",
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter temperature_threshold in node node 1 is not valid for reason must be a number",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidTempMovingAvgDuration(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 0,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter temperature_moving_average_duration in node node 1 is not valid for reason must be >= 1",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidTempComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 5,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "bad",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter temperature_comparison in node node 1 is not valid for reason comparison type is not valid",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidDays(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": -1, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException, "Parameter max_change in node node 1 is not valid for reason must be >= 0"
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_missingRisk(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {}
        with self.assertRaisesRegex(
            InvalidSpatialAggException, "The spatial aggregation parameters provided are not valid"
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidRiskValue(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [74, 50], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidSpatialAggException,
            "The spatial aggregation parameter moderate is invalid because lower bound must be < upper bound",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidRiskGap(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [60, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidSpatialAggException,
            "The spatial aggregation parameter risk_categories is invalid because must include full range 0-100",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidMissingAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException, "Parameter assessment in node node 1 is not valid for reason section must exist"
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidMissingYearlyAgg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {"temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1}},
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter assessment in node node 1 is not valid for reason must contain yearly_agg section",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "BAD", "num_successes": 1},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter comparison in node node 1 is not valid for reason comparison type is not valid",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidNumSuccess(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": -2},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException, "Parameter num_successes in node node 1 is not valid for reason must be >= 0"
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidMissingTemporalAgg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {"yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"}},
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter assessment in node node 1 is not valid for reason must contain temporal_agg section",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidThreshold(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": "BAD"},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException, "Parameter threshold in node node 1 is not valid for reason must be a number"
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidRiskOrder(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [50, 74], "moderate": [75, 100], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidSpatialAggException,
            "The spatial aggregation parameter risk_categories is invalid because high must be > moderate must be > low",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidTemperature(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": None,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter temperature_threshold in node node 1 is not valid for reason must be a number",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_validation_invalidTemperatureMissingData(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv"},
                "parameters": {
                    "lowflow": {"use_median_flow": False, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 5}, "end": {"day": 30, "month": 4}},
                        "temperature_threshold": 12,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 5, "consecutive_days": 7},
                        "larvae": {"daily_change": 5, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        with self.assertRaisesRegex(
            InvalidParameterException,
            "Parameter temperature in node node 1 is not valid for reason must provide temperature data if using temperature based season",
        ):
            settings = self.createSettings(nodes, spatial_agg)
            Model(settings, self.data_store).run()

    def test_ambassisAgassizii_run_NoTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(map(lambda d: d["success"], daily_intermediate))
        self.assertEqual(num_success, 106)

    def test_ambassisAgassizii_run_NoTempAllowPartialSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": False,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": True,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(map(lambda d: d["success"], daily_intermediate))
        self.assertEqual(num_success, 106)

    def test_ambassisAgassizii_run_UseTemp(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(map(lambda d: d["success"], daily_intermediate))
        self.assertEqual(num_success, 26)

    def test_ambassisAgassizii_run_UseTempMovingAvg(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": True,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(map(lambda d: d["success"], daily_intermediate))
        self.assertEqual(num_success, 27)

    def test_ambassisAgassizii_run_UseTempLTEComparison(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True, "flow_threshold": 5},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "lte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        spatial_agg = {"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}
        settings = self.createSettings(nodes, spatial_agg)
        Model(settings, self.data_store).run()
        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        num_success = sum(map(lambda d: d["success"], daily_intermediate))
        self.assertEqual(num_success, 80)

    def test_ambassisAgassizii_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth Single Year.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp Single Year.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 7}, "end": {"day": 30, "month": 6}},
                        "temperature_threshold": 22,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.055, "consecutive_days": 20},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "threshold": None},
                    "temporal_agg": {"agg_type": "sum", "num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data

        num_success = sum(
            map(
                lambda d: d["success"] if d["success"] is not None else 0,
                daily_intermediate,
            )
        )
        num_in_season = sum(
            map(
                lambda d: d["in_season"] if d["in_season"] is not None else 0,
                daily_intermediate,
            )
        )
        num_flow_threshold_not_exceeded = sum(
            map(
                lambda d: d["flow_threshold_not_exceeded"] if d["flow_threshold_not_exceeded"] is not None else 0,
                daily_intermediate,
            )
        )
        num_suitable_time_to_complete_event = sum(
            map(
                lambda d: d["suitable_time_to_complete_event"]
                if d["suitable_time_to_complete_event"] is not None
                else 0,
                daily_intermediate,
            )
        )
        num_egg_criteria_met = sum(
            map(
                lambda d: d["egg_criteria_met"] if d["egg_criteria_met"] is not None else 0,
                daily_intermediate,
            )
        )
        num_larvae_criteria_met = sum(
            map(
                lambda d: d["larvae_criteria_met"] if d["larvae_criteria_met"] is not None else 0,
                daily_intermediate,
            )
        )
        self.assertEqual(num_success, 26)
        self.assertEqual(num_in_season, 206)
        self.assertEqual(num_flow_threshold_not_exceeded, 94)
        self.assertEqual(num_suitable_time_to_complete_event, 94)
        self.assertEqual(num_egg_criteria_met, 43)
        self.assertEqual(num_larvae_criteria_met, 26)

        self.assertEqual(daily_intermediate[97]["date"], "06/10/2018")
        self.assertEqual(daily_intermediate[97]["in_season"], 1)
        self.assertEqual(daily_intermediate[97]["flow_threshold_not_exceeded"], 1)
        self.assertEqual(daily_intermediate[97]["suitable_time_to_complete_event"], 1)
        self.assertEqual(daily_intermediate[97]["egg_criteria_met"], 1)
        self.assertEqual(daily_intermediate[97]["larvae_criteria_met"], 1)
        self.assertEqual(daily_intermediate[97]["success"], 1)

    def test_ambassisAgassizii_results_yearlyAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 1, "month": 8}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.05, "consecutive_days": 14},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "threshold": None},
                    "temporal_agg": {"agg_type": "sum", "num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_assessment = self.data_store.get_results(ResultTypes.YEARLY_ASSESSMENT)[0].data
        self.assertEqual(yearly_assessment[0]["year"], 1889)
        self.assertEqual(yearly_assessment[0]["success"], 1)

    def test_ambassisAgassizii_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {
                    "depth": ".\\tests\\test_data\\Site 1 Depth 10 Years.csv",
                    "temperature": ".\\tests\\test_data\\Site 1 Temp 10 Years.csv",
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                },
                "parameters": {
                    "lowflow": {"use_median_flow": True},
                    "breeding_season": {
                        "use_temperature": True,
                        "season": {"start": {"day": 1, "month": 1}, "end": {"day": 1, "month": 8}},
                        "temperature_threshold": 16,
                        "use_temperature_moving_average": False,
                        "temperature_moving_average_duration": 7,
                        "allow_partial_seasons": False,
                        "temperature_comparison": "gte",
                    },
                    "life_stages_criteria": {
                        "egg": {"max_change": 0.05, "consecutive_days": 7},
                        "larvae": {"daily_change": 0.05, "consecutive_days": 14},
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "threshold": None},
                    "temporal_agg": {"agg_type": "sum", "num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
