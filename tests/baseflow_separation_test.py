import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class BaseflowSeperationPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "baseflow_separation"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_baseflowSeparation_validation_invalidNoName(self):
        nodes = [
            {
                "meta_data": {},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_invalidDataPath(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\NA\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_invalidDataType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "bad"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_missingDataOfType(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "other_timeseries"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_invalidSeasonStart(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": -1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_invalidSeasonEnd(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 40, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_validation_invalidAlpha(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": "bad"},
                },
            }
        ]
        with self.assertRaises(InvalidParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_baseflowSeparation_run_975alpha(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.975},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        result = yearly_result.data[0]
        self.assertEqual(result["year"], 1889)
        self.assertEqual(result["baseflow_index"], 0.4466292376811933)
        self.assertEqual(result["quickflow_index"], 0.17491724869192685)
        self.assertEqual(result["mean_baseflow"], 66.62749429742072)
        self.assertEqual(result["mean_quickflow"], 26.09389848781192)

    def test_baseflowSeparation_run_5alpha(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.5},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_result = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0]
        result = yearly_result.data[0]
        self.assertEqual(result["year"], 1889)
        self.assertEqual(result["baseflow_index"], 0.7924376946369727)
        self.assertEqual(result["quickflow_index"], 0.0700210120349467)
        self.assertEqual(result["mean_baseflow"], 118.21469246976115)
        self.assertEqual(result["mean_quickflow"], 10.44563182714916)

    def test_baseflowSeparation_results_dailyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.5},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        daily_intermediate = self.data_store.get_results(ResultTypes.DAILY_INTERMEDIATE)[0].data
        self.assertEqual(daily_intermediate[0]["date"], "01/07/1889")
        self.assertEqual(daily_intermediate[0]["flow"], 158.33)
        self.assertEqual(daily_intermediate[0]["quickflow_pass_1"], 2.4518415472610053)
        self.assertEqual(daily_intermediate[0]["baseflow_pass_1"], 155.878158452739)
        self.assertEqual(daily_intermediate[0]["quickflow_pass_2"], 0.6129602840740682)
        self.assertEqual(daily_intermediate[0]["baseflow_pass_2"], 155.26519816866494)
        self.assertEqual(daily_intermediate[0]["quickflow_pass_3"], 2.132922250040872)
        self.assertEqual(daily_intermediate[0]["baseflow_pass_3"], 153.13227591862406)

    def test_baseflowSeparation_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.5},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[2]["year"], 1891)
        self.assertEqual(yearly_intermediate[2]["baseflow_index"], 0.8423166168103726)
        self.assertEqual(yearly_intermediate[2]["quickflow_index"], 0.039643509821164595)
        self.assertEqual(yearly_intermediate[2]["mean_baseflow"], 449.46983914040004)
        self.assertEqual(yearly_intermediate[2]["mean_quickflow"], 21.154233012467262)

    def test_baseflowSeparation_results_summaryIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node 1"},
                "data": {"flow": ".\\data\\sample\\Site 1 Flow 10 Years.csv"},
                "parameters": {
                    "data": {"data_type": "flow"},
                    "season": {"start": {"day": 1, "month": 1}, "end": {"day": 31, "month": 12}},
                    "method": {"alpha": 0.5},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        summary_intermediate = self.data_store.get_results(ResultTypes.SUMMARY_INTERMEDIATE)[0].data
        self.assertEqual(summary_intermediate[0]["baseflow_index"], 0.834110057124849)
        self.assertEqual(summary_intermediate[0]["quickflow_index"], 0.04014149600711001)
        self.assertEqual(summary_intermediate[0]["mean_baseflow"], 341.0415221900113)
        self.assertEqual(summary_intermediate[0]["mean_quickflow"], 16.573284000494862)
