import datetime
import unittest

from core.data import Data
from core.exceptions import InvalidDataException, MissingDataFileException
from core.globals import OtherDataTypes, TimeseriesDataTypes

FLOW_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 10 Years.csv"
DEPTH_10_YEARS_DATA_PATH = ".\\tests\\test_data\\Site 1 Depth 10 Years.csv"

FLOW_10_YEARS_MISSING_DAYS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 10 Years Missing Days.csv"
FLOW_10_YEARS_INVALID_DAYS_DATA_PATH = ".\\tests\\test_data\\Site 1 Flow 10 Years Invalid Days.csv"
DEPTH_INTERDAY_VALUES_DATA_PATH = ".\\tests\\test_data\\waterhole\\depth.csv"
CLIMATE_DATA_PATH = ".\\tests\\test_data\\Climate.csv"


class DataTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_singleDataTypeFlow = Data()
        cls.data_singleDataTypeFlow.flow = cls.data_singleDataTypeFlow.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_singleDataTypeInterdayDepth = Data()
        cls.data_singleDataTypeInterdayDepth.depth = cls.data_singleDataTypeInterdayDepth.try_import_data(
            {TimeseriesDataTypes.DEPTH: DEPTH_INTERDAY_VALUES_DATA_PATH}, TimeseriesDataTypes.DEPTH
        )

        cls.data_dataWithMissingDays = Data()
        cls.data_dataWithMissingDays.flow = cls.data_dataWithMissingDays.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_MISSING_DAYS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_dataWithInvalidDays = Data()
        cls.data_dataWithInvalidDays.flow = cls.data_dataWithInvalidDays.try_import_data(
            {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_INVALID_DAYS_DATA_PATH}, TimeseriesDataTypes.FLOW
        )

        cls.data_climate = Data()
        cls.data_climate.climate = cls.data_climate.try_import_data(
            {OtherDataTypes.CLIMATE: CLIMATE_DATA_PATH}, OtherDataTypes.CLIMATE
        )

    def test_handleNullPaths_someNullPaths(self):
        data = Data()
        paths = {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH, TimeseriesDataTypes.DEPTH: None}

        result = data.handle_null_paths(paths)

        self.assertEqual(result, {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH})

    def test_handleNullPaths_noNullPaths(self):
        data = Data()
        paths = {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH, TimeseriesDataTypes.DEPTH: DEPTH_10_YEARS_DATA_PATH}

        result = data.handle_null_paths(paths)

        self.assertEqual(result, paths)

    def test_tryImportData_fileExists(self):
        data = Data()
        paths = {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}

        result = data.try_import_data(paths, TimeseriesDataTypes.FLOW)

        self.assertEqual(len(result), 3652)
        self.assertEqual(result[0][0], "01/07/1889")
        self.assertEqual(result[0][1], "158.33")

    def test_tryImportData_invalidDataType(self):
        data = Data()
        paths = {TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}

        with self.assertRaises(MissingDataFileException):
            result = data.try_import_data(paths, TimeseriesDataTypes.DEPTH)

    def test_parseRunPeriod_validFullPeriod(self):
        data = Data()

        run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
            "end": {"day": 31, "month": 12, "year": 1890},
        }

        data.parse_run_period(run_period)

        parsed_run_period = {
            "start": datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
            "end": datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        }

        self.assertEqual(data.run_period, parsed_run_period)

    def test_parseRunPeriod_validStartOnly(self):
        data = Data()

        run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
        }

        data.parse_run_period(run_period)

        parsed_run_period = {"start": datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc), "end": None}

        self.assertEqual(data.run_period, parsed_run_period)

    def test_parseRunPeriod_validEndOnly(self):
        data = Data()

        run_period = {
            "end": {"day": 31, "month": 12, "year": 1890},
        }

        data.parse_run_period(run_period)

        parsed_run_period = {
            "start": None,
            "end": datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        }

        self.assertEqual(data.run_period, parsed_run_period)

    def test_parseRunPeriod_invalidPeriod(self):
        data = Data()

        run_period = {
            "something": {"day": 1, "month": 1},
            "end": {"day": 31, "month": 12, "year": 1890},
        }

        data.parse_run_period(run_period)

        parsed_run_period = {
            "start": None,
            "end": datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        }

        self.assertEqual(data.run_period, parsed_run_period)

    def test_parseRunPeriod_nonePeriod(self):
        data = Data()

        data.parse_run_period(None)

        self.assertEqual(data.run_period, None)

    def test_parseRunPeriod_invalidDate(self):
        data = Data()

        run_period = {
            "start": {"day": 1, "month": 100, "year": 1890},
            "end": {"day": 31, "month": 12, "year": 1890},
        }

        data.parse_run_period(run_period)

        parsed_run_period = {
            "start": None,
            "end": datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        }

        self.assertEqual(data.run_period, parsed_run_period)

    def test_parseCsvToTimeseriesDays_basic(self):
        data = self.data_singleDataTypeFlow

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 158.33)

    def test_parseCsvToTimeseriesDays_customFieldName(self):
        data = self.data_singleDataTypeFlow

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, TimeseriesDataTypes.DEPTH)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.DEPTH), 158.33)

    def test_parseCsvToTimeseriesDays_considerRunPeriod(self):
        data = Data()
        data.flow = data.try_import_data({TimeseriesDataTypes.FLOW: FLOW_10_YEARS_DATA_PATH}, TimeseriesDataTypes.FLOW)

        run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
            "end": {"day": 31, "month": 12, "year": 1890},
        }
        data.parse_run_period(run_period)

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, consider_run_period=True)

        self.assertEqual(len(result), 365)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1890, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1890, 12, 31).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 2454.12)

    def test_parseCsvToTimeseriesDays_notNumberValues(self):
        data = self.data_climate

        result = data.parse_csv_to_timeseries_days(OtherDataTypes.CLIMATE, is_number_values=False)

        self.assertEqual(len(result), 80901)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1800, 1, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(2021, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(OtherDataTypes.CLIMATE), "Wet")

    def test_parseCsvToTimeseriesDays_infilWithNone(self):
        data = self.data_dataWithMissingDays

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[1].get_value(TimeseriesDataTypes.FLOW), None)
        self.assertEqual(result[2].get_value(TimeseriesDataTypes.FLOW), 155.62)

    def test_parseCsvToTimeseriesDays_infilWithPreviousDayValue(self):
        data = self.data_dataWithMissingDays

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, infill_with_previous_value=True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[1].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[2].get_value(TimeseriesDataTypes.FLOW), 155.62)

    def test_parseCsvToTimeseriesDays_badDayValueReplaceWithNone(self):
        data = self.data_dataWithInvalidDays

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[1].get_value(TimeseriesDataTypes.FLOW), None)
        self.assertEqual(result[2].get_value(TimeseriesDataTypes.FLOW), 155.62)

    def test_parseCsvToTimeseriesDays_badDayValueReplaceWithPreviousDayValue(self):
        data = self.data_dataWithInvalidDays

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.FLOW, infill_with_previous_value=True)

        self.assertEqual(len(result), 3652)
        self.assertEqual(
            result[0].date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(1899, 6, 30).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[1].get_value(TimeseriesDataTypes.FLOW), 158.33)
        self.assertEqual(result[2].get_value(TimeseriesDataTypes.FLOW), 155.62)

    def test_parseCsvToTimeseriesDays_multipleValuesPerDayAreAggregatedToSingleValue(self):
        data = self.data_singleDataTypeInterdayDepth

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.DEPTH)

        self.assertEqual(len(result), 140)
        self.assertEqual(
            result[0].date,
            datetime.datetime(2009, 5, 28).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(
            result[-1].date,
            datetime.datetime(2009, 10, 14).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result[0].get_value(TimeseriesDataTypes.DEPTH), 4.997875)

    def test_parseCsvToTimeseriesDays_invalidDataType(self):
        data = self.data_singleDataTypeFlow

        result = data.parse_csv_to_timeseries_days(TimeseriesDataTypes.DEPTH)

        self.assertEqual(len(result), 0)

    def test_parseDay_toFloat(self):
        data = Data()

        date_string = "01/07/1889"

        result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "1", True, False)

        self.assertEqual(
            result.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result.get_value(TimeseriesDataTypes.FLOW), 1)

    def test_parseDay_notToFloat(self):
        data = Data()

        date_string = "01/07/1889"

        result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "1", False, False)

        self.assertEqual(
            result.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result.get_value(TimeseriesDataTypes.FLOW), "1")

    def test_parseDay_dateWithHoursAndMins(self):
        data = Data()

        date_string = "01/07/1889 10:10"

        result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "1", True, False)

        self.assertEqual(
            result.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )

    def test_parseDay_unrecognisableDate(self):
        data = Data()

        date_string = "not/a/date"

        with self.assertRaisesRegex(
            InvalidDataException, "Data file of type flow in node None contains an invalid date of 'not/a/date'"
        ):
            result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "1", True, True)

    def test_parseDay_toFloatWithUnparsableValue(self):
        data = Data()

        date_string = "01/07/1889"

        result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "abc", True, False)

        self.assertEqual(
            result.date,
            datetime.datetime(1889, 7, 1).replace(tzinfo=datetime.timezone.utc),
        )
        self.assertEqual(result.get_value(TimeseriesDataTypes.FLOW), None)

    def test_parseDay_toFloatWithUnparsableValueShouldThrowError(self):
        data = Data()

        date_string = "01/07/1889"

        with self.assertRaisesRegex(
            InvalidDataException,
            "Data file of type flow in node None contains an invalid value of 'abc' for date 01/07/1889",
        ):
            result = data.parse_day(date_string, TimeseriesDataTypes.FLOW, "abc", True, True)

    def test_findDateFormat_knownDDMMYYDate(self):
        data = Data()

        result = data.find_date_format("01/07/1889")

        self.assertEqual(result, "%d/%m/%Y")

    def test_findDateFormat_knownDDMMYYHHMMDate(self):
        data = Data()

        result = data.find_date_format("01/07/1889 10:10")

        self.assertEqual(result, "%d/%m/%Y %H:%M")

    def test_findDateFormat_unknownDate(self):
        data = Data()

        with self.assertRaises(ValueError):
            result = data.find_date_format("not-a/date")

    def test_formatRunPeriod_fullRunPeriod(self):
        data = Data()
        run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
            "end": {"day": 31, "month": 12, "year": 1890},
        }
        data.parse_run_period(run_period)

        result = data.format_run_period()

        self.assertEqual(result, "1/1/1890 - 31/12/1890")

    def test_formatRunPeriod_noStartRunPeriod(self):
        data = Data()
        run_period = {
            "end": {"day": 31, "month": 12, "year": 1890},
        }
        data.parse_run_period(run_period)

        result = data.format_run_period()

        self.assertEqual(result, "N/A - 31/12/1890")

    def test_formatRunPeriod_noEndRunPeriod(self):
        data = Data()
        run_period = {
            "start": {"day": 1, "month": 1, "year": 1890},
        }
        data.parse_run_period(run_period)

        result = data.format_run_period()

        self.assertEqual(result, "1/1/1890 - N/A")

    def test_formatRunPeriod_noRunPeriodSet(self):
        data = Data()

        result = data.format_run_period()

        self.assertEqual(result, "N/A - N/A")
