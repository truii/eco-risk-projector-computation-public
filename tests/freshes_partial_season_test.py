import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class FreshesPartialSeasonPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "freshes_partial_season"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_freshPartialSeason_validation_noFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_freshPartialSeason_validation_noLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
        ]
        with self.assertRaises(MissingParameterException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_freshPartialSeason_run_1YearComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.3)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.3)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.15)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.3)

    def test_freshPartialSeason_run_1YearNoComp(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.3)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.15)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_freshPartialSeason_run_smallerSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.045)
        self.assertEqual(yearly_intermediate[0]["count"], 1)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.045)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_freshPartialSeason_run_eventlessSeasons(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [90, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 0)
        self.assertEqual(yearly_intermediate[0]["mag"], 0)
        self.assertEqual(yearly_intermediate[0]["count"], 0)
        self.assertEqual(yearly_intermediate[0]["ind"], 0)
        self.assertEqual(yearly_intermediate[0]["summary"], 0)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_freshPartialSeason_run_1YearHighIndependance(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 0,
                    "rules": {
                        "Wet": {"mag": 100, "dur": 5, "count": 2, "ind": 100},
                        "Average": {"mag": 100, "dur": 5, "count": 2, "ind": 100},
                        "Dry": {"mag": 100, "dur": 5, "count": 2, "ind": 100},
                        "Very Dry": {"mag": 100, "dur": 5, "count": 2, "ind": 100},
                        "Drought": {"mag": 100, "dur": 5, "count": 2, "ind": 100},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 1)
        self.assertEqual(yearly_intermediate[0]["count"], 1)
        self.assertEqual(yearly_intermediate[0]["ind"], 0.805)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.805)
        self.assertEqual(yearly_intermediate[0]["compliance"], None)

    def test_freshPartialSeason_run_positiveLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": 5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["event_log"][0]["duration"], 5)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.3)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.15)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.086)

    def test_freshPartialSeason_run_negativeLag(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["event_log"][0]["duration"], 5)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.3)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.15)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.279)

    def test_freshPartialSeason_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 2018)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.3)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["count"], 0.5)
        self.assertEqual(yearly_intermediate[0]["ind"], 1)
        self.assertEqual(yearly_intermediate[0]["compliance"], 0.279)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.15)

    def test_freshPartialSeason_results_eventsIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_intermediate = self.data_store.get_results(ResultTypes.EVENTS_INTERMEDIATE)[0].data
        self.assertEqual(events_intermediate[0]["start_date"], "28/01/2019")
        self.assertEqual(events_intermediate[0]["end_date"], "01/02/2019")
        self.assertEqual(events_intermediate[0]["duration"], 5)

    def test_freshPartialSeason_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 2018)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_freshPartialSeason_results_spatialAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "compliance": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "lag": -5,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Average": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Very Dry": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                        "Drought": {"mag": 10000, "dur": 5, "count": 2, "ind": 10},
                    },
                    "partial_tables": {
                        "mag": [[0, 0], [100, 100]],
                        "dur": [[0, 0], [100, 100]],
                        "count": [[0, 0], [100, 100]],
                        "ind": [[0, 0], [100, 100]],
                    },
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 2018)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
