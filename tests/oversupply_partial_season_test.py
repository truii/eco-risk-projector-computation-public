import unittest
from core.globals import ResultTypes
from core.model import Model
from core.exceptions import *
from tests.test_context import get_data_store


class OversupplyPartialSeasonPluginTests(unittest.TestCase):
    def setUp(self):
        self.plugin = "oversupply_partial_season"
        self.data_store = get_data_store()

    def tearDown(self):
        self.data_store.cleanup()

    def createSettings(self, nodes, spatial_agg={"high": [75, 100], "moderate": [50, 74], "low": [0, 49]}):
        return {"plugin": self.plugin, "nodes": nodes, "spatial_agg": spatial_agg}

    def test_oversupplyPartialSeason_validation_noFlow(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {"climate": ".\\tests\\test_data\\Climate.csv"},
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
            {
                "meta_data": {"name": "node_2"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"agg_type": "sum", "num_years": 4, "comparison": "gte", "threshold": 1},
                },
            },
        ]
        with self.assertRaises(MissingDataFileException):
            settings = self.createSettings(nodes)
            Model(settings, self.data_store).run()

    def test_oversupplyPartialSeason_run_1Year(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 100],
                        [2, 100],
                        [3, 100],
                        [4, 100],
                        [5, 100],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 100],
                        [10, 100],
                        [11, 100],
                        [12, 100],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 1)

    def test_oversupplyPartialSeason_run_smallerSeason(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow Single Year.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 10000, "dur": 50},
                        "Average": {"mag": 10000, "dur": 50},
                        "Dry": {"mag": 10000, "dur": 50},
                        "Very Dry": {"mag": 10000, "dur": 50},
                        "Drought": {"mag": 10000, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 4, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()
        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data

        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.837)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.837)

    def test_oversupplyPartialSeason_results_yearlyIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 100, "dur": 50},
                        "Average": {"mag": 100, "dur": 50},
                        "Dry": {"mag": 100, "dur": 50},
                        "Very Dry": {"mag": 100, "dur": 50},
                        "Drought": {"mag": 100, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        yearly_intermediate = self.data_store.get_results(ResultTypes.YEARLY_INTERMEDIATE)[0].data
        self.assertEqual(yearly_intermediate[0]["year"], 1889)
        self.assertEqual(yearly_intermediate[0]["mag"], 0.72)
        self.assertEqual(yearly_intermediate[0]["dur"], 1)
        self.assertEqual(yearly_intermediate[0]["summary"], 0.72)

    def test_oversupplyPartialSeason_results_eventsIntermediate(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 100, "dur": 50},
                        "Average": {"mag": 100, "dur": 50},
                        "Dry": {"mag": 100, "dur": 50},
                        "Very Dry": {"mag": 100, "dur": 50},
                        "Drought": {"mag": 100, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        events_intermediate = self.data_store.get_results(ResultTypes.EVENTS_INTERMEDIATE)[0].data
        self.assertEqual(events_intermediate[0]["start_date"], "11/08/1889")
        self.assertEqual(events_intermediate[0]["end_date"], "01/11/1889")
        self.assertEqual(events_intermediate[0]["duration"], 82)

    def test_oversupplyPartialSeason_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 100, "dur": 50},
                        "Average": {"mag": 100, "dur": 50},
                        "Dry": {"mag": 100, "dur": 50},
                        "Very Dry": {"mag": 100, "dur": 50},
                        "Drought": {"mag": 100, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        temporal_assessment = self.data_store.get_results(ResultTypes.TEMPORAL_ASSESSMENT)[0].data
        self.assertEqual(temporal_assessment[0]["year"], 1889)
        self.assertEqual(temporal_assessment[0]["success"], 1)

    def test_oversupplyPartialSeason_results_temporalAssessment(self):
        nodes = [
            {
                "meta_data": {"name": "node_1"},
                "data": {
                    "flow": ".\\tests\\test_data\\Site 1 Flow 10 Years.csv",
                    "climate": ".\\tests\\test_data\\Climate.csv",
                },
                "parameters": {
                    "season": [
                        [1, 0],
                        [2, 0],
                        [3, 0],
                        [4, 50],
                        [5, 75],
                        [6, 100],
                        [7, 100],
                        [8, 100],
                        [9, 75],
                        [10, 50],
                        [11, 0],
                        [12, 0],
                    ],
                    "start_month": 7,
                    "rules": {
                        "Wet": {"mag": 100, "dur": 50},
                        "Average": {"mag": 100, "dur": 50},
                        "Dry": {"mag": 100, "dur": 50},
                        "Very Dry": {"mag": 100, "dur": 50},
                        "Drought": {"mag": 100, "dur": 50},
                    },
                    "partial_tables": {"dur": [[0, 0], [100, 100]]},
                },
                "assessment": {
                    "yearly_agg": {"comparison": "gte", "num_successes": 1, "start_month": "january"},
                    "daily_agg": {"comparison": "lte", "criteria": "median"},
                    "yearly_conversion": {"comparison": "gte", "criteria": "median"},
                    "temporal_agg": {"num_years": 1, "comparison": "gte", "threshold": 1},
                },
            }
        ]
        settings = self.createSettings(nodes)
        Model(settings, self.data_store).run()

        spatial_assessment = self.data_store.get_results(ResultTypes.SPATIAL_ASSESSMENT)[0].data
        self.assertEqual(spatial_assessment[0]["year"], 1889)
        self.assertEqual(spatial_assessment[0]["risk"], "low")
